﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Shared.Configuration
{
    public static class ConfigurationHelper
    {
        public static IConfigurationBuilder CreateDefaultConfigurationBuiler()
        {
            return new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true);
        }
    }
}