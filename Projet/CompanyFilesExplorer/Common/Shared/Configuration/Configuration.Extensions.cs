﻿using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace Shared.Configuration
{
    /// <summary>
    ///     This static class provides the developpers with the ability to use extensions method for
    ///     <see cref="IConfiguration" />.
    /// </summary>
    public static class ConfigurationExtensions
    {
        public const string AppSettings = "AppSettings";

        /// <summary>
        ///     Gets the application settings value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="configuration">The configuration.</param>
        /// <param name="key">The key.</param>
        /// <returns>T.</returns>
        public static T GetAppSettingsValue<T>(this IConfiguration configuration, string key)
        {
            return configuration.GetValue<T>($"{AppSettings}:{key}");
        }

        /// <summary>
        ///     Gets the computed connection string.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static string GetComputedConnectionString(this IConfiguration configuration, string key)
        {
            MySqlConnectionStringBuilder connectionStringBuilder = new MySqlConnectionStringBuilder(configuration.GetConnectionString(key));
            return connectionStringBuilder.ConnectionString;
        }
    }
}