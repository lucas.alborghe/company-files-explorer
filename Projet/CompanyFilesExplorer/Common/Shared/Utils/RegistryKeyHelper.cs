﻿using System;
using Microsoft.Win32;

namespace Shared.Utils
{
    /// <summary>
    ///     This static class is able to manage the RegistryKey reading.
    /// </summary>
    public static class RegistryKeyHelper
    {
        public static bool TryRead(this RegistryKey root, string key, out RegistryKey registryKey, bool writable = false)
        {
            return TryRead(e => root.OpenSubKey(e, writable), key, out registryKey);
        }

        public static bool TryGetValue(this RegistryKey registryKey, string key, out string value)
        {
            value = registryKey.GetValue(key)?.ToString();
            return value != null;
        }

        public static bool TryGetDefaultValue(this RegistryKey registryKey, out string value)
        {
            return TryGetValue(registryKey, null, out value);
        }

        public static string GetDefaultValue(this RegistryKey registryKey)
        {
            return registryKey.GetValue(null)?.ToString();
        }

        public static void SetDefaultValue(this RegistryKey registryKey, string value)
        {
            registryKey.SetValue(null, value, RegistryValueKind.String);
        }

        public static void SetStringValue(this RegistryKey registryKey, string key, string value)
        {
            registryKey.SetValue(key, value, RegistryValueKind.String);
        }

        public static bool TryReadOrCreate(this RegistryKey root, string key, out RegistryKey registryKey)
        {
            if(!root.TryRead(key, out registryKey, true))
            {
                registryKey = root.CreateSubKey(key);
                return false;
            }

            return true;
        }

        public static bool TryFind(this RegistryKey parentKey, string valueKey, string name, out RegistryKey registryKey)
        {
            string[] keyNames = parentKey.GetSubKeyNames();
            foreach(string keyName in keyNames)
            {
                registryKey = parentKey.OpenSubKey(keyName);
                try
                {
                    if(registryKey != null && string.Equals(registryKey.GetValue(valueKey).ToString(), name, StringComparison.OrdinalIgnoreCase))
                    {
                        return true;
                    }
                } catch
                {
                }
            }

            registryKey = null;
            return false;
        }

        private static bool TryRead(Func<string, RegistryKey> read, string key, out RegistryKey registryKey)
        {
            registryKey = read(key);
            return registryKey != null;
        }
    }
}