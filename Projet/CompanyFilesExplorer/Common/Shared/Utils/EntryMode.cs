﻿namespace Shared.Utils
{
    public enum EntryMode
    {
        AskForCredentials,
        Connect,
        Expert,
        ShowUsage
    }
}