﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Utils
{
    /// <summary>
    ///     This static class is able to the arguments of the <see cref="AppBootstrap.Main">entry call</see>.
    /// </summary>
    public static class AppArgumentsHelper
    {
        private const string CustomModePrefix = "-";

        public static readonly Dictionary<string, EntryMode> EntryModeByArgument = new Dictionary<string, EntryMode>(StringComparer.OrdinalIgnoreCase)
        {
                {
                        ExpertModeArgument, EntryMode.Expert
                }
        };

        private const string ExpertModeArgument = CustomModePrefix + "expert";

        public static EntryMode GetEntryMode(string[] mainArguments, out string host)
        {
            switch(mainArguments.Length)
            {
                case 0:

                    //If the application is executed without parameters, display the GUI, otherwise connect the user
                    host = null;
                    return EntryMode.AskForCredentials;
                case 1:
                    string argument = mainArguments.First();
                    if(EntryModeByArgument.TryGetValue(argument, out EntryMode customMode))
                    {
                        host = null;
                        return customMode;
                    }

                    if(argument.StartsWith(CustomModePrefix))
                    {
                        host = null;
                        return EntryMode.ShowUsage;
                    }

                    host = argument;
                    return EntryMode.Connect;
                default:
                    host = null;
                    return EntryMode.ShowUsage;
            }
        }
    }
}