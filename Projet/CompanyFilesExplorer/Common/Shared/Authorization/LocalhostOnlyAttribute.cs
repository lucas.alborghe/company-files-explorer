﻿using System;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Shared.Authorization
{
    /// <summary>
    ///     Specifies that the class or method that this attribute is applied is only accessible from localhost.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Filters.IAuthorizationFilter" />
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class LocalHostOnlyAttribute : Attribute, IAuthorizationFilter
    {
        [Authorize]
        public void OnAuthorization([NotNull] AuthorizationFilterContext context)
        {
            if(context.HttpContext.Request.Host.Host != "localhost")
            {
                context.Result = new StatusCodeResult(StatusCodes.Status403Forbidden);
            }
        }
    }
}