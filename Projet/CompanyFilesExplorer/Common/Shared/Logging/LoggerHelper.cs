﻿using System;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;

namespace Shared.Logging
{
    public static class LoggerHelper
    {

        public static bool IsExceptionOn { get; set; }

        public static LogEventLevel Level { get; private set; } = LogEventLevel.Warning;

        public static LoggerConfiguration CreateDefaultLoggerConfiguration(IConfiguration configuration)
        {
            IsExceptionOn = configuration.GetValue("Serilog:ExceptionOn", false);
            Level = (LogEventLevel)Enum.Parse(typeof(LogEventLevel), configuration.GetValue("Serilog:MinimumLevel:Default", "Warning"));
            return new LoggerConfiguration().ReadFrom.Configuration(configuration).Enrich.FromLogContext();
        }
    }
}