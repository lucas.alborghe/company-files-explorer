﻿using Serilog.Events;

namespace Shared.Logging.Database
{
    public class StdLogEvent
    {
        public LogEvent LogEvent { get; set; }

        public string Message { get; set; }
    }
}