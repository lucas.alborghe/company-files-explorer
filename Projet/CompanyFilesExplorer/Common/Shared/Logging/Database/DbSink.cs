﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;
using Serilog.Core;
using Serilog.Debugging;
using Serilog.Events;
using Shared.Components;

namespace Shared.Logging.Database
{
    internal class DbSink : AbstractBatchProvider<LogEvent>, ILogEventSink
    {
        private readonly ILogManager _logManager;
        private readonly string _connectionString;
        private readonly TimeSpan? _retentionPeriod;
        private readonly Stopwatch _retentionWatch = new Stopwatch();

        public DbSink(ILogManager logManager, string connectionString, TimeSpan? retentionPeriod)
                : base(TimeSpan.FromSeconds(10))
        {
            _logManager = logManager;
            _connectionString = connectionString;

            if(retentionPeriod.HasValue)
            {
                // impose a min retention period of 1 minute
                _retentionPeriod = new[]
                {
                        retentionPeriod.Value,
                        TimeSpan.FromMinutes(1)
                }.Max();
            }

            InitializeDatabase();
        }

        #region ILogEvent implementation

        public void Emit([NotNull] LogEvent logEvent)
        {
            PushItem(logEvent);
        }

        #endregion

        private void InitializeDatabase()
        {
            _logManager.CreateDatabase(_connectionString);
        }

        protected override void WriteItems(IList<LogEvent> logEventsBatch)
        {
            try
            {
                ApplyRetentionPolicy();

                _logManager.AddAll(_connectionString, logEventsBatch.ToArray());
            } catch(Exception e)
            {
                SelfLog.WriteLine(e.Message);
            }
        }

        private void ApplyRetentionPolicy()
        {
            if(!_retentionPeriod.HasValue)
            {
                // there is no retention policy
                return;
            }

            if(_retentionWatch.IsRunning && _retentionWatch.Elapsed < _retentionPeriod.Value)
            {
                // Besides deleting records older than X 
                // let's only delete records every X often
                // because of the check whether the _retentionWatch is running,
                // the first write operation during this application run
                // will result in deleting old records
                return;
            }

            DateTimeOffset epoch = DateTimeOffset.Now.Subtract(_retentionPeriod.Value);
            SelfLog.WriteLine("Deleting log entries older than {0}", epoch);
            _logManager.DeleteOldLogs(_connectionString, epoch);

            _retentionWatch.Restart();
        }
    }
}