﻿using System;
using Serilog.Events;

namespace Shared.Logging.Database
{
    public interface ILogManager
    {
        /// <summary>
        ///     Adds all the specified log events.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="logEvents">The log events.</param>
        void AddAll(string connectionString, params LogEvent[] logEvents);

        /// <summary>
        ///     Creates the database if necessary.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns>If the database has been deleted previously</returns>
        void CreateDatabase(string connectionString);

        void RemoveDatabase(string connectionString);

        /// <summary>
        ///     Deletes the logs older than the specified offset.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="dateTimeOffset">The date time offset.</param>
        void DeleteOldLogs(string connectionString, DateTimeOffset dateTimeOffset);
    }
}