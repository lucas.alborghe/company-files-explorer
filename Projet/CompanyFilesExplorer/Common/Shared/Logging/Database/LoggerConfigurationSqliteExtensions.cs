﻿using System;
using JetBrains.Annotations;
using Serilog;
using Serilog.Configuration;
using Serilog.Debugging;
using Serilog.Events;

namespace Shared.Logging.Database
{
    /// <summary>
    ///     Adds the WriteTo.MySql() extension method to <see cref="LoggerConfiguration" />.
    /// </summary>
    public static class LoggerConfigurationDbExtensions
    {
        /// <summary>
        /// Adds a sink that writes log events to a MySql database.
        /// </summary>
        /// <param name="loggerConfiguration">The logger configuration.</param>
        /// <param name="logManager">The log manager.</param>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="restrictedToMinimumLevel">The minimum log event level required in order to write an event to the sink.</param>
        /// <param name="storeTimestampInUtc">Store timestamp in UTC format</param>
        /// <param name="retentionPeriod">The maximum time that a log entry will be kept in the database, or null to disable automatic deletion of old log entries. Non-null values smaller than 1 minute will be replaced with 1 minute.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// loggerConfiguration
        /// or
        /// connectionString
        /// </exception>
        /// <exception cref="ArgumentNullException">A required parameter is null.</exception>
        public static LoggerConfiguration MySql(
            [CanBeNull] this LoggerSinkConfiguration loggerConfiguration,
            ILogManager logManager,
            string connectionString,
            LogEventLevel restrictedToMinimumLevel = LevelAlias.Minimum,
            bool storeTimestampInUtc = false,
            TimeSpan? retentionPeriod = null)
        {
            if(loggerConfiguration == null)
            {
                SelfLog.WriteLine("Logger configuration is null");
                throw new ArgumentNullException(nameof(loggerConfiguration));
            }

            if(string.IsNullOrEmpty(connectionString))
            {
                SelfLog.WriteLine("Invalid connectionString");
                throw new ArgumentNullException(nameof(connectionString));
            }

            try
            {
                return loggerConfiguration.Sink(new DbSink(logManager, connectionString, retentionPeriod), restrictedToMinimumLevel);
            } catch(Exception ex)
            {
                SelfLog.WriteLine(ex.Message);
                throw;
            }
        }
    }
}