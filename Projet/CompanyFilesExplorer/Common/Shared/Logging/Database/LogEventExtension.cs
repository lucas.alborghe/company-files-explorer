﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Serilog.Events;

namespace Shared.Logging.Database
{
    public static class LogEventExtensions
    {
        private static readonly string[] ExcludedKeys =
        {
                "SourceContext",
                "ActionId",
                "ActionName",
                "RequestPath"
        };

        public static string Json(this LogEvent logEvent)
        {
            return JsonConvert.SerializeObject(ConvertToDictionary(logEvent));
        }

        public static IDictionary<string, object> Dictionary(this LogEvent logEvent)
        {
            return ConvertToDictionary(logEvent);
        }

        public static string Json(this IReadOnlyDictionary<string, LogEventPropertyValue> properties)
        {
            return JsonConvert.SerializeObject(ConvertToDictionary(properties));
        }

        public static IDictionary<string, object> Dictionary(this IReadOnlyDictionary<string, LogEventPropertyValue> properties)
        {
            return ConvertToDictionary(properties);
        }

        #region Private implementation

        private static dynamic ConvertToDictionary(IReadOnlyDictionary<string, LogEventPropertyValue> properties)
        {
            IDictionary<string, object> expObject = new ExpandoObject();
            foreach(KeyValuePair<string, LogEventPropertyValue> property in properties.Where(p => !ExcludedKeys.Contains(p.Key)))
            {
                expObject.Add(property.Key, Simplify(property.Value));
            }

            return expObject;
        }

        private static dynamic ConvertToDictionary(LogEvent logEvent)
        {
            IDictionary<string, object> eventObject = new ExpandoObject();
            eventObject.Add("Timestamp", logEvent.Timestamp.ToUniversalTime().ToString("o"));

            eventObject.Add("Level", logEvent.Level.ToString());
            eventObject.Add("MessageTemplate", logEvent.MessageTemplate.ToString());
            eventObject.Add("Exception", logEvent.Exception);
            eventObject.Add("Properties", logEvent.Properties.Dictionary());

            return eventObject;
        }

        [CanBeNull]
        private static object Simplify(LogEventPropertyValue data)
        {
            switch(data)
            {
                case ScalarValue value:
                    return value.Value;

                case IReadOnlyDictionary<string, LogEventPropertyValue> dictValue:
                    IDictionary<string, object> expObject = new ExpandoObject();
                    foreach(string item in dictValue.Keys)
                    {
                        expObject.Add(item, Simplify(dictValue[item]));
                    }

                    return expObject;

                case SequenceValue seq:
                    return seq.Elements.Select(Simplify).ToArray();
            }

            if(!(data is StructureValue str))
            {
                return null;
            }

            try
            {
                if(str.TypeTag == null)
                {
                    return str.Properties.ToDictionary(p => p.Name, p => Simplify(p.Value));
                }

                if(!str.TypeTag.StartsWith("DictionaryEntry") && !str.TypeTag.StartsWith("KeyValuePair"))
                {
                    return str.Properties.ToDictionary(p => p.Name, p => Simplify(p.Value));
                }

                object key = Simplify(str.Properties[0].Value);
                if(key == null)
                {
                    return null;
                }

                IDictionary<string, object> expObject = new ExpandoObject();
                expObject.Add(key.ToString(), Simplify(str.Properties[1].Value));
                return expObject;
            } catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return null;
        }

        #endregion
    }
}