﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog.Debugging;

namespace Shared.Components
{
    public abstract class AbstractBatchProvider<TItem> : IDisposable
    {
        private readonly uint _batchSize;
        private readonly CancellationTokenSource _cancellationToken = new CancellationTokenSource();
        private readonly CancellationTokenSource _itemCancellationToken = new CancellationTokenSource();
        private readonly ConcurrentQueue<TItem> _itemBatch;
        private readonly BlockingCollection<IList<TItem>> _batchItemsCollection;
        private readonly BlockingCollection<TItem> _itemsCollection;
        private readonly TimeSpan _thresholdTimeSpan;
        private readonly AutoResetEvent _timerResetItem = new AutoResetEvent(false);
        private readonly Task _timerTask;
        private readonly Task _batchTask;
        private readonly Task _itemPumpTask;
        private readonly List<Task> _workerTasks = new List<Task>();

        private bool _canStop;

        protected AbstractBatchProvider(TimeSpan threshold, uint batchSize = 100)
        {
            _thresholdTimeSpan = threshold;
            _batchSize = batchSize == 0 ? 1 : batchSize;
            _itemBatch = new ConcurrentQueue<TItem>();
            _batchItemsCollection = new BlockingCollection<IList<TItem>>();
            _itemsCollection = new BlockingCollection<TItem>();

            _batchTask = Task.Factory.StartNew(Pump, TaskCreationOptions.LongRunning);
            _timerTask = Task.Factory.StartNew(TimerPump, TaskCreationOptions.LongRunning);
            _itemPumpTask = Task.Factory.StartNew(ItemPump, TaskCreationOptions.LongRunning);
        }

        private void Pump()
        {
            try
            {
                while(true)
                {
                    IList<TItem> items = _batchItemsCollection.Take(_cancellationToken.Token);
                    Task task = Task.Factory.StartNew(x =>
                    {
                        if(!(x is IList<TItem> crtItems) || !crtItems.Any())
                        {
                            return;
                        }

                        WriteItems(crtItems);
                    }, items);

                    _workerTasks.Add(task);

                    if(_workerTasks.Count <= 32)
                    {
                        continue;
                    }

                    Task.WaitAll(_workerTasks.ToArray(), _cancellationToken.Token);
                    _workerTasks.Clear();
                }
            } catch(OperationCanceledException)
            {
                SelfLog.WriteLine("Shutting down batch processing");
            } catch(Exception e)
            {
                SelfLog.WriteLine(e.Message);
            }
        }

        private void TimerPump()
        {
            while(!_canStop)
            {
                _timerResetItem.WaitOne(_thresholdTimeSpan);
                FlushItemBatch();
            }
        }

        private void ItemPump()
        {
            try
            {
                while(true)
                {
                    TItem item = _itemsCollection.Take(_itemCancellationToken.Token);
                    _itemBatch.Enqueue(item);

                    if(_itemBatch.Count >= _batchSize)
                    {
                        FlushItemBatch();
                    }
                }
            } catch(OperationCanceledException)
            {
                SelfLog.WriteLine("Shutting down item pump");
            } catch(Exception e)
            {
                SelfLog.WriteLine(e.Message);
            }
        }

        private void FlushItemBatch()
        {
            if(!_itemBatch.Any())
            {
                return;
            }

            int itemBatchSize = _itemBatch.Count >= _batchSize ? (int)_batchSize : _itemBatch.Count;
            List<TItem> itemList = new List<TItem>();

            for(int i = 0; i < itemBatchSize; i++)
            {
                if(_itemBatch.TryDequeue(out TItem item))
                {
                    itemList.Add(item);
                }
            }

            _batchItemsCollection.Add(itemList);
        }

        protected void PushItem(TItem item)
        {
            _itemsCollection.Add(item);
        }

        protected abstract void WriteItems(IList<TItem> itemsBatch);

        #region IDisposable Support

        private bool _disposedValue; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if(!_disposedValue)
            {
                if(disposing)
                {
                    FlushAndCloseItemHandlers();

                    SelfLog.WriteLine("Sink halted successfully.");
                }

                _disposedValue = true;
            }
        }

        private void FlushAndCloseItemHandlers()
        {
            try
            {
                SelfLog.WriteLine("Halting sink...");

                _itemCancellationToken.Cancel();
                _cancellationToken.Cancel();

                Task.WaitAll(_workerTasks.ToArray());

                _canStop = true;
                _timerResetItem.Set();

                // Flush items collection
                while(_itemsCollection.TryTake(out TItem item))
                {
                    _itemBatch.Enqueue(item);

                    if(_itemBatch.Count >= _batchSize)
                    {
                        FlushItemBatch();
                    }
                }

                FlushItemBatch();

                // Flush items batch
                while(_batchItemsCollection.TryTake(out IList<TItem> itemBatch))
                {
                    WriteItems(itemBatch);
                }

                Task.WaitAll(new[]
                {
                        _itemPumpTask,
                        _batchTask,
                        _timerTask
                }, TimeSpan.FromSeconds(30));
            } catch(Exception ex)
            {
                SelfLog.WriteLine(ex.Message);
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}