﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Shared.FileSystem
{
    public static class FileSystemHelper
    {
        public static string AppFolder
        {
            get
            {
                if(_appFolder == null)
                {
                    throw new InvalidOperationException("The AppFolder settings must be set.");
                }

                return _appFolder;
            }
            set => _appFolder = FormatPathInternally(value);
        }

        public static long AvailableFreeSpaceAlertThresholdInMegaBytes { get; set; }

        public static readonly string LocalAppDataPath = Environment.GetEnvironmentVariable(RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? "LocalAppData" : "HOME");
        public static readonly string TempDataPath = RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? Environment.GetEnvironmentVariable("temp") : "/tmp";
        private static string _appFolder;

        /// <summary>
        ///     Formats a path, substitutes '%localappdata%' to OS specific path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string FormatPath(string path)
        {
            path = path.ReplaceInPath("%appFolder%", AppFolder);
            path = FormatPathInternally(path);
            Uri uri = new Uri(path, UriKind.RelativeOrAbsolute);
            return uri.IsAbsoluteUri ? uri.AbsolutePath : Path.Combine(AppFolder, path);
        }

        private static string FormatPathInternally(string path)
        {
            path = path.ReplaceInPath("%localappdata%", LocalAppDataPath);
            path = path.ReplaceInPath("%temp%", TempDataPath);
            path = path.ReplaceInPath("./", Directory.GetCurrentDirectory());
            return path;
        }

        private static string ReplaceInPath(this string path, string toReplace, string replacement)
        {
            if(path.StartsWith(toReplace, StringComparison.InvariantCultureIgnoreCase))
            {
                path = path.Remove(0, toReplace.Length).TrimStart('/');
                path = Path.Combine(replacement, path);
            }

            return path;
        }

        /// <summary>
        ///     Gets the disk space information for the disk containing the AppFolder.
        /// </summary>
        /// <returns></returns>
        public static DiskSpaceInfo GetDiskSpaceInfo()
        {
            FileInfo fileInfo = new FileInfo(AppFolder);
            DriveInfo driveInfo = new DriveInfo(fileInfo.Directory.Root.FullName);
            return new DiskSpaceInfo(driveInfo.AvailableFreeSpace, driveInfo.TotalSize, AvailableFreeSpaceAlertThresholdInMegaBytes * 1024 * 1024);
        }
    }
}