﻿using System.Collections.Generic;

namespace Shared.FileSystem
{
    public class DiskSpaceInfo
    {
        public const int NbBytesPerKb = 1024;

        private static readonly IReadOnlyList<string> Units = new[]
        {
                "bytes",
                "KB",
                "MB",
                "GB",
                "TB"
        };

        public long AvailableFreeSpaceInBytes { get; }

        public long TotalSizeInBytes { get; }

        public long AvailableFreeSpaceAlertThresholdInBytes { get; }

        public bool IsThresholdExceeded { get; }

        public string UserReadableAvailableFreeSpace { get; }

        public string UserReadableTotalSize { get; }

        public DiskSpaceInfo(long availableFreeSpaceInBytes, long totalSizeInBytes, long availableFreeSpaceAlertThresholdInBytes)
        {
            AvailableFreeSpaceInBytes = availableFreeSpaceInBytes;
            TotalSizeInBytes = totalSizeInBytes;
            AvailableFreeSpaceAlertThresholdInBytes = availableFreeSpaceAlertThresholdInBytes;
            IsThresholdExceeded = AvailableFreeSpaceInBytes < AvailableFreeSpaceAlertThresholdInBytes;
            UserReadableAvailableFreeSpace = GetReadableSize(AvailableFreeSpaceInBytes);
            UserReadableTotalSize = GetReadableSize(TotalSizeInBytes);
        }

        private static string GetReadableSize(long size)
        {
            double convertedSize = size;
            int unitIndex = 0;

            while(convertedSize > NbBytesPerKb && unitIndex < Units.Count - 1)
            {
                unitIndex++;
                convertedSize /= NbBytesPerKb;
            }

            return $"{convertedSize:0.#} {Units[unitIndex]}";
        }
    }
}