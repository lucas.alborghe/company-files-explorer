﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Core.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "allowedextension",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    extension = table.Column<string>(type: "varchar(32)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_allowedextension", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "CustomConfigs",
                columns: table => new
                {
                    Key = table.Column<int>(nullable: false),
                    Value = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomConfigs", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "procedure",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    description = table.Column<string>(type: "varchar(256)", nullable: true),
                    name = table.Column<string>(type: "varchar(64)", nullable: false),
                    procedure = table.Column<string>(type: "varchar(128)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_procedure", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "repository",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    absolutePath = table.Column<string>(type: "varchar(256)", nullable: false),
                    password = table.Column<string>(type: "varchar(64)", nullable: true),
                    protocol = table.Column<string>(type: "varchar(64)", nullable: false),
                    username = table.Column<string>(type: "varchar(64)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_repository", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    isAdmin = table.Column<sbyte>(type: "tinyint(4)", nullable: false, defaultValueSql: "'0'"),
                    login = table.Column<string>(type: "varchar(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "metadata",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    description = table.Column<string>(type: "varchar(256)", nullable: true),
                    fonction = table.Column<string>(type: "varchar(64)", nullable: true),
                    fonctionParam = table.Column<string>(type: "varchar(64)", nullable: true),
                    name = table.Column<string>(type: "varchar(64)", nullable: false),
                    procedure_id = table.Column<int>(nullable: false),
                    type = table.Column<string>(type: "varchar(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_metadata", x => x.id);
                    table.ForeignKey(
                        name: "fk_metadata_procedure1",
                        column: x => x.procedure_id,
                        principalTable: "procedure",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "directory",
                columns: table => new
                {
                    path = table.Column<string>(type: "varchar(256)", nullable: false),
                    lastAccessDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    repository_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_directory", x => x.path);
                    table.ForeignKey(
                        name: "fk_directory_repositoryDetail1",
                        column: x => x.repository_id,
                        principalTable: "repository",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "file",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    createdDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    extension = table.Column<string>(type: "varchar(32)", nullable: false),
                    filename = table.Column<string>(type: "varchar(64)", nullable: false),
                    filepath = table.Column<string>(type: "varchar(256)", nullable: false),
                    hashcode = table.Column<string>(type: "varchar(256)", nullable: false),
                    modifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    repository_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_file", x => x.id);
                    table.ForeignKey(
                        name: "fk_file_repositoryDetail1",
                        column: x => x.repository_id,
                        principalTable: "repository",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "virtualdirectory",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    isActive = table.Column<sbyte>(type: "tinyint(4)", nullable: false, defaultValueSql: "'1'"),
                    name = table.Column<string>(type: "varchar(64)", nullable: false),
                    procedure_id = table.Column<int>(nullable: true),
                    user_id = table.Column<int>(nullable: false),
                    virtualDirectory_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_virtualdirectory", x => x.id);
                    table.ForeignKey(
                        name: "fk_virtualDirectory_procedure1",
                        column: x => x.procedure_id,
                        principalTable: "procedure",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_virtualDirectory_user1",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_virtualDirectory_virtualDirectory1",
                        column: x => x.virtualDirectory_id,
                        principalTable: "virtualdirectory",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "userpreference",
                columns: table => new
                {
                    virtualDirectory_id = table.Column<int>(nullable: false),
                    file_id = table.Column<int>(nullable: false),
                    filename = table.Column<string>(type: "varchar(64)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_userpreference", x => new { x.virtualDirectory_id, x.file_id });
                    table.ForeignKey(
                        name: "fk_virtualDirectory_has_file_file1",
                        column: x => x.file_id,
                        principalTable: "file",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_virtualDirectory_has_file_virtualDirectory1",
                        column: x => x.virtualDirectory_id,
                        principalTable: "virtualdirectory",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "fk_directory_repositoryDetail1_idx",
                table: "directory",
                column: "repository_id");

            migrationBuilder.CreateIndex(
                name: "fk_file_repositoryDetail1_idx",
                table: "file",
                column: "repository_id");

            migrationBuilder.CreateIndex(
                name: "fk_metadata_procedure1_idx",
                table: "metadata",
                column: "procedure_id");

            migrationBuilder.CreateIndex(
                name: "fk_virtualDirectory_has_file_file1_idx",
                table: "userpreference",
                column: "file_id");

            migrationBuilder.CreateIndex(
                name: "fk_virtualDirectory_has_file_virtualDirectory1_idx",
                table: "userpreference",
                column: "virtualDirectory_id");

            migrationBuilder.CreateIndex(
                name: "fk_virtualDirectory_procedure1_idx",
                table: "virtualdirectory",
                column: "procedure_id");

            migrationBuilder.CreateIndex(
                name: "fk_virtualDirectory_user1_idx",
                table: "virtualdirectory",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "fk_virtualDirectory_virtualDirectory1_idx",
                table: "virtualdirectory",
                column: "virtualDirectory_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "allowedextension");

            migrationBuilder.DropTable(
                name: "CustomConfigs");

            migrationBuilder.DropTable(
                name: "directory");

            migrationBuilder.DropTable(
                name: "metadata");

            migrationBuilder.DropTable(
                name: "userpreference");

            migrationBuilder.DropTable(
                name: "file");

            migrationBuilder.DropTable(
                name: "virtualdirectory");

            migrationBuilder.DropTable(
                name: "repository");

            migrationBuilder.DropTable(
                name: "procedure");

            migrationBuilder.DropTable(
                name: "user");
        }
    }
}
