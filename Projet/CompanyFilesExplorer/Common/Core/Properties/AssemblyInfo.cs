﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("CoreTests")]
[assembly: InternalsVisibleTo("WebsiteTests")]