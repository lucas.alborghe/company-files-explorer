﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.DataAccess;
using Core.Dtos.Directory;
using Core.Dtos.File;
using Core.Dtos.Repository;
using Core.Dtos.User;
using Core.Extensions;
using Core.Services.Interfaces;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Core.Services
{
    class RepositoryService : IRepositoryService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<IRepositoryService> _logger;

        public static readonly Expression<Func<RepositoryDb, RepositoryListDto>> GetListDto = item => new RepositoryListDto
        {
            Id = item.Id,
            AbsolutePath = item.AbsolutePath,
            Protocol = item.Protocol,
            Username = item.Username,
            Password = item.Password
        };

        public RepositoryService(ApplicationDbContext context, ILogger<IRepositoryService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<IReadOnlyCollection<RepositoryListDto>> GetAllAsync()
        {
            return await _context.RepositoriesDetail.Select(GetListDto).ToArrayAsync();
        }

        public async Task<IReadOnlyCollection<DirectoryListDto>> GetDirectoriesAsync(int repositoryId)
        {
            return await _context.Directories.Where(d => d.RepositoryId == repositoryId).Select(DirectoryService.GetListDto).ToArrayAsync();
        }

        public async Task<IReadOnlyCollection<FileListDto>> GetFilesAsync(int repositoryId)
        {
            return await _context.FilesDb.Where(f => f.RepositoryId == repositoryId).Select(FileService.GetListDto).ToArrayAsync();
        }

        public async Task<RepositoryDto> GetAsync(int repositoryId)
        {
            RepositoryDb[] dbRepositories = await _context.RepositoriesDetail.ToArrayAsync();
            RepositoryDb dbRepository = dbRepositories.SingleOrDefault(v => v.Id == repositoryId);
            if (dbRepository != null)
                return new RepositoryDto()
                {
                    Id = dbRepository.Id,
                    AbsolutePath = dbRepository.AbsolutePath,
                    Protocol = dbRepository.Protocol,
                    Username = dbRepository.Username,
                    Password = dbRepository.Password
                };
            return null;
        }

        public async Task<RepositoryDto> CreateAsync(RepositoryInputDto repository)
        {
            RepositoryDb toAdd = new RepositoryDb
            {
                AbsolutePath = repository.AbsolutePath,
                Protocol = repository.Protocol,
                Username = repository.Username,
                Password = repository.Password
            };
            _context.RepositoriesDetail.Add(toAdd);
            await _context.SaveChangesAsync();
            return new RepositoryDto
            {
                Id = toAdd.Id,
                AbsolutePath = toAdd.AbsolutePath,
                Protocol = toAdd.Protocol,
                Username = toAdd.Username,
                Password = toAdd.Password
            };
        }

        public async Task<RepositoryDto> UpdateAsync(int repositoryId, RepositoryInputDto repository)
        {
            RepositoryDb[] dbRepositories = await _context.RepositoriesDetail.ToArrayAsync();
            RepositoryDb dbRepository = dbRepositories.SingleOrDefault(v => v.Id == repositoryId);
            // If the repository does exist, update it
            if (dbRepository != null)
            {
                dbRepository.AbsolutePath = repository.AbsolutePath;
                dbRepository.Protocol = repository.Protocol;
                dbRepository.Username = repository.Username;
                dbRepository.Password = repository.Password;
                await _context.SaveChangesAsync();
                return new RepositoryDto
                {
                    Id = dbRepository.Id,
                    AbsolutePath = dbRepository.AbsolutePath,
                    Protocol = dbRepository.Protocol,
                    Username = dbRepository.Username,
                    Password = dbRepository.Password
                };
            }
            return null;
        }

        public async Task<bool> RemoveAsync(int repositoryId)
        {
            RepositoryDb[] dbRepositories = await _context.RepositoriesDetail.ToArrayAsync();
            RepositoryDb dbRepository = dbRepositories.SingleOrDefault(v => v.Id == repositoryId);
            if (dbRepository != null)
            {
                _context.RepositoriesDetail.Remove(dbRepository);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

    }
}
