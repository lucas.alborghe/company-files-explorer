﻿using System;
using Core.Services.Interfaces;

namespace Core.Services
{
    internal class DateTimeService : IDateTimeService
    {
        public DateTime UtcNow()
        {
            return DateTime.UtcNow;
        }
    }
}