﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.File;
using Core.Dtos.Metadata;
using Core.Dtos.Procedure;
using Core.Dtos.VirtualDirectory;

namespace Core.Services.Interfaces
{
    public interface IProcedureService
    {
        /// <summary>
        ///     Get all the procedures asynchronously.
        /// </summary>
        /// <returns>All the procedures otherwise empty</returns>
        Task<IReadOnlyCollection<ProcedureListDto>> GetAllAsync();

        /// <summary>
        ///     Get all the files where the filename match with the given rule asynchronously.
        /// </summary>
        /// <param name="rule">The rule</param>
        /// <returns>All the files which matched otherwise empty</returns>
        Task<IReadOnlyCollection<FileListDto>> GetFilesFromRuleAsync(string rule);

        /// <summary>
        ///     Get all the metadata of the procedure specified by the id asynchronously.
        /// </summary>
        /// <param name="procedureId">The id</param>
        /// <returns>All the metadata otherwise empty</returns>
        Task<IReadOnlyCollection<MetadataListDto>> GetMetadataAsync(int procedureId);

        /// <summary>
        ///     Get all the virtual directories of the procedure specified by the id asynchronously.
        /// </summary>
        /// <param name="procedureId">The id</param>
        /// <returns>All the virtual directories otherwise empty</returns>
        Task<IReadOnlyCollection<VirtualDirectoryListDto>> GetVirtualDirectoriesAsync(int procedureId);

        /// <summary>
        ///     Get the procedure specified by the id asynchronously.
        /// </summary>
        /// <param name="procedureId">The id</param>
        /// <returns>The procedure with the given id otherwise null</returns>
        Task<ProcedureDto> GetAsync(int procedureId);

        /// <summary>
        ///     Create the procedure asynchronously.
        /// </summary>
        /// <param name="procedure">The procedure</param>
        /// <returns>The procedure saved otherwise null</returns>
        Task<ProcedureDto> CreateAsync(ProcedureInputDto procedure);

        /// <summary>
        ///     Update the specified procedure asynchronously.
        /// </summary>
        /// <param name="procedureId">The if of the procedure to update</param>
        /// <param name="procedure">The procedure</param>
        /// <returns>The procedure updated otherwise null</returns>
        Task<ProcedureDto> UpdateAsync(int procedureId, ProcedureInputDto procedure);

        /// <summary>
        ///     Remove the procedure specified by the id asynchronously.
        /// </summary>
        /// <param name="procedureId">The id</param>
        /// <returns>True if the procedure has been removed otherwise false</returns>
        Task<bool> RemoveAsync(int procedureId);
        
    }
}
