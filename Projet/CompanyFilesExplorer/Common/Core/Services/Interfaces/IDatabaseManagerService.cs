﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.File;

namespace Core.Services.Interfaces
{
    public interface IDatabaseManagerService
    {
        /// <summary>
        ///     Refresh the database.
        /// Browse through the files contained in the repositories which match the allowed extensions.
        ///  - If the encountered file isnt in the database : add it
        ///  - If the encountered file is in the database and has changed : update it.
        ///  - If a file which is in the database isnt in its directory anymore : remove it.
        ///  - If the encountered file is already in the database : skip it.
        /// </summary>
        /// <returns>All the files which have be impacted by the refresh</returns>
        Task<List<FileListDto>> RefreshDatabaseAsync();

        /// <summary>
        ///     Refresh the database from a specific directory.
        /// Browse through the files contained in the repositories which match the allowed extensions.
        ///  - If the encountered file isnt in the database : add it
        ///  - If the encountered file is in the database and has changed : update it.
        ///  - If a file which is in the database isnt in its directory anymore : remove it.
        ///  - If the encountered file is already in the database : skip it.
        /// </summary>
        /// <param name="path"></param>
        /// <returns>All the files which have be impacted by the refresh</returns>
        Task<List<FileListDto>> RefreshDatabaseFromPathAsync(string path);

    }
}
