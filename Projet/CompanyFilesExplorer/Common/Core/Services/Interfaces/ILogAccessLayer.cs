﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTOs.Log;
using Data.Entities;
using JetBrains.Annotations;
using Serilog.Events;
using Shared.Logging.Database;

namespace Core.Services.Interfaces
{
    public interface ILogAccessLayer : ILogManager
    {
        /// <summary>
        ///     Clears the database.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        void ClearDatabase(string connectionString);

        /// <summary>
        ///     Clears logs when the level is less than the <see cref="levelLimit">limit</see>
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="levelLimit">The level limit.</param>
        void ClearLogsByType(string connectionString, LogEventLevel levelLimit);

        /// <summary>
        ///     Deletes the logs filtering by the <see cref="LogFilterDto">filter</see>
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="filter">The password.</param>
        /// <returns></returns>
        Task DeleteLogsAsync(string connectionString, LogFilterDto filter);

        /// <summary>
        ///     Gets the logs filtering by the <see cref="LogFilterDto">filter</see>
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        [ItemNotNull]
        Task<IEnumerable<StdLog>> GetLogsAsync(string connectionString, LogFilterDto filter);

        /// <summary>
        ///     Gets the logs filtering and paging by the <see cref="PagedLogFilterDto">filter</see>
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        [ItemNotNull]
        Task<StdLogPage> GetPagedLogsAsync(string connectionString, PagedLogFilterDto filter);
    }
}