﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.File;
using Core.Dtos.Repository;
using Microsoft.AspNetCore.Mvc;

namespace Core.Services.Interfaces
{
    public interface IFileService
    {
        /// <summary>
        ///     Get all the files asynchronously.
        /// </summary>
        /// <returns>All the files otherwise empty</returns>
        Task<IReadOnlyCollection<FileListDto>> GetAllAsync();

        /// <summary>
        ///     Get all the files where the filename match with the given filter asynchronously.
        /// </summary>
        /// <param name="filter">The filter</param>
        /// <returns>All the files which matched otherwise empty</returns>
        Task<IReadOnlyCollection<FileListDto>> GetAllByFilenameAsync(string filter);

        /// <summary>
        ///     Get all the files of the specified path asynchronously.
        /// </summary>
        /// <param name="path">The path</param>
        /// <returns>All the files of the path otherwise empty</returns>
        Task<IReadOnlyCollection<FileListDto>> GetAllByPathAsync(string path);

        /// <summary>
        ///     Get all the files which contain the specified filter asynchronously.
        /// </summary>
        /// <param name="filter">The filter</param>
        /// <returns>All the directories which contain 'filter' otherwise empty</returns>
        Task<IReadOnlyCollection<FileListDto>> GetAllByFilterAsync(string filter);

        /// <summary>
        ///     Get the repository of the file specified by the id asynchronously.
        /// </summary>
        /// <param name="fileId">The id</param>
        /// <returns>The repository otherwhise null</returns>
        Task<RepositoryDto> GetRepositoryAsync(int fileId);

        /// <summary>
        ///     Get the file specified by the id formatted in json asynchronously.
        /// </summary>
        /// <param name="fileId">The id</param>
        /// <returns>The file with the given id otherwise null</returns>
        Task<string> GetJsonAsync(int fileId);

        /// <summary>
        ///     Get the file specified by the id asynchronously.
        /// </summary>
        /// <param name="fileId">The id</param>
        /// <returns>The file with the given id otherwise null</returns>
        Task<FileDto> GetAsync(int fileId);

        /// <summary>
        ///     Create a new physical file by copying the source to the destination asynchronously.
        /// </summary>
        /// <param name="input">The input containing both source and destination file</param>
        /// <returns>The file 'created' otherwise null</returns>
        Task<FileDto> CreatePhysicalFile(FileMoveInputDto input);

        /// <summary>
        ///     Save the specified file asynchronously.
        /// </summary>
        /// <param name="file">The file</param>
        /// <returns>The file saved otherwise null</returns>
        Task<FileDto> SaveAsync(FileInputDto file);

        /// <summary>
        ///     Remove the file specified by the id asynchronously.
        /// </summary>
        /// <param name="fileId">The id</param>
        /// <returns>True if the file has been removed otherwise false</returns>
        Task<bool> RemoveAsync(int fileId);

    }
}
