﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTOs.Log;
using Serilog.Events;

namespace Core.Services.Interfaces
{
    public interface ILogService
    {
        /// <summary>
        ///     Gets all the <see cref="LogListDto">instances</see> asynchronously.
        /// </summary>
        /// <param name="logFilterDto">The log input dto.</param>
        /// <returns>
        ///     Task&lt;IReadOnlyCollection&lt;ApiKeyListDto&gt;&gt;.
        /// </returns>
        Task<IReadOnlyCollection<LogListDto>> GetAllAsync(LogFilterDto logFilterDto);

        /// <summary>
        ///     Gets all the <see cref="LogListDto">instances</see> using <see cref="PagedLogFilterDto.PageIndex"/> and <see cref="PagedLogFilterDto.PageSize"/> asynchronously.
        /// </summary>
        /// <param name="logFilterDto">The log input dto.</param>
        /// <returns>
        ///     Task&lt;IReadOnlyCollection&lt;ApiKeyListDto&gt;&gt;.
        /// </returns>
        Task<PagedLogListDto> GetAllAsync(PagedLogFilterDto logFilterDto);

        /// <summary>
        /// Clears the database.
        /// </summary>
        void ClearDatabase();

        /// <summary>
        /// Clears logs when the level is less than the <see cref="levelLimit">limit</see>
        /// </summary>
        /// <param name="levelLimit">The level limit.</param>
        void ClearLogsByType(LogEventLevel levelLimit);

        /// <summary>
        ///     Deletes all the <see cref="LogListDto">instances</see> filtered by the specified logFilterDto asynchronously.
        /// </summary>
        /// <param name="inputDto">The input dto.</param>
        /// <returns></returns>
        Task DeleteAsync(LogFilterDto inputDto);
    }
}