﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.File;
using Core.Dtos.Procedure;
using Core.Dtos.User;
using Core.Dtos.VirtualDirectory;

namespace Core.Services.Interfaces
{
    public interface IVirtualDirectoryService
    {
        /// <summary>
        ///     Get all the virtual directories asynchronously.
        /// </summary>
        /// <returns>All the virtual directories otherwise empty</returns>
        Task<IReadOnlyCollection<VirtualDirectoryListDto>> GetAllAsync();

        /// <summary>
        ///     Get the virtual directory specified by the id asynchronously.
        /// </summary>
        /// <param name="virtualDirectoryId">The id</param>
        /// <returns>The virtual directory with the given id otherwise null</returns>
        Task<VirtualDirectoryDto> GetAsync(int virtualDirectoryId);

        /// <summary>
        ///     Get the procedure of the virtual directory specified by the id asynchronously.
        /// </summary>
        /// <param name="virtualDirectoryId">The id</param>
        /// <returns>The procedure of the virtual directory otherwise null</returns>
        Task<ProcedureDto> GetProcedureAsync(int virtualDirectoryId);

        /// <summary>
        ///     Get the user of the virtual directory specified by the id asynchronously.
        /// </summary>
        /// <param name="virtualDirectoryId">The id</param>
        /// <returns>The user of the virtual directory otherwise null</returns>
        Task<UserDto> GetUserAsync(int virtualDirectoryId);

        /// <summary>
        ///     Get the parent of the virtual directory specified by the id asynchronously.
        /// </summary>
        /// <param name="virtualDirectoryId">The id</param>
        /// <returns>The parent of the virtual directory otherwise null</returns>
        Task<VirtualDirectoryDto> GetParentAsync(int virtualDirectoryId);

        /// <summary>
        ///     Get the children of the virtual directory specified by the id asynchronously.
        /// </summary>
        /// <param name="virtualDirectoryId">The id</param>
        /// <returns>The children of the virtual directory otherwise empty</returns>
        Task<IReadOnlyCollection<VirtualDirectoryListDto>> GetChildrenAsync(int virtualDirectoryId);

        /// <summary>
        ///     Get the preferences of the virtual directory specified by the id asynchronously.
        /// </summary>
        /// <param name="virtualDirectoryId">The id</param>
        /// <returns>The preferences of the virtual directory otherwise empty</returns>
        Task<IReadOnlyCollection<VirtualDirectoryPreferenceListDto>> GetPreferencesAsync(int virtualDirectoryId);

        /// <summary>
        ///     Get the files of the virtual directory specified by the id asynchronously.
        /// </summary>
        /// <param name="virtualDirectoryId">The id</param>
        /// <returns>All the files of the virtual directory otherwise empty</returns>
        Task<IReadOnlyCollection<FileListDto>> GetFilesVirtualDirectoryAsync(int virtualDirectoryId);

        /// <summary>
        ///     Create the virtual directory asynchronously.
        /// </summary>
        /// <param name="virtualDirectory">The virtual directory</param>
        /// <returns>The saved virtual directory otherwise null</returns>
        Task<VirtualDirectoryDto> CreateAsync(VirtualDirectoryInputDto virtualDirectory);

        /// <summary>
        ///     Update the specified virtual directory asynchronously.
        /// </summary>
        /// <param name="virtualDirectoryId">The id of the virtual directory to update</param>
        /// <param name="virtualDirectory">The virtual directory</param>
        /// <returns>The updated virtual directory otherwise null</returns>
        Task<VirtualDirectoryDto> UpdateAsync(int virtualDirectoryId, VirtualDirectoryInputDto virtualDirectory);

        /// <summary>
        ///     Remove the virtual directory specified by the id asynchronously.
        /// </summary>
        /// <param name="virtualDirectoryId">The id</param>
        /// <returns>True if the virtual directory has been removed otherwise false</returns>
        Task<bool> RemoveAsync(int virtualDirectoryId);

        /// <summary>
        ///     Save the specified file in the specified virtual directory with its customized filename asynchronously.
        /// </summary>
        /// <param name="virtualDirectoryId">Thd id of the virtual directory</param>
        /// <param name="fileId">The id of the file</param>
        /// <param name="filename">The filename</param>
        /// <returns>The saved user preference otherwise null</returns>
        Task<VirtualDirectoryPreferenceDto> SaveFileVirtualDirectoryAsync(int virtualDirectoryId, int fileId, string filename);

        /// <summary>
        ///     Remove the specified file in the specified virtual directory with its customized filename asynchronously.
        /// </summary>
        /// <param name="virtualDirectoryId">Thd id of the virtual directory</param>
        /// <param name="fileId">The id of the file</param>
        /// <returns>True if the file has been removed of the virtual directory otherwise false</returns>
        Task<bool> RemoveFileVirtualDirectoryAsync(int virtualDirectoryId, int fileId);

    }
}
