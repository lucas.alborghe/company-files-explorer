﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTOs.CustomConfig;
using Data.Enums;

namespace Core.Services.Interfaces
{
    public interface ICustomConfigService
    {
        /// <summary>
        ///     Get the unique configuration asynchronously.
        /// </summary>
        Task<CustomConfigDto> GetAsync();

        /// <summary>
        ///     Gets the number asynchronously.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>Task&lt;System.Int32&gt;.</returns>
        Task<int> GetNumberAsync(CustomConfigKey key);

        /// <summary>
        ///     Gets the value asynchronously.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>Task&lt;System.String&gt;.</returns>
        Task<string> GetValueAsync(CustomConfigKey key);

        /// <summary>
        ///     Get the values asynchronously.
        /// </summary>
        Task<IReadOnlyCollection<CustomConfigListDto>> GetValuesAsync();

        /// <summary>
        ///     Saves the specified configuration asynchronously.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        Task<CustomConfigListDto[]> SaveAsync(CustomConfigInputDto configuration);

        /// <summary>
        ///     Gets the automatic API key deactivation inactivity limit.
        /// </summary>
        /// <returns>Task&lt;DateTime&gt;.</returns>
        Task<DateTime> GetAutoApiKeyDeactivationInactivityLimit();

        /// <summary>
        ///     Gets the next password validity period.
        /// </summary>
        /// <returns>Task&lt;DateTime&gt;.</returns>
        Task<DateTime> GetNextPasswordValidityPeriod();
    }
}