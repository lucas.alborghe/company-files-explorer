﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.User;
using Core.Dtos.VirtualDirectory;

namespace Core.Services.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        ///     Get all the users asynchronously.
        /// </summary>
        /// <returns>All the users otherwise empty</returns>
        Task<IReadOnlyCollection<UserListDto>> GetAllAsync();

        /// <summary>
        ///     Get the user specified by the id asynchronously.
        /// </summary>
        /// <param name="userId">The id</param>
        /// <returns>The user with the given id otherwise null</returns>
        Task<UserDto> GetAsync(int userId);

        /// <summary>
        ///     Get the user specified by the login asynchronously.
        /// </summary>
        /// <param name="login">The login</param>
        /// <returns>The user with the given login otherwise null</returns>
        Task<UserDto> GetAsync(string login);

        /// <summary>
        ///     Get all the virtual directories of the user specified by the id asynchronously.
        /// </summary>
        /// <param name="userId">The id</param>
        /// <returns>All the virtual directories otherwise empty</returns>
        Task<IReadOnlyCollection<VirtualDirectoryListDto>> GetVirtualDirectoriesAsync(int userId);

        /// <summary>
        ///     Create the user asynchronously.
        /// </summary>
        /// <param name="user">The user</param>
        /// <returns>The user saved otherwise null</returns>
        Task<UserDto> CreateAsync(UserInputDto user);

        /// <summary>
        ///     Update the specified user asynchronously.
        /// </summary>
        /// <param name="userId">The id of the user to update</param>
        /// <param name="user">The user</param>
        /// <returns>The user updated otherwise null</returns>
        Task<UserDto> UpdateAsync(int userId, UserInputDto user);

        /// <summary>
        ///     Remove the user specified by the id asynchronously.
        /// </summary>
        /// <param name="userId">The id</param>
        /// <returns>True if the user has been removed otherwise false</returns>
        Task<bool> RemoveAsync(int userId);

    }
}
