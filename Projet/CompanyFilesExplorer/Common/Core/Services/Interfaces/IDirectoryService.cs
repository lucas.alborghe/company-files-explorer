﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.Directory;
using Core.Dtos.Repository;

namespace Core.Services.Interfaces
{
    public interface IDirectoryService
    {
        /// <summary>
        ///     Get all the directories asynchronously.
        /// </summary>
        /// <returns>All the directories otherwise empty</returns>
        Task<IReadOnlyCollection<DirectoryListDto>> GetAllAsync();

        /// <summary>
        ///     Get all the directories which start with the specified path asynchronously.
        /// </summary>
        /// <param name="path">The keyword</param>
        /// <returns>All the directories which start with 'path' otherwise empty</returns>
        Task<IReadOnlyCollection<DirectoryListDto>> GetAllByPathAsync(string path);

        /// <summary>
        ///     Get all the directories which contain the specified filter asynchronously.
        /// </summary>
        /// <param name="filter">The filter</param>
        /// <returns>All the directories which contain 'filter' otherwise empty</returns>
        Task<IReadOnlyCollection<DirectoryListDto>> GetAllByFilterAsync(string filter);

        /// <summary>
        ///     Get the repository of the directory specified by the path asynchronously.
        /// </summary>
        /// <param name="path">The path</param>
        /// <returns>The repository otherwhise null</returns>
        Task<RepositoryDto> GetRepositoryAsync(string path);

        /// <summary>
        ///     Get the directory specified by the path asynchronously.
        /// </summary>
        /// <param name="path">The path</param>
        /// <returns>The directory with the given path otherwise null</returns>
        Task<DirectoryDto> GetAsync(string path);

        /// <summary>
        ///     Save the specified directory asynchronously.
        /// </summary>
        /// <param name="directory">The directory</param>
        /// <returns>The saved directory otherwise null</returns>
        Task<DirectoryDto> SaveAsync(DirectoryInputDto directory);

        /// <summary>
        ///     Remove the directory specified by the path asynchronously.
        /// </summary>
        /// <param name="path">The path</param>
        /// <returns>True if the directory has been removed otherwise false</returns>
        Task<bool> RemoveAsync(string path);

    }
}
