﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.Directory;
using Core.Dtos.File;
using Core.Dtos.Repository;

namespace Core.Services.Interfaces
{
    public interface IRepositoryService
    {
        /// <summary>
        ///     Get all the repositories asynchronously.
        /// </summary>
        /// <returns>All the repositories otherwise empty</returns>
        Task<IReadOnlyCollection<RepositoryListDto>> GetAllAsync();

        /// <summary>
        ///     Get all the directories of the user specified by the id asynchronously.
        /// </summary>
        /// <param name="repositoryId">The id</param>
        /// <returns>All the directories otherwise empty</returns>
        Task<IReadOnlyCollection<DirectoryListDto>> GetDirectoriesAsync(int repositoryId);

        /// <summary>
        ///     Get all the files of the repository specified by the id asynchronously.
        /// </summary>
        /// <param name="repositoryId">The id</param>
        /// <returns>All the files otherwise empty</returns>
        Task<IReadOnlyCollection<FileListDto>> GetFilesAsync(int repositoryId);

        /// <summary>
        ///     Get the repository specified by the id asynchronously.
        /// </summary>
        /// <param name="repositoryId">The id</param>
        /// <returns>The repository with the given id otherwise null</returns>
        Task<RepositoryDto> GetAsync(int repositoryId);

        /// <summary>
        ///     Create the repository asynchronously.
        /// </summary>
        /// <param name="repository">The repository</param>
        /// <returns>The repository saved otherwise null</returns>
        Task<RepositoryDto> CreateAsync(RepositoryInputDto repository);

        /// <summary>
        ///     Save the specified repository asynchronously.
        /// </summary>
        /// <param name="repositoryId">The id of the repository to update</param>
        /// <param name="repository">The repository</param>
        /// <returns>The repository updated otherwise null</returns>
        Task<RepositoryDto> UpdateAsync(int repositoryId, RepositoryInputDto repository);

        /// <summary>
        ///     Remove the repository specified by the id asynchronously.
        /// </summary>
        /// <param name="repositoryId">The id</param>
        /// <returns>True if the repository has been removed otherwise false</returns>
        Task<bool> RemoveAsync(int repositoryId);

    }
}
