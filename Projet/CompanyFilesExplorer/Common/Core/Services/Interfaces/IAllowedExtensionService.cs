﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.Extension;

namespace Core.Services.Interfaces
{
    public interface IAllowedExtensionService
    {
        /// <summary>
        ///     Get all the allowed extensions asynchronously.
        /// </summary>
        /// <returns>All the allowed extensions otherwise empty</returns>
        Task<IReadOnlyCollection<ExtensionListDto>> GetAllAsync();

        /// <summary>
        ///     Get the allowed extension specified by the id asynchronously.
        /// </summary>
        /// <param name="extensionId">The id</param>
        /// <returns>The allowed extension with the given id otherwise null</returns>
        Task<ExtensionDto> GetAsync(int extensionId);

        /// <summary>
        ///     Create the allowed extension asynchronously.
        /// </summary>
        /// <param name="extension">The extension</param>
        /// <returns>The allowed extension saved otherwise null</returns>
        Task<ExtensionDto> CreateAsync(ExtensionInputDto extension);

        /// <summary>
        ///     Update the specified allowed extension asynchronously.
        /// </summary>
        /// <param name="extensionId">The id of the extension to update</param>
        /// <param name="extension">The extension</param>
        /// <returns>The allowed extension updated otherwise null</returns>
        Task<ExtensionDto> UpdateAsync(int extensionId, ExtensionInputDto extension);

        /// <summary>
        ///     Remove the allowed extension specified by the id asynchronously.
        /// </summary>
        /// <param name="extensionId">The id</param>
        /// <returns>True if the allowed extension has been removed otherwise false</returns>
        Task<bool> RemoveAsync(int extensionId);

    }
}
