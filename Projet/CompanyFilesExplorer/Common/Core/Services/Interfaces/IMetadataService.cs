﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.Metadata;
using Core.Dtos.Procedure;

namespace Core.Services.Interfaces
{
    public interface IMetadataService
    {
        /// <summary>
        ///     Get all the metadata asynchronously.
        /// </summary>
        /// <returns>All the metadata otherwise empty</returns>
        Task<IReadOnlyCollection<MetadataListDto>> GetAllAsync();

        /// <summary>
        ///     Get the procedure of the metadata specified by the id asynchronously.
        /// </summary>
        /// <param name="metadataId">The id</param>
        /// <returns>The procedure otherwhise null</returns>
        Task<ProcedureDto> GetProcedureAsync(int metadataId);

        /// <summary>
        ///     Get the metadata specified by the id asynchronously.
        /// </summary>
        /// <param name="metadataId">The id</param>
        /// <returns>The metadata with the given id otherwise null</returns>
        Task<MetadataDto> GetAsync(int metadataId);

        /// <summary>
        ///     Create the metadata asynchronously.
        /// </summary>
        /// <param name="metadata">The metadata</param>
        /// <returns>The metadata saved otherwise null</returns>
        Task<MetadataDto> CreateAsync(MetadataInputDto metadata);

        /// <summary>
        ///     Update the specified metadata asynchronously.
        /// </summary>
        /// <param name="metadataId">The if of the metadata to update</param>
        /// <param name="metadata">The metadata</param>
        /// <returns>The metadata updated otherwise null</returns>
        Task<MetadataDto> UpdateAsync(int metadataId, MetadataInputDto metadata);

        /// <summary>
        ///     Remove the metadata specified by the id asynchronously.
        /// </summary>
        /// <param name="metadataId">The id</param>
        /// <returns>True if the metadata has been removed otherwise false</returns>
        Task<bool> RemoveAsync(int metadataId);

    }
}
