﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Core.DataAccess;
using Core.Dtos.Directory;
using Core.Dtos.Extension;
using Core.Dtos.File;
using Core.Dtos.Repository;
using Core.Services.Interfaces;
using Microsoft.Extensions.Logging;

namespace Core.Services
{
    class DatabaseManagerService : IDatabaseManagerService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<IDatabaseManagerService> _logger;
        private readonly IDirectoryService _directoryService;
        private readonly IFileService _fileService;
        private readonly IRepositoryService _repositoryService;
        private readonly IAllowedExtensionService _extensionService;

        public DatabaseManagerService(ApplicationDbContext context, ILogger<IDatabaseManagerService> logger, IDirectoryService directoryService, IFileService fileService, IRepositoryService repositoryService, IAllowedExtensionService extensionService)
        {
            _context = context;
            _logger = logger;
            _directoryService = directoryService;
            _fileService = fileService;
            _repositoryService = repositoryService;
            _extensionService = extensionService;
        }

        public async Task<List<FileListDto>> RefreshDatabaseAsync()
        {
            // List of extensions allowed
            IEnumerable<ExtensionListDto> extensionsFromDb = await _extensionService.GetAllAsync();
            string[] extensions = new string[extensionsFromDb.Count()];
            for (int i = 0; i < extensions.Length; i++)
                extensions[i] = "*" + extensionsFromDb.ElementAt(i).Extension;

            // List of repositories
            IEnumerable<RepositoryListDto> repositories = await _repositoryService.GetAllAsync();

            // List of refreshed files
            List<FileListDto> refreshedFiles = new List<FileListDto>();

            try
            {
                // For each repository, get the files matching the allowed extensions
                foreach (RepositoryListDto repository in repositories)
                {
                    // Add root and all its subdirectories, even if there isnt any file directly under
                    foreach (string directory in Directory.GetDirectories(repository.AbsolutePath, "*", SearchOption.AllDirectories))
                    {
                        // Get the directory (absolute path)
                        DirectoryInputDto d = new DirectoryInputDto()
                        {
                            Path = Path.GetDirectoryName(directory),
                            RepositoryId = repository.Id
                        };
                        await _directoryService.SaveAsync(d);
                    }

                    // Add all files in subdirectories
                    foreach (string file in GetFilesFromPath(repository.AbsolutePath, extensions))
                    {
                        // Get the directory (useful for the files which are as leaf)
                        DirectoryInputDto d = new DirectoryInputDto()
                        {
                            Path = Path.GetDirectoryName(file),
                            RepositoryId = repository.Id
                        };
                        await _directoryService.SaveAsync(d);

                        // Get the informations of the current file
                        FileInputDto f = new FileInputDto()
                        {
                            RepositoryId = repository.Id,
                            Filepath = Path.GetDirectoryName(file),
                            Filename = Path.GetFileNameWithoutExtension(file),
                            Extension = Path.GetExtension(file)
                        };
                        await _fileService.SaveAsync(f);

                        // Add f to addedFiles
                        refreshedFiles.Add(new FileListDto()
                        {
                            RepositoryId = repository.Id,
                            Filepath = Path.GetDirectoryName(file),
                            Filename = Path.GetFileNameWithoutExtension(file),
                            Extension = Path.GetExtension(file)
                        });
                    }
                }
                // Save changes
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return null;
            }
            return refreshedFiles;
        }

        public async Task<List<FileListDto>> RefreshDatabaseFromPathAsync(string path)
        {
            string pathToRefresh = null;
            int repositoryToRefreshId = 0;

            // List of extensions allowed
            IEnumerable<ExtensionListDto> extensionsFromDb = await _extensionService.GetAllAsync();
            string[] extensions = new string[extensionsFromDb.Count()];
            for (int i = 0; i < extensions.Length; i++)
                extensions[i] = "*" + extensionsFromDb.ElementAt(i).Extension;

            // List of refreshed files
            List<FileListDto> refreshedFiles = new List<FileListDto>();

            // Check if the given path is ok (should be part of one of the existing repositories)
            IEnumerable<RepositoryListDto> repositories = await _repositoryService.GetAllAsync();
            foreach (RepositoryListDto repository in repositories)
            {
                if (path.ToLower().StartsWith(repository.AbsolutePath.ToLower()))
                {
                    pathToRefresh = path;
                    repositoryToRefreshId = repository.Id;
                    break;
                }
            }

            // Refresh database if path is valid
            if (pathToRefresh != null)
            {
                try
                {
                    IEnumerable<string> filesPath = GetFilesFromPath(pathToRefresh, extensions);
                    foreach (string file in filesPath)
                    {
                        // Get the directory (absolute path) of the current file
                        DirectoryInputDto d = new DirectoryInputDto()
                        {
                            Path = Path.GetDirectoryName(file),
                            RepositoryId = repositoryToRefreshId
                        };
                        await _directoryService.SaveAsync(d);

                        // Get the informations of the current file
                        FileInputDto f = new FileInputDto()
                        {
                            RepositoryId = repositoryToRefreshId,
                            Filepath = Path.GetDirectoryName(file),
                            Filename = Path.GetFileNameWithoutExtension(file),
                            Extension = Path.GetExtension(file)
                        };
                        await _fileService.SaveAsync(f);

                        // Add f to addedFiles
                        refreshedFiles.Add(new FileListDto()
                        {
                            RepositoryId = repositoryToRefreshId,
                            Filepath = Path.GetDirectoryName(file),
                            Filename = Path.GetFileNameWithoutExtension(file),
                            Extension = Path.GetExtension(file)
                        });
                    }
                    // Save changes
                    _context.SaveChanges();
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return refreshedFiles;
        }

        // Works in .Net 4.0 - takes same patterns as old method, and executes in parallel
        // Source : https://www.techmikael.com/2010/02/directory-search-with-multiple-filters.html
        private IEnumerable<string> GetFilesFromPath(string path, string[] searchPatterns, SearchOption searchOption = SearchOption.AllDirectories)
        {
            return searchPatterns.AsParallel().SelectMany(searchPattern => Directory.EnumerateFiles(path, searchPattern, searchOption));
        }
    }
}
