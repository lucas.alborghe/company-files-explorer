﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.DataAccess;
using Core.Dtos.File;
using Core.Dtos.Metadata;
using Core.Dtos.Procedure;
using Core.Dtos.VirtualDirectory;
using Core.Services.Interfaces;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Core.Services
{
    class ProcedureService : IProcedureService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<IProcedureService> _logger;
        private readonly IFileService _fileService;

        public static readonly Expression<Func<ProcedureDb, ProcedureListDto>> GetListDto = item => new ProcedureListDto
        {
            Id = item.Id,
            Name = item.Name,
            Rule = item.Rule,
            Description = item.Description
        };

        public ProcedureService(ApplicationDbContext context, ILogger<IProcedureService> logger, IFileService fileService)
        {
            _context = context;
            _logger = logger;
            _fileService = fileService;
        }

        public async Task<IReadOnlyCollection<ProcedureListDto>> GetAllAsync()
        {
            return await _context.ProceduresDb.Select(GetListDto).ToArrayAsync();
        }

        public async Task<IReadOnlyCollection<FileListDto>> GetFilesFromRuleAsync(string rule)
        {
            return await _fileService.GetAllByFilterAsync(rule);
        }

        public async Task<IReadOnlyCollection<MetadataListDto>> GetMetadataAsync(int procedureId)
        {
            return await _context.Metadata.Where(m => m.ProcedureId == procedureId).Select(MetadataService.GetListDto).ToArrayAsync();
        }

        public async Task<IReadOnlyCollection<VirtualDirectoryListDto>> GetVirtualDirectoriesAsync(int procedureId)
        {
            return await _context.VirtualDirectories.Where(vd => vd.ProcedureId == procedureId).Select(VirtualDirectoryService.GetListDto).ToArrayAsync();
        }

        public async Task<ProcedureDto> GetAsync(int procedureId)
        {
            ProcedureDb[] dbProcedures = await _context.ProceduresDb.ToArrayAsync();
            ProcedureDb dbProcedure = dbProcedures.SingleOrDefault(v => v.Id == procedureId);
            if (dbProcedure != null)
                return new ProcedureDto()
                {
                    Id = dbProcedure.Id,
                    Name = dbProcedure.Name,
                    Rule = dbProcedure.Rule,
                    Description = dbProcedure.Description
                };
            return null;
        }

        public async Task<ProcedureDto> CreateAsync(ProcedureInputDto procedure)
        {
            ProcedureDb toAdd = new ProcedureDb
            {
                Name = procedure.Name,
                Rule = procedure.Rule,
                Description = procedure.Description
            };
            _context.ProceduresDb.Add(toAdd);
            await _context.SaveChangesAsync();
            return new ProcedureDto
            {
                Id = toAdd.Id,
                Name = toAdd.Name,
                Rule = toAdd.Rule,
                Description = toAdd.Description
            };
        }

        public async Task<ProcedureDto> UpdateAsync(int procedureId, ProcedureInputDto procedure)
        {
            ProcedureDb[] dbProcedures = await _context.ProceduresDb.ToArrayAsync();
            ProcedureDb dbProcedure = dbProcedures.SingleOrDefault(v => v.Id == procedureId);
            // If the procedure does exist, update it
            if (dbProcedure != null)
            {
                dbProcedure.Name = procedure.Name;
                dbProcedure.Rule = procedure.Rule;
                dbProcedure.Description = procedure.Description;
                await _context.SaveChangesAsync();
                return new ProcedureDto
                {
                    Id = dbProcedure.Id,
                    Name = dbProcedure.Name,
                    Rule = dbProcedure.Rule,
                    Description = dbProcedure.Description
                };
            }
            return null;
        }

        public async Task<bool> RemoveAsync(int procedureId)
        {
            ProcedureDb[] dbProcedures = await _context.ProceduresDb.ToArrayAsync();
            ProcedureDb dbProcedure = dbProcedures.SingleOrDefault(v => v.Id == procedureId);
            if (dbProcedure != null)
            {
                _context.ProceduresDb.Remove(dbProcedure);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
