﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.DataAccess;
using Core.Dtos.Extension;
using Core.Services.Interfaces;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Core.Services
{
    class AllowedExtensionService : IAllowedExtensionService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<IAllowedExtensionService> _logger;

        public static readonly Expression<Func<AllowedExtension, ExtensionListDto>> GetListDto = item => new ExtensionListDto
        {
            Id = item.Id,
            Extension = item.Extension
        };

        public AllowedExtensionService(ApplicationDbContext context, ILogger<IAllowedExtensionService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<IReadOnlyCollection<ExtensionListDto>> GetAllAsync()
        {
            return await _context.AllowedExtensions.Select(GetListDto).ToArrayAsync();
        }

        public async Task<ExtensionDto> GetAsync(int extensionId)
        {
            AllowedExtension[] dbExtensions = await _context.AllowedExtensions.ToArrayAsync();
            AllowedExtension dbExtension = dbExtensions.SingleOrDefault(v => v.Id == extensionId);
            if (dbExtension != null)
                return new ExtensionDto()
                {
                    Id = dbExtension.Id,
                    Extension = dbExtension.Extension
                };
            return null;
        }

        public async Task<ExtensionDto> CreateAsync(ExtensionInputDto extension)
        {
            AllowedExtension toAdd = new AllowedExtension
            {
                Extension = extension.Extension
            };
            _context.AllowedExtensions.Add(toAdd);
            await _context.SaveChangesAsync();
            return new ExtensionDto
            {
                Id = toAdd.Id,
                Extension = toAdd.Extension
            };
        }

        public async Task<ExtensionDto> UpdateAsync(int extensionId, ExtensionInputDto extension)
        {
            AllowedExtension[] dbExtensions = await _context.AllowedExtensions.ToArrayAsync();
            AllowedExtension dbExtension = dbExtensions.SingleOrDefault(v => v.Id == extensionId);
            // If the extension does exist, update it
            if (dbExtension != null)
            {
                dbExtension.Extension = extension.Extension;
                await _context.SaveChangesAsync();
                return new ExtensionDto
                {
                    Id = dbExtension.Id,
                    Extension = dbExtension.Extension
                };
            }
            return null;
        }

        public async Task<bool> RemoveAsync(int extensionId)
        {
            AllowedExtension[] dbExtensions = await _context.AllowedExtensions.ToArrayAsync();
            AllowedExtension dbExtension = dbExtensions.SingleOrDefault(v => v.Id == extensionId);
            if (dbExtension != null)
            {
                _context.AllowedExtensions.Remove(dbExtension);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
