﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Core.DataAccess;
using Core.Dtos.File;
using Core.Dtos.Repository;
using Core.Services.Interfaces;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Core.Services
{
    class FileService : IFileService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<IFileService> _logger;
        private readonly IDateTimeService _dateTimeService;
        private readonly IRepositoryService _repositoryService;

        public static readonly Expression<Func<FileDb, FileListDto>> GetListDto = item => new FileListDto
        {
            Id = item.Id,
            RepositoryId = item.RepositoryId,
            Filepath = item.Filepath,
            Filename = item.Filename,
            Extension = item.Extension,
            Hashcode = item.Hashcode,
            CreatedDate = item.CreatedDate,
            ModifiedDate = item.ModifiedDate
        };

        public FileService(ApplicationDbContext context, ILogger<IFileService> logger, IDateTimeService dateTimeService, IRepositoryService repositoryService)
        {
            _context = context;
            _logger = logger;
            _dateTimeService = dateTimeService;
            _repositoryService = repositoryService;
        }

        public async Task<IReadOnlyCollection<FileListDto>> GetAllAsync()
        {
            return await _context.FilesDb.Select(GetListDto).ToArrayAsync();
        }

        public async Task<IReadOnlyCollection<FileListDto>> GetAllByFilenameAsync(string filter)
        {
            return await _context.FilesDb.Where(f => f.Filename.ToLower().Contains(filter.ToLower())).Select(GetListDto).ToArrayAsync();
        }

        public async Task<IReadOnlyCollection<FileListDto>> GetAllByPathAsync(string path)
        {
            return await _context.FilesDb.Where(f => f.Filepath.ToLower().StartsWith(path.ToLower())).Select(GetListDto).ToArrayAsync();
        }

        public async Task<IReadOnlyCollection<FileListDto>> GetAllByFilterAsync(string filter)
        {
            return await _context.FilesDb.Where(f => Path.Combine(f.Filepath, f.Filename, f.Extension).ToLower().Contains(filter.ToLower())).Select(GetListDto).ToArrayAsync();
        }

        public async Task<RepositoryDto> GetRepositoryAsync(int fileId)
        {
            FileDb[] dbFiles = await _context.FilesDb.ToArrayAsync();
            FileDb dbFile = dbFiles.SingleOrDefault(v => v.Id == fileId);
            if (dbFile != null)
                return await _repositoryService.GetAsync(dbFile.RepositoryId);
            return null;
        }

        public async Task<string> GetJsonAsync(int fileId)
        {
            return JsonConvert.SerializeObject(await GetAsync(fileId), Formatting.Indented);
        }

        public async Task<FileDto> GetAsync(int fileId)
        {
            FileDb[] dbFiles = await _context.FilesDb.ToArrayAsync();
            FileDb dbFile = dbFiles.SingleOrDefault(v => v.Id == fileId);
            if (dbFile != null)
                return new FileDto()
                {
                    Id = dbFile.Id,
                    RepositoryId = dbFile.RepositoryId,
                    Filepath = dbFile.Filepath,
                    Filename = dbFile.Filename,
                    Extension = dbFile.Extension,
                    Hashcode = dbFile.Hashcode,
                    CreatedDate = dbFile.CreatedDate,
                    ModifiedDate = dbFile.ModifiedDate
                };
            return null;
        }

        public async Task<FileDto> CreatePhysicalFile(FileMoveInputDto input)
        {
            FileDto newPhysicalFile = null;
            // Extract data
            string sourceFilename = input.SourceFilename + "" + input.SourceExtension;
            string sourcePath = Path.Combine(input.SourceFilepath, sourceFilename);
            string destinationFilename = input.DestinationFilename + "" + input.DestinationExtension;
            string destinationPath = Path.Combine(input.DestinationFilepath, destinationFilename);
            // Copy the file to its new directory if possible
            if (Directory.Exists(input.DestinationFilepath))
                if (!File.Exists(destinationPath))
                    File.Copy(sourcePath, destinationPath, false);
            // If the file has been copied, add it in the database
            if(File.Exists(destinationPath))
                newPhysicalFile = await SaveAsync(new FileInputDto()
                {
                    RepositoryId = input.DestinationRepositoryId,
                    Filepath = Path.GetDirectoryName(destinationPath),
                    Filename = Path.GetFileNameWithoutExtension(destinationPath),
                    Extension = Path.GetExtension(destinationPath)
                });
            return newPhysicalFile;
        }

        public async Task<FileDto> SaveAsync(FileInputDto file)
        {
            FileDb[] dbFiles = await _context.FilesDb.ToArrayAsync();
            FileDb dbFile = dbFiles.SingleOrDefault(v => v.Filepath == file.Filepath && v.Filename == file.Filename && v.Extension == file.Extension);
            // If the file doesn't exist yet, add it
            if (dbFile == null)
            {
                FileDb toAdd = new FileDb
                {
                    RepositoryId = file.RepositoryId,
                    Filepath = file.Filepath,
                    Filename = file.Filename,
                    Extension = file.Extension,
                    Hashcode = CalculateMD5(Path.Combine(file.Filepath, file.Filename + file.Extension)),
                    CreatedDate = _dateTimeService.UtcNow(),
                    ModifiedDate = _dateTimeService.UtcNow()
                };
                _context.FilesDb.Add(toAdd);
                await _context.SaveChangesAsync();
                return new FileDto
                {
                    Id = toAdd.Id,
                    RepositoryId = toAdd.RepositoryId,
                    Filepath = toAdd.Filepath,
                    Filename = toAdd.Filename,
                    Extension = toAdd.Extension
                };
            }
            // If the file does exist already, update it
            dbFile.RepositoryId = file.RepositoryId;
            dbFile.Filepath = file.Filepath;
            dbFile.Filename = file.Filename;
            dbFile.Extension = file.Extension;
            dbFile.Hashcode = CalculateMD5(Path.Combine(file.Filepath, file.Filename + file.Extension));
            dbFile.ModifiedDate = _dateTimeService.UtcNow();
            await _context.SaveChangesAsync();
            return new FileDto
            {
                Id = dbFile.Id,
                RepositoryId = dbFile.RepositoryId,
                Filepath = dbFile.Filepath,
                Filename = dbFile.Filename,
                Extension = dbFile.Extension,
                Hashcode = dbFile.Hashcode,
                CreatedDate = dbFile.CreatedDate,
                ModifiedDate = dbFile.ModifiedDate
            };
        }

        public async Task<bool> RemoveAsync(int fileId)
        {
            FileDb[] dbFiles = await _context.FilesDb.ToArrayAsync();
            FileDb dbFile = dbFiles.SingleOrDefault(v => v.Id == fileId);
            if (dbFile != null)
            {
                _context.FilesDb.Remove(dbFile);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        // If you need to represent the hash as a string, you could convert it to hex using BitConverter
        // Source : https://stackoverflow.com/questions/10520048/calculate-md5-checksum-for-a-file
        private string CalculateMD5(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }
    }
}
