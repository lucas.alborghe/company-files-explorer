﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.DataAccess;
using Core.Dtos.File;
using Core.Dtos.Procedure;
using Core.Dtos.User;
using Core.Dtos.VirtualDirectory;
using Core.Services.Interfaces;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Core.Services
{
    class VirtualDirectoryService : IVirtualDirectoryService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<IVirtualDirectoryService> _logger;
        private readonly IProcedureService _procedureService;
        private readonly IUserService _userService;

        public static readonly Expression<Func<VirtualDirectory, VirtualDirectoryListDto>> GetListDto = item => new VirtualDirectoryListDto
        {
            Id = item.Id,
            ProcedureId = item.ProcedureId,
            UserId = item.UserId,
            VirtualDirectoryId = item.VirtualDirectoryId,
            Name = item.Name,
            IsActive = item.IsActive
        };

        public static readonly Expression<Func<UserPreference, VirtualDirectoryPreferenceListDto>> GetListPrefDto = item => new VirtualDirectoryPreferenceListDto
        {
            VirtualDirectoryId = item.VirtualDirectoryId,
            FileId = item.FileId,
            Filename = item.Filename
        };

        public VirtualDirectoryService(ApplicationDbContext context, ILogger<IVirtualDirectoryService> logger, IProcedureService procedureService, IUserService userService)
        {
            _context = context;
            _logger = logger;
            _procedureService = procedureService;
            _userService = userService;
        }

        public async Task<IReadOnlyCollection<VirtualDirectoryListDto>> GetAllAsync()
        {
            return await _context.VirtualDirectories.Select(GetListDto).ToArrayAsync();
        }

        public async Task<VirtualDirectoryDto> GetAsync(int id)
        {
            VirtualDirectory[] dbVirtualDirectories = await _context.VirtualDirectories.ToArrayAsync();
            VirtualDirectory dbVirtualDirectory = dbVirtualDirectories.SingleOrDefault(v => v.Id == id);
            if (dbVirtualDirectory != null)
                return new VirtualDirectoryDto()
                {
                    Id = dbVirtualDirectory.Id,
                    UserId = dbVirtualDirectory.UserId,
                    VirtualDirectoryId = dbVirtualDirectory.VirtualDirectoryId,
                    ProcedureId = dbVirtualDirectory.ProcedureId,
                    Name = dbVirtualDirectory.Name,
                    IsActive = dbVirtualDirectory.IsActive
                };
            return null;
        }

        public async Task<ProcedureDto> GetProcedureAsync(int virtualDirectoryId)
        {
            VirtualDirectory[] dbVirtualDirectories = await _context.VirtualDirectories.ToArrayAsync();
            VirtualDirectory dbVirtualDirectory = dbVirtualDirectories.SingleOrDefault(v => v.Id == virtualDirectoryId);
            if (dbVirtualDirectory != null)
            {
                int procedureId = dbVirtualDirectory.ProcedureId ?? 0;
                if (procedureId > 0)
                    return await _procedureService.GetAsync(procedureId);
            }
            return null;
        }

        public async Task<UserDto> GetUserAsync(int virtualDirectoryId)
        {
            VirtualDirectory[] dbVirtualDirectories = await _context.VirtualDirectories.ToArrayAsync();
            VirtualDirectory dbVirtualDirectory = dbVirtualDirectories.SingleOrDefault(v => v.Id == virtualDirectoryId);
            if (dbVirtualDirectory != null)
                return await _userService.GetAsync(dbVirtualDirectory.UserId);
            return null;
        }

        public async Task<VirtualDirectoryDto> GetParentAsync(int virtualDirectoryId)
        {
            VirtualDirectory[] dbVirtualDirectories = await _context.VirtualDirectories.ToArrayAsync();
            VirtualDirectory dbVirtualDirectory = dbVirtualDirectories.SingleOrDefault(v => v.Id == virtualDirectoryId);
            if (dbVirtualDirectory != null)
            {
                int parentId = dbVirtualDirectory.VirtualDirectoryId ?? 0;
                if (parentId > 0)
                    return await GetAsync(parentId);
            }
            return null;
        }

        public async Task<IReadOnlyCollection<VirtualDirectoryListDto>> GetChildrenAsync(int virtualDirectoryId)
        {
            return await _context.VirtualDirectories.Where(vd => vd.VirtualDirectoryId == virtualDirectoryId).Select(GetListDto).ToArrayAsync();
        }

        public async Task<IReadOnlyCollection<VirtualDirectoryPreferenceListDto>> GetPreferencesAsync(int virtualDirectoryId)
        {
            return await _context.UserPreferences.Where(up => up.VirtualDirectoryId == virtualDirectoryId).Select(GetListPrefDto).ToArrayAsync();
        }

        public async Task<IReadOnlyCollection<FileListDto>> GetFilesVirtualDirectoryAsync(int virtualDirectoryId)
        {
            var listOfFilesId = _context.UserPreferences.Where(up => up.VirtualDirectoryId == virtualDirectoryId).Select(up => up.FileId);
            return await _context.FilesDb.Where(f => listOfFilesId.Contains(f.Id)).Select(FileService.GetListDto).ToArrayAsync();
        }

        public async Task<VirtualDirectoryDto> CreateAsync(VirtualDirectoryInputDto virtualDirectory)
        {
            VirtualDirectory toAdd = new VirtualDirectory
            {
                UserId = virtualDirectory.UserId,
                VirtualDirectoryId = virtualDirectory.VirtualDirectoryId,
                Name = virtualDirectory.Name,
                ProcedureId = virtualDirectory.ProcedureId,
                IsActive = virtualDirectory.IsActive ?? 1
            };
            _context.VirtualDirectories.Add(toAdd);
            await _context.SaveChangesAsync();
            return new VirtualDirectoryDto
            {
                Id = toAdd.Id,
                UserId = toAdd.UserId,
                VirtualDirectoryId = toAdd.VirtualDirectoryId,
                Name = toAdd.Name,
                ProcedureId = toAdd.ProcedureId,
                IsActive = toAdd.IsActive
            };
        }

        public async Task<VirtualDirectoryDto> UpdateAsync(int virtualDirectoryId, VirtualDirectoryInputDto virtualDirectory)
        {
            VirtualDirectory[] dbVirtualDirectories = await _context.VirtualDirectories.ToArrayAsync();
            VirtualDirectory dbVirtualDirectory = dbVirtualDirectories.SingleOrDefault(v => v.Id == virtualDirectoryId);
            // If the virtual directory does exist, update it
            if (dbVirtualDirectory != null)
            {
                dbVirtualDirectory.UserId = virtualDirectory.UserId;
                dbVirtualDirectory.VirtualDirectoryId = virtualDirectory.VirtualDirectoryId;
                dbVirtualDirectory.Name = virtualDirectory.Name;
                dbVirtualDirectory.ProcedureId = virtualDirectory.ProcedureId;
                dbVirtualDirectory.IsActive = virtualDirectory.IsActive ?? 1;
                await _context.SaveChangesAsync();
                return new VirtualDirectoryDto
                {
                    Id = dbVirtualDirectory.Id,
                    UserId = dbVirtualDirectory.UserId,
                    VirtualDirectoryId = dbVirtualDirectory.VirtualDirectoryId,
                    Name = dbVirtualDirectory.Name,
                    ProcedureId = dbVirtualDirectory.ProcedureId,
                    IsActive = dbVirtualDirectory.IsActive
                };
            }
            return null;
        }

        public async Task<bool> RemoveAsync(int id)
        {
            VirtualDirectory[] dbVirtualDirectories = await _context.VirtualDirectories.ToArrayAsync();
            VirtualDirectory dbVirtualDirectory = dbVirtualDirectories.SingleOrDefault(v => v.Id == id);
            if (dbVirtualDirectory != null)
            {
                _context.VirtualDirectories.Remove(dbVirtualDirectory);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<VirtualDirectoryPreferenceDto> SaveFileVirtualDirectoryAsync(int virtualDirectoryId, int fileId, string filename)
        {
            UserPreference[] dbUserPreferences = await _context.UserPreferences.ToArrayAsync();
            UserPreference dbUserPreference = dbUserPreferences.SingleOrDefault(v => v.VirtualDirectoryId == virtualDirectoryId && v.FileId == fileId);
            if (dbUserPreference == null)
            {
                // CREATE
                _context.UserPreferences.Add(new UserPreference()
                {
                    VirtualDirectoryId = virtualDirectoryId,
                    FileId = fileId,
                    Filename = filename
                });
            }
            else
            {
                // UPDATE
                dbUserPreference.Filename = filename;
            }
            await _context.SaveChangesAsync();
            return new VirtualDirectoryPreferenceDto
            {
                VirtualDirectoryId = virtualDirectoryId,
                FileId = fileId,
                Filename = filename
            };
        }

        public async Task<bool> RemoveFileVirtualDirectoryAsync(int virtualDirectoryId, int fileId)
        {
            UserPreference[] dbUserPreferences = await _context.UserPreferences.ToArrayAsync();
            UserPreference dbUserPreference = dbUserPreferences.SingleOrDefault(v => v.VirtualDirectoryId == virtualDirectoryId && v.FileId == fileId);
            if (dbUserPreference != null)
            {
                _context.UserPreferences.Remove(dbUserPreference);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
