﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.DataAccess;
using Core.Dtos.User;
using Core.Dtos.VirtualDirectory;
using Core.Services.Interfaces;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Core.Services
{
    class UserService : IUserService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<IUserService> _logger;

        public static readonly Expression<Func<UserDb, UserListDto>> GetListDto = item => new UserListDto
        {
            Id = item.Id,
            Login = item.Login,
            IsAdmin = item.IsAdmin
        };

        public UserService(ApplicationDbContext context, ILogger<IUserService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<IReadOnlyCollection<UserListDto>> GetAllAsync()
        {
            return await _context.UsersDb.Select(GetListDto).ToArrayAsync();
        }

        public async Task<UserDto> GetAsync(int userId)
        {
            UserDb[] dbUsers = await _context.UsersDb.ToArrayAsync();
            UserDb dbUser = dbUsers.SingleOrDefault(v => v.Id == userId);
            if (dbUser != null)
                return new UserDto()
                {
                    Id = dbUser.Id,
                    Login = dbUser.Login,
                    IsAdmin = dbUser.IsAdmin
                };
            return null;
        }

        public async Task<UserDto> GetAsync(string login)
        {
            UserDb[] dbUsers = await _context.UsersDb.ToArrayAsync();
            UserDb dbUser = dbUsers.SingleOrDefault(b => b.Login == login);
            if (dbUser != null)
                return new UserDto()
                {
                    Id = dbUser.Id,
                    Login = dbUser.Login,
                    IsAdmin = dbUser.IsAdmin
                };
            return null;
        }

        public async Task<IReadOnlyCollection<VirtualDirectoryListDto>> GetVirtualDirectoriesAsync(int userId)
        {
            return await _context.VirtualDirectories.Where(vd => vd.UserId == userId).Select(VirtualDirectoryService.GetListDto).ToArrayAsync();
        }

        public async Task<UserDto> CreateAsync(UserInputDto user)
        {
            UserDb toAdd = new UserDb
            {
                Login = user.Login,
                IsAdmin = user.IsAdmin
            };
            _context.UsersDb.Add(toAdd);
            await _context.SaveChangesAsync();
            return new UserDto
            {
                Id = toAdd.Id,
                Login = toAdd.Login,
                IsAdmin = toAdd.IsAdmin
            };
        }

        public async Task<UserDto> UpdateAsync(int userId, UserInputDto user)
        {
            UserDb[] dbUsers = await _context.UsersDb.ToArrayAsync();
            UserDb dbUser = dbUsers.SingleOrDefault(v => v.Id == userId);
            // If the user does exist, update it
            if (dbUser!= null)
            {
                dbUser.Login = user.Login;
                dbUser.IsAdmin = user.IsAdmin;
                await _context.SaveChangesAsync();
                return new UserDto
                {
                    Id = dbUser.Id,
                    Login = dbUser.Login,
                    IsAdmin = dbUser.IsAdmin
                };
            }
            return null;
        }

        public async Task<bool> RemoveAsync(int userId)
        {
            UserDb[] dbUsers = await _context.UsersDb.ToArrayAsync();
            UserDb dbUser = dbUsers.SingleOrDefault(v => v.Id == userId);
            if (dbUser != null)
            {
                _context.UsersDb.Remove(dbUser);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
