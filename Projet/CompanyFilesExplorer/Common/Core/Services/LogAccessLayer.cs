﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Core.DTOs.Log;
using Core.Extensions;
using Core.Services.Interfaces;
using Core.Validation;
using Dapper;
using Data.Entities;
using Data.Extensions;
using MySql.Data.MySqlClient;
using Serilog.Events;

namespace Core.Services
{
    public class LogAccessLayer : ILogAccessLayer
    {
        private const string LogsTableName = "logs";

        public void AddAll(string connectionString, params LogEvent[] logEvents)
        {
            if (!logEvents.Any())
            {
                return;
            }

            using (DbConnection sqlConnection = CreateDbConnection(connectionString))
            {
                using (DbTransaction tr = sqlConnection.BeginTransaction())
                {
                    using (DbCommand sqlCommand = CreateInsertCommand(sqlConnection))
                    {
                        sqlCommand.Transaction = tr;

                        foreach (LogEvent logEvent in logEvents)
                        {
                            sqlCommand.Parameters["@timeStamp"].Value = logEvent.Timestamp.ToUniversalTime();
                            sqlCommand.Parameters["@level"].Value = logEvent.Level.ToString();
                            sqlCommand.Parameters["@exception"].Value = logEvent.Exception?.ToString() ?? string.Empty;
                            sqlCommand.Parameters["@renderedMessage"].Value = logEvent.MessageTemplate.Text;
                            sqlCommand.Parameters["@properties"].Value = DbLogProperties.GetProperties(logEvent).SerializeToJson();

                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    tr.Commit();
                }
            }
        }

        public void ClearDatabase(string connectionString)
        {
            string[] tables =
            {
                    LogsTableName
            };
            using (DbConnection sqlConnection = CreateDbConnection(connectionString))
            {
                using (DbTransaction tr = sqlConnection.BeginTransaction())
                {
                    foreach (string table in tables)
                    {
                        using (DbCommand sqlCommand = sqlConnection.CreateCommand())
                        {
                            sqlCommand.Transaction = tr;
                            sqlCommand.CommandType = CommandType.Text;
                            sqlCommand.CommandText = $"DELETE FROM {table}";

                            sqlCommand.ExecuteNonQuery();
                        }
                    }

                    tr.Commit();
                }
            }
        }

        public void ClearLogsByType(string connectionString, LogEventLevel levelLimit)
        {
            LogEventLevel[] levelsToRemove = EnumExtensions.GetValues<LogEventLevel>().Where(e => e < levelLimit).ToArray();

            if (levelsToRemove.Length == 0)
            {
                return;
            }

            using (DbConnection sqlConnection = CreateDbConnection(connectionString))
            {
                using (DbTransaction tr = sqlConnection.BeginTransaction())
                {
                    using (DbCommand sqlCommand = sqlConnection.CreateCommand())
                    {
                        sqlCommand.Transaction = tr;
                        sqlCommand.CommandType = CommandType.Text;
                        string condition = string.Join(" OR ", levelsToRemove.Select(l => $"Level = '{l}'"));
                        sqlCommand.CommandText = $"DELETE FROM {LogsTableName} Where {condition}";

                        sqlCommand.ExecuteNonQuery();
                    }

                    tr.Commit();
                }
            }
        }

        public void CreateDatabase(string connectionString)
        {
            using (DbConnection sqlConnection = CreateDbConnection(connectionString))
            {
                string logsColumns = "Timestamp TEXT,";
                logsColumns += "Level VARCHAR(20),";
                logsColumns += "Exception TEXT,";
                logsColumns += "RenderedMessage TEXT,";
                logsColumns += "Properties TEXT";

                string sqlCreateTableText = $"CREATE TABLE IF NOT EXISTS {LogsTableName} ({logsColumns})";
                CreateDbCommand(sqlCreateTableText, sqlConnection).ExecuteNonQuery();
            }
        }

        public void RemoveDatabase(string connectionString)
        {

        }

        public async Task DeleteLogsAsync(string connectionString, LogFilterDto filter)
        {
            using (DbConnection sqlConnection = CreateDbConnection(connectionString))
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("from", filter.From);
                parameters.Add("to", filter.To);

                string whereParticules = $"{nameof(StdLog.TimeStamp)} <= @to AND {nameof(StdLog.TimeStamp)} >= @from";
                if (filter.Level != DtoValidationResultLevel.None)
                {
                    parameters.Add("levels", filter.Level.ToSerilogLevels());
                    whereParticules += $" AND {nameof(StdLog.Level)} IN @levels";
                }

                await sqlConnection.ExecuteAsync($"Delete from {LogsTableName} where {whereParticules}", parameters);
            }
        }

        public void DeleteOldLogs(string connectionString, DateTimeOffset dateTimeOffset)
        {
            DeleteOldLogs(connectionString, dateTimeOffset.ToUniversalTime().ToUnixTimeSeconds());
        }

        public async Task<IEnumerable<StdLog>> GetLogsAsync(string connectionString, LogFilterDto filter)
        {
            using (DbConnection sqlConnection = CreateDbConnection(connectionString))
            {
                string cmd = CreateSelectCommand(filter, out DynamicParameters parameters);
                return (await sqlConnection.QueryAsync<DbLog>(cmd, parameters)).Select(e => e.Create(filter.IsExceptionOn));
            }
        }

        public async Task<StdLogPage> GetPagedLogsAsync(string connectionString, PagedLogFilterDto filter)
        {
            using (DbConnection sqlConnection = CreateDbConnection(connectionString))
            {
                string cmd = CreateSelectCommand(filter, filter.PageIndex, filter.PageSize, out DynamicParameters selectParameters);
                IEnumerable<StdLog> logs = (await sqlConnection.QueryAsync<DbLog>(cmd, selectParameters)).Select(e => e.Create(filter.IsExceptionOn));
                string countCmd = CreateCountCommand(filter, out DynamicParameters countParameters);
                int totalCount = await sqlConnection.QueryFirstAsync<int>(countCmd, countParameters);
                int pagesCount = StdLogPage.ComputePagesCount(filter.PageSize, totalCount);
                return new StdLogPage
                {
                    PageSize = filter.PageSize,
                    PageIndex = Math.Min(pagesCount + 1, filter.PageIndex),
                    Logs = logs,
                    PagesCount = pagesCount,
                    TotalCount = totalCount
                };
            }
        }

        /// <summary>
        ///     Creates the select command.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        private string CreateCountCommand(LogFilterDto filter, out DynamicParameters parameters)
        {
            parameters = new DynamicParameters();
            parameters.Add("from", filter.From);
            parameters.Add("to", filter.To);

            string whereParticules = $"{nameof(StdLog.TimeStamp)} <= @to AND {nameof(StdLog.TimeStamp)} >= @from";
            if (filter.Level != DtoValidationResultLevel.None)
            {
                parameters.Add("levels", filter.Level.ToSerilogLevels());
                whereParticules += $" AND {nameof(StdLog.Level)} IN @levels";
            }

            return $"Select Count(*) from {LogsTableName} where {whereParticules}";
        }

        private DbCommand CreateInsertCommand(DbConnection connection)
        {
            string sqlInsertText = "INSERT INTO {0} (Timestamp, Level, Exception, RenderedMessage, Properties)";
            sqlInsertText += " VALUES (@timeStamp, @level, @exception, @renderedMessage, @properties)";
            sqlInsertText = string.Format(sqlInsertText, LogsTableName);

            DbCommand sqlCommand = connection.CreateCommand();
            sqlCommand.CommandText = sqlInsertText;
            sqlCommand.CommandType = CommandType.Text;

            sqlCommand.Parameters.Add(new MySqlParameter("@timeStamp", DbType.DateTime2));
            sqlCommand.Parameters.Add(new MySqlParameter("@level", DbType.String));
            sqlCommand.Parameters.Add(new MySqlParameter("@exception", DbType.String));
            sqlCommand.Parameters.Add(new MySqlParameter("@renderedMessage", DbType.String));
            sqlCommand.Parameters.Add(new MySqlParameter("@properties", DbType.String));

            return sqlCommand;
        }

        /// <summary>
        ///     Creates the select command.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        private string CreateSelectCommand(LogFilterDto filter, out DynamicParameters parameters)
        {
            parameters = new DynamicParameters();
            parameters.Add("from", filter.From);
            parameters.Add("to", filter.To);

            string whereParticules = $"{nameof(StdLog.TimeStamp)} <= @to AND {nameof(StdLog.TimeStamp)} >= @from";
            if (filter.Level != DtoValidationResultLevel.None)
            {
                parameters.Add("levels", filter.Level.ToSerilogLevels());
                whereParticules += $" AND {nameof(StdLog.Level)} IN @levels";
            }

            return $"Select * from {LogsTableName} where {whereParticules} order by {nameof(StdLog.TimeStamp)} DESC";
        }

        /// <summary>
        ///     Creates the select command using pageSize and pageIndex.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        private string CreateSelectCommand(PagedLogFilterDto filter, int pageIndex, int pageSize, out DynamicParameters parameters)
        {
            parameters = new DynamicParameters();
            parameters.Add("from", filter.From);
            parameters.Add("to", filter.To);

            string whereParticules = $"{nameof(StdLog.TimeStamp)} <= @to AND {nameof(StdLog.TimeStamp)} >= @from";
            if (filter.Level != DtoValidationResultLevel.None)
            {
                parameters.Add("levels", filter.Level.ToSerilogLevels());
                whereParticules += $" AND {nameof(StdLog.Level)} IN @levels";
            }

            return
                    $"Select * from {LogsTableName} where {whereParticules} order by {nameof(StdLog.TimeStamp)} DESC LIMIT {pageSize} OFFSET {pageIndex * pageSize}";
        }

        /// <summary>
        ///     Creates the DB connection.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns></returns>
        private DbConnection CreateDbConnection(string connectionString)
        {
            DbConnection sqlConnection = new MySqlConnection(connectionString);
            sqlConnection.Open();
            return sqlConnection;
        }

        private void DeleteOldLogs(string connectionString, long timestampLimit)
        {
            using (DbConnection sqlConnection = CreateDbConnection(connectionString))
            {
                DbCommand cmd = sqlConnection.CreateCommand();
                cmd.CommandText = $"DELETE FROM {LogsTableName} WHERE Timestamp < @dateTimeOffset";
                DbParameter param = new MySqlParameter("@dateTimeOffset", DbType.DateTime2);
                param.Value = timestampLimit;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
            }
        }

        private DbCommand CreateDbCommand(string sqlCommand, DbConnection sqlConnection)
        {
            DbCommand command = sqlConnection.CreateCommand();
            command.CommandText = sqlCommand;
            command.ExecuteNonQuery();
            return command;
        }
    }
}