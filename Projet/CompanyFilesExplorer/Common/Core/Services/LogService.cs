﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.DTOs.Log;
using Core.Extensions;
using Core.Services.Interfaces;
using Data.Entities;
using Serilog.Events;

namespace Core.Services
{
    public class LogService : ILogService
    {
        private readonly string _connectionString;
        private readonly ILogAccessLayer _accessLayer;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogService" /> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="accessLayer">The access layer.</param>
        public LogService(string connectionString, ILogAccessLayer accessLayer)
        {
            _connectionString = connectionString;
            _accessLayer = accessLayer;
        }

        public async Task<IReadOnlyCollection<LogListDto>> GetAllAsync(LogFilterDto logFilterDto)
        {
            return (await _accessLayer.GetLogsAsync(_connectionString, logFilterDto)).ToDtos();
        }

        public async Task<PagedLogListDto> GetAllAsync(PagedLogFilterDto logFilterDto) {
            PagedLogListDto result = new PagedLogListDto
            {
                    PageSize = logFilterDto.PageSize,
                    PageIndex = logFilterDto.PageIndex
            };
            StdLogPage[] pages = {
                    await _accessLayer.GetPagedLogsAsync(_connectionString, logFilterDto)
            };
            result.Logs = pages[0].Logs.ToDtos();
            result.TotalCount = pages.Sum(page => page.TotalCount);
            result.PagesCount = StdLogPage.ComputePagesCount(result.PageSize, result.TotalCount);
            result.PageIndex = Math.Min(result.PagesCount + 1, result.PageIndex);
            return result;
        }

        public void ClearDatabase()
        {
            _accessLayer.ClearDatabase(_connectionString);
        }

        public void ClearLogsByType(LogEventLevel levelLimit)
        {
            _accessLayer.ClearLogsByType(_connectionString, levelLimit);
        }

        public async Task DeleteAsync(LogFilterDto filtering)
        {
            await _accessLayer.DeleteLogsAsync(_connectionString, filtering);
        }
    }
}