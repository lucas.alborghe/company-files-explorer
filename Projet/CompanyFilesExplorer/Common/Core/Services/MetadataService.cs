﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.DataAccess;
using Core.Dtos.Metadata;
using Core.Dtos.Procedure;
using Core.Services.Interfaces;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Core.Services
{
    class MetadataService : IMetadataService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<IMetadataService> _logger;
        private readonly IProcedureService _procedureService;

        public static readonly Expression<Func<Metadata, MetadataListDto>> GetListDto = item => new MetadataListDto
        {
            Id = item.Id,
            ProcedureId = item.ProcedureId,
            Name = item.Name,
            Type = item.Type,
            FonctionParam = item.FonctionParam,
            Fonction = item.Fonction,
            Description = item.Description
        };

        public MetadataService(ApplicationDbContext context, ILogger<IMetadataService> logger, IProcedureService procedureService)
        {
            _context = context;
            _logger = logger;
            _procedureService = procedureService;
        }

        public async Task<IReadOnlyCollection<MetadataListDto>> GetAllAsync()
        {
            return await _context.Metadata.Select(GetListDto).ToArrayAsync();
        }

        public async Task<ProcedureDto> GetProcedureAsync(int metadataId)
        {
            Metadata[] dbMetadatas = await _context.Metadata.ToArrayAsync();
            Metadata dbMetadata = dbMetadatas.SingleOrDefault(v => v.Id == metadataId);
            if (dbMetadata != null)
                return await _procedureService.GetAsync(dbMetadata.ProcedureId);
            return null;
        }

        public async Task<MetadataDto> GetAsync(int metadataId)
        {
            Metadata[] dbMetadatas = await _context.Metadata.ToArrayAsync();
            Metadata dbMetadata = dbMetadatas.SingleOrDefault(v => v.Id == metadataId);
            if (dbMetadata != null)
                return new MetadataDto()
                {
                    Id = dbMetadata.Id,
                    ProcedureId = dbMetadata.ProcedureId,
                    Name = dbMetadata.Name,
                    Type = dbMetadata.Type,
                    FonctionParam = dbMetadata.FonctionParam,
                    Fonction = dbMetadata.Fonction,
                    Description = dbMetadata.Description
                };
            return null;
        }

        public async Task<MetadataDto> CreateAsync(MetadataInputDto metadata)
        {
            Metadata toAdd = new Metadata
            {
                ProcedureId = metadata.ProcedureId,
                Name = metadata.Name,
                Type = metadata.Type,
                FonctionParam = metadata.FonctionParam,
                Fonction = metadata.Fonction,
                Description = metadata.Description
            };
            _context.Metadata.Add(toAdd);
            await _context.SaveChangesAsync();
            return new MetadataDto
            {
                Id = toAdd.Id,
                ProcedureId = toAdd.ProcedureId,
                Name = toAdd.Name,
                Type = toAdd.Type,
                FonctionParam = toAdd.FonctionParam,
                Fonction = toAdd.Fonction,
                Description = toAdd.Description
            };

        }

        public async Task<MetadataDto> UpdateAsync(int metadataId, MetadataInputDto metadata)
        {
            Metadata[] dbMetadatas = await _context.Metadata.ToArrayAsync();
            Metadata dbMetadata = dbMetadatas.SingleOrDefault(v => v.Id == metadataId);
            // If the metadata does exist, update it
            if (dbMetadata != null)
            {
                dbMetadata.ProcedureId = metadata.ProcedureId;
                dbMetadata.Name = metadata.Name;
                dbMetadata.Type = metadata.Type;
                dbMetadata.FonctionParam = metadata.FonctionParam;
                dbMetadata.Fonction = metadata.Fonction;
                dbMetadata.Description = metadata.Description;
                await _context.SaveChangesAsync();
                return new MetadataDto
                {
                    Id = dbMetadata.Id,
                    ProcedureId = dbMetadata.ProcedureId,
                    Name = dbMetadata.Name,
                    Type = dbMetadata.Type,
                    FonctionParam = dbMetadata.FonctionParam,
                    Fonction = dbMetadata.Fonction,
                    Description = dbMetadata.Description
                };
            }
            return null;
        }

        public async Task<bool> RemoveAsync(int metadataId)
        {
            Metadata[] dbMetadatas = await _context.Metadata.ToArrayAsync();
            Metadata dbMetadata = dbMetadatas.SingleOrDefault(v => v.Id == metadataId);
            if (dbMetadata != null)
            {
                _context.Metadata.Remove(dbMetadata);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
