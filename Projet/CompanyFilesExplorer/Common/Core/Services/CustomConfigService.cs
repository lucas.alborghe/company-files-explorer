﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.DataAccess;
using Core.DTOs.CustomConfig;
using Core.Extensions;
using Core.Services.Interfaces;
using Data.Entities;
using Data.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Core.Services
{
    internal class CustomConfigService : ICustomConfigService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<ICustomConfigService> _logger;
        private readonly IDateTimeService _dateTimeService;

        private static readonly Expression<Func<CustomConfigValue, CustomConfigListDto>> _getListDto = item => new CustomConfigListDto
        {
                Key = item.Key,
                Value = item.Value
        };

        private static readonly Func<CustomConfigValue, CustomConfigListDto> _getListDtoFunc = _getListDto.Compile();

        /// <summary>
        ///     Initializes a new instance of the <see cref="CustomConfigService" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="dateTimeService">The date time service.</param>
        public CustomConfigService(ApplicationDbContext context, ILogger<ICustomConfigService> logger, IDateTimeService dateTimeService)
        {
            _context = context;
            _logger = logger;
            _dateTimeService = dateTimeService;
        }

        public async Task<CustomConfigDto> GetAsync()
        {
            CustomConfigValue[] values = await _context.CustomConfigs.ToArrayAsync();
            CustomConfigDto configuration = new CustomConfigDto();
            configuration.InitValues();
            configuration.LoadValues(values);
            return configuration;
        }

        public async Task<int> GetNumberAsync(CustomConfigKey key)
        {
            if(!key.IsInt())
            {
                throw new ArgumentException("The specified id is not a configuration having number");
            }

            return int.Parse(await GetValueAsync(key));
        }

        public async Task<string> GetValueAsync(CustomConfigKey key)
        {
            CustomConfigValue config = await _context.CustomConfigs.SingleOrDefaultAsync(k => k.Key == key);
            return config == null ? key.GetDefaultValue() : config.Value;
        }

        public async Task<IReadOnlyCollection<CustomConfigListDto>> GetValuesAsync()
        {
            return await _context.CustomConfigs.Select(_getListDto).ToArrayAsync();
        }

        public async Task<CustomConfigListDto[]> SaveAsync(CustomConfigInputDto configuration)
        {
            CustomConfigValue[] values = configuration.GetValues();
            CustomConfigValue[] dbValues = await _context.CustomConfigs.ToArrayAsync();
            foreach(CustomConfigValue value in values)
            {
                CustomConfigValue dbValue = dbValues.SingleOrDefault(v => v.Key == value.Key);
                if(dbValue == null)
                {
                    _context.CustomConfigs.Add(value);
                } else
                {
                    dbValue.Value = value.Value;
                }
            }

            await _context.SaveChangesAsync();
            return _logger.LogUpdated(dbValues.Select(_getListDtoFunc).ToArray());
        }

        public async Task<DateTime> GetAutoApiKeyDeactivationInactivityLimit()
        {
            int periodInDays = await GetNumberAsync(CustomConfigKey.AutoApiKeyDeactivationInactivityPeriod);
            DateTime now = _dateTimeService.UtcNow();
            return now.AddDays(periodInDays);
        }

        public async Task<DateTime> GetNextPasswordValidityPeriod()
        {
            int periodInDays = await GetNumberAsync(CustomConfigKey.PasswordValidityPeriod);
            DateTime now = _dateTimeService.UtcNow();
            return now.AddDays(periodInDays);
        }
    }
}