﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.DataAccess;
using Core.Dtos.Directory;
using Core.Dtos.Repository;
using Core.Extensions;
using Core.Services.Interfaces;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Core.Services
{
    public class DirectoryService : IDirectoryService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<IDirectoryService> _logger;
        private readonly IDateTimeService _dateTimeService;
        private readonly IRepositoryService _repositoryService;

        public static readonly Expression<Func<DirectoryDb, DirectoryListDto>> GetListDto = item => new DirectoryListDto
        {
            Path = item.Path,
            RepositoryId = item.RepositoryId,
            LastAccessDate = item.LastAccessDate
        };

        public DirectoryService(ApplicationDbContext context, ILogger<IDirectoryService> logger, IDateTimeService dateTimeService, IRepositoryService repositoryService)
        {
            _context = context;
            _logger = logger;
            _dateTimeService = dateTimeService;
            _repositoryService = repositoryService;
        }

        public async Task<IReadOnlyCollection<DirectoryListDto>> GetAllAsync()
        {
            return await _context.Directories.Select(GetListDto).ToArrayAsync();
        }

        public async Task<IReadOnlyCollection<DirectoryListDto>> GetAllByPathAsync(string path)
        {
            return await _context.Directories.Where(d => d.Path.ToLower().StartsWith(path.ToLower())).Select(GetListDto).ToArrayAsync();
        }

        public async Task<IReadOnlyCollection<DirectoryListDto>> GetAllByFilterAsync(string filter)
        {
            return await _context.Directories.Where(d => d.Path.ToLower().Contains(filter.ToLower())).Select(GetListDto).ToArrayAsync();
        }

        public async Task<RepositoryDto> GetRepositoryAsync(string path)
        {
            DirectoryDb[] dbDirectories = await _context.Directories.ToArrayAsync();
            DirectoryDb dbDirectory = dbDirectories.SingleOrDefault(v => v.Path == path);
            if (dbDirectory != null)
                return await _repositoryService.GetAsync(dbDirectory.RepositoryId);
            return null;
        }

        public async Task<DirectoryDto> GetAsync(string path)
        {
            DirectoryDb[] dbDirectories = await _context.Directories.ToArrayAsync();
            DirectoryDb dbDirectory = dbDirectories.SingleOrDefault(v => v.Path == path);
            if (dbDirectory != null)
                return new DirectoryDto()
                {
                    Path = dbDirectory.Path,
                    RepositoryId = dbDirectory.RepositoryId,
                    LastAccessDate = dbDirectory.LastAccessDate
                };
            return null;
        }

        public async Task<DirectoryDto> SaveAsync(DirectoryInputDto directory)
        {
            DirectoryDb[] dbDirectories = await _context.Directories.ToArrayAsync();
            DirectoryDb dbDirectory = dbDirectories.SingleOrDefault(v => v.Path == directory.Path);
            if (dbDirectory == null)
            {
                _context.Directories.Add(new DirectoryDb()
                {
                    Path = directory.Path,
                    RepositoryId = directory.RepositoryId,
                    LastAccessDate = _dateTimeService.UtcNow()
                });
            }
            else
            {
                dbDirectory.RepositoryId = directory.RepositoryId;
                dbDirectory.LastAccessDate = _dateTimeService.UtcNow();
            }
            await _context.SaveChangesAsync();
            return _logger.LogUpdated(new DirectoryDto()
            {
                Path = directory.Path,
                RepositoryId = directory.RepositoryId,
                LastAccessDate = _dateTimeService.UtcNow()
        });
        }

        public async Task<bool> RemoveAsync(string path)
        {
            DirectoryDb[] dbDirectories = await _context.Directories.ToArrayAsync();
            DirectoryDb dbDirectory = dbDirectories.SingleOrDefault(v => v.Path == path);
            if (dbDirectory != null)
            {
                _context.Directories.Remove(dbDirectory);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

    }
}
