﻿using Core.Services;
using Core.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Shared.Logging.Database;

namespace Core.DI
{
    public static class MvcServiceCollectionExtensions
    {
        /// <summary>
        ///     Adds the core components.
        /// </summary>
        /// <param name="services">The services.</param>
        public static void AddCoreComponents(this IServiceCollection services)
        {
            services.AddTransient<ICustomConfigService, CustomConfigService>();
            services.AddSingleton<IDateTimeService, DateTimeService>();
            services.AddTransient<ILogAccessLayer, LogAccessLayer>();
            services.AddTransient<ILogManager, LogAccessLayer>();

            services.AddTransient<IAllowedExtensionService, AllowedExtensionService>();
            services.AddTransient<IDatabaseManagerService, DatabaseManagerService>();
            services.AddTransient<IDirectoryService, DirectoryService>();
            services.AddTransient<IFileService, FileService>();
            services.AddTransient<IMetadataService, MetadataService>();
            services.AddTransient<IProcedureService, ProcedureService>();
            services.AddTransient<IRepositoryService, RepositoryService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IVirtualDirectoryService, VirtualDirectoryService>();
        }
    }
}