﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Dtos.Repository
{
    public class RepositoryDto
    {
        public int Id { get; set; }

        public string AbsolutePath { get; set; }

        public string Protocol { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
