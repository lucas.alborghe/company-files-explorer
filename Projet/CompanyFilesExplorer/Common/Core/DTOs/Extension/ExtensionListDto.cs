﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Dtos.Extension
{
    public class ExtensionListDto
    {
        public int Id { get; set; }

        public string Extension { get; set; }
    }
}
