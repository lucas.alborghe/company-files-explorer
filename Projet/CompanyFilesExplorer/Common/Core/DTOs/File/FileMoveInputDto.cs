﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Dtos.File
{
    public class FileMoveInputDto
    {
        public int SourceRepositoryId { get; set; }

        public string SourceFilepath { get; set; }

        public string SourceFilename { get; set; }

        public string SourceExtension { get; set; }

        public int DestinationRepositoryId { get; set; }

        public string DestinationFilepath { get; set; }

        public string DestinationFilename { get; set; }

        public string DestinationExtension { get; set; }
    }
}
