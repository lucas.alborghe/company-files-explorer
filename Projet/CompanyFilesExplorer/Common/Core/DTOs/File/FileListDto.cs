﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Dtos.File
{
    public class FileListDto
    {
        public int Id { get; set; }

        public int RepositoryId { get; set; }

        public string Filename { get; set; }

        public string Extension { get; set; }

        public string Filepath { get; set; }

        public string Hashcode { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }
    }
}
