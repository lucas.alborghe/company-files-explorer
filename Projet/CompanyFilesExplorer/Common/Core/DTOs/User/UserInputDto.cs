﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Dtos.User
{
    public class UserInputDto
    {
        public string Login { get; set; }

        public sbyte IsAdmin { get; set; }
    }
}
