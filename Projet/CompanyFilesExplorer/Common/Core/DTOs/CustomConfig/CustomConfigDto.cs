﻿using System.ComponentModel.DataAnnotations;
using Data.Localization;

namespace Core.DTOs.CustomConfig
{
    public class CustomConfigDto
    {
        /// <summary>
        ///     Gets or sets the AutoApiKeyDeactivationInactivityPeriodInDays.
        /// </summary>
        /// <value>
        ///     The AutoApiKeyDeactivationInactivityPeriodInDays.
        /// </value>
        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_AutoApiKeyDeactivationInactivityPeriod_Title", Order = 0)]
        [Range(1, 100, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        public int AutoApiKeyDeactivationInactivityPeriodInDays { get; set; }

        /// <summary>
        ///     Gets or sets the AutoLogoutInactivityPeriodInMinutes.
        /// </summary>
        /// <value>
        ///     The AutoLogoutInactivityPeriodInMinutes.
        /// </value>
        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_AutoLogoutInactivityPeriod_Title", Order = 1)]
        [Range(1, 100, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        public int AutoLogoutInactivityPeriodInMinutes { get; set; }

        /// <summary>
        ///     Gets or sets the CustomHomeMessage.
        /// </summary>
        /// <value>
        ///     The CustomHomeMessage.
        /// </value>
        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_CustomHomeMessage_Title", Order = 2)]
        [Required(AllowEmptyStrings = true, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Required")]
        [DataType(DataType.Text)]
        public string CustomHomeMessage { get; set; }

        /// <summary>
        ///     Gets or sets the CustomLoginMessage.
        /// </summary>
        /// <value>
        ///     The CustomLoginMessage.
        /// </value>
        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_CustomLoginMessage_Title", Order = 2)]
        [Required(AllowEmptyStrings = true, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Required")]
        [DataType(DataType.Text)]
        public string CustomLoginMessage { get; set; }

        /// <summary>
        ///     Gets or sets the MaxApiRequestsCountPerMinute.
        /// </summary>
        /// <value>
        ///     The MaxApiRequestsCountPerMinute.
        /// </value>
        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_MaxApiRequestsCount_Title", Order = 4)]
        [Range(1, 60000, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        public int MaxApiRequestsCountPerMinute { get; set; }

        /// <summary>
        ///     Gets or sets the MaxPLCRequestsCountPerMinute.
        /// </summary>
        /// <value>
        ///     The MaxPLCRequestsCountPerMinute.
        /// </value>
        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_MaxPLCRequestsCount_Title", Order = 5)]
        [Range(1, 6000, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        public int MaxPLCRequestsCountPerMinute { get; set; }

        /// <summary>
        ///     Gets or sets the PasswordValidatityPeriod.
        /// </summary>
        /// <value>
        ///     The PasswordValidatityPeriod.
        /// </value>
        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_PasswordValidityPeriod_Title", Order = 10)]
        [Range(1, 100, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        [DataType(DataType.Text)]
        public int PasswordValidityPeriodInDays { get; set; }

        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_PLCRequestTimeout_Title", Order = 6)]
        [Range(100, 60000, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        [DataType(DataType.Text)]
        public int PLCRequestTimeoutInMs { get; set; }

        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_PLCPingTimeout_Title", Order = 7)]
        [Range(20, 1000, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        [DataType(DataType.Text)]
        public int PLCPingTimeoutInMs { get; set; }

        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_LogsPageSize_Title", Order = 11)]
        [Range(10, 1000, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        [DataType(DataType.Text)]
        public int LogsPageSize { get; set; }

        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_MaxKeyGuessingAttemptsCount_Title", Order = 8)]
        [Range(1, 100, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        [DataType(DataType.Text)]
        public int MaxKeyGuessingAttemptsCount { get; set; }

        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_KeyGuessingBanDuration_Title", Order = 9)]
        [Range(1, 60, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        [DataType(DataType.Text)]
        public int KeyGuessingBanDurationInMinutes { get; set; }
    }
}