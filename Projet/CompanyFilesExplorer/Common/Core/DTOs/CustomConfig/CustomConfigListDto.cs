﻿using System.ComponentModel.DataAnnotations;
using Data.Entities;
using Data.Enums;
using Data.Localization;

namespace Core.DTOs.CustomConfig
{
    public class CustomConfigListDto
    {
        /// <summary>
        ///     Gets or sets the Key.
        /// </summary>
        /// <value>
        ///     The Key.
        /// </value>
        [Display(ResourceType = typeof(Entities), Name = "CustomConfigItem_Key_Title", Order = 0)]
        public CustomConfigKey Key { get; set; }

        /// <summary>
        ///     Gets or sets the Value.
        /// </summary>
        /// <value>
        ///     The Value.
        /// </value>
        [Display(ResourceType = typeof(Entities), Name = "CustomConfigItem_Value_Title", Order = 1)]
        [Required(AllowEmptyStrings = true, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Required")]
        [DataType(DataType.Text)]
        public string Value { get; set; }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CustomConfigListDto" /> class.
        /// </summary>
        public CustomConfigListDto()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CustomConfigListDto" /> class.
        /// </summary>
        /// <param name="e">The e.</param>
        public CustomConfigListDto(CustomConfigValue e)
        {
            Key = e.Key;
            Value = e.Value;
        }

        /// <summary>
        ///     To the entity.
        /// </summary>
        /// <returns>Data.Entities.CustomConfig.</returns>
        public CustomConfigValue ToEntity()
        {
            return new CustomConfigValue
            {
                    Key = Key,
                    Value = Value
            };
        }
    }
}