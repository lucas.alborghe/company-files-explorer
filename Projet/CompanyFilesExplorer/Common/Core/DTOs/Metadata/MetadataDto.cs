﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Dtos.Metadata
{
    public class MetadataDto
    {
        public int Id { get; set; }

        public int ProcedureId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Fonction { get; set; }

        public string FonctionParam { get; set; }

        public string Description { get; set; }
    }
}
