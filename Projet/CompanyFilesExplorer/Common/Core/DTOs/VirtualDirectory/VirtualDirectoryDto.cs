﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Dtos.VirtualDirectory
{
    public class VirtualDirectoryDto
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int? VirtualDirectoryId { get; set; }

        public int? ProcedureId { get; set; }

        public string Name { get; set; }

        public sbyte IsActive { get; set; }
    }
}
