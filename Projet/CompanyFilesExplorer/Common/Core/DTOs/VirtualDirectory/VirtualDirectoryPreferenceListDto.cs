﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Dtos.VirtualDirectory
{
    public class VirtualDirectoryPreferenceListDto
    {
        public int VirtualDirectoryId { get; set; }

        public int FileId { get; set; }

        public string Filename { get; set; }
    }
}
