﻿using System;
using System.Linq;
using System.Text;
using Core.Extensions;
using Data.Entities;

namespace Core.DTOs.Log
{
    public class DbLog
    {
        public string Level { get; set; }

        public string Template { get; set; }

        public string Exception { get; set; }

        public DateTime Timestamp { get; set; }

        public string Properties { get; set; }

        private string GetMessage(bool displayException)
        {
            string messageTemplate = Template;
            DbLogProperties logProperties = Properties.DeserializeJson<DbLogProperties>();
            StringBuilder messageBuilder = new StringBuilder();
            if(!logProperties.SpecificValues.Any() && !logProperties.StringFormatArguments.Any())
            {
                messageBuilder.Append(messageTemplate);
            } else
            {
                string format = messageTemplate;
                if(logProperties.SpecificValues.Any())
                {
                    StringBuilder formatBuilder = new StringBuilder(messageTemplate);
                    for(int i = 0; i < logProperties.SpecificKeys.Length; i++)
                    {
                        string specificKey = logProperties.SpecificKeys[i];
                        string specificValue = logProperties.SpecificValues[i];
                        formatBuilder.Replace("@" + specificKey, specificValue);
                    }

                    format = formatBuilder.ToString();
                }

                if(logProperties.StringFormatArguments.Any())
                {
                    messageBuilder.AppendFormat(format, logProperties.StringFormatArguments.ToArray<object>());
                } else
                {
                    messageBuilder.Append(format);
                }
            }

            if(displayException)
            {
                if(!string.IsNullOrWhiteSpace(Exception))
                {
                    messageBuilder.AppendLine();
                    messageBuilder.Append(Exception);
                }
            }

            return messageBuilder.ToString();
        }

        public StdLog Create(bool displayException)
        {
            return new StdLog
            {
                    RenderedMessage = GetMessage(displayException),
                    Level = Level,
                    Properties = Properties,
                    TimeStamp = Timestamp
            };
        }
    }
}