using System;
using System.Linq;
using Newtonsoft.Json;
using Serilog.Events;

namespace Core.DTOs.Log
{
    public class DbLogProperties
    {
        [JsonProperty("R")]
        public string RequestId { get; set; }

        [JsonProperty("A")]
        public string[] StringFormatArguments { get; set; }

        [JsonProperty("N")]
        public string[] SpecificValues { get; set; }

        [JsonProperty("K")]
        public string[] SpecificKeys { get; set; }

        private static readonly string RequestIdKey = "RequestId";

        private static readonly string[] ExcludedKeys =
        {
                "SourceContext",
                "ActionId",
                "ActionName",
                "RequestPath",
                RequestIdKey
        };

        public static DbLogProperties GetProperties(LogEvent logEvent)
        {
            var extractedProperties = logEvent.Properties.Where(p => !ExcludedKeys.Contains(p.Key, StringComparer.InvariantCultureIgnoreCase)).Select(p => new
            {
                    p.Key,
                    Parseable = int.TryParse(p.Key, out int r),
                    Index = r,
                    StringValue = p.Value is ScalarValue scalar ? scalar.Value.ToString() : p.Value.ToString()
            }).ToArray();
            return new DbLogProperties
            {
                    RequestId = logEvent.Properties.TryGetValue(RequestIdKey, out LogEventPropertyValue value) ? value.ToString() : null,
                    StringFormatArguments = extractedProperties.Where(p => p.Parseable).OrderBy(e => e.Index).Select(s => s.StringValue).ToArray(),
                    SpecificValues = extractedProperties.Where(p => !p.Parseable).Select(s => s.StringValue).ToArray(),
                    SpecificKeys = extractedProperties.Where(p => !p.Parseable).Select(s => s.Key).ToArray()
            };
        }
    }
}