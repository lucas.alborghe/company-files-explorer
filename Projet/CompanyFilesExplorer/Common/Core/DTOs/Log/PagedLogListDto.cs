﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Data.Localization;

namespace Core.DTOs.Log
{
    public class PagedLogListDto
    {
        /// <summary>
        ///     Gets or sets the logs.
        /// </summary>
        /// <value>
        ///     The logs.
        /// </value>
        [Display(ResourceType = typeof(Views), Name = "Logs_Title", Order = 1)]
        public IReadOnlyCollection<LogListDto> Logs { get; set; }

        /// <summary>
        ///     Gets or sets the total count.
        /// </summary>
        /// <value>
        ///     The total count.
        /// </value>
        [Display(ResourceType = typeof(Views), Name = "Logs_Count_Title", Order = 2)]
        public int TotalCount { get; set; }

        [Display(AutoGenerateField = false)]
        [Range(0, int.MaxValue, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        public int PageIndex { get; set; }

        /// <summary>
        ///     Gets or sets the size of the page.
        /// </summary>
        /// <value>
        ///     The size of the page.
        /// </value>
        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_LogsPageSize_Title", Order = 3)]
        [Range(10, 1000, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        public int PageSize { get; set; }

        [Display(AutoGenerateField = false)]
        public int PagesCount { get; set; }
    }
}