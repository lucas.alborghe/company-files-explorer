﻿using System.ComponentModel.DataAnnotations;
using Data.Localization;

namespace Core.DTOs.Log
{
    public class PagedLogFilterDto : LogFilterDto
    {
        public PagedLogFilterDto()
        {
        }

        public PagedLogFilterDto(PagedLogFilterDto logFilterDto)
        {
            From = logFilterDto.From;
            To = logFilterDto.To;
            IsExceptionOn = logFilterDto.IsExceptionOn;
            Level = logFilterDto.Level;
            PageSize = (logFilterDto.PageIndex + 1) * logFilterDto.PageSize;
            PageIndex = 0;
        }

        [Display(AutoGenerateField = false)]
        [Range(0, int.MaxValue, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        public int PageIndex { get; set; }

        /// <summary>
        ///     Gets or sets the size of the page.
        /// </summary>
        /// <value>
        ///     The size of the page.
        /// </value>
        [Display(ResourceType = typeof(Enums), Name = "CustomConfigKey_LogsPageSize_Title", Order = 9)]
        [Range(10, 1000, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Range")]
        public int PageSize { get; set; }
    }
}