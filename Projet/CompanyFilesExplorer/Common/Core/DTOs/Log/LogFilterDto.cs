﻿using System;
using System.ComponentModel.DataAnnotations;
using Core.Validation;
using Core.Validation.Attributes;
using Data.Localization;

namespace Core.DTOs.Log
{
    public class LogFilterDto
    {
        /// <summary>
        ///     Gets or sets from.
        /// </summary>
        /// <value>
        ///     From.
        /// </value>
        [Display(ResourceType = typeof(Entities), Name = "Log_From_Title", Order = 1)]
        [DataType(DataType.DateTime)]
        public DateTime From { get; set; }

        /// <summary>
        ///     Gets or sets to.
        /// </summary>
        /// <value>
        ///     To.
        /// </value>
        [Display(ResourceType = typeof(Entities), Name = "Log_To_Title", Order = 2)]
        [DataType(DataType.DateTime)]
        [DateGreaterThan("From", ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_DateGreater")]
        public DateTime To { get; set; }

        /// <summary>
        ///     Gets or sets the level.
        /// </summary>
        /// <value>
        ///     The level.
        /// </value>
        [Display(ResourceType = typeof(Entities), Name = "Log_Level_Title", Order = 3)]
        public DtoValidationResultLevel Level { get; set; }

        [Display(AutoGenerateField = false)]
        public bool IsExceptionOn { get; set; }
    }
}