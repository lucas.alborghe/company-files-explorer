﻿using System;
using System.ComponentModel.DataAnnotations;
using Core.Validation;
using Data.Localization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Core.DTOs.Log
{
    public class LogListDto
    {
        /// <summary>
        ///     Gets or sets the source.
        /// </summary>
        /// <value>
        ///     The source.
        /// </value>
        [Display(ResourceType = typeof(Entities), Name = "Log_Date_Title", Order = 1)]
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

        /// <summary>
        ///     Gets or sets the level.
        /// </summary>
        /// <value>
        ///     The level.
        /// </value>
        [Display(ResourceType = typeof(Entities), Name = "Log_Level_Title", Order = 2)]
        [JsonConverter(typeof(StringEnumConverter))]
        public DtoValidationResultLevel Level { get; set; }

        /// <summary>
        ///     Gets or sets the message.
        /// </summary>
        /// <value>
        ///     The message.
        /// </value>
        [Display(ResourceType = typeof(Entities), Name = "Log_Message_Title", Order = 3)]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Required")]
        public string Message { get; set; }
    }
}