﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Dtos.Directory
{
    public class DirectoryDto
    {
        public string Path { get; set; }

        public int RepositoryId { get; set; }

        public DateTime? LastAccessDate { get; set; }
    }
}
