﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Dtos.Directory
{
    public class DirectoryInputDto
    {
        public string Path { get; set; }

        public int RepositoryId { get; set; }
    }
}
