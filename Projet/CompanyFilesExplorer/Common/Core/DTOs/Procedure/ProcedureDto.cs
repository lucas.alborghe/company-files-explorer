﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Dtos.Procedure
{
    public class ProcedureDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Rule { get; set; }

        public string Description { get; set; }
    }
}
