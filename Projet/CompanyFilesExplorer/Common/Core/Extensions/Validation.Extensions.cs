﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;

namespace Core.Extensions
{
    public static class ValidationExtensions
    {
        /// <summary>
        ///     Gets the invalid properties using its validation attributes.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="validationContext">The validation context.</param>
        /// <returns></returns>
        public static IEnumerable<PropertyInfo> GetInvalidProperties([NotNull] this object obj, [CanBeNull] ValidationContext validationContext = null)
        {
            PropertyInfo[] properties = obj.GetType().GetProperties();
            return obj.GetInvalidProperties(properties, validationContext);
        }

        /// <summary>
        ///     Gets the invalid properties using its validation attributes.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="properties">The properties.</param>
        /// <param name="validationContext">The validation context.</param>
        /// <returns></returns>
        public static IEnumerable<PropertyInfo> GetInvalidProperties([NotNull] this object obj, [NotNull] PropertyInfo[] properties, [CanBeNull] ValidationContext validationContext = null)
        {
            return properties.Where(p => !obj.IsValid(p, validationContext));
        }

        /// <summary>
        ///     Gets the property value of the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="property">The property.</param>
        /// <returns></returns>
        public static object GetPropertyValue([NotNull] this object obj, [NotNull] PropertyInfo property)
        {
            return property.GetValue(obj, BindingFlags.GetProperty, null, null, null);
        }

        /// <summary>
        ///     Returns true if all the validation attributes of the property are valid.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="property">The property.</param>
        /// <param name="validationContext">The validation context.</param>
        /// <returns>
        ///     <c>true</c> if the specified object is valid; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValid([NotNull] this object obj, [NotNull] PropertyInfo property, [CanBeNull] ValidationContext validationContext = null)
        {
            return !obj.GetInvalidAttributes(property, validationContext).Any();
        }

        /// <summary>
        ///     Gets the invalid attributes.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="property">The property.</param>
        /// <param name="validationContext">The validation context.</param>
        /// <returns></returns>
        public static IEnumerable<ValidationAttribute> GetInvalidAttributes([NotNull] this object obj, [NotNull] PropertyInfo property, [CanBeNull] ValidationContext validationContext = null)
        {
            IEnumerable<ValidationAttribute> validationAttributes = property.GetCustomAttributes<ValidationAttribute>(true);
            object value = obj.GetPropertyValue(property);
            return validationAttributes.Where(v => v.GetValidationResult(value, validationContext) != null);
        }

        /// <summary>
        ///     Gets the invalid attributes.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="property">The property.</param>
        /// <param name="validationContext">The validation context.</param>
        /// <returns></returns>
        public static IEnumerable<ValidationResult> GetInvalidAttributesErrors([NotNull] this object obj, [NotNull] PropertyInfo property, [CanBeNull] ValidationContext validationContext = null)
        {
            IEnumerable<ValidationAttribute> validationAttributes = property.GetCustomAttributes<ValidationAttribute>(true);
            object value = obj.GetPropertyValue(property);
            return validationAttributes.Select(v => v.GetValidationResult(value, validationContext)).NotNull();
        }
    }
}