﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Core.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with the ability to use extensions method for
    ///     <see cref="IEnumerable{T}" />.
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        ///     Returns the enumerable filtering the null items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        [ItemNotNull]
        public static IEnumerable<T> NotNull<T>([ItemCanBeNull] this IEnumerable<T> items)
        {
            return items.Where(item => item != null);
        }

        /// <summary>
        ///     Loops the specified loopCount and call getSelector at each time.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="loopCount">The loop count.</param>
        /// <param name="getSelector">The get selector.</param>
        /// <returns></returns>
        public static IEnumerable<T> ForEach<T>(this int loopCount, Func<int, T> getSelector)
        {
            for(int i = 0; i < loopCount; i++)
            {
                yield return getSelector(i);
            }
        }

        /// <summary>
        ///     Performs the specified action on each element of the <see cref="IEnumerable{T}">items</see>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">The items.</param>
        /// <param name="action">The action.</param>
        public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            foreach(T item in items)
            {
                action(item);
            }
        }

        /// <summary>
        ///     Performs the specified action on each element of the <see cref="IEnumerable{T}">items</see>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">The items.</param>
        /// <param name="action">The action.</param>
        public static void ForEach<T>(this IEnumerable<T> items, Action<T, int> action)
        {
            int i = 0;
            foreach(T item in items)
            {
                action(item, i);
                i++;
            }
        }

        /// <summary>
        ///     Returns the enumerable sort by the element itself.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        [ItemNotNull]
        public static IEnumerable<T> Ordered<T>([ItemNotNull] this IEnumerable<T> items)
        {
            return items.OrderBy(item => item);
        }

        /// <summary>
        ///     Chunks the source by the specified chunk size.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="chunkSize">Size of the chunk.</param>
        /// <returns></returns>
        public static IEnumerable<T[]> ChunkBy<T>(this IEnumerable<T> source, int chunkSize)
        {
            return source.Select((x, i) => new
            {
                    Index = i,
                    Value = x
            }).GroupBy(x => x.Index / chunkSize).Select(x => x.Select(v => v.Value).ToArray());
        }
    }
}