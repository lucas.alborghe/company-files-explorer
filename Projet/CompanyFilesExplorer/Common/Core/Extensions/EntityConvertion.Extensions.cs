﻿using System.Collections.Generic;
using System.Linq;
using Core.DTOs.Log;
using Data.Entities;

namespace Core.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with the ability to apply Entity/DTO and InputDTO/Entity convertion.
    /// </summary>
    public static class EntityConvertionExtensions
    {
        /// <summary>
        ///     Converts the <see cref="StdLog">instances</see> To the
        ///     <see cref="LogListDto">DTO instances</see>.
        /// </summary>
        /// <param name="logs">The logs.</param>
        /// <returns></returns>
        public static LogListDto[] ToDtos(this IEnumerable<StdLog> logs)
        {
            return logs.Select(log => new LogListDto
            {
                    Date = log.TimeStamp,
                    Level = LogLevelExtensions.Parse(log.Level),
                    Message = log.RenderedMessage
            }).ToArray();
        }
    }
}