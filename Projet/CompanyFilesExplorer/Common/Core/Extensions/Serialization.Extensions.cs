﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace Core.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with <see cref="byte">array</see> extension
    ///     methods.
    /// </summary>
    public static class SerializationExtensions
    {
        /// <summary>
        ///     Serializes this instance to binary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <returns>System.Byte[].</returns>
        public static byte[] SerializeToBinary<T>(this T item)
        {
            using(MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, item);
                ms.Position = 0L;
                return ms.ToArray();
            }
        }

        /// <summary>
        ///     Deserializes this instance to the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializedData">The serialized data.</param>
        /// <returns>T instance.</returns>
        [CanBeNull]
        public static T DeserializeBinary<T>(this byte[] serializedData)
                where T : class
        {
            using(MemoryStream memStream = new MemoryStream())
            {
                BinaryFormatter binForm = new BinaryFormatter();
                memStream.Write(serializedData, 0, serializedData.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                object obj = binForm.Deserialize(memStream);
                return obj as T;
            }
        }

        /// <summary>
        ///     Deserializes this instance to the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonContent">Content of the json.</param>
        /// <returns>T instance.</returns>
        public static T DeserializeJson<T>(this string jsonContent)
                where T : class
        {
            return JsonConvert.DeserializeObject<T>(jsonContent);
        }

        /// <summary>
        ///     Deserializes this instance to the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream">The stream.</param>
        /// <returns>T instance.</returns>
        public static T DeserializeJson<T>(this Stream stream)
        {
            JsonSerializer serializer = new JsonSerializer();

            using(StreamReader sr = new StreamReader(stream))
            {
                using(JsonTextReader jsonTextReader = new JsonTextReader(sr))
                {
                    return serializer.Deserialize<T>(jsonTextReader);
                }
            }
        }

        /// <summary>
        ///     Serializes the current instance to json.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance">The instance.</param>
        /// <returns>System.String.</returns>
        public static string SerializeToJson<T>(this T instance)
        {
            return JsonConvert.SerializeObject(instance);
        }
    }
}