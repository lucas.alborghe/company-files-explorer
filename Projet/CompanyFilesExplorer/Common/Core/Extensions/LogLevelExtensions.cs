﻿using System;
using Core.Validation;
using Data.Extensions;

namespace Core.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with the ability to use extensions method for
    ///     <see cref="DtoValidationResultLevel" />.
    /// </summary>
    public static class LogLevelExtensions
    {
        private const string TraceEquivalent = "Debug";
        private const string CriticalEquivalent = "Fatal";
        private const string DebugEquivalent = "Verbose";

        /// <summary>
        ///     Parses the specified serilog level.
        /// </summary>
        /// <param name="serilogLevel">The serilog level.</param>
        /// <returns></returns>
        public static DtoValidationResultLevel Parse(string serilogLevel)
        {
            if(serilogLevel.Equals(CriticalEquivalent))
            {
                return DtoValidationResultLevel.Critical;
            }

            if(serilogLevel.Equals(DebugEquivalent))
            {
                return DtoValidationResultLevel.Debug;
            }

            return EnumExtensions.Parse<DtoValidationResultLevel>(serilogLevel);
        }

        /// <summary>
        ///     Converts to the serilog levels.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <returns></returns>
        public static string[] ToSerilogLevels(this DtoValidationResultLevel level)
        {
            switch(level)
            {
                case DtoValidationResultLevel.Trace:
                    return new[]
                    {
                            TraceEquivalent
                    };
                case DtoValidationResultLevel.Debug:
                    return new[]
                    {
                            level.ToString(),
                            DebugEquivalent
                    };
                case DtoValidationResultLevel.Information:
                case DtoValidationResultLevel.Warning:
                case DtoValidationResultLevel.Error:
                    return new[]
                    {
                            level.ToString()
                    };
                case DtoValidationResultLevel.Critical:
                    return new[]
                    {
                            level.ToString(),
                            CriticalEquivalent
                    };
                case DtoValidationResultLevel.None:
                    return new string[0];
                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }
    }
}