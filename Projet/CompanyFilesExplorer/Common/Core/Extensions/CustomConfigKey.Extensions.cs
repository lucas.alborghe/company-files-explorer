﻿using System.Linq;
using Data.Enums;
using Data.Extensions;

namespace Core.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with the ability to use extensions method for
    ///     <see cref="CustomConfigKey" />.
    /// </summary>
    public static class CustomConfigKeyExtensions
    {
        /// <summary>
        ///     Determines whether the specified key is editable.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns><c>true</c> if the specified key is editable; otherwise, <c>false</c>.</returns>
        public static bool IsEditable(this CustomConfigKey key)
        {
            return true;
        }

        /// <summary>
        ///     Determines whether the specified key is int.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns><c>true</c> if the specified key is int; otherwise, <c>false</c>.</returns>
        public static bool IsInt(this CustomConfigKey key)
        {
            return key != CustomConfigKey.CustomHomeMessage && key != CustomConfigKey.CustomLoginMessage;
        }

        /// <summary>
        ///     Tries to convert to <see cref="CustomConfigKey" />.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>nullable CustomConfigKey</returns>
        public static CustomConfigKey? TryGetCustomConfigKey(this int value)
        {
            return EnumExtensions.GetValues<CustomConfigKey>().Where(c => (int)c == value).Select(e => (CustomConfigKey?)e).FirstOrDefault();
        }
    }
}