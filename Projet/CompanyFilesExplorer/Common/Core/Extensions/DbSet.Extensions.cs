﻿using Microsoft.EntityFrameworkCore;

namespace Core.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with the ability to use extensions method for
    ///     <see cref="DbSet{TEntity}" />.
    /// </summary>
    public static class DbSetExtensions
    {
        /// <summary>
        ///     Truncates the table having the specified name.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="tableName">Name of the table.</param>
        public static void Clear(this DbContext context, string tableName)
        {
            context.Database.ExecuteSqlCommand(string.Format("DELETE FROM [{0}]", tableName));
        }

        /// <summary>
        ///     Sets the column.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="value">The value.</param>
        public static void SetColumn(this DbContext context, string tableName, string columnName, string value)
        {
            context.Database.ExecuteSqlCommand(string.Format("UPDATE [{0}] SET {1} = {2}", tableName, columnName, value));
        }
    }
}