﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with the ability to use extensions method for
    ///     <see cref="KeyValuePair{TKey,TValue}" />.
    /// </summary>
    public static class KeyValuePairExtensions
    {
        /// <summary>
        ///     Returns the Key property of all the specified pair.
        /// </summary>
        /// <typeparam name="TKey">The type of the t key.</typeparam>
        /// <typeparam name="TValue">The type of the t value.</typeparam>
        /// <param name="items">The items.</param>
        /// <returns>IEnumerable&lt;TKey&gt;.</returns>
        public static IEnumerable<TKey> Keys<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> items)
        {
            return items.Select(i => i.Key);
        }

        /// <summary>
        ///     Returns the Value property of all the specified pair.
        /// </summary>
        /// <typeparam name="TKey">The type of the t key.</typeparam>
        /// <typeparam name="TValue">The type of the t value.</typeparam>
        /// <param name="items">The items.</param>
        /// <returns>IEnumerable&lt;TValue&gt;.</returns>
        public static IEnumerable<TValue> Values<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> items)
        {
            return items.Select(i => i.Value);
        }

        /// <summary>
        ///     Selects the pair.
        /// </summary>
        /// <typeparam name="TElt"> The type of the elt. </typeparam>
        /// <typeparam name="TKey"> The type of the key. </typeparam>
        /// <typeparam name="TValue"> The type of the value. </typeparam>
        /// <param name="items"> The items. </param>
        /// <param name="keySelector"> The key selector. </param>
        /// <param name="valueSelector"> The value selector. </param>
        /// <returns> </returns>
        public static IEnumerable<KeyValuePair<TKey, TValue>> SelectPair<TElt, TKey, TValue>(this IEnumerable<TElt> items, Func<TElt, TKey> keySelector, Func<TElt, TValue> valueSelector)
        {
            return items.Select(e => new KeyValuePair<TKey, TValue>(keySelector(e), valueSelector(e)));
        }

        /// <summary>
        ///     Selects the pair.
        /// </summary>
        /// <typeparam name="TElt"> The type of the elt. </typeparam>
        /// <typeparam name="TKey"> The type of the key. </typeparam>
        /// <typeparam name="TValue"> The type of the value. </typeparam>
        /// <param name="items"> The items. </param>
        /// <param name="keySelector"> The key selector. </param>
        /// <param name="valueSelector"> The value selector. </param>
        /// <returns> </returns>
        public static IEnumerable<KeyValuePair<TKey, TValue>> SelectPair<TElt, TKey, TValue>(this IEnumerable<TElt> items, Func<TElt, int, TKey> keySelector, Func<TElt, int, TValue> valueSelector)
        {
            return items.Select((e, i) => new KeyValuePair<TKey, TValue>(keySelector(e, i), valueSelector(e, i)));
        }

        /// <summary>
        ///     Creates a key value pair array from a System.Collections.Generic.IEnumerable{TElt}.
        /// </summary>
        /// <typeparam name="TKey"> The type of the key. </typeparam>
        /// <typeparam name="TValue"> The type of the value. </typeparam>
        /// <param name="items"> The items. </param>
        /// <param name="valueSelector"> The value selector. </param>
        /// <returns> </returns>
        public static KeyValuePair<TKey, TValue>[] ToPairArray<TKey, TValue>(this IEnumerable<TKey> items, Func<TKey, TValue> valueSelector)
        {
            return items.ToPairArray(e => e, valueSelector);
        }

        /// <summary>
        ///     Creates a key value pair array from a System.Collections.Generic.IEnumerable{TElt}.
        /// </summary>
        /// <typeparam name="TKey"> The type of the key. </typeparam>
        /// <typeparam name="TValue"> The type of the value. </typeparam>
        /// <param name="items"> The items. </param>
        /// <param name="valueSelector"> The value selector. </param>
        /// <returns> </returns>
        public static KeyValuePair<TKey, TValue>[] ToPairArray<TKey, TValue>(this IEnumerable<TKey> items, Func<TKey, int, TValue> valueSelector)
        {
            return items.ToPairArray((e, i) => e, valueSelector);
        }

        /// <summary>
        ///     Creates a key value pair array from a System.Collections.Generic.IEnumerable{TElt}.
        /// </summary>
        /// <typeparam name="TElt"> The type of the elt. </typeparam>
        /// <typeparam name="TKey"> The type of the key. </typeparam>
        /// <typeparam name="TValue"> The type of the value. </typeparam>
        /// <param name="items"> The items. </param>
        /// <param name="keySelector"> The key selector. </param>
        /// <param name="valueSelector"> The value selector. </param>
        /// <returns> </returns>
        public static KeyValuePair<TKey, TValue>[] ToPairArray<TElt, TKey, TValue>(this IEnumerable<TElt> items, Func<TElt, TKey> keySelector, Func<TElt, TValue> valueSelector)
        {
            return items.SelectPair(keySelector, valueSelector).ToArray();
        }

        /// <summary>
        ///     Creates a key value pair array from a System.Collections.Generic.IEnumerable{TElt}.
        /// </summary>
        /// <typeparam name="TElt"> The type of the elt. </typeparam>
        /// <typeparam name="TKey"> The type of the key. </typeparam>
        /// <typeparam name="TValue"> The type of the value. </typeparam>
        /// <param name="items"> The items. </param>
        /// <param name="keySelector"> The key selector. </param>
        /// <param name="valueSelector"> The value selector. </param>
        /// <returns> </returns>
        public static KeyValuePair<TKey, TValue>[] ToPairArray<TElt, TKey, TValue>(this IEnumerable<TElt> items, Func<TElt, int, TKey> keySelector, Func<TElt, int, TValue> valueSelector)
        {
            return items.SelectPair(keySelector, valueSelector).ToArray();
        }

        /// <summary>
        ///     Creates a dictionary from a System.Collections.Generic.IEnumerable{TElt} key value pair array.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> items)
        {
            return items.ToDictionary(e => e.Key, e => e.Value);
        }

        /// <summary>
        ///     Adds the pair.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="items">The items.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public static void AddPair<TKey, TValue>(this IList<KeyValuePair<TKey, TValue>> items, TKey key, TValue value)
        {
            items.Add(new KeyValuePair<TKey, TValue>(key, value));
        }
    }
}