﻿using System.Collections.Generic;

namespace Core.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with the ability to use extensions method for
    ///     <see cref="ICollection{T}" />.
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        ///     Removes the specified items from the collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="itemsToRemove">The items to remove.</param>
        public static void RemoveRange<T>(this ICollection<T> collection, IEnumerable<T> itemsToRemove)
        {
            foreach(T itemToRemove in itemsToRemove)
            {
                collection.Remove(itemToRemove);
            }
        }

        /// <summary>
        ///     Adds the specified items from the collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="itemsToAdd">The items to add.</param>
        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> itemsToAdd)
        {
            foreach(T itemToAdd in itemsToAdd)
            {
                collection.Add(itemToAdd);
            }
        }
    }
}