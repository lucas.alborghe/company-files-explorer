﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Data.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Core.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with the ability to use extensions method for
    ///     <see cref="IQueryable" />.
    /// </summary>
    public static class QueryableExtensions
    {
        /// <summary>
        ///     Returns the only element of a sequence, or throw NotFound exception if the sequence is empty; this method throws an
        ///     exception if there is more than one element in the sequence.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <returns>T.</returns>
        public static T SingleOrNotFound<T>(this IQueryable<T> query)
        {
            T item = query.SingleOrDefault();
            AssertFound(item);
            return item;
        }

        /// <summary>
        ///     Returns the only element of a sequence, or throw NotFound exception if the sequence is empty; this method throws an
        ///     exception if there is more than one element in the sequence.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns>T.</returns>
        public static T SingleOrNotFound<T>(this IQueryable<T> query, Expression<Func<T, bool>> predicate)
        {
            T item = query.SingleOrDefault(predicate);
            AssertFound(item);
            return item;
        }

        /// <summary>
        ///     Returns the only element of a sequence, or throw NotFound exception if the sequence is empty; this method throws an
        ///     exception if there is more than one element in the sequence.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <returns>T.</returns>
        public static async Task<T> SingleOrNotFoundAsync<T>(this IQueryable<T> query)
        {
            T item = await query.SingleOrDefaultAsync();
            AssertFound(item);
            return item;
        }

        /// <summary>
        ///     Returns the only element of a sequence, or throw NotFound exception if the sequence is empty; this method throws an
        ///     exception if there is more than one element in the sequence.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns>T.</returns>
        public static async Task<T> SingleOrNotFoundAsync<T>(this IQueryable<T> query, Expression<Func<T, bool>> predicate)
        {
            T item = await query.SingleOrDefaultAsync(predicate);
            AssertFound(item);
            return item;
        }

        // ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Local
        private static void AssertFound<T>(T item)
        {
            if(item == null)
            {
                throw EntityNotFoundException.Create<T>(@"The entity can't be found");
            }
        }
    }
}