﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Core.Exceptions;
using Core.Validation;
using Data.Localization;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;

namespace Core.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with the ability to use extensions method for
    ///     <see cref="DtoValidationResult" />, <see cref="DtoValidationResultLevel" /> and
    ///     <see cref="DtoValidationException" />.
    /// </summary>
    public static class DtoValidationExtensions
    {
        /// <summary>
        ///     Adds the errors to the ModelStateDictionary instance.
        /// </summary>
        /// <param name="modelState">State of the model.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void AddErrors(this ModelStateDictionary modelState, params ValidationResult[] validationResults)
        {
            foreach(ValidationResult error in validationResults.OfTypeError())
            {
                modelState.AddModelError(error.MemberNames.FirstOrDefault() ?? "Entity", error.ErrorMessage);
            }
        }

        /// <summary>
        ///     Adds the errors to the ModelStateDictionary instance.
        /// </summary>
        /// <param name="modelState">State of the model.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void AddErrors(this ModelStateDictionary modelState, DtoValidationResults validationResults)
        {
            foreach(ValidationResult error in validationResults.OfTypeError())
            {
                modelState.AddModelError(error.MemberNames.FirstOrDefault() ?? "Entity", error.ErrorMessage);
            }
        }

        /// <summary>
        ///     Logs the errors to the logger instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="logger">The logger.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void LogErrors<T>(this ILogger<T> logger, DtoValidationResults validationResults)
        {
            foreach(ValidationResult error in validationResults.OfTypeError())
            {
                logger.Log(error);
            }
        }

        /// <summary>
        ///     Logs the messages to the logger instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="logger">The logger.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void LogRange<T>(this ILogger<T> logger, DtoValidationResults validationResults)
        {
            foreach(ValidationResult error in validationResults)
            {
                logger.Log(error);
            }
        }

        /// <summary>
        ///     Logs the message to the logger instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="logger">The logger.</param>
        /// <param name="message">The message.</param>
        /// <param name="validationResults">The validation results.</param>
        /// <param name="memberNames">The member names.</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static void Log<T>(this ILogger<T> logger, string message, DtoValidationResults validationResults, params string[] memberNames)
        {
            DtoValidationResultLevel level = validationResults.OfType<DtoValidationResult>().Any(e => e.Level == DtoValidationResultLevel.Critical) ? DtoValidationResultLevel.Critical
                    : (validationResults.HasError() ? DtoValidationResultLevel.Error : DtoValidationResultLevel.Information);
            string joinedMessage = validationResults.ToMessage(message);
            logger.Log(new DtoValidationResult(joinedMessage, level, memberNames));
        }

        /// <summary>
        ///     Adds the message to the logger instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="logger">The logger.</param>
        /// <param name="validationResult">The validation result.</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static void Log<T>(this ILogger<T> logger, ValidationResult validationResult)
        {
            switch(validationResult is DtoValidationResult dtoValidationResult ? dtoValidationResult.Level : DtoValidationResultLevel.Error)
            {
                case DtoValidationResultLevel.None:
                case DtoValidationResultLevel.Trace:
                    logger.LogDebug(validationResult.ErrorMessage);
                    break;
                case DtoValidationResultLevel.Debug:
                    logger.LogDebug(validationResult.ErrorMessage);
                    break;
                case DtoValidationResultLevel.Information:
                    logger.LogInformation(validationResult.ErrorMessage);
                    break;
                case DtoValidationResultLevel.Warning:
                    logger.LogWarning(validationResult.ErrorMessage);
                    break;
                case DtoValidationResultLevel.Error:
                    logger.LogError(validationResult.ErrorMessage);
                    break;
                case DtoValidationResultLevel.Critical:
                    logger.LogCritical(validationResult.ErrorMessage);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        ///     Adds to the ModelStateDictionary instance.
        /// </summary>
        /// <param name="modelState">State of the model.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void AddRange(this ModelStateDictionary modelState, IEnumerable<ValidationResult> validationResults)
        {
            foreach(ValidationResult result in validationResults.OfTypeError())
            {
                modelState.AddModelError(result.MemberNames.FirstOrDefault() ?? "Entity", result.ErrorMessage);
            }
        }

        /// <summary>
        ///     Determines whether the specified validation results has error.
        /// </summary>
        /// <param name="validationResults">The validation results.</param>
        /// <returns>IEnumerable&lt;ValidationResult&gt;.</returns>
        public static bool HasError(this IEnumerable<ValidationResult> validationResults)
        {
            return validationResults.OfTypeError().Any();
        }

        /// <summary>
        ///     Determines whether the specified validation result is error.
        /// </summary>
        /// <param name="validationResult">The validation result.</param>
        /// <returns><c>true</c> if the specified validation result is error; otherwise, <c>false</c>.</returns>
        public static bool IsError(this ValidationResult validationResult)
        {
            return !(validationResult is DtoValidationResult) || ((DtoValidationResult)validationResult).Level.IsError();
        }

        /// <summary>
        ///     Determines whether the specified level is error.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <returns><c>true</c> if the specified level is error; otherwise, <c>false</c>.</returns>
        public static bool IsError(this DtoValidationResultLevel level)
        {
            return level == DtoValidationResultLevel.Critical || level == DtoValidationResultLevel.Error;
        }

        /// <summary>
        ///     Determines whether the specified validation result is information.
        /// </summary>
        /// <param name="validationResult">The validation result.</param>
        /// <returns><c>true</c> if the specified validation result is information; otherwise, <c>false</c>.</returns>
        public static bool IsInfo(this ValidationResult validationResult)
        {
            return !validationResult.IsError();
        }

        /// <summary>
        ///     Determines whether the specified level is information.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <returns><c>true</c> if the specified level is information; otherwise, <c>false</c>.</returns>
        public static bool IsInfo(this DtoValidationResultLevel level)
        {
            return !level.IsError();
        }

        /// <summary>
        ///     Return the validationResults considered as error.
        /// </summary>
        /// <param name="validationResults">The validation results.</param>
        /// <returns>IEnumerable&lt;ValidationResult&gt;.</returns>
        public static IEnumerable<T> OfTypeError<T>(this IEnumerable<T> validationResults)
                where T : ValidationResult
        {
            return validationResults.Where(v => v.IsError());
        }

        /// <summary>
        ///     Return the validationResults not considered as error.
        /// </summary>
        /// <param name="validationResults">The validation results.</param>
        /// <returns>IEnumerable&lt;ValidationResult&gt;.</returns>
        public static IEnumerable<T> NotOfTypeError<T>(this IEnumerable<T> validationResults)
                where T : ValidationResult
        {
            return validationResults.Where(v => !v.IsError());
        }

        /// <summary>
        ///     Return the validationResults considered as information.
        /// </summary>
        /// <param name="validationResults">The validation results.</param>
        /// <returns>IEnumerable&lt;ValidationResult&gt;.</returns>
        public static IEnumerable<T> OfTypeInfo<T>(this IEnumerable<T> validationResults)
                where T : ValidationResult
        {
            return validationResults.Where(v => v.IsInfo());
        }

        /// <summary>
        ///     Throws the <see cref="DtoValidationException" />.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="message">The message.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void ThrowValidationException<T>(this T item, string message, params ValidationResult[] validationResults)
        {
            item.ThrowValidationException(message, new DtoValidationResults(validationResults));
        }

        /// <summary>
        ///     Throws the <see cref="DtoValidationException" />.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="message">The message.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void ThrowValidationException<T>(this T item, string message, DtoValidationResults validationResults)
        {
            throw DtoValidationException.Create<T>(message, validationResults);
        }

        /// <summary>
        ///     Throws the <see cref="DtoValidationException" />.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void ThrowValidationException<T>(this T item, params ValidationResult[] validationResults)
        {
            item.ThrowValidationException(new DtoValidationResults(validationResults));
        }

        /// <summary>
        ///     Throws the validation exception.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void ThrowValidationException<T>(this T item, DtoValidationResults validationResults)
        {
            item.ThrowValidationException(string.Format(Errors.Validation_Message_Format, nameof(T)), validationResults);
        }

        /// <summary>
        ///     Throws the <see cref="DtoValidationException" />.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void ThrowValidationException<T>(this T item, string message, Exception exception, params ValidationResult[] validationResults)
        {
            item.ThrowValidationException(message, exception, new DtoValidationResults(validationResults));
        }

        /// <summary>
        ///     Throws the <see cref="DtoValidationException" />.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void ThrowValidationException<T>(this T item, string message, Exception exception, DtoValidationResults validationResults)
        {
            throw DtoValidationException.Create<T>(message, exception, validationResults);
        }

        /// <summary>
        ///     Throws the <see cref="DtoValidationException" />.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void ThrowValidationException<T>(this T item, Exception exception, params ValidationResult[] validationResults)
        {
            item.ThrowValidationException(exception, new DtoValidationResults(validationResults));
        }

        /// <summary>
        ///     Throws the <see cref="DtoValidationException" />.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void ThrowValidationException<T>(this T item, Exception exception, DtoValidationResults validationResults)
        {
            item.ThrowValidationException(string.Format(Errors.Validation_Message_Format, nameof(T)), exception, validationResults);
        }

        /// <summary>
        ///     Throws the <see cref="DtoValidationException" /> if necessary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="message">The message.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void ThrowValidationExceptionIfNecessary<T>(this T item, string message, params ValidationResult[] validationResults)
        {
            if(validationResults.HasError())
            {
                item.ThrowValidationException(message, validationResults);
            }
        }

        /// <summary>
        ///     Throws the <see cref="DtoValidationException" /> if necessary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="message">The message.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void ThrowValidationExceptionIfNecessary<T>(this T item, string message, DtoValidationResults validationResults)
        {
            if(validationResults.HasError())
            {
                item.ThrowValidationException(message, validationResults);
            }
        }

        /// <summary>
        ///     Throws the <see cref="DtoValidationException" /> if necessary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void ThrowValidationExceptionIfNecessary<T>(this T item, params ValidationResult[] validationResults)
        {
            if(validationResults.HasError())
            {
                item.ThrowValidationException(validationResults);
            }
        }

        /// <summary>
        ///     Throws the <see cref="DtoValidationException" /> if necessary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="validationResults">The validation results.</param>
        public static void ThrowValidationExceptionIfNecessary<T>(this T item, DtoValidationResults validationResults)
        {
            if(validationResults.HasError())
            {
                item.ThrowValidationException(validationResults);
            }
        }

        /// <summary>
        ///     Converts to the json.
        /// </summary>
        /// <param name="modelState">State of the model.</param>
        /// <returns>Dictionary&lt;System.String, System.String[]&gt;.</returns>
        public static Dictionary<string, string[]> ToDictionnary(this ModelStateDictionary modelState)
        {
            return modelState.Where(e => e.Value.Errors.Any()).ToDictionary(kvp => kvp.Key, kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray());
        }

        /// <summary>
        ///     Converts to the message.
        /// </summary>
        /// <param name="validationResults">The validation results.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        private static string ToMessage(this DtoValidationResults validationResults, string message)
        {
            return string.Join(Environment.NewLine, new[]
            {
                    message
            }.Concat(validationResults.Select(v => v.ToMessage())));
        }

        /// <summary>
        ///     Converts to the message.
        /// </summary>
        /// <param name="validationResult">The validation result.</param>
        /// <returns></returns>
        private static string ToMessage(this ValidationResult validationResult)
        {
            if(validationResult is DtoValidationResult dtoValidationResult)
            {
                return $"[{dtoValidationResult.Level}] {validationResult.ErrorMessage}";
            }

            return validationResult.ErrorMessage;
        }
    }
}