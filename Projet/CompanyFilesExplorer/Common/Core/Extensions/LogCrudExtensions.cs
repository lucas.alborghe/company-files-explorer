﻿using Data.Localization;
using Microsoft.Extensions.Logging;

namespace Core.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with the ability to use extensions method for
    ///     <see cref="ILogger" />.
    /// </summary>
    public static class LogCrudExtensions
    {
        /// <summary>
        ///     Logs that the specified item has been added.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="logger">The logger.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static T LogAdded<T>(this ILogger logger, T item)
        {
            logger.LogInformation(Messages.Item_Added, item.SerializeToJson());
            return item;
        }

        /// <summary>
        ///     Logs that the specified item has been removed.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="logger">The logger.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static T LogRemoved<T>(this ILogger logger, T item)
        {
            logger.LogInformation(Messages.Item_Deleted, item.SerializeToJson());
            return item;
        }

        /// <summary>
        ///     Logs that the specified item has been removed.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="logger">The logger.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static T LogUpdated<T>(this ILogger logger, T item)
        {
            logger.LogInformation(Messages.Item_Updated, item.SerializeToJson());
            return item;
        }
    }
}