﻿using System;
using System.Linq;
using Core.DTOs.CustomConfig;
using Data.Entities;
using Data.Enums;
using Data.Extensions;
using Microsoft.Extensions.Configuration;
using Shared.Configuration;
using Shared.FileSystem;

namespace Core.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with the ability to use extensions method for
    ///     <see cref="CustomConfigDto" />, <see cref="CustomConfigInputDto" /> and <see cref="CustomConfigListDto" />.
    /// </summary>
    public static class CustomConfigExtensions
    {
        private static int _autoLogoutInactivityPeriodInMinutes = DefaultAutoLogoutInactivityPeriodInMinutes;
        private const int DefaultAutoApiKeyDeactivationInactivityPeriodInDays = 60;
        private const int DefaultAutoLogoutInactivityPeriodInMinutes = 5;
        private const string DefaultCustomHomeMessage = "";
        private const string DefaultCustomLoginMessage = "";
        private const int DefaultMaxAPIRequestsCount = 3000;
        private const int DefaultMaxPLCRequestsCount = 20;
        private const int DefaultPasswordValidityPeriodInDays = 60;
        private const int DefaultPLCRequestTimeoutInMs = 50000;
        private const int DefaultPLCPingTimeoutInMs = 400;
        private const int DefaultLogsPageSize = 100;
        private const int DefaultMaxKeyGuessingAttemptsCount = 40;
        private const int DefaultKeyGuessingBanDurationInMinutes = 5;

        public const string AutoLogoutInactivityPeriodKey = "AutoLogoutInactivityPeriod";
        public const string AppFolderKey = "ApplicationFolder";
        public const string AvailableFreeSpaceAlertThresholdKey = "AvailableFreeSpaceAlertThresholdInMegaBytes";
        public const string RestApiUrlKey = "RestApiUrl";

        /// <summary>
        ///     Gets the automatic logout inactivity period.
        /// </summary>
        /// <value>The automatic logout inactivity period.</value>
        public static TimeSpan AutoLogoutInactivityPeriod => TimeSpan.FromMinutes(_autoLogoutInactivityPeriodInMinutes);

        public static string RestApiUrl { get; private set; }

        /// <summary>
        ///     Gets the default value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="ArgumentOutOfRangeException">key - null</exception>
        public static string GetDefaultValue(this CustomConfigKey key)
        {
            switch(key)
            {
                case CustomConfigKey.AutoApiKeyDeactivationInactivityPeriod:
                    return DefaultAutoApiKeyDeactivationInactivityPeriodInDays.ToString();
                case CustomConfigKey.AutoLogoutInactivityPeriod:
                    return _autoLogoutInactivityPeriodInMinutes.ToString();
                case CustomConfigKey.MaxApiRequestsCount:
                    return DefaultMaxAPIRequestsCount.ToString();
                case CustomConfigKey.MaxPLCRequestsCount:
                    return DefaultMaxPLCRequestsCount.ToString();
                case CustomConfigKey.PasswordValidityPeriod:
                    return DefaultPasswordValidityPeriodInDays.ToString();
                case CustomConfigKey.CustomHomeMessage:
                    return DefaultCustomHomeMessage;
                case CustomConfigKey.CustomLoginMessage:
                    return DefaultCustomLoginMessage;
                case CustomConfigKey.PLCRequestTimeout:
                    return DefaultPLCRequestTimeoutInMs.ToString();
                case CustomConfigKey.PLCPingTimeout:
                    return DefaultPLCPingTimeoutInMs.ToString();
                case CustomConfigKey.LogsPageSize:
                    return DefaultLogsPageSize.ToString();
                case CustomConfigKey.MaxKeyGuessingAttemptsCount:
                    return DefaultMaxKeyGuessingAttemptsCount.ToString();
                case CustomConfigKey.KeyGuessingBanDuration:
                    return DefaultKeyGuessingBanDurationInMinutes.ToString();
                default:
                    throw new ArgumentOutOfRangeException(nameof(key), key, null);
            }
        }

        /// <summary>
        ///     Gets the available keys.
        /// </summary>
        /// <returns>CustomConfigKey[].</returns>
        public static CustomConfigKey[] GetEditableKeys()
        {
            return EnumExtensions.GetValues<CustomConfigKey>().Where(c => c.IsEditable()).ToArray();
        }

        /// <summary>
        ///     Gets the value.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="key">The key.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="ArgumentOutOfRangeException">key - null</exception>
        public static string GetValue(this CustomConfigInputDto config, CustomConfigKey key)
        {
            switch(key)
            {
                case CustomConfigKey.AutoApiKeyDeactivationInactivityPeriod:
                    return config.AutoApiKeyDeactivationInactivityPeriodInDays.ToString();
                case CustomConfigKey.AutoLogoutInactivityPeriod:
                    return config.AutoLogoutInactivityPeriodInMinutes.ToString();
                case CustomConfigKey.MaxApiRequestsCount:
                    return config.MaxApiRequestsCountPerMinute.ToString();
                case CustomConfigKey.MaxPLCRequestsCount:
                    return config.MaxPLCRequestsCountPerMinute.ToString();
                case CustomConfigKey.PasswordValidityPeriod:
                    return config.PasswordValidityPeriodInDays.ToString();
                case CustomConfigKey.CustomHomeMessage:
                    return config.CustomHomeMessage;
                case CustomConfigKey.CustomLoginMessage:
                    return config.CustomLoginMessage;
                case CustomConfigKey.PLCRequestTimeout:
                    return config.PLCRequestTimeoutInMs.ToString();
                case CustomConfigKey.PLCPingTimeout:
                    return config.PLCPingTimeoutInMs.ToString();
                case CustomConfigKey.LogsPageSize:
                    return config.LogsPageSize.ToString();
                case CustomConfigKey.MaxKeyGuessingAttemptsCount:
                    return config.MaxKeyGuessingAttemptsCount.ToString();
                case CustomConfigKey.KeyGuessingBanDuration:
                    return config.KeyGuessingBanDurationInMinutes.ToString();
                default:
                    throw new ArgumentOutOfRangeException(nameof(key), key, null);
            }
        }

        /// <summary>
        ///     Gets the values.
        /// </summary>
        public static CustomConfigValue[] GetValues(this CustomConfigInputDto config)
        {
            return GetEditableKeys().Select(e => new CustomConfigValue
            {
                    Key = e,
                    Value = GetValue(config, e)
            }).ToArray();
        }

        /// <summary>
        ///     Initializes the values.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public static void InitValues(this CustomConfigDto config)
        {
            CustomConfigKey[] keys = GetEditableKeys();
            LoadValues(config, keys.Select(e => new CustomConfigValue
            {
                    Key = e,
                    Value = GetDefaultValue(e)
            }).ToArray());
        }

        /// <summary>
        ///     Loads the values.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="values">The values.</param>
        public static void LoadValues(this CustomConfigDto config, CustomConfigValue[] values)
        {
            foreach(CustomConfigValue value in values)
            {
                SetValue(config, value.Key, value.Value);
            }
        }

        /// <summary>
        ///     Sets the value.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <exception cref="ArgumentOutOfRangeException">key - null</exception>
        public static void SetValue(this CustomConfigDto config, CustomConfigKey key, string value)
        {
            switch(key)
            {
                case CustomConfigKey.AutoApiKeyDeactivationInactivityPeriod:
                    config.AutoApiKeyDeactivationInactivityPeriodInDays = int.Parse(value);
                    break;
                case CustomConfigKey.AutoLogoutInactivityPeriod:
                    config.AutoLogoutInactivityPeriodInMinutes = int.Parse(value);
                    break;
                case CustomConfigKey.MaxApiRequestsCount:
                    config.MaxApiRequestsCountPerMinute = int.Parse(value);
                    break;
                case CustomConfigKey.MaxPLCRequestsCount:
                    config.MaxPLCRequestsCountPerMinute = int.Parse(value);
                    break;
                case CustomConfigKey.PasswordValidityPeriod:
                    config.PasswordValidityPeriodInDays = int.Parse(value);
                    break;
                case CustomConfigKey.CustomHomeMessage:
                    config.CustomHomeMessage = value;
                    break;
                case CustomConfigKey.CustomLoginMessage:
                    config.CustomLoginMessage = value;
                    break;
                case CustomConfigKey.PLCRequestTimeout:
                    config.PLCRequestTimeoutInMs = int.Parse(value);
                    break;
                case CustomConfigKey.PLCPingTimeout:
                    config.PLCPingTimeoutInMs = int.Parse(value);
                    break;
                case CustomConfigKey.LogsPageSize:
                    config.LogsPageSize = int.Parse(value);
                    break;
                case CustomConfigKey.MaxKeyGuessingAttemptsCount:
                    config.MaxKeyGuessingAttemptsCount = int.Parse(value);
                    break;
                case CustomConfigKey.KeyGuessingBanDuration:
                    config.KeyGuessingBanDurationInMinutes = int.Parse(value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(key), key, null);
            }
        }

        /// <summary>
        ///     Loads the configuration.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public static void LoadConfiguration(IConfiguration configuration)
        {
            TimeSpan autoLogoutInactivityPeriod = configuration.GetAppSettingsValue<TimeSpan>(AutoLogoutInactivityPeriodKey);
            if(autoLogoutInactivityPeriod == TimeSpan.Zero)
            {
                autoLogoutInactivityPeriod = TimeSpan.FromMinutes(DefaultAutoLogoutInactivityPeriodInMinutes);
            }

            _autoLogoutInactivityPeriodInMinutes = (int)autoLogoutInactivityPeriod.TotalMinutes;
            RestApiUrl = configuration.GetAppSettingsValue<string>(RestApiUrlKey);
            FileSystemHelper.AppFolder = configuration.GetAppSettingsValue<string>(AppFolderKey);
            FileSystemHelper.AvailableFreeSpaceAlertThresholdInMegaBytes = configuration.GetAppSettingsValue<long>(AvailableFreeSpaceAlertThresholdKey);
        }
    }
}