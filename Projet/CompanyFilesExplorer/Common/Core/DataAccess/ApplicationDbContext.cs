﻿using Data.Entities;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Core.DataAccess
{
    public sealed class ApplicationDbContext : DbContext
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ApplicationDbContext" /> class.
        /// </summary>
        /// <param name="options">The options.</param>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        /// <summary>
        ///     Gets or sets the custom configs.
        /// </summary>
        /// <value>The custom configs.</value>
        public DbSet<CustomConfigValue> CustomConfigs { get; set; }
        
        /// <summary>
        ///     Gets or sets the allowed extensions
        /// </summary>
        /// <value>The allowed extensions.</value>
        public DbSet<AllowedExtension> AllowedExtensions { get; set; }

        /// <summary>
        ///     Gets or sets the directories
        /// </summary>
        /// <value>The directories.</value>
        public DbSet<DirectoryDb> Directories { get; set; }

        /// <summary>
        ///     Gets or sets the files from the database
        /// </summary>
        /// <value>The files from the database.</value>
        public DbSet<FileDb> FilesDb { get; set; }

        /// <summary>
        ///     Gets or sets the metadata
        /// </summary>
        /// <value>The metadata.</value>
        public DbSet<Metadata> Metadata { get; set; }

        /// <summary>
        ///     Gets or sets the preocedures from the database
        /// </summary>
        /// <value>The preocedures from the database.</value>
        public DbSet<ProcedureDb> ProceduresDb { get; set; }

        /// <summary>
        ///     Gets or sets the repository detail
        /// </summary>
        /// <value>The repository detail.</value>
        public DbSet<RepositoryDb> RepositoriesDetail { get; set; }

        /// <summary>
        ///     Gets or sets the users from the database
        /// </summary>
        /// <value>The users from the database.</value>
        public DbSet<UserDb> UsersDb { get; set; }

        /// <summary>
        ///     Gets or sets the users preferences
        /// </summary>
        /// <value>The users preferences.</value>
        public DbSet<UserPreference> UserPreferences { get; set; }

        /// <summary>
        ///     Gets or sets the virtual directories
        /// </summary>
        /// <value>The virtual directories.</value>
        public DbSet<VirtualDirectory> VirtualDirectories { get; set; }

        /// <inheritdoc />
        protected override void OnModelCreating([NotNull] ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CustomConfigValue>().HasKey(e => e.Key);

            modelBuilder.Entity<AllowedExtension>(entity =>
            {
                //entity.ToTable("allowedextension");
                entity.ToTable("allowedExtension");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Extension)
                    .IsRequired()
                    .HasColumnName("extension")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<DirectoryDb>(entity =>
            {
                entity.HasKey(e => e.Path);

                //entity.ToTable("directory");
                entity.ToTable("directoryDb");

                entity.HasIndex(e => e.RepositoryId)
                    .HasName("fk_directory_repositoryDetail1_idx");

                entity.Property(e => e.Path)
                    .HasColumnName("path")
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.LastAccessDate)
                    .HasColumnName("lastAccessDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.RepositoryId)
                    .HasColumnName("repository_id");

                entity.HasOne(d => d.Repository)
                    .WithMany(p => p.Directories)
                    .HasForeignKey(d => d.RepositoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_directory_repositoryDetail1");
            });

            modelBuilder.Entity<FileDb>(entity =>
            {
                //entity.ToTable("file");
                entity.ToTable("fileDb");

                entity.HasIndex(e => e.RepositoryId)
                    .HasName("fk_file_repositoryDetail1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("createdDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Extension)
                    .IsRequired()
                    .HasColumnName("extension")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.Filename)
                    .IsRequired()
                    .HasColumnName("filename")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Filepath)
                    .IsRequired()
                    .HasColumnName("filepath")
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.Hashcode)
                    .IsRequired()
                    .HasColumnName("hashcode")
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnName("modifiedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.RepositoryId)
                    .HasColumnName("repository_id");

                entity.HasOne(d => d.Repository)
                    .WithMany(p => p.Files)
                    .HasForeignKey(d => d.RepositoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_file_repositoryDetail1");
            });

            modelBuilder.Entity<Metadata>(entity =>
            {
                entity.ToTable("metadata");

                entity.HasIndex(e => e.ProcedureId)
                    .HasName("fk_metadata_procedure1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.Fonction)
                    .HasColumnName("fonction")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.FonctionParam)
                    .HasColumnName("fonctionParam")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.ProcedureId)
                    .HasColumnName("procedure_id");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasColumnType("varchar(64)");

                entity.HasOne(d => d.Procedure)
                    .WithMany(p => p.Metadata)
                    .HasForeignKey(d => d.ProcedureId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_metadata_procedure1");
            });

            modelBuilder.Entity<ProcedureDb>(entity =>
            {
                //entity.ToTable("procedure");
                entity.ToTable("procedureDb");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Rule)
                    .IsRequired()
                    .HasColumnName("procedure")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<RepositoryDb>(entity =>
            {
                //entity.ToTable("repository");
                entity.ToTable("repositoryDb");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.AbsolutePath)
                    .IsRequired()
                    .HasColumnName("absolutePath")
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Protocol)
                    .IsRequired()
                    .HasColumnName("protocol")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<UserDb>(entity =>
            {
                //entity.ToTable("user");
                entity.ToTable("userDb");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.IsAdmin)
                    .HasColumnName("isAdmin")
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Login)
                    .IsRequired()
                    .HasColumnName("login")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<UserPreference>(entity =>
            {
                entity.HasKey(e => new { e.VirtualDirectoryId, e.FileId });

                //entity.ToTable("userpreference");
                entity.ToTable("userPreference");

                entity.HasIndex(e => e.FileId)
                    .HasName("fk_virtualDirectory_has_file_file1_idx");

                entity.HasIndex(e => e.VirtualDirectoryId)
                    .HasName("fk_virtualDirectory_has_file_virtualDirectory1_idx");

                entity.Property(e => e.VirtualDirectoryId)
                    .HasColumnName("virtualDirectory_id");

                entity.Property(e => e.FileId)
                    .HasColumnName("file_id");

                entity.Property(e => e.Filename)
                    .HasColumnName("filename")
                    .HasColumnType("varchar(64)");

                entity.HasOne(d => d.File)
                    .WithMany(p => p.UserPreferences)
                    .HasForeignKey(d => d.FileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_virtualDirectory_has_file_file1");

                entity.HasOne(d => d.VirtualDirectory)
                    .WithMany(p => p.UserPreferences)
                    .HasForeignKey(d => d.VirtualDirectoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_virtualDirectory_has_file_virtualDirectory1");
            });

            modelBuilder.Entity<VirtualDirectory>(entity =>
            {
                //entity.ToTable("virtualdirectory");
                entity.ToTable("virtualDirectory");

                entity.HasIndex(e => e.ProcedureId)
                    .HasName("fk_virtualDirectory_procedure1_idx");

                entity.HasIndex(e => e.UserId)
                    .HasName("fk_virtualDirectory_user1_idx");

                entity.HasIndex(e => e.VirtualDirectoryId)
                    .HasName("fk_virtualDirectory_virtualDirectory1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.IsActive)
                    .HasColumnName("isActive")
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.ProcedureId)
                    .HasColumnName("procedure_id");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id");

                entity.Property(e => e.VirtualDirectoryId)
                    .HasColumnName("parent_id");
                    //.HasColumnName("virtualDirectory_id");

                entity.HasOne(d => d.Procedure)
                    .WithMany(p => p.VirtualDirectories)
                    .HasForeignKey(d => d.ProcedureId)
                    .HasConstraintName("fk_virtualDirectory_procedure1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.VirtualDirectories)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_virtualDirectory_user1");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.Children)
                    .HasForeignKey(d => d.VirtualDirectoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_virtualDirectory_virtualDirectory1");
            });
        }
    }
}
