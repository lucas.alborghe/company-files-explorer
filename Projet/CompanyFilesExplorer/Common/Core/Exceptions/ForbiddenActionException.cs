﻿using System;
using System.Runtime.Serialization;

namespace Core.Exceptions
{
    /// <summary>
    ///     Used when a user does something that is not allowed by the application
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class ForbiddenActionException : Exception
    {
        public ForbiddenActionException()
        {
        }

        public ForbiddenActionException(string message)
                : base(message)
        {
        }

        public ForbiddenActionException(string message, Exception innerException)
                : base(message, innerException)
        {
        }

        protected ForbiddenActionException(SerializationInfo info, StreamingContext context)
                : base(info, context)
        {
        }
    }
}