﻿using System;
using System.ComponentModel.DataAnnotations;
using Core.Validation;

namespace Core.Exceptions
{
    public class DtoValidationException : ValidationException
    {
        /// <summary>
        ///     Gets the type of the entity.
        /// </summary>
        /// <value>The type of the entity.</value>
        public Type EntityType { get; }

        /// <summary>
        ///     Gets the validation results.
        /// </summary>
        /// <value>The validation results.</value>
        public DtoValidationResults ValidationResults { get; }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DtoValidationException" /> class.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="message">The message.</param>
        /// <param name="validationResults">The validation results.</param>
        public DtoValidationException(Type entityType, string message, DtoValidationResults validationResults)
                : base(message)
        {
            EntityType = entityType;
            ValidationResults = validationResults;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DtoValidationException" /> class.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <param name="validationResults">The validation results.</param>
        public DtoValidationException(Type entityType, string message, Exception innerException, DtoValidationResults validationResults)
                : base(message, innerException)
        {
            EntityType = entityType;
            ValidationResults = validationResults;
        }

        /// <summary>
        ///     Creates the DtoValidationException instance with the specified message.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message">The message.</param>
        /// <param name="validationResults">The validation results.</param>
        /// <returns>DtoValidationException.</returns>
        public static DtoValidationException Create<T>(string message, DtoValidationResults validationResults)
        {
            return new DtoValidationException(typeof(T), message, validationResults);
        }

        /// <summary>
        ///     Creates the DtoValidationException instance with the specified message.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <param name="validationResults">The validation results.</param>
        /// <returns>DtoValidationException.</returns>
        public static DtoValidationException Create<T>(string message, Exception innerException, DtoValidationResults validationResults)
        {
            return new DtoValidationException(typeof(T), message, innerException, validationResults);
        }
    }
}