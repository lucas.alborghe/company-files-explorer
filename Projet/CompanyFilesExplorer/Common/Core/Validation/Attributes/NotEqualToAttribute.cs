﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Validation.Attributes
{

    /// <summary>
    ///     Validation attribute used to validate that a property is different than an other property
    /// </summary>
    /// <seealso cref="System.ComponentModel.DataAnnotations.ValidationAttribute" />
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class NotEqualToAttribute : ValidationAttribute
    {
        private const string DefaultErrorMessageFormat = "{0} cannot be the same as {1}.";

        public string OtherProperty { get; }

        public NotEqualToAttribute(string otherProperty) : base(DefaultErrorMessageFormat)
        {
            if (string.IsNullOrWhiteSpace(otherProperty))
            {
                throw new ArgumentNullException(nameof(otherProperty));
            }
            OtherProperty = otherProperty;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(ErrorMessageString, name, OtherProperty);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                var otherProperty = validationContext.ObjectInstance.GetType().GetProperty(OtherProperty);
                var otherPropertyValue = otherProperty.GetValue(validationContext.ObjectInstance, null);

                if (value.Equals(otherPropertyValue))
                {
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                }
            }

            return ValidationResult.Success;
        }
    }
}
