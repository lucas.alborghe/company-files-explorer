﻿using System.ComponentModel.DataAnnotations;
using JetBrains.Annotations;

namespace Core.Validation.Attributes
{
    public class PositiveIntAttribute : ValidationAttribute
    {
        private readonly bool _acceptNull;

        /// <summary>
        ///     Initializes a new instance of the <see cref="PositiveIntAttribute" /> class.
        /// </summary>
        /// <param name="acceptNull">if set to <c>true</c> [accept null].</param>
        public PositiveIntAttribute(bool acceptNull = false)
        {
            _acceptNull = acceptNull;
        }

        /// <summary>
        ///     Determines whether [is valid value] [the specified value].
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="acceptNull">if set to <c>true</c> [accept null].</param>
        /// <returns>
        ///     <c>true</c> if [is valid value] [the specified value]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidValue([CanBeNull] object value, bool acceptNull = false)
        {
            if(value == null)
            {
                return acceptNull;
            }

            string stringValue = value.ToString();
            return int.TryParse(stringValue, out int intValue) && intValue > 0;
        }

        /// <summary>
        ///     Gets the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static int GetValue(object value)
        {
            return int.Parse(value.ToString());
        }

        /// <inheritdoc />
        protected override ValidationResult IsValid([CanBeNull] object value, [CanBeNull] ValidationContext validationContext)
        {
            if(IsValidValue(value, _acceptNull))
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(FormatErrorMessage(validationContext?.DisplayName));
        }
    }
}