﻿using System;
using System.ComponentModel.DataAnnotations;
using JetBrains.Annotations;

namespace Core.Validation.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DateGreaterThanAttribute : ValidationAttribute
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DateGreaterThanAttribute" /> class.
        /// </summary>
        /// <param name="dateToCompareToFieldName">Name of the date to compare to field.</param>
        public DateGreaterThanAttribute(string dateToCompareToFieldName)
        {
            DateToCompareToFieldName = dateToCompareToFieldName;
        }

        private string DateToCompareToFieldName { get; }

        /// <inheritdoc />
        protected override ValidationResult IsValid([NotNull] object value, [NotNull] ValidationContext validationContext)
        {
            DateTime crtDate = (DateTime)value;

            DateTime smallerDate = (DateTime)validationContext.ObjectType.GetProperty(DateToCompareToFieldName).GetValue(validationContext.ObjectInstance, null);

            if(smallerDate < crtDate)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }
    }
}