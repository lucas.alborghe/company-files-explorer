﻿using System.ComponentModel.DataAnnotations;

namespace Core.Validation
{
    public class DtoValidationResult : ValidationResult
    {
        /// <summary>
        ///     Gets the level.
        /// </summary>
        /// <value>The level.</value>
        public DtoValidationResultLevel Level { get; }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DtoValidationResult" /> class.
        /// </summary>
        /// <param name="validationResult">The validation result object.</param>
        /// <param name="level">The level.</param>
        protected DtoValidationResult(ValidationResult validationResult, DtoValidationResultLevel level)
            : base(validationResult)
        {
            Level = level;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DtoValidationResult" /> class.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="level">The level.</param>
        public DtoValidationResult(string errorMessage, DtoValidationResultLevel level)
            : base(errorMessage)
        {
            Level = level;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DtoValidationResult" /> class.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="level">The level.</param>
        /// <param name="memberNames">The list of member names that have validation errors.</param>
        public DtoValidationResult(string errorMessage, DtoValidationResultLevel level, params string[] memberNames)
            : base(errorMessage, memberNames)
        {
            Level = level;
        }
    }
}