﻿using Data.Localization;

namespace Core.Validation
{
    [LocalizedResource(typeof(Enums))]
    public enum DtoValidationResultLevel
    {
        [LocalizedDescription("DtoValidationResultLevel_Trace_Title")]
        Trace,
        [LocalizedDescription("DtoValidationResultLevel_Debug_Title")]
        Debug,
        [LocalizedDescription("DtoValidationResultLevel_Information_Title")]
        Information,
        [LocalizedDescription("DtoValidationResultLevel_Warning_Title")]
        Warning,
        [LocalizedDescription("DtoValidationResultLevel_Error_Title")]
        Error,
        [LocalizedDescription("DtoValidationResultLevel_Critical_Title")]
        Critical,
        [LocalizedDescription("DtoValidationResultLevel_None_Title")]
        None
    }
}