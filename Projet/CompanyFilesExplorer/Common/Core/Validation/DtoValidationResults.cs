﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core.Validation
{
    public class DtoValidationResults : List<ValidationResult>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DtoValidationResults" /> class.
        /// </summary>
        /// <param name="collection">The collection whose elements are copied to the new list.</param>
        public DtoValidationResults(IEnumerable<ValidationResult> collection)
            : base(collection)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DtoValidationResults" /> class.
        /// </summary>
        public DtoValidationResults()
        {
        }

        /// <summary>
        ///     Adds the specified error.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="memberNames">The member names.</param>
        public void Add(DtoValidationResultLevel level, string errorMessage, params string[] memberNames)
        {
            Add(new DtoValidationResult(errorMessage, level, memberNames));
        }

        /// <summary>
        ///     Adds an error with the following level : debug.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="memberNames">The member names.</param>
        public void AddDebug(string errorMessage, params string[] memberNames)
        {
            Add(DtoValidationResultLevel.Debug, errorMessage, memberNames);
        }

        /// <summary>
        ///     Adds an error with the following level : trace.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="memberNames">The member names.</param>
        public void AddTrace(string errorMessage, params string[] memberNames)
        {
            Add(DtoValidationResultLevel.Trace, errorMessage, memberNames);
        }

        /// <summary>
        ///     Adds an error with the following level : information.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="memberNames">The member names.</param>
        public void AddInformation(string errorMessage, params string[] memberNames)
        {
            Add(DtoValidationResultLevel.Information, errorMessage, memberNames);
        }

        /// <summary>
        ///     Adds an error with the following level : warning.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="memberNames">The member names.</param>
        public void AddWarning(string errorMessage, params string[] memberNames)
        {
            Add(DtoValidationResultLevel.Warning, errorMessage, memberNames);
        }

        /// <summary>
        ///     Adds an error with the following level : error.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="memberNames">The member names.</param>
        public void AddError(string errorMessage, params string[] memberNames)
        {
            Add(DtoValidationResultLevel.Error, errorMessage, memberNames);
        }

        /// <summary>
        ///     Adds a critical error.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="memberNames">The member names.</param>
        public void AddCritical(string errorMessage, params string[] memberNames)
        {
            Add(DtoValidationResultLevel.Critical, errorMessage, memberNames);
        }
    }
}