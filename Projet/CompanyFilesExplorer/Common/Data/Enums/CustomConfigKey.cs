﻿using Data.Localization;

namespace Data.Enums
{
    /// <summary>
    ///     This enumeration is used by the CustomConfig keys. It contains 7 values
    /// </summary>
    [LocalizedResource(typeof(Localization.Enums))]
    public enum CustomConfigKey
    {
        [LocalizedDescription("CustomConfigKey_AutoApiKeyDeactivationInactivityPeriod_Title")]
        AutoApiKeyDeactivationInactivityPeriod = 30,

        [LocalizedDescription("CustomConfigKey_AutoLogoutInactivityPeriod_Title")]
        AutoLogoutInactivityPeriod = 40,

        [LocalizedDescription("CustomConfigKey_CustomLoginMessage_Title")]
        CustomLoginMessage = 10,

        [LocalizedDescription("CustomConfigKey_MaxApiRequestsCount_Title")]
        MaxApiRequestsCount = 50,

        [LocalizedDescription("CustomConfigKey_MaxPLCRequestsCount_Title")]
        MaxPLCRequestsCount = 60,

        [LocalizedDescription("CustomConfigKey_PasswordValidityPeriod_Title")]
        PasswordValidityPeriod = 70,

        [LocalizedDescription("CustomConfigKey_CustomHomeMessage_Title")]
        CustomHomeMessage = 20,

        [LocalizedDescription("CustomConfigKey_PLCRequestTimeout_Title")]
        PLCRequestTimeout = 80,

        [LocalizedDescription("CustomConfigKey_PLCPingTimeout_Title")]
        PLCPingTimeout = 90,

        [LocalizedDescription("CustomConfigKey_LogsPageSize_Title")]
        LogsPageSize = 100,

        [LocalizedDescription("CustomConfigKey_MaxKeyGuessingAttemptsCount_Title")]
        MaxKeyGuessingAttemptsCount = 110,

        [LocalizedDescription("CustomConfigKey_KeyGuessingBanDuration_Title")]
        KeyGuessingBanDuration = 120
    }
}