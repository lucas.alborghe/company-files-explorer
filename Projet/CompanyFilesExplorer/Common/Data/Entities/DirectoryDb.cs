﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    public partial class DirectoryDb
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Path { get; set; }

        public int RepositoryId { get; set; }

        public DateTime? LastAccessDate { get; set; }

        public RepositoryDb Repository { get; set; }
    }
}
