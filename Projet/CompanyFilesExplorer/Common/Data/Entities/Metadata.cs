﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public partial class Metadata
    {
        [Key]
        public int Id { get; set; }

        public int ProcedureId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Fonction { get; set; }

        public string FonctionParam { get; set; }

        public string Description { get; set; }

        public ProcedureDb Procedure { get; set; }
    }
}
