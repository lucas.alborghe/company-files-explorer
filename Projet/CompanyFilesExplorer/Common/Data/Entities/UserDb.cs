﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public partial class UserDb
    {
        public UserDb()
        {
            VirtualDirectories = new HashSet<VirtualDirectory>();
        }

        [Key]
        public int Id { get; set; }

        public string Login { get; set; }

        public sbyte IsAdmin { get; set; }

        public ICollection<VirtualDirectory> VirtualDirectories { get; set; }
    }
}
