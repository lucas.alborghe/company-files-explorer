﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public partial class VirtualDirectory
    {
        public VirtualDirectory()
        {
            Children = new HashSet<VirtualDirectory>();
            UserPreferences = new HashSet<UserPreference>();
        }

        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }

        public int? VirtualDirectoryId { get; set; }

        public int? ProcedureId { get; set; }

        public string Name { get; set; }

        public sbyte IsActive { get; set; }

        public ProcedureDb Procedure { get; set; }
        public UserDb User { get; set; }
        public VirtualDirectory Parent { get; set; }
        public ICollection<VirtualDirectory> Children { get; set; }
        public ICollection<UserPreference> UserPreferences { get; set; }
    }
}
