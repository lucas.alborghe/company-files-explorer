﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public partial class RepositoryDb
    {
        public RepositoryDb()
        {
            Directories = new HashSet<DirectoryDb>();
            Files = new HashSet<FileDb>();
        }

        [Key]
        public int Id { get; set; }

        public string AbsolutePath { get; set; }

        public string Protocol { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public ICollection<DirectoryDb> Directories { get; set; }
        public ICollection<FileDb> Files { get; set; }
    }
}
