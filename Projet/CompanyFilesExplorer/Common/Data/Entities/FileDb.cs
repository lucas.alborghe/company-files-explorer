﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public partial class FileDb
    {
        public FileDb()
        {
            UserPreferences = new HashSet<UserPreference>();
        }

        [Key]
        public int Id { get; set; }

        public int RepositoryId { get; set; }

        public string Filename { get; set; }

        public string Extension { get; set; }

        public string Filepath { get; set; }

        public string Hashcode { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public RepositoryDb Repository { get; set; }
        public ICollection<UserPreference> UserPreferences { get; set; }
    }
}
