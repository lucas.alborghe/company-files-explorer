﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public partial class AllowedExtension
    {
        [Key]
        public int Id { get; set; }

        public string Extension { get; set; }
    }
}
