﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public partial class ProcedureDb
    {
        public ProcedureDb()
        {
            Metadata = new HashSet<Metadata>();
            VirtualDirectories = new HashSet<VirtualDirectory>();
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Rule { get; set; }

        public string Description { get; set; }

        public ICollection<Metadata> Metadata { get; set; }
        public ICollection<VirtualDirectory> VirtualDirectories { get; set; }
    }
}
