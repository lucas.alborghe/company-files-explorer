﻿using System;
using JetBrains.Annotations;

namespace Data.Entities
{
    /// <summary>
    ///     This class contains the log of the applications.
    ///     However, this class is not mapped with EF but with dapper (look in LogAccessLayer).
    /// </summary>
    public class StdLog
    {
        /// <summary>
        ///     Gets or sets the time stamp.
        /// </summary>
        /// <value>
        ///     The time stamp.
        /// </value>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        ///     Gets or sets the level.
        /// </summary>
        /// <value>
        ///     The level.
        /// </value>
        public string Level { get; set; }

        /// <summary>
        ///     Gets or sets the rendered message.
        /// </summary>
        /// <value>
        ///     The rendered message.
        /// </value>
        public string RenderedMessage { get; set; }

        /// <summary>
        ///     Gets or sets the properties.
        /// </summary>
        /// <value>
        ///     The properties.
        /// </value>
        [CanBeNull]
        public string Properties { get; set; }
    }
}