﻿using System.ComponentModel.DataAnnotations;
using Data.Enums;
using Data.Localization;

namespace Data.Entities
{
    public class CustomConfigValue
    {
        /// <summary>
        ///     Gets or sets the Key.
        /// </summary>
        /// <value>
        ///     The Key.
        /// </value>
        [Display(ResourceType = typeof(Localization.Entities), Name = "CustomConfigItem_Key_Title", Order = 0)]
        public CustomConfigKey Key { get; set; }

        /// <summary>
        ///     Gets or sets the Value.
        /// </summary>
        /// <value>
        ///     The Value.
        /// </value>
        [Required(AllowEmptyStrings = true, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Validation_Required")]
        [Display(ResourceType = typeof(Localization.Entities), Name = "CustomConfigItem_Value_Title", Order = 1)]
        public string Value { get; set; }
    }
}