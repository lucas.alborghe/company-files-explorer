﻿using System;
using System.Collections.Generic;

namespace Data.Entities
{
    /// <summary>
    ///     This class contains a page of log of the applications.
    ///     However, this class is not mapped with EF but with dapper (look in LogAccessLayer).
    /// </summary>
    public class StdLogPage
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the logs.
        /// </summary>
        /// <value>
        ///     The logs.
        /// </value>
        public IEnumerable<StdLog> Logs { get; set; }

        /// <summary>
        ///     Gets or sets the pages count.
        /// </summary>
        /// <value>
        ///     The pages count.
        /// </value>
        public int PagesCount { get; set; }

        /// <summary>
        ///     Gets or sets the index of the page.
        /// </summary>
        /// <value>
        ///     The index of the page.
        /// </value>
        public int PageIndex { get; set; }

        /// <summary>
        ///     Gets or sets the size of the page.
        /// </summary>
        /// <value>
        ///     The size of the page.
        /// </value>
        public int PageSize { get; set; }

        /// <summary>
        ///     Gets or sets the total count.
        /// </summary>
        /// <value>
        ///     The total count.
        /// </value>
        public int TotalCount { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Computes the pages count.
        /// </summary>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalCount">The total count.</param>
        /// <returns></returns>
        public static int ComputePagesCount(int pageSize, int totalCount)
        {
            return (int)Math.Ceiling((double)totalCount / pageSize);
        }

        #endregion
    }
}