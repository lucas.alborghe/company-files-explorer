﻿using System;

namespace Data.Localization
{
    /// <inheritdoc />
    /// <summary>
    ///     Provides a resource type to use for localized enumeration.
    /// </summary>
    [AttributeUsage(AttributeTargets.Enum)]
    public sealed class LocalizedResourceAttribute : Attribute
    {
        /// <summary>
        ///     Gets the resource type stored in this attribute.
        /// </summary>
        /// <value>The resource type stored in the attribute.</value>
        public Type Type { get; }

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="T:Data.Localization.LocalizedResourceAttribute" />
        ///     class.
        /// </summary>
        /// <param name="type">
        ///     The description to store in this attribute.
        /// </param>
        public LocalizedResourceAttribute(Type type)
        {
            Type = type;
        }
    }
}