﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data.Localization {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Errors {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Errors() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Data.Localization.Errors", typeof(Errors).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An uncatched error occured: {0}.
        /// </summary>
        public static string General_Errors {
            get {
                return ResourceManager.GetString("General_Errors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The super-administrator has already been created..
        /// </summary>
        public static string User_SuperAdmin_AlreadyExists {
            get {
                return ResourceManager.GetString("User_SuperAdmin_AlreadyExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The super-administrator can never be created..
        /// </summary>
        public static string User_SuperAdmin_CreateForbidden {
            get {
                return ResourceManager.GetString("User_SuperAdmin_CreateForbidden", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The server returns an error while saving the super-administrator..
        /// </summary>
        public static string User_SuperAdmin_CreationError {
            get {
                return ResourceManager.GetString("User_SuperAdmin_CreationError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The CreateSuperAdmin view has been called but the super-administrator has already been created..
        /// </summary>
        public static string User_SuperAdmin_Recall {
            get {
                return ResourceManager.GetString("User_SuperAdmin_Recall", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The CreateSuperAdmin post has been called but the super-administrator has already been created..
        /// </summary>
        public static string User_SuperAdmin_Resave {
            get {
                return ResourceManager.GetString("User_SuperAdmin_Resave", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The super-administrator can&apos;t be modified or deleted..
        /// </summary>
        public static string User_SuperAdmin_Restricted {
            get {
                return ResourceManager.GetString("User_SuperAdmin_Restricted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field &apos;{0}&apos; and field &apos;{1}&apos; do not match..
        /// </summary>
        public static string Validation_Compare {
            get {
                return ResourceManager.GetString("Validation_Compare", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field &apos;{0}&apos; doesn&apos;t match the specific condition..
        /// </summary>
        public static string Validation_CustomValidation {
            get {
                return ResourceManager.GetString("Validation_CustomValidation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field &apos;{0}&apos; can&apos;t be converted to the specific type..
        /// </summary>
        public static string Validation_DataType {
            get {
                return ResourceManager.GetString("Validation_DataType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field &apos;{0}&apos; shoud be after the &apos;from&apos; date.
        /// </summary>
        public static string Validation_DateGreater {
            get {
                return ResourceManager.GetString("Validation_DateGreater", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to the username or password is incorrect please input again..
        /// </summary>
        public static string Validation_LoginError {
            get {
                return ResourceManager.GetString("Validation_LoginError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field &apos;{0}&apos; should be a valid email address..
        /// </summary>
        public static string Validation_Mail {
            get {
                return ResourceManager.GetString("Validation_Mail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The &apos;{0}&apos; can&apos;t be validated..
        /// </summary>
        public static string Validation_Message_Format {
            get {
                return ResourceManager.GetString("Validation_Message_Format", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The title can&apos;t be validated..
        /// </summary>
        public static string Validation_Message_Title {
            get {
                return ResourceManager.GetString("Validation_Message_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field &apos;{0}&apos; should have at least {1} element(s)..
        /// </summary>
        public static string Validation_MinCount {
            get {
                return ResourceManager.GetString("Validation_MinCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field &apos;{0}&apos; and field &apos;{1}&apos; cannot have the same value..
        /// </summary>
        public static string Validation_NotEqualTo {
            get {
                return ResourceManager.GetString("Validation_NotEqualTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The {0} must be at least 8 characters long and contains at least 1 lower case letter, 1 upper case letter, 1 number and a special character..
        /// </summary>
        public static string Validation_Password {
            get {
                return ResourceManager.GetString("Validation_Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field &apos;{0}&apos; should be between {1} and {2}.
        /// </summary>
        public static string Validation_Range {
            get {
                return ResourceManager.GetString("Validation_Range", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field &apos;{0}&apos; should be greater than {1}.
        /// </summary>
        public static string Validation_RangeMin {
            get {
                return ResourceManager.GetString("Validation_RangeMin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field &apos;{0}&apos; is not valid..
        /// </summary>
        public static string Validation_RegularExpression {
            get {
                return ResourceManager.GetString("Validation_RegularExpression", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field &apos;{0}&apos; is required..
        /// </summary>
        public static string Validation_Required {
            get {
                return ResourceManager.GetString("Validation_Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The {1} must be at least {2} characters long..
        /// </summary>
        public static string Validation_StringLength {
            get {
                return ResourceManager.GetString("Validation_StringLength", resourceCulture);
            }
        }
    }
}
