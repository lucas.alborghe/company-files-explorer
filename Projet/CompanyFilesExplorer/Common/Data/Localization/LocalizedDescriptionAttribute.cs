﻿using System;

namespace Data.Localization
{
    /// <inheritdoc />
    /// <summary>
    ///     Provides enum's value localized resource description key.
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public class LocalizedDescriptionAttribute : Attribute
    {
        /// <summary>
        ///     Gets the localized desciption key stored in this attribute.
        /// </summary>
        /// <value>The localized description key stored in the attribute.</value>
        public string Description { get; }

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the
        ///     <see cref="T:Data.Localization.LocalizedDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="description">
        ///     The localized description key to store in this attribute.
        /// </param>
        public LocalizedDescriptionAttribute(string description)
        {
            Description = description;
        }
    }
}