﻿using System;
using JetBrains.Annotations;

namespace Data.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with extension methods shortest the cast management.
    /// </summary>
    public static class ConvertionExtensions
    {
        /// <summary>
        ///     Casts the specified o.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="o">The o.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        [CanBeNull]
        public static T Cast<T>(this object o, T defaultValue = default)
        {
            // ReSharper disable once MergeCastWithTypeCheck
            if(o is T)
            {
                return (T)o;
            }

            return defaultValue;
        }

        /// <summary>
        ///     Tries to cast.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="item">The item.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        [CanBeNull]
        public static TResult ForceCast<TValue, TResult>(this TValue item, TResult defaultValue = default)
                where TResult : IConvertible
        {
            try
            {
                return (TResult)item.ForceCast(typeof(TResult));
            } catch
            {
                return defaultValue;
            }
        }

        /// <summary>
        ///     Forces the cast.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="item">The item.</param>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        [CanBeNull]
        public static object ForceCast<TValue>(this TValue item, Type result)
        {
            return ForceChangeType(item, result);
        }

        /// <summary>
        ///     Forces the cast.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        [CanBeNull]
        public static object ForceChangeType([CanBeNull] this object item, Type result)
        {
            try
            {
                if(item == null)
                {
                    if(result.IsClass || Nullable.GetUnderlyingType(result) != null)
                    {
                        return null;
                    }
                }

                if(item != null)
                {
                    Type crtType = item.GetType();
                    if(crtType == result || crtType.IsSubclassOf(result))
                    {
                        return item;
                    }
                }

                if(!(item is IConvertible))
                {
                    return Activator.CreateInstance(result);
                }

                return Convert.ChangeType(item, result, null);
            } catch
            {
                return null;
            }
        }
    }
}