﻿using System;
using System.Linq;
using System.Reflection;

namespace Data.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with <see cref="Enum" /> extension methods.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        ///     Gets the value.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static TEnum GetValue<TEnum>(int value, TEnum defaultValue = default)
                where TEnum : struct, IConvertible
        {
            TEnum[] result = GetValues<TEnum>().Where(e => e.ForceCast<TEnum, int>() == value).ToArray();
            if(result.Any())
            {
                return result.First();
            }

            return defaultValue;
        }

        /// <summary>
        ///     Gets all values fot the the values.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <returns></returns>
        public static TEnum[] GetValues<TEnum>()
                where TEnum : struct, IConvertible
        {
            Type type = typeof(TEnum);
            return GetValues(type).Select(e => e.Cast<TEnum>()).ToArray();
        }

        /// <summary>
        ///     Gets all values fot the the values.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Invalid input type: the input type should be an enum</exception>
        public static Enum[] GetValues(Type type)
        {
            // check for the right type
            if(!type.IsEnum)
            {
                throw new ArgumentException("Invalid input type: the input type should be an enum");
            }

            FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.Static);
            Enum[] array = new Enum[fields.Length];
            for(int i = 0; i < fields.Length; i++)
            {
                object obj = fields[i].GetValue(null);
                array[i] = (Enum)obj;
            }

            return array;
        }

        /// <summary>
        ///     Parses the specified other enum.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <param name="otherEnum">The other enum.</param>
        /// <returns></returns>
        public static TEnum Parse<TEnum>(this Enum otherEnum)
                where TEnum : struct, IConvertible
        {
            return Parse<TEnum>(otherEnum.ToString());
        }

        /// <summary>
        ///     Parses the specified enum string.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <param name="enumString">The enum string.</param>
        /// <returns></returns>
        public static TEnum Parse<TEnum>(string enumString)
                where TEnum : struct, IConvertible
        {
            return (TEnum)Enum.Parse(typeof(TEnum), enumString);
        }
    }
}