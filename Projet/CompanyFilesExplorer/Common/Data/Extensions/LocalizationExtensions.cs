﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Data.Localization;
using JetBrains.Annotations;

namespace Data.Extensions
{
    /// <summary>
    ///     This static class provides the developpers with <see cref="LocalizedDescriptionAttribute" /> extension methods.
    /// </summary>
    public static class LocalizationExtensions
    {
        /// <summary>
        ///     Gets the key.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string GetKey(this Enum input)
        {
            return GetLocalizedDescriptionKey(input) ?? GetDescriptionAttributeValue(input) ?? input.ToString();
        }

        /// <summary>
        ///     Get description for the input enum value.
        /// </summary>
        /// <param name="input">The input enum value.</param>
        /// <returns>The corresponding description if any.</returns>
        public static string GetLocalizedDescription(this Enum input)
        {
            Type type = input.GetType();
            if(!type.IsEnum)
            {
                throw new ArgumentException("Invalid input type: the input type should be an enum", "input");
            }

            string key = input.GetKey();
            return GetLocalizedValue(input, key);
        }

        /// <summary>
        ///     Retrieve the enum value's description if any, null otherwise.
        /// </summary>
        /// <param name="input">The input value.</param>
        /// <returns>The enum value's description if any, null otherwise.</returns>
        [CanBeNull]
        private static string GetDescriptionAttributeValue(object input)
        {
            Type type = input.GetType();

            // get the member info
            MemberInfo[] mi = type.GetMember(input.ToString());
            if(0 < mi.Length)
            {
                // retrieve all description attributes
                object[] attributes = mi[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                // get the description info
                if(0 < attributes.Length)
                {
                    // return the corresponding description
                    return ((DescriptionAttribute)attributes[0]).Description;
                }
            }

            // return no description
            return null;
        }

        /// <summary>
        ///     Get the description for the input value.
        /// </summary>
        /// <param name="input">The input value.</param>
        /// <returns>The corresponding description is fuccessful, an empty string otherwise.</returns>
        [CanBeNull]
        private static string GetLocalizedDescriptionKey(object input)
        {
            // get the input type
            Type type = input.GetType();

            // retrieve all resource attributes; should be only one
            object[] attributesLocalizedResource = type.GetCustomAttributes(typeof(LocalizedResourceAttribute), false);

            // get the description info
            if(attributesLocalizedResource.Length <= 0)
            {
                return null;
            }

            // return no description
            // initialize property name with enum's value
            string propertyName = input.ToString();

            // check for a localized description attribute
            MemberInfo[] mi = type.GetMember(input.ToString());
            if(mi.Length != 0)
            {
                // retrieve all localized description attributes
                object[] attributesLocalizedDescription = mi[0].GetCustomAttributes(typeof(LocalizedDescriptionAttribute), false);

                // get the localized description info
                if(attributesLocalizedDescription.Length > 0)
                {
                    // set property name to the corresponding description
                    propertyName = ((LocalizedDescriptionAttribute)attributesLocalizedDescription[0]).Description;
                }
            }

            return propertyName;
        }

        /// <summary>
        ///     Gets the localized value.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        private static string GetLocalizedValue(Enum input, [NotNull] string key)
        {
            Type type = input.GetType();
            LocalizedResourceAttribute[] attributesLocalizedResource = type.GetCustomAttributes(typeof(LocalizedResourceAttribute), false).OfType<LocalizedResourceAttribute>().ToArray();
            Type resourceType = attributesLocalizedResource.Select(a => a.Type).FirstOrDefault();
            if(resourceType == null)
            {
                return key;
            }

            PropertyInfo propertyInfo = resourceType.GetProperty(key, BindingFlags.Static | BindingFlags.Public);
            if(propertyInfo != null)
            {
                // return the corresponding value
                return (string)propertyInfo.GetValue(null, null);
            }

            return key;
        }
    }
}