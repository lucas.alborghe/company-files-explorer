﻿using System;

namespace Data.Exceptions
{
    public class DuplicateParameterException : ArgumentException
    {
        public DuplicateParameterException(string paramName)
                : this("The parameter contains duplicated values", paramName)
        {
        }

        public DuplicateParameterException(string message, string paramName)
                : base(message, paramName)
        {
        }

        public DuplicateParameterException(string message, string paramName, Exception innerException)
                : base(message, paramName, innerException)
        {
        }
    }
}