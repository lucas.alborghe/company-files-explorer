﻿using System;

namespace Data.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        /// <summary>
        ///     Gets the type of the entity.
        /// </summary>
        /// <value>The type of the entity.</value>
        public Type EntityType { get; }

        /// <summary>
        ///     Initializes a new instance of the <see cref="EntityNotFoundException" /> class.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="message">The message.</param>
        public EntityNotFoundException(Type entityType, string message)
                : base(message)
        {
            EntityType = entityType;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="EntityNotFoundException" /> class.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public EntityNotFoundException(Type entityType, string message, Exception innerException)
                : base(message, innerException)
        {
            EntityType = entityType;
        }

        /// <summary>
        ///     Creates the EntityNotFoundException instance with the specified message.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message">The message.</param>
        /// <returns>EntityNotFoundException.</returns>
        public static EntityNotFoundException Create<T>(string message)
        {
            return new EntityNotFoundException(typeof(T), message);
        }

        /// <summary>
        ///     Creates the EntityNotFoundException instance with the specified message.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <returns>EntityNotFoundException.</returns>
        public static EntityNotFoundException Create<T>(string message, Exception innerException)
        {
            return new EntityNotFoundException(typeof(T), message, innerException);
        }
    }
}