using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.Deployment.WindowsInstaller;
using Shared.Utils;

namespace CompanyFilesExplorer.Installer.CustomActions
{
    public class CustomActions
    {
        private const string LaunchingFileExtension = ".bat";

        [CustomAction]
        public static ActionResult Register(Session session)
        {
            ActionResult result;
            string installDirectory = null;
            string installPath;
            session.Log("Registrating protocol handler");
            try
            {
                installDirectory = session.CustomActionData["InstallPath"];
                installPath = Path.Combine(installDirectory, ProtocolHandlerHelper.FileName);
                ProtocolHandlerHelper.Register(installPath);
                session.Log("Protocol handler registered successfully");
                result = ActionResult.Success;
            }
            catch (Exception ex)
            {
                session.Log("Exception occurred during protocol registration as Message: {0}\r\n StackTrace: {1}", ex.Message, ex.StackTrace);
                result = ActionResult.Failure;
            }

            try
            {
                // Prepare the cmds to launch after installation
                // This method can contains the installDirectory because it uses impersonation
                // But it can also not launch the command using the user
                // The cmd are launch by the StartApplications using the user values
                string batPath = Path.Combine(Path.GetTempPath(), $"{Guid.NewGuid()}{LaunchingFileExtension}");
                string systemTrayPath = Path.Combine(installDirectory, ProtocolHandlerHelper.TrayFileName);
                string openSystemTrayCmd = $"\"{systemTrayPath}\"";
                installPath = Path.Combine(installDirectory, ProtocolHandlerHelper.FileName);
                string openExpertModeCmd = $"\"{installPath}\" {AppArgumentsHelper.EntryModeByArgument.First(e => e.Value == EntryMode.Expert).Key}";
                session.Log($"Preparing launching file {batPath}");
                File.WriteAllLines(batPath, new[]
                {
                        "@echo off",
                        "timeout /t 1 /nobreak",
                        $"start \"\" {openSystemTrayCmd}",
                        $"start \"\" {openExpertModeCmd}"
                });
                session.Log($"Prepared launching file {batPath}");
            }
            catch (Exception ex)
            {
                session.Log("Exception occurred during launching bat as Message: {0}\r\n StackTrace: {1}", ex.Message, ex.StackTrace);
                result = ActionResult.Failure;
            }

            if (result == ActionResult.Failure)
            {
                session.Log("Never fail installation, the program must be install anyway");
                result = ActionResult.Success;
            }

            return result;
        }

        [CustomAction]
        public static ActionResult StartApplications(Session session)
        {
            try
            {
                session.Log("Starting applications");
                string tmpPath = Path.GetTempPath();
                string batPath = Directory.GetFiles(tmpPath).Where(f => Path.GetExtension(f) == LaunchingFileExtension).OrderByDescending(File.GetCreationTimeUtc).First();
                session.Log($"Red launching file {batPath}");
                Process.Start(batPath);
                session.Log("Bat launched");
            }
            catch (Exception ex)
            {
                session.Log("Exception occurred during launching cmds as Message: {0}\r\n StackTrace: {1}", ex.Message, ex.StackTrace);
            }

            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult Unregister(Session session)
        {
            try
            {
                ProtocolHandlerHelper.Unregister();
            }
            catch (Exception ex)
            {
                session.Log("Exception occurred during protocol unregistration as Message: {0}\r\n StackTrace: {1}", ex.Message, ex.StackTrace);
            }

            return ActionResult.Success; // Never fail, the program must be uninstall anyway
        }
    }
}