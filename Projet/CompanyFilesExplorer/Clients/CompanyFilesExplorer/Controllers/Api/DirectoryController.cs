﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.Directory;
using Core.Dtos.Repository;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CompanyFilesExplorer.Controllers.Api
{
    [Route("api/directory")]
    public class DirectoryController : Controller
    {
        private readonly IDirectoryService _directoryService;

        public DirectoryController(IDirectoryService directoryService)
        {
            _directoryService = directoryService;
        }

        [HttpGet]
        public async Task<IEnumerable<DirectoryListDto>> GetAll()
        {
            return await _directoryService.GetAllAsync();
        }

        [HttpGet("filter/{filter}")]
        public async Task<IEnumerable<DirectoryListDto>> GetAllByFilter(string filter)
        {
            return await _directoryService.GetAllByFilterAsync(filter);
        }

        [HttpPost("path")]
        public async Task<IEnumerable<DirectoryListDto>> GetAllByPath([FromBody]DirectoryInputDto directory)
        {
            return await _directoryService.GetAllByPathAsync(directory.Path);
        }

        [HttpPost("repository")]
        public async Task<RepositoryDto> GetRepository([FromBody]DirectoryInputDto directory)
        {
            //return await _directoryService.GetRepositoryAsync(path);
            return null;
        }

        [HttpPost("directory")]
        public async Task<DirectoryDto> Get([FromBody]DirectoryInputDto directory)
        {
            //return await _directoryService.GetAsync(path);
            return null;
        }

        [HttpPost]
        public async Task<DirectoryDto> Create([FromBody]DirectoryInputDto input)
        {
            return await _directoryService.SaveAsync(input);
        }

        [HttpDelete("{path}")]
        public async Task<bool> Remove(string path)
        {
            return await _directoryService.RemoveAsync(path);
        }

    }
}