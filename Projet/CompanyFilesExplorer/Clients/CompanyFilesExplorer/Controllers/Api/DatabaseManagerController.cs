﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.Directory;
using Core.Dtos.File;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CompanyFilesExplorer.Controllers.Api
{
    [Route("api/database-manager")]
    [ApiController]
    public class DatabaseManagerController : ControllerBase
    {
        private static IDatabaseManagerService _databaseManagerService;

        public DatabaseManagerController(IDatabaseManagerService databaseManagerService)
        {
            _databaseManagerService = databaseManagerService;
        }

        [HttpPost]
        public async Task<IReadOnlyCollection<FileListDto>> RefreshDatabase()
        {
            return await _databaseManagerService.RefreshDatabaseAsync();
        }

        [HttpPost("path")]
        public async Task<IReadOnlyCollection<FileListDto>> RefreshDatabaseFromPath([FromBody]DirectoryInputDto directory)
        {
            return await _databaseManagerService.RefreshDatabaseFromPathAsync(directory.Path);
        }

    }
}