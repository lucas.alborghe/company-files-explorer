﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.User;
using Core.Dtos.VirtualDirectory;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CompanyFilesExplorer.Controllers.Api
{
    [Route("api/user")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IVirtualDirectoryService _virtualDirectoryService;

        public UserController(IUserService userService, IVirtualDirectoryService virtualDirectoryService)
        {
            _userService = userService;
            _virtualDirectoryService = virtualDirectoryService;
        }

        [HttpGet]
        public async Task<IEnumerable<UserListDto>> GetAll()
        {
            return await _userService.GetAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<UserDto> Get(int id)
        {
            return await _userService.GetAsync(id);
        }

        [HttpGet("current")]
        public async Task<UserDto> GeCurrentUser()
        {
            string userName = Environment.UserName;
            UserDto currentUser = await _userService.GetAsync(userName);
            if (currentUser == null)
            {
                // Create the user
                currentUser = await _userService.CreateAsync(new UserInputDto()
                {
                    Login = userName,
                    IsAdmin = 0
                });
                // Create his default virtual directory
                await _virtualDirectoryService.CreateAsync(new VirtualDirectoryInputDto()
                {
                    UserId = currentUser.Id,
                    Name = "My favorites"
                });
            }
            return currentUser;
        }

        [HttpGet("virtual-directories/{id}")]
        public async Task<IEnumerable<VirtualDirectoryListDto>> GetVirtualDirectories(int id)
        {
            return await _userService.GetVirtualDirectoriesAsync(id);
        }

        [HttpPost]
        public async Task<UserDto> Create([FromBody]UserInputDto input)
        {
            return await _userService.CreateAsync(input);
        }

        [HttpPut("{id}")]
        public async Task<UserDto> Update(int id, [FromBody]UserInputDto input)
        {
            return await _userService.UpdateAsync(id, input);
        }

        [HttpDelete("{id}")]
        public async Task<bool> Remove(int id)
        {
            return await _userService.RemoveAsync(id);
        }

    }
}