﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.Directory;
using Core.Dtos.File;
using Core.Dtos.Repository;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CompanyFilesExplorer.Controllers.Api
{
    [Route("api/file")]
    public class FileController : Controller
    {
        private readonly IFileService _fileService;

        public FileController(IFileService fileService)
        {
            _fileService = fileService;
        }

        [HttpGet]
        public async Task<IEnumerable<FileListDto>> GetAll()
        {
            return await _fileService.GetAllAsync();
        }

        [HttpPost("path")]
        public async Task<IEnumerable<FileListDto>> GetAllByPath([FromBody]DirectoryInputDto directory)
        {
            return await _fileService.GetAllByPathAsync(directory.Path);
        }

        [HttpGet("filter/filename/{filter}")]
        public async Task<IEnumerable<FileListDto>> GetAllByFilename(string filter)
        {
            return await _fileService.GetAllByFilenameAsync(filter);
        }

        [HttpGet("filter/path/{filter}")]
        public async Task<IEnumerable<FileListDto>> GetAllByFilter(string filter)
        {
            return await _fileService.GetAllByFilterAsync(filter);
        }

        [HttpGet("repository/{id}")]
        public async Task<RepositoryDto> GetRepository(int id)
        {
            return await _fileService.GetRepositoryAsync(id);
        }

        [HttpGet("{id}")]
        public async Task<FileDto> Get(int id)
        {
            return await _fileService.GetAsync(id);
        }

        [HttpGet("json/{id}")]
        public async Task<string> GetJson(int id)
        {
            return await _fileService.GetJsonAsync(id);
        }

        [HttpPost("physical")]
        public async Task<FileDto> CreatePhysicalFile([FromBody]FileMoveInputDto input)
        {
            return await _fileService.CreatePhysicalFile(input);
        }

        [HttpPost]
        public async Task<FileDto> Create([FromBody]FileInputDto input)
        {
            return await _fileService.SaveAsync(input);
        }

        [HttpDelete("{id}")]
        public async Task<bool> Remove(int id)
        {
            return await _fileService.RemoveAsync(id);
        }

    }
}