﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.File;
using Core.Dtos.Metadata;
using Core.Dtos.Procedure;
using Core.Dtos.VirtualDirectory;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CompanyFilesExplorer.Controllers.Api
{
    [Route("api/procedure")]
    public class ProcedureController : Controller
    {
        private readonly IProcedureService _procedureService;

        public ProcedureController(IProcedureService procedureService)
        {
            _procedureService = procedureService;
        }

        [HttpGet]
        public async Task<IEnumerable<ProcedureListDto>> GetAll()
        {
            return await _procedureService.GetAllAsync();
        }

        [HttpGet("files/{rule}")]
        public async Task<IEnumerable<FileListDto>> GetFilesFromRule(string rule)
        {
            return await _procedureService.GetFilesFromRuleAsync(rule);
        }

        [HttpGet("metadata/{id}")]
        public async Task<IEnumerable<MetadataListDto>> GetMetadata(int id)
        {
            return await _procedureService.GetMetadataAsync(id);
        }

        [HttpGet("virtual-directories/{id}")]
        public async Task<IEnumerable<VirtualDirectoryListDto>> GetVirtualDirectories(int id)
        {
            return await _procedureService.GetVirtualDirectoriesAsync(id);
        }

        [HttpGet("{id}")]
        public async Task<ProcedureDto> Get(int id)
        {
            return await _procedureService.GetAsync(id);
        }

        [HttpPost]
        public async Task<ProcedureDto> Create([FromBody]ProcedureInputDto input)
        {
            return await _procedureService.CreateAsync(input);
        }

        [HttpPut("{id}")]
        public async Task<ProcedureDto> Update(int id, [FromBody]ProcedureInputDto input)
        {
            return await _procedureService.UpdateAsync(id, input);
        }

        [HttpDelete("{id}")]
        public async Task<bool> Remove(int id)
        {
            return await _procedureService.RemoveAsync(id);
        }

    }
}