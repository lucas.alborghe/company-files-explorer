﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.File;
using Core.Dtos.Procedure;
using Core.Dtos.User;
using Core.Dtos.VirtualDirectory;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CompanyFilesExplorer.Controllers.Api
{
    [Route("api/virtual-directory")]
    public class VirtualDirectoryController : Controller
    {
        private readonly IVirtualDirectoryService _virtualDirectoryService;

        public VirtualDirectoryController(IVirtualDirectoryService virtualDirectoryService)
        {
            _virtualDirectoryService = virtualDirectoryService;
        }

        [HttpGet]
        public async Task<IEnumerable<VirtualDirectoryListDto>> GetAll()
        {
            return await _virtualDirectoryService.GetAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<VirtualDirectoryDto> Get(int id)
        {
            return await _virtualDirectoryService.GetAsync(id);
        }

        [HttpGet("procedure/{id}")]
        public async Task<ProcedureDto> GetProcedure(int id)
        {
            return await _virtualDirectoryService.GetProcedureAsync(id);
        }

        [HttpGet("user/{id}")]
        public async Task<UserDto> GetUser(int id)
        {
            return await _virtualDirectoryService.GetUserAsync(id);
        }

        [HttpGet("parent/{id}")]
        public async Task<VirtualDirectoryDto> GetParent(int id)
        {
            return await _virtualDirectoryService.GetParentAsync(id);
        }

        [HttpGet("children/{id}")]
        public async Task<IEnumerable<VirtualDirectoryListDto>> GetChildren(int id)
        {
            return await _virtualDirectoryService.GetChildrenAsync(id);
        }

        [HttpGet("user-preferences/{id}")]
        public async Task<IEnumerable<VirtualDirectoryPreferenceListDto>> GetUserPreferences(int id)
        {
            return await _virtualDirectoryService.GetPreferencesAsync(id);
        }

        [HttpGet("files/{id}")]
        public async Task<IEnumerable<FileListDto>> GetFiles(int id)
        {
            return await _virtualDirectoryService.GetFilesVirtualDirectoryAsync(id);
        }

        [HttpPost]
        public async Task<VirtualDirectoryDto> Create([FromBody]VirtualDirectoryInputDto input)
        {
            return await _virtualDirectoryService.CreateAsync(input);
        }

        [HttpPut("{id}")]
        public async Task<VirtualDirectoryDto> Update(int id, [FromBody]VirtualDirectoryInputDto input)
        {
            return await _virtualDirectoryService.UpdateAsync(id, input);
        }

        [HttpDelete("{id}")]
        public async Task<bool> Remove(int id)
        {
            return await _virtualDirectoryService.RemoveAsync(id);
        }

        [HttpPost("preference")]
        public async Task<VirtualDirectoryPreferenceDto> CreateUserPreference([FromBody] VirtualDirectoryPreferenceInputDto input)
        {
            return await _virtualDirectoryService.SaveFileVirtualDirectoryAsync(input.VirtualDirectoryId, input.FileId, input.Filename);
        }

        [HttpPut("preference")]
        public async Task<VirtualDirectoryPreferenceDto> UpdateUserPreference([FromBody]VirtualDirectoryPreferenceInputDto input)
        {
            return await _virtualDirectoryService.SaveFileVirtualDirectoryAsync(input.VirtualDirectoryId, input.FileId, input.Filename);
        }

        [HttpDelete("preference/{vdId}/{fId}")]
        public async Task<bool> RemoveUserPreference(int vdId, int fId)
        {
            return await _virtualDirectoryService.RemoveFileVirtualDirectoryAsync(vdId, fId);
        }

    }
}