﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.Directory;
using Core.Dtos.File;
using Core.Dtos.Repository;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CompanyFilesExplorer.Controllers.Api
{
    [Route("api/repository")]
    public class RepositoryController : Controller
    {
        private readonly IRepositoryService _repositoryService;

        public RepositoryController(IRepositoryService repositoryService)
        {
            _repositoryService = repositoryService;
        }

        [HttpGet]
        public async Task<IEnumerable<RepositoryListDto>> GetAll()
        {
            return await _repositoryService.GetAllAsync();
        }

        [HttpGet("directories/{id}")]
        public async Task<IEnumerable<DirectoryListDto>> GetDirectories(int id)
        {
            return await _repositoryService.GetDirectoriesAsync(id);
        }

        [HttpGet("files/{id}")]
        public async Task<IEnumerable<FileListDto>> GetFiles(int id)
        {
            return await _repositoryService.GetFilesAsync(id);
        }

        [HttpGet("{id}")]
        public async Task<RepositoryDto> Get(int id)
        {
            return await _repositoryService.GetAsync(id);
        }

        [HttpPost]
        public async Task<RepositoryDto> Create([FromBody]RepositoryInputDto input)
        {
            return await _repositoryService.CreateAsync(input);
        }

        [HttpPut("{id}")]
        public async Task<RepositoryDto> Update(int id, [FromBody]RepositoryInputDto input)
        {
            return await _repositoryService.UpdateAsync(id, input);
        }

        [HttpDelete("{id}")]
        public async Task<bool> Remove(int id)
        {
            return await _repositoryService.RemoveAsync(id);
        }

    }
}