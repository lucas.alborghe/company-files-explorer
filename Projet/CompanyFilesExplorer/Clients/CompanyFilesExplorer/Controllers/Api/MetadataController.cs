﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.Metadata;
using Core.Dtos.Procedure;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CompanyFilesExplorer.Controllers.Api
{
    [Route("api/metadata")]
    public class MetadataController : Controller
    {
        private readonly IMetadataService _metadataService;

        public MetadataController(IMetadataService metadataService)
        {
            _metadataService = metadataService;
        }

        [HttpGet]
        public async Task<IEnumerable<MetadataListDto>> GetAll()
        {
            return await _metadataService.GetAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<MetadataDto> Get(int id)
        {
            return await _metadataService.GetAsync(id);
        }

        [HttpGet("procedure/{id}")]
        public async Task<ProcedureDto> GetProcedure(int id)
        {
            return await _metadataService.GetProcedureAsync(id);
        }

        [HttpPost]
        public async Task<MetadataDto> Create([FromBody]MetadataInputDto input)
        {
            return await _metadataService.CreateAsync(input);
        }

        [HttpPut("{id}")]
        public async Task<MetadataDto> Update(int id, [FromBody]MetadataInputDto input)
        {
            return await _metadataService.UpdateAsync(id, input);
        }

        [HttpDelete("{id}")]
        public async Task<bool> Remove(int id)
        {
            return await _metadataService.RemoveAsync(id);
        }

    }
}