﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.Extension;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CompanyFilesExplorer.Controllers.Api
{
    [Route("api/extension")]
    public class ExtensionController : Controller
    {
        private readonly IAllowedExtensionService _extensionService;

        public ExtensionController(IAllowedExtensionService extensionService)
        {
            _extensionService = extensionService;
        }

        [HttpGet]
        public async Task<IEnumerable<ExtensionListDto>> GetAll()
        {
            return await _extensionService.GetAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<ExtensionDto> Get(int id)
        {
            return await _extensionService.GetAsync(id);
        }

        [HttpPost]
        public async Task<ExtensionDto> Create([FromBody]ExtensionInputDto input)
        {
            return await _extensionService.CreateAsync(input);
        }

        [HttpPut("{id}")]
        public async Task<ExtensionDto> Update(int id, [FromBody]ExtensionInputDto input)
        {
            return await _extensionService.UpdateAsync(id, input);
        }

        [HttpDelete("{id}")]
        public async Task<bool> Remove(int id)
        {
            return await _extensionService.RemoveAsync(id);
        }

    }
}