import { Injectable, Inject } from "@angular/core";
import { Headers, Http, Response, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Rx";

import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Injectable()
export class ApiService {

  constructor(private readonly http: Http, @Inject("BASE_URL") private readonly baseUrl: string) { }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': "application/json",
      'Accept': "application/json"
    };
    return new Headers(headersConfig);
  }

  get(path: string, params: URLSearchParams = new URLSearchParams()): Observable<any> {
    return this.http.get(`${this.baseUrl}${path}`, { headers: this.setHeaders(), search: params })
      .catch(this.formatErrors)
      .map((res: Response) => res.json());
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(`${this.baseUrl}${path}`, JSON.stringify(body), { headers: this.setHeaders() })
      .catch(this.formatErrors)
      .map((res: Response) => res.text() ? res.json() : null);
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(`${this.baseUrl}${path}`, JSON.stringify(body), { headers: this.setHeaders() })
      .catch(this.formatErrors)
      .map((res: Response) => res.text() ? res.json() : null);
  }

  delete(path: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}${path}`, { headers: this.setHeaders() })
      .catch(this.formatErrors);
  }

  private formatErrors(error: any) {
    return Observable.throw(error.json());
  }
}
