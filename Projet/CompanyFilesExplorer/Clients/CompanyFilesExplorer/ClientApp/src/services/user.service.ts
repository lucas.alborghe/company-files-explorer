import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { ApiService } from "./api.service";

import { UserDto } from "../entities/user/UserDto";
import { UserInputDto } from "../entities/user/UserInputDto";
import { UserListDto } from "../entities/user/UserListDto";

import { VirtualDirectoryListDto } from "../entities/virtualDirectory/VirtualDirectoryListDto";

@Injectable()
export class UserService {

  private static readonly serviceUrl = "api/user/";

  constructor(private readonly apiService: ApiService) { }

  getAllUsers(): Observable<UserListDto[]> {
    return this.apiService.get(UserService.serviceUrl);
  }

  getAllVirtualDirectories(id: number): Observable<VirtualDirectoryListDto[]> {
    return this.apiService.get(UserService.serviceUrl + "virtual-directories/" + id);
  }

  getUser(id: number): Observable<UserDto> {
    return this.apiService.get(UserService.serviceUrl + id);
  }

  getCurrentUser(): Observable<UserDto> {
    return this.apiService.get(UserService.serviceUrl + "current");
  }

  createUser(user: UserInputDto): Observable<UserDto> {
    return this.apiService.post(UserService.serviceUrl, user);
  }

  updateUser(id: number, user: UserInputDto): Observable<UserDto> {
    return this.apiService.put(UserService.serviceUrl + id, user);
  }

  deleteUser(id: number): Observable<boolean> {
    return this.apiService.delete(UserService.serviceUrl + id);
  }

}
