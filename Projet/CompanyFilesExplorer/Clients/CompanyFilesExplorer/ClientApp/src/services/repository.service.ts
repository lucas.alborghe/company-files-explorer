import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { ApiService } from "./api.service";

import { RepositoryDto } from '../entities/Repository/RepositoryDto';
import { RepositoryInputDto } from '../entities/Repository/RepositoryInputDto';
import { RepositoryListDto } from '../entities/Repository/RepositoryListDto';

import { DirectoryListDto } from '../entities/Directory/DirectoryListDto';
import { FileListDto } from '../entities/File/FileListDto';

@Injectable()
export class RepositoryService {

  private static readonly serviceUrl = "api/repository/";

  constructor(private readonly apiService: ApiService) { }

  getAllRepositories(): Observable<RepositoryListDto[]> {
    return this.apiService.get(RepositoryService.serviceUrl);
  }

  getAllDirectories(id: number): Observable<DirectoryListDto[]> {
    return this.apiService.get(RepositoryService.serviceUrl + "directories/" + id);
  }

  getAllFiles(id: number): Observable<FileListDto[]> {
    return this.apiService.get(RepositoryService.serviceUrl + "files/" + id);
  }

  getRepository(id: number): Observable<RepositoryDto> {
    return this.apiService.get(RepositoryService.serviceUrl + id);
  }

  createRepository(repository: RepositoryInputDto): Observable<RepositoryDto> {
    return this.apiService.post(RepositoryService.serviceUrl, repository);
  }

  updateRepository(id: number, repository: RepositoryInputDto): Observable<RepositoryDto> {
    return this.apiService.put(RepositoryService.serviceUrl + id, repository);
  }

  deleteRepository(id: number): Observable<boolean> {
    return this.apiService.delete(RepositoryService.serviceUrl + id);
  }

}
