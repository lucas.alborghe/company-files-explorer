import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { ApiService } from "./api.service";

import { FileDto } from '../entities/File/FileDto';
import { FileInputDto } from '../entities/File/FileInputDto';
import { FileListDto } from '../entities/File/FileListDto';
import { FileMoveInputDto } from '../entities/File/FileMoveInputDto';

import { DirectoryDto } from '../entities/Directory/DirectoryDto';
import { DirectoryInputDto } from '../entities/Directory/DirectoryInputDto';
import { DirectoryListDto } from '../entities/Directory/DirectoryListDto';

import { RepositoryDto } from '../entities/Repository/RepositoryDto';

@Injectable()
export class FileService {

  private static readonly protocolScheme = "filesexplorer://";
  private static readonly serviceUrl = "api/file/";

  constructor(private readonly apiService: ApiService) { }

  getAllFiles(): Observable<FileListDto[]> {
    return this.apiService.get(FileService.serviceUrl);
  }

  getAllFilesByPath(directory: DirectoryInputDto): Observable<FileListDto[]> {
    return this.apiService.post(FileService.serviceUrl + "path", directory);
  }

  getAllFilesByFilename(filter: string): Observable<FileListDto[]> {
    return this.apiService.get(FileService.serviceUrl + "filter/filename/" + filter);
  }

  getAllFilesByFilter(filter: string): Observable<FileListDto[]> {
    return this.apiService.get(FileService.serviceUrl + "filter/path/" + filter);
  }

  getRepository(id: number): Observable<RepositoryDto> {
    return this.apiService.get(FileService.serviceUrl + "repository/" + id);
  }

  getFile(id: number): Observable<FileDto> {
    return this.apiService.get(FileService.serviceUrl + id);
  }

  openFile(id: number): string {
    return `${FileService.protocolScheme}${id}`;
  }

  saveNewFile(files: FileMoveInputDto): Observable<FileDto> {
    return this.apiService.post(FileService.serviceUrl + "physical", files);
  }

  createFile(file: FileInputDto): Observable<FileDto> {
    return this.apiService.post(FileService.serviceUrl, file);
  }

  updateFile(id: number, file: FileInputDto): Observable<FileDto> {
    return this.apiService.put(FileService.serviceUrl + id, file);
  }

  deleteFile(id: number): Observable<boolean> {
    return this.apiService.delete(FileService.serviceUrl + id);
  }

}
