import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { ApiService } from "./api.service";

import { ExtensionDto } from '../entities/Extension/ExtensionDto';
import { ExtensionInputDto } from '../entities/Extension/ExtensionInputDto';
import { ExtensionListDto } from '../entities/Extension/ExtensionListDto';

@Injectable()
export class ExtensionService {

  private static readonly serviceUrl = "api/extension/";

  constructor(private readonly apiService: ApiService) { }

  getAllExtensions(): Observable<ExtensionListDto[]> {
    return this.apiService.get(ExtensionService.serviceUrl);
  }

  getExtension(id: number): Observable<ExtensionDto> {
    return this.apiService.get(ExtensionService.serviceUrl + id);
  }

  createExtension(extension: ExtensionInputDto): Observable<ExtensionDto> {
    return this.apiService.post(ExtensionService.serviceUrl, extension);
  }

  updateExtension(id: number, extension: ExtensionInputDto): Observable<ExtensionDto> {
    return this.apiService.put(ExtensionService.serviceUrl + id, extension);
  }

  deleteExtension(id: number): Observable<boolean> {
    return this.apiService.delete(ExtensionService.serviceUrl + id);
  }

}
