import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { ApiService } from "./api.service";

import { DirectoryDto } from '../entities/Directory/DirectoryDto';
import { DirectoryInputDto } from '../entities/Directory/DirectoryInputDto';
import { DirectoryListDto } from '../entities/Directory/DirectoryListDto';

import { RepositoryDto } from '../entities/Repository/RepositoryDto';

@Injectable()
export class DirectoryService {

  private static readonly serviceUrl = "api/directory/";

  constructor(private readonly apiService: ApiService) { }

  getAllDirectories(): Observable<DirectoryListDto[]> {
    return this.apiService.get(DirectoryService.serviceUrl);
  }

  getAllDirectoriesByPath(directory: DirectoryInputDto): Observable<DirectoryListDto[]> {
    return this.apiService.post(DirectoryService.serviceUrl + "path", directory);
  }

  getAllDirectoriesByFilter(filter: string): Observable<DirectoryListDto[]> {
    return this.apiService.get(DirectoryService.serviceUrl + "filter/" + filter);
  }

  getRepository(id: number): Observable<RepositoryDto> {
    return this.apiService.get(DirectoryService.serviceUrl + "repository/" + id);
  }

  getDirectory(path: string): Observable<DirectoryDto> {
    return this.apiService.get(DirectoryService.serviceUrl + path);
  }

  createDirectory(directory: DirectoryInputDto): Observable<DirectoryDto> {
    return this.apiService.post(DirectoryService.serviceUrl, directory);
  }

  updateDirectory(path: string, directory: DirectoryInputDto): Observable<DirectoryDto> {
    return this.apiService.put(DirectoryService.serviceUrl + path, directory);
  }

  deleteDirectory(path: string): Observable<boolean> {
    return this.apiService.delete(DirectoryService.serviceUrl + path);
  }

}
