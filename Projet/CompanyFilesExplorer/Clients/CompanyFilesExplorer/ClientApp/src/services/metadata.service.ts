import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { ApiService } from "./api.service";

import { MetadataDto } from '../entities/Metadata/MetadataDto';
import { MetadataInputDto } from '../entities/Metadata/MetadataInputDto';
import { MetadataListDto } from '../entities/Metadata/MetadataListDto';

import { ProcedureDto } from '../entities/Procedure/ProcedureDto';
import { ProcedureInputDto } from '../entities/Procedure/ProcedureInputDto';
import { ProcedureListDto } from '../entities/Procedure/ProcedureListDto';

@Injectable()
export class MetadataService {

  private static readonly serviceUrl = "api/metadata/";

  constructor(private readonly apiService: ApiService) { }

  getAllMetadata(): Observable<MetadataListDto[]> {
    return this.apiService.get(MetadataService.serviceUrl);
  }

  getMetadata(id: number): Observable<MetadataDto> {
    return this.apiService.get(MetadataService.serviceUrl + id);
  }

  getProcedure(id: number): Observable<ProcedureDto> {
    return this.apiService.get(MetadataService.serviceUrl + "procedure/" + id);
  }

  createMetadata(metadata: MetadataInputDto): Observable<MetadataDto> {
    return this.apiService.post(MetadataService.serviceUrl, metadata);
  }

  updateMetadata(id: number, metadata: MetadataInputDto): Observable<MetadataDto> {
    return this.apiService.put(MetadataService.serviceUrl + id, metadata);
  }

  deleteMetadata(id: number): Observable<boolean> {
    return this.apiService.delete(MetadataService.serviceUrl + id);
  }

}
