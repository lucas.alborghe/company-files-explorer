import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { ApiService } from "./api.service";

import { ProcedureDto } from '../entities/Procedure/ProcedureDto';
import { ProcedureInputDto } from '../entities/Procedure/ProcedureInputDto';
import { ProcedureListDto } from '../entities/Procedure/ProcedureListDto';

import { MetadataDto } from '../entities/Metadata/MetadataDto';
import { MetadataInputDto } from '../entities/Metadata/MetadataInputDto';
import { MetadataListDto } from '../entities/Metadata/MetadataListDto';

import { FileListDto } from '../entities/File/FileListDto';

@Injectable()
export class ProcedureService {

  private static readonly serviceUrl = "api/procedure/";

  constructor(private readonly apiService: ApiService) { }

  getAllProcedures(): Observable<ProcedureListDto[]> {
    return this.apiService.get(ProcedureService.serviceUrl);
  }

  getFilesFromRule(rule: string): Observable<FileListDto[]> {
    return this.apiService.get(ProcedureService.serviceUrl + "files/" + rule);
  }

  getMetadata(id: number): Observable<MetadataListDto[]> {
    return this.apiService.get(ProcedureService.serviceUrl + "metadata/" + id);
  }

  getProcedure(id: number): Observable<ProcedureDto> {
    return this.apiService.get(ProcedureService.serviceUrl + id);
  }

  createProcedure(procedure: ProcedureInputDto): Observable<ProcedureDto> {
    return this.apiService.post(ProcedureService.serviceUrl, procedure);
  }

  updateProcedure(id: number, procedure: ProcedureInputDto): Observable<ProcedureDto> {
    return this.apiService.put(ProcedureService.serviceUrl + id, procedure);
  }

  deleteProcedure(id: number): Observable<boolean> {
    return this.apiService.delete(ProcedureService.serviceUrl + id);
  }

}
