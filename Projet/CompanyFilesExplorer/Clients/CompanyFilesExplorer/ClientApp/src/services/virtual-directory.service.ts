import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { ApiService } from "./api.service";

import { FileDto } from '../entities/File/FileDto';
import { FileInputDto } from '../entities/File/FileInputDto';
import { FileListDto } from '../entities/File/FileListDto';

import { VirtualDirectoryDto } from '../entities/VirtualDirectory/VirtualDirectoryDto';
import { VirtualDirectoryInputDto } from '../entities/VirtualDirectory/VirtualDirectoryInputDto';
import { VirtualDirectoryListDto } from '../entities/VirtualDirectory/VirtualDirectoryListDto';

import { UserPreferenceDto } from '../entities/UserPreference/UserPreferenceDto';
import { UserPreferenceInputDto } from '../entities/UserPreference/UserPreferenceInputDto';
import { UserPreferenceListDto } from '../entities/UserPreference/UserPreferenceListDto';

import { ProcedureDto } from '../entities/Procedure/ProcedureDto';

@Injectable()
export class VirtualDirectoryService {

  private static readonly serviceUrl = "api/virtual-directory/";

  constructor(private readonly apiService: ApiService) { }

  getAllVirtualDirectories(): Observable<VirtualDirectoryListDto[]> {
    return this.apiService.get(VirtualDirectoryService.serviceUrl);
  }

  getFiles(id: number): Observable<FileListDto[]> {
    return this.apiService.get(VirtualDirectoryService.serviceUrl + "files/" + id);
  }

  getChildren(id: number): Observable<VirtualDirectoryListDto[]> {
    return this.apiService.get(VirtualDirectoryService.serviceUrl + "children/" + id);
  }

  getUserPreferences(id: number): Observable<UserPreferenceListDto[]> {
    return this.apiService.get(VirtualDirectoryService.serviceUrl + "user-preferences/" + id);
  }

  getParent(id: number): Observable<VirtualDirectoryDto> {
    return this.apiService.get(VirtualDirectoryService.serviceUrl + "parent/" + id);
  }

  getProcedure(id: number): Observable<ProcedureDto> {
    return this.apiService.get(VirtualDirectoryService.serviceUrl + "procedure/" + id);
  }

  getVirtualDirectory(id: number): Observable<VirtualDirectoryDto> {
    return this.apiService.get(VirtualDirectoryService.serviceUrl + id);
  }

  createVirtualDirectory(virtualDirectory: VirtualDirectoryInputDto): Observable<VirtualDirectoryDto> {
    return this.apiService.post(VirtualDirectoryService.serviceUrl, virtualDirectory);
  }

  updateVirtualDirectory(id: number, virtualDirectory: VirtualDirectoryInputDto): Observable<VirtualDirectoryDto> {
    return this.apiService.put(VirtualDirectoryService.serviceUrl + id, virtualDirectory);
  }

  deleteVirtualDirectory(id: number): Observable<boolean> {
    return this.apiService.delete(VirtualDirectoryService.serviceUrl + id);
  }
  
  createUserPreference(userPreference: UserPreferenceInputDto): Observable<UserPreferenceDto> {
    return this.apiService.post(VirtualDirectoryService.serviceUrl + "preference", userPreference);
  }

  updateUserPreference(userPreference: UserPreferenceInputDto): Observable<UserPreferenceDto> {
    return this.apiService.put(VirtualDirectoryService.serviceUrl + "preference", userPreference);
  }

  deleteUserPreference(vdId: number, fId: number): Observable<boolean> {
    return this.apiService.delete(VirtualDirectoryService.serviceUrl + "preference/" + vdId + "/" + fId);
  }


}
