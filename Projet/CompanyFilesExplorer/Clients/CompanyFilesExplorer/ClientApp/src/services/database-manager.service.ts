import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { ApiService } from "./api.service";

import { FileListDto } from '../entities/File/FileListDto';
import { DirectoryInputDto } from '../entities/Directory/DirectoryInputDto';

@Injectable()
export class DatabaseManagerService {

  private static readonly serviceUrl = "api/database-manager/";

  constructor(private readonly apiService: ApiService) { }

  refreshDatabase(): Observable<FileListDto[]> {
    return this.apiService.post(DatabaseManagerService.serviceUrl);
  }

  refreshDatabaseFromPath(directory: DirectoryInputDto): Observable<FileListDto[]> {
    return this.apiService.post(DatabaseManagerService.serviceUrl + "path", directory);
  }

}
