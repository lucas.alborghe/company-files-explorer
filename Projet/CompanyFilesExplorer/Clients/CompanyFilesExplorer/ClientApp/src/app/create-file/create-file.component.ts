import { Component, OnInit } from '@angular/core';

import { FileService } from '../../services/file.service';
import { ProcedureService } from '../../services/procedure.service';
import { DirectoryService } from '../../services/directory.service';
import { UserService } from '../../services/user.service';

import { UserDto } from '../../entities/User/UserDto';
import { DirectoryListDto } from '../../entities/Directory/DirectoryListDto';
import { FileMoveInputDto } from '../../entities/File/FileMoveInputDto';
import { ProcedureDto } from '../../entities/Procedure/ProcedureDto';
import { ProcedureListDto } from '../../entities/Procedure/ProcedureListDto';
import { MetadataDto } from '../../entities/Metadata/MetadataDto';
import { MetadataListDto } from '../../entities/Metadata/MetadataListDto';


@Component({
  selector: 'create-file',
  templateUrl: './create-file.component.html',
  styleUrls: ['./create-file.component.css']
})
export class CreateFileComponent implements OnInit {

  currentUser: UserDto;

  procedureId: number = -1;
  procedure: ProcedureDto;
  procedures: ProcedureListDto[];

  metadataId: number = -1;
  metadata: MetadataDto;
  metadatas: MetadataListDto[];

  metadata1: MetadataListDto;
  metadata2: MetadataListDto;

  directoryId: number = -1;
  directories: DirectoryListDto[];

  fileAbsolutePath: string;
  oldFileDirectory: string;
  oldFileName: string;
  oldFileExtension: string;
  fileDirectory: string;
  filenameMetadata1: string;
  filenameMetadata2: string;

  createModel = new FileMoveInputDto(0, '', '', '', 0, '', '', '');

  constructor(
    private userService: UserService,
    private fileService: FileService,
    private procedureService: ProcedureService,
    private directoryService: DirectoryService) { }

  ngOnInit() {
    this.getAllProcedures();
    this.getDirectories();
  }

  private getAllProcedures() {
    this.procedureService.getAllProcedures().subscribe(result => {
      this.procedures = result as ProcedureListDto[];
    });
  }

  changed(id: number) {
    this.procedureService.getProcedure(id).subscribe(result => {
      this.procedure = result as ProcedureDto;
      if (this.procedure != null) {
        this.procedureService.getMetadata(id).subscribe(result => {
          this.metadatas = result as MetadataListDto[];
          if (this.metadatas != null)
            if (this.metadatas.length > 0) {
              this.metadata1 = this.metadatas[0];
              if (this.metadatas.length > 1) {
                this.metadata2 = this.metadatas[1];
              }
            }
        });
      }
    });
  }

  private getDirectories() {
    this.directoryService.getAllDirectories().subscribe(result => {
      this.directories = result as DirectoryListDto[];
    });
  }

  // Not good at all
  onCreateSubmit() {
    var re = /(?:\.([^.]+))?$/;
    this.oldFileExtension = re.exec(this.fileAbsolutePath)[1];
    this.oldFileName = this.fileAbsolutePath.replace(/^.*[\\\/]/, '');
    this.oldFileDirectory = this.fileAbsolutePath.slice(0, (this.fileAbsolutePath.length - this.oldFileName.length - 1));

    this.createModel.sourceRepositoryId = 1004;
    this.createModel.sourceFilepath = this.oldFileDirectory;
    this.createModel.sourceFilename = this.oldFileName.slice(0, this.oldFileName.length - this.oldFileExtension.length - 1);
    this.createModel.sourceExtension = "." + this.oldFileExtension;
    this.createModel.destinationRepositoryId = 1004;
    this.createModel.destinationFilepath = this.fileDirectory;
    this.createModel.destinationFilename = this.filenameMetadata1 + "_" + this.filenameMetadata2;
    this.createModel.destinationExtension = "." + this.oldFileExtension;

    this.fileService.saveNewFile(this.createModel).subscribe(result => {
      if (result != null) {
        this.createModel = new FileMoveInputDto(0, '', '', '', 0, '', '', '');
      }
    });
  }
  
}
