import { Component, OnInit, Input } from '@angular/core';

import { UserService } from '../../../services/user.service';
import { FileService } from '../../../services/file.service';
import { VirtualDirectoryService } from '../../../services/virtual-directory.service';

import { UserDto } from '../../../entities/User/UserDto';
import { FileListDto } from '../../../entities/File/FileListDto';
import { UserPreferenceInputDto } from '../../../entities/UserPreference/UserPreferenceInputDto';
import { VirtualDirectoryListDto } from '../../../entities/VirtualDirectory/VirtualDirectoryListDto';

@Component({
  selector: 'create-user-preference',
  templateUrl: './create-user-preference.component.html',
  styleUrls: ['./create-user-preference.component.css']
})
export class CreateUserPreferenceComponent implements OnInit {

  @Input('vdId') currentVirtualDirectoryId: number;

  currentUser: UserDto;
  files: FileListDto[];
  virtualDirectories: VirtualDirectoryListDto[];
  createPreferenceModel = new UserPreferenceInputDto(0, 0);

  constructor(
    private userService: UserService,
    private fileService: FileService,
    private virtualDirectoryService: VirtualDirectoryService) { }

  ngOnInit() {
    this.getAllVirtualDirectories();
    this.getAllFiles();
    if (this.currentVirtualDirectoryId != null && this.currentVirtualDirectoryId > 0)
      this.createPreferenceModel.virtualDirectoryId = this.currentVirtualDirectoryId;
  }

  onCreateSubmit() {
    this.virtualDirectoryService.createUserPreference(this.createPreferenceModel).subscribe(result => {
      if (result)
        this.createPreferenceModel = new UserPreferenceInputDto(0, 0);
    });
  }

  private getAllVirtualDirectories() {
    this.userService.getCurrentUser().subscribe(result => {
      this.currentUser = result as UserDto;
      if (this.currentUser != null) {
        this.userService.getAllVirtualDirectories(this.currentUser.id).subscribe(result => {
          this.virtualDirectories = result as VirtualDirectoryListDto[];
        });
      }
    });
  }

  private getAllFiles() {
    this.fileService.getAllFiles().subscribe(result => {
      this.files = result as FileListDto[];
    });
  }

  private cancel() {
    this.createPreferenceModel = new UserPreferenceInputDto(0, 0);
  }

}
