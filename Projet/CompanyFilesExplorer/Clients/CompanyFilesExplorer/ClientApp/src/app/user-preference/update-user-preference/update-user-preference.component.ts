import { Component, OnInit, Input } from '@angular/core';

import { UserService } from '../../../services/user.service';
import { FileService } from '../../../services/file.service';
import { VirtualDirectoryService } from '../../../services/virtual-directory.service';

import { UserDto } from '../../../entities/User/UserDto';
import { FileListDto } from '../../../entities/File/FileListDto';
import { UserPreferenceInputDto } from '../../../entities/UserPreference/UserPreferenceInputDto';
import { VirtualDirectoryListDto } from '../../../entities/VirtualDirectory/VirtualDirectoryListDto';

@Component({
  selector: 'update-user-preference',
  templateUrl: './update-user-preference.component.html',
  styleUrls: ['./update-user-preference.component.css']
})
export class UpdateUserPreferenceComponent implements OnInit {

  @Input('vdId') updateVirtualDirectoryId: number;
  @Input('fId') updateFileId: number;
  @Input('name') updateFilename: string;

  currentUser: UserDto;
  files: FileListDto[];
  virtualDirectories: VirtualDirectoryListDto[];
  updatePreferenceModel = new UserPreferenceInputDto(0, 0);

  constructor(
    private userService: UserService,
    private fileService: FileService,
    private virtualDirectoryService: VirtualDirectoryService) { }

  ngOnInit() {
    this.getAllVirtualDirectories();
    this.getAllFiles();
    this.updatePreferenceModel = new UserPreferenceInputDto(this.updateVirtualDirectoryId, this.updateFileId, this.updateFilename);
  }

  onUpdateSubmit() {
    this.virtualDirectoryService.updateUserPreference(this.updatePreferenceModel).subscribe(result => {
      if (result)
        this.updatePreferenceModel = new UserPreferenceInputDto(0, 0);
    });
  }

  private getAllVirtualDirectories() {
    this.userService.getCurrentUser().subscribe(result => {
      this.currentUser = result as UserDto;
      if (this.currentUser != null) {
        this.userService.getAllVirtualDirectories(this.currentUser.id).subscribe(result => {
          this.virtualDirectories = result as VirtualDirectoryListDto[];
        });
      }
    });
  }

  private getAllFiles() {
    this.fileService.getAllFiles().subscribe(result => {
      this.files = result as FileListDto[];
    });
  }

  private cancel() {
    this.updatePreferenceModel = new UserPreferenceInputDto(0, 0);
  }

}
