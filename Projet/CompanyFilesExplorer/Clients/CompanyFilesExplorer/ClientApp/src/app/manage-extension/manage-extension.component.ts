import { Component, OnInit } from '@angular/core';

import { ExtensionService } from '../../services/extension.service';
import { ExtensionDto } from '../../entities/Extension/ExtensionDto';
import { ExtensionInputDto } from '../../entities/Extension/ExtensionInputDto';
import { ExtensionListDto } from '../../entities/Extension/ExtensionListDto';

@Component({
  selector: 'manage-extension',
  templateUrl: './manage-extension.component.html',
  styleUrls: ['./manage-extension.component.css']
})
export class ManageExtensionComponent implements OnInit {

  extensionId: number = -1;
  extension: ExtensionDto;
  extensions: ExtensionListDto[];

  createModel = new ExtensionInputDto('');
  updateModel = new ExtensionInputDto('');

  constructor(private extensionService: ExtensionService) { }

  ngOnInit() {
    this.getAll();
  }

  setSelected(id: number) {
    this.extensionId = id;
    this.get(this.extensionId);
  }

  onCreateSubmit() {
    this.create(this.createModel);
  }

  onUpdateSubmit() {
    if (this.updateModel != this.extension)
      this.update(this.extension.id, this.updateModel);
  }

  private getAll() {
    this.extensionService.getAllExtensions().subscribe(result => {
      this.extensions = result as ExtensionListDto[];
    });
  }

  private get(id: number) {
    this.extensionService.getExtension(id).subscribe(result => {
      this.extension = result as ExtensionDto;
      this.updateModel = this.extension;
    });
  }

  private create(extension: ExtensionInputDto) {
    this.extensionService.createExtension(extension).subscribe(result => {
      if (result as ExtensionDto != null) {
        this.createModel = new ExtensionInputDto('');
        this.getAll();
      }
    });
  }

  private update(id: number, extension: ExtensionInputDto) {
    this.extensionService.updateExtension(id, extension).subscribe(result => {
      if (result as ExtensionDto != null) {
        this.getAll();
      }
    });
  }

  private delete(id: number) {
    if (confirm("Are you sure to delete this extension?"))
      this.extensionService.deleteExtension(id).subscribe(result => {
        if (result) {
          this.getAll();
        }
      });
  }

}
