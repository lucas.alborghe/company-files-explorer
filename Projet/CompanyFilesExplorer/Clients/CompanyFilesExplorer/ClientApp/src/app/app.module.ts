import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

// default components
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';

// "home page"
import { FilesExplorerComponent } from './files-explorer/files-explorer.component';

// "personal" page
import { PersonalDirectoriesComponent } from './personal-directories/personal-directories.component';
import { CreateUserPreferenceComponent } from './user-preference/create-user-preference/create-user-preference.component';
import { UpdateUserPreferenceComponent } from './user-preference/update-user-preference/update-user-preference.component';
import { CreateVirtualDirectoryComponent } from './virtual-directory/create-virtual-directory/create-virtual-directory.component';
import { UpdateVirtualDirectoryComponent } from './virtual-directory/update-virtual-directory/update-virtual-directory.component';

// "procedure" page
import { ManageProcedureComponent } from './manage-procedure/manage-procedure.component';
import { ListProceduresComponent } from './list-procedures/list-procedures.component';
import { ListMetadataComponent } from './list-metadata/list-metadata.component';

// "add file" page
import { CreateFileComponent } from './create-file/create-file.component';

// "database" page
import { ManageDatabaseComponent } from './manage-database/manage-database.component';
import { ManageExtensionComponent } from './manage-extension/manage-extension.component';
import { ManageRepositoryComponent } from './manage-repository/manage-repository.component';

// services
import { ApiService } from '../services/api.service';
import { DatabaseManagerService } from '../services/database-manager.service';
import { DirectoryService } from '../services/directory.service';
import { ExtensionService } from '../services/extension.service';
import { FileService } from '../services/file.service';
import { MetadataService } from '../services/metadata.service';
import { ProcedureService } from '../services/procedure.service';
import { RepositoryService } from '../services/repository.service';
import { UserService } from '../services/user.service';
import { VirtualDirectoryService } from '../services/virtual-directory.service';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    ListProceduresComponent,
    ListMetadataComponent,
    ManageDatabaseComponent,
    ManageExtensionComponent,
    ManageRepositoryComponent,
    FilesExplorerComponent,
    PersonalDirectoriesComponent,
    CreateVirtualDirectoryComponent,
    CreateUserPreferenceComponent,
    CreateFileComponent,
    UpdateUserPreferenceComponent,
    UpdateVirtualDirectoryComponent,
    ManageProcedureComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: FilesExplorerComponent, pathMatch: 'full' },
      { path: 'files-explorer', component: FilesExplorerComponent },                    // Files Explorer
      { path: 'personal-directories', component: PersonalDirectoriesComponent },        // Personal
      { path: 'create-user-preference', component: CreateUserPreferenceComponent },     // -
      { path: 'update-user-preference', component: UpdateUserPreferenceComponent },     // -
      { path: 'create-virtual-directory', component: CreateVirtualDirectoryComponent }, // -
      { path: 'update-virtual-directory', component: UpdateVirtualDirectoryComponent }, // -
      { path: 'manage-procedure', component: ManageProcedureComponent },                // Manage procedure
      { path: 'list-procedures', component: ListProceduresComponent },                  // -
      { path: 'list-metadata', component: ListMetadataComponent },                      // -
      { path: 'create-file', component: CreateFileComponent },                          // Create File
      { path: 'manage-database', component: ManageDatabaseComponent },                  // Manage database
      { path: 'manage-extension', component: ManageExtensionComponent },                // -
      { path: 'manage-repository', component: ManageRepositoryComponent },              // -
      { path: '**', redirectTo: '' }
    ])
  ],
  providers: [
    ApiService,
    DatabaseManagerService,
    DirectoryService,
    ExtensionService,
    FileService,
    MetadataService,
    ProcedureService,
    RepositoryService,
    UserService,
    VirtualDirectoryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
