import { Component, OnInit } from '@angular/core';

import { RepositoryService } from '../../services/repository.service';
import { RepositoryDto } from '../../entities/Repository/RepositoryDto';
import { RepositoryInputDto } from '../../entities/Repository/RepositoryInputDto';
import { RepositoryListDto } from '../../entities/Repository/RepositoryListDto';

@Component({
  selector: 'manage-repository',
  templateUrl: './manage-repository.component.html',
  styleUrls: ['./manage-repository.component.css']
})
export class ManageRepositoryComponent implements OnInit {

  repositoryId: number = -1;
  repository: RepositoryDto;
  repositories: RepositoryListDto[];

  createModel = new RepositoryInputDto('');
  updateModel = new RepositoryInputDto('');

  constructor(private repositoryService: RepositoryService) { }

  ngOnInit() {
    this.getAll();
  }

  setSelected(id: number) {
    this.repositoryId = id;
  }

  onCreateSubmit() {
    this.create(this.createModel);
  }

  onUpdateSubmit() {
    if (this.updateModel != this.repository)
      this.update(this.repository.id, this.updateModel);
  }

  private getAll() {
    this.repositoryService.getAllRepositories().subscribe(result => {
      this.repositories = result as RepositoryListDto[];
    });
  }

  private get(id: number) {
    this.repositoryService.getRepository(id).subscribe(result => {
      this.repository = result as RepositoryDto;
      this.updateModel = this.repository;
    });
  }

  private create(extension: RepositoryInputDto) {
    this.repositoryService.createRepository(extension).subscribe(result => {
      if (result as RepositoryDto != null) {
        this.createModel = new RepositoryInputDto('');
        this.getAll();
      }
    });
  }

  private update(id: number, extension: RepositoryInputDto) {
    this.repositoryService.updateRepository(id, extension).subscribe(result => {
      if (result as RepositoryDto != null) {
        this.getAll();
      }
    });
  }

  private delete(id: number) {
    if (confirm("Are you sure to delete this repository?"))
      this.repositoryService.deleteRepository(id).subscribe(result => {
        if (result) {
          this.getAll();
        }
      });
  }
}
