import { Component, OnInit } from '@angular/core';

import { ProcedureService } from '../../services/procedure.service';
import { ProcedureDto } from '../../entities/Procedure/ProcedureDto';
import { ProcedureInputDto } from '../../entities/Procedure/ProcedureInputDto';
import { ProcedureListDto } from '../../entities/Procedure/ProcedureListDto';

@Component({
  selector: 'list-procedures',
  templateUrl: './list-procedures.component.html',
  styleUrls: ['./list-procedures.component.css']
})
export class ListProceduresComponent implements OnInit {

  procedureId: number = -1;
  procedure: ProcedureDto;
  procedures: ProcedureListDto[];

  createModel = new ProcedureInputDto('', '');
  updateModel = new ProcedureInputDto('', '');

  constructor(private procedureService: ProcedureService) { }

  ngOnInit() {
    this.getAll();
  }

  setSelected(id: number) {
    this.procedureId = id;
    this.get(this.procedureId);
  }

  onCreateSubmit() {
    this.create(this.createModel);
  }

  onUpdateSubmit() {
    this.update(this.procedure.id, this.updateModel);
  }

  private getAll() {
    this.procedureService.getAllProcedures().subscribe(result => {
      this.procedures = result as ProcedureListDto[];
    });
  }

  private get(id: number) {
    this.procedureService.getProcedure(id).subscribe(result => {
      this.procedure = result as ProcedureDto;
      this.updateModel = this.procedure;
    });
  }

  private create(extension: ProcedureInputDto) {
    this.procedureService.createProcedure(extension).subscribe(result => {
      if (result as ProcedureDto != null) {
        this.createModel = new ProcedureInputDto('', '');
        this.getAll();
      }
    });
  }

  private update(id: number, extension: ProcedureInputDto) {
    this.procedureService.updateProcedure(id, extension).subscribe(result => {
      if (result as ProcedureDto != null) {
        this.getAll();
      }
    });
  }

  private delete(id: number) {
    if (confirm("Are you sure to delete this procedure?"))
    this.procedureService.deleteProcedure(id).subscribe(result => {
      if (result) {
        this.getAll();
      }
    });
  }

}
