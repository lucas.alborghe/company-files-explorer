import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { UserService } from '../../services/user.service';
import { FileService } from '../../services/file.service';
import { ProcedureService } from '../../services/procedure.service';
import { VirtualDirectoryService } from '../../services/virtual-directory.service';

import { UserDto } from '../../entities/User/UserDto';
import { FileListDto } from '../../entities/File/FileListDto';
import { UserPreferenceListDto } from '../../entities/UserPreference/UserPreferenceListDto';
import { VirtualDirectoryDto } from '../../entities/VirtualDirectory/VirtualDirectoryDto';
import { VirtualDirectoryListDto } from '../../entities/VirtualDirectory/VirtualDirectoryListDto';
import { ProcedureDto } from '../../entities/Procedure/ProcedureDto';

@Component({
  selector: 'personal-directories',
  templateUrl: './personal-directories.component.html',
  styleUrls: ['./personal-directories.component.css']
})
export class PersonalDirectoriesComponent implements OnInit {

  currentUser: UserDto;

  virtualDirectoryProcedure: ProcedureDto;
  currentVirtualDirectory: VirtualDirectoryDto;
  currentUserPreferences: UserPreferenceListDto[];

  currentFiles: FileListDto[];
  currentVirtualDirectories: VirtualDirectoryListDto[];
  allVirtualDirectories: VirtualDirectoryListDto[];

  selectedVDIndex: number;
  selectedFileIndex: number;
  selectedFileName: string;

  isCreateVirtualDirectory: boolean = false;
  isUpdateVirtualDirectory: boolean = false;
  isCreateFile: boolean = false;
  isUpdateFile: boolean = false;

  constructor(
    private sanitizer: DomSanitizer,
    private userService: UserService,
    private virtualDirectoryService: VirtualDirectoryService,
    private fileService: FileService,
    private procedureService: ProcedureService) { }

  ngOnInit() {
    // Get virtual directories of the root
    this.getRootVirtualDirectories();
  }

  private openFile(id: number) {
    return this.sanitizer.bypassSecurityTrustUrl(this.fileService.openFile(id));
  }

  setSelected(id: number) {
    // Initialize
    this.currentFiles = new Array<FileListDto>();
    this.currentVirtualDirectories = new Array<VirtualDirectoryListDto>();
    // Get files from procedure
    this.virtualDirectoryService.getVirtualDirectory(id).subscribe(result => {
      this.currentVirtualDirectory = result as VirtualDirectoryDto;
      if (this.currentVirtualDirectory != null) {
        var procedureId = this.currentVirtualDirectory.procedureId;
        if (typeof procedureId != 'undefined' && procedureId) {
          this.virtualDirectoryService.getProcedure(this.currentVirtualDirectory.id).subscribe(result => {
            this.virtualDirectoryProcedure = result as ProcedureDto;
            if (this.virtualDirectoryProcedure != null) {
              this.procedureService.getFilesFromRule(this.virtualDirectoryProcedure.rule).subscribe(result => {
                this.currentFiles = result as FileListDto[];
              });
            }
          });
        } else {
          this.getCurrentVirtualDirectories(id);
          this.getCurrentFiles(id);
        }
      }
    });
  }

  private getRootVirtualDirectories() {
    // Initialize
    this.allVirtualDirectories = new Array<VirtualDirectoryListDto>();
    this.currentFiles = new Array<FileListDto>();
    this.currentVirtualDirectories = new Array<VirtualDirectoryListDto>();
    // Get root items
    this.userService.getCurrentUser().subscribe(result => {
      this.currentUser = result as UserDto;
      if (this.currentUser != null) {
        this.userService.getAllVirtualDirectories(this.currentUser.id).subscribe(result => {
          this.allVirtualDirectories = result as VirtualDirectoryListDto[];
          if (this.allVirtualDirectories != null) {
            var j = 0;
            for (var i = 0; i < this.allVirtualDirectories.length; i++)
              if (this.allVirtualDirectories[i].virtualDirectoryId == undefined)
                this.currentVirtualDirectories[j++] = this.allVirtualDirectories[i];
          }
        });
      }
    });
  }

  private getCurrentVirtualDirectories(vdId: number) {
    this.virtualDirectoryService.getChildren(vdId).subscribe(result => {
      this.currentVirtualDirectories = result as VirtualDirectoryListDto[];
    });
  }

  private getCurrentFiles(vdId: number) {
    this.virtualDirectoryService.getFiles(vdId).subscribe(result => {
      this.currentFiles = result as FileListDto[];
      if (this.currentFiles != null)
        this.getPreferences(vdId);
    });
  }

  private getPreferences(vdId: number) {
    this.virtualDirectoryService.getUserPreferences(vdId).subscribe(result => {
      this.currentUserPreferences = result as UserPreferenceListDto[];
      if (this.currentUserPreferences != null) {
        for (var i = 0; i < this.currentFiles.length; i++)
          for (var j = 0; j < this.currentUserPreferences.length; j++)
            if (this.currentFiles[i].id == this.currentUserPreferences[j].fileId
              && (typeof this.currentUserPreferences[j].filename != 'undefined' && this.currentUserPreferences[j].filename)) {
              this.currentFiles[i].filename = this.currentUserPreferences[j].filename;
            }
      }
    });
  }

  private previous() {
    if (this.currentVirtualDirectory != null) {
      this.virtualDirectoryService.getParent(this.currentVirtualDirectory.id).subscribe(result => {
        if (result == null) {
          this.currentVirtualDirectory = null;
          this.getRootVirtualDirectories();
        } else {
          this.currentVirtualDirectory = result as VirtualDirectoryDto;
          this.getCurrentVirtualDirectories(this.currentVirtualDirectory.id);
          this.getCurrentFiles(this.currentVirtualDirectory.id);
        }
      });
    }
  }

  private refresh() {
    if (this.currentVirtualDirectory != null) {
      this.getCurrentVirtualDirectories(this.currentVirtualDirectory.id);
      this.getCurrentFiles(this.currentVirtualDirectory.id);
    } else {
      this.getRootVirtualDirectories();
    }
  }

  private clickAddVirtualDirectory() {
    // Change create virtual directory to true
    if (this.currentVirtualDirectory != null)
      this.selectedVDIndex = this.currentVirtualDirectory.id;
    else
      this.selectedVDIndex = 0;
    this.isCreateVirtualDirectory = !this.isCreateVirtualDirectory;
    // Be sure that it it the only one opened
    this.isUpdateVirtualDirectory = false;
    this.isCreateFile = false;
    this.isUpdateFile = false;
  }

  private clickUpdateVirtualDirectory(vdId: number) {
    // Change update virtual directory to true
    this.selectedVDIndex = vdId;
    this.isUpdateVirtualDirectory = !this.isUpdateVirtualDirectory;
    // Be sure that it it the only one opened
    this.isCreateVirtualDirectory = false;
    this.isCreateFile = false;
    this.isUpdateFile = false;
  }

  private clickDeleteVirtualDirectory(vdId: number) {
    if (confirm("Are you sure to delete this item?"))
      this.virtualDirectoryService.deleteVirtualDirectory(vdId).subscribe(result => {
        if (result) {
          if (this.currentVirtualDirectory != null)
            this.getCurrentVirtualDirectories(this.currentVirtualDirectory.id);
          else
            this.getRootVirtualDirectories();
        }
      });
  }

  private clickAddFile() {
    // Change create file to true
    if (this.currentVirtualDirectory != null)
      this.selectedVDIndex = this.currentVirtualDirectory.id;
    else
      this.selectedVDIndex = 0;
    this.isCreateFile = !this.isCreateFile;
    // Be sure that it it the only one opened
    this.isCreateVirtualDirectory = false;
    this.isUpdateVirtualDirectory = false;
    this.isUpdateFile = false;
  }

  private clickUpdateFile(fId: number, filename: string) {
    // Change create file to true
    this.selectedVDIndex = this.currentVirtualDirectory.id;
    this.selectedFileIndex = fId;
    this.selectedFileName = filename;
    this.isUpdateFile = !this.isUpdateFile;
    // Be sure that it it the only one opened
    this.isCreateVirtualDirectory = false;
    this.isUpdateVirtualDirectory = false;
    this.isCreateFile = false;
  }

  private clickDeleteFile(fId: number) {
    if (confirm("Are you sure to delete this item?"))
      this.virtualDirectoryService.deleteUserPreference(this.currentVirtualDirectory.id, fId).subscribe(result => {
        if (result) {
          if (this.currentVirtualDirectory != null) {
            this.getCurrentVirtualDirectories(this.currentVirtualDirectory.id);
            this.getCurrentFiles(this.currentVirtualDirectory.id);
          }
          else
            this.getRootVirtualDirectories();
        }
      });
  }

}
