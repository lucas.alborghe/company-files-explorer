import { Component, OnInit } from '@angular/core';

import { UserService } from '../../services/user.service';
import { UserDto } from '../../entities/User/UserDto';

@Component({
  selector: 'nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  currentUser: UserDto;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    if (this.currentUser == null)
      this.getCurrentUser();
  }

  private getCurrentUser() {
    this.userService.getCurrentUser().subscribe(result => {
      this.currentUser = result as UserDto;
    });
  }

}
