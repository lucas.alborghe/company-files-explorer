import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { RepositoryService } from '../../services/repository.service';
import { FileService } from '../../services/file.service';
import { DirectoryService } from '../../services/directory.service';

import { RepositoryDto } from '../../entities/Repository/RepositoryDto';
import { RepositoryListDto } from '../../entities/Repository/RepositoryListDto';
import { FileListDto } from '../../entities/File/FileListDto';
import { DirectoryInputDto } from '../../entities/Directory/DirectoryInputDto';
import { DirectoryListDto } from '../../entities/Directory/DirectoryListDto';

@Component({
  selector: 'files-explorer',
  templateUrl: './files-explorer.component.html',
  styleUrls: ['./files-explorer.component.css']
})
export class FilesExplorerComponent implements OnInit {

  searchValue: string;

  lastDirectory: DirectoryInputDto;
  currentDirectory: DirectoryInputDto;

  repository: RepositoryDto;
  repositories: RepositoryListDto[];

  allFiles: FileListDto[];
  allDirectories: DirectoryListDto[];

  currentFiles: FileListDto[];
  currentDirectories: DirectoryListDto[];

  constructor(
    private sanitizer: DomSanitizer,
    private repositoryService: RepositoryService,
    private fileService: FileService,
    private directoryService: DirectoryService) { }

  ngOnInit() {
    // Initialize
    this.allFiles = new Array<FileListDto>();
    this.allDirectories = new Array<DirectoryListDto>();
    this.currentFiles = new Array<FileListDto>();
    this.currentDirectories = new Array<DirectoryListDto>();
    // Get root items
    this.getRootItems();
  }

  private openFile(id: number) {
    return this.sanitizer.bypassSecurityTrustUrl(this.fileService.openFile(id));
  }

  private search() {
    // Initialize
    this.allFiles = new Array<FileListDto>();
    this.allDirectories = new Array<DirectoryListDto>();
    this.currentFiles = new Array<FileListDto>();
    this.currentDirectories = new Array<DirectoryListDto>();
    // Do research
    this.getFilesByFilter(this.searchValue);
  }

  private cancelSearch() {
    // Clear search
    this.searchValue = "";
    // Initialize
    this.allFiles = new Array<FileListDto>();
    this.allDirectories = new Array<DirectoryListDto>();
    this.currentFiles = new Array<FileListDto>();
    this.currentDirectories = new Array<DirectoryListDto>();
    // Get root items
    this.getRootItems();
  }

  private getFilesByFilter(filter: string) {
    this.fileService.getAllFilesByFilename(filter).subscribe(result => {
      this.currentFiles = result as FileListDto[];
    });
  }

  private getRootItems() {
    this.repositoryService.getAllRepositories().subscribe(result => {
      this.repositories = result as RepositoryListDto[];
      if (this.repositories != null && this.repositories.length >= 0) {
        this.repository = this.repositories[0];
        this.currentDirectory = new DirectoryInputDto(this.repository.absolutePath, this.repository.id);
        this.getCurrentFiles(this.currentDirectory);
        this.getCurrentDirectories(this.currentDirectory);
      }
    });
  }

  private getCurrentFiles(directory: DirectoryInputDto) {
    this.fileService.getAllFilesByPath(directory).subscribe(result => {
      this.allFiles = result as FileListDto[];
      if (this.allFiles != null) {
        var j = 0;
        for (var i = 0; i < this.allFiles.length; i++)
          if (this.allFiles[i].filepath === directory.path)
            this.currentFiles[j++] = this.allFiles[i];
      }
    });
  }

  private getCurrentDirectories(directory: DirectoryInputDto) {
    this.directoryService.getAllDirectoriesByPath(directory).subscribe(result => {
      this.allDirectories = result as DirectoryListDto[];
      if (this.allDirectories != null) {
        var j = 0;
        for (var i = 0; i < this.allDirectories.length; i++)
          if (this.allDirectories[i].path.startsWith(directory.path + "\\")) 
            if (this.allDirectories[i].path.substring(directory.path.length, this.allDirectories[i].path.length).split("\\").length == 2) 
              this.currentDirectories[j++] = this.allDirectories[i];
      }
    });
  }

  private getItemsFromDirectory(directoryPath: string) {
    // Initialize
    this.allFiles = new Array<FileListDto>();
    this.allDirectories = new Array<DirectoryListDto>();
    this.currentFiles = new Array<FileListDto>();
    this.currentDirectories = new Array<DirectoryListDto>();
    // Update directories
    this.lastDirectory = this.currentDirectory;
    this.currentDirectory = new DirectoryInputDto(directoryPath, this.repository.id);
    // Get items
    this.currentDirectory = new DirectoryInputDto(directoryPath, this.repository.id);
    this.getCurrentFiles(this.currentDirectory);
    this.getCurrentDirectories(this.currentDirectory);
  }
  
  private previous() {
    if (this.currentDirectory.path != this.repository.absolutePath) {
      var previousDirectory = "";
      for (var i = 0; i < (this.currentDirectory.path.split("\\").length - 1); i++) {
        if (i + 2 == this.currentDirectory.path.split("\\").length)
          previousDirectory = previousDirectory.concat(this.currentDirectory.path.split('\\')[i]);
        else
          previousDirectory = previousDirectory.concat(this.currentDirectory.path.split('\\')[i]) + "\\";
      }
      this.getItemsFromDirectory(previousDirectory);
    }
  }
  
}
