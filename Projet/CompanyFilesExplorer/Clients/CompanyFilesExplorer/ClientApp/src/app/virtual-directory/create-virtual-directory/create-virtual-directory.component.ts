import { Component, OnInit, Input } from '@angular/core';

import { UserService } from '../../../services/user.service';
import { ProcedureService } from '../../../services/procedure.service';
import { VirtualDirectoryService } from '../../../services/virtual-directory.service';

import { UserDto } from '../../../entities/User/UserDto';
import { ProcedureListDto } from '../../../entities/Procedure/ProcedureListDto';
import { VirtualDirectoryInputDto } from '../../../entities/VirtualDirectory/VirtualDirectoryInputDto';
import { VirtualDirectoryListDto } from '../../../entities/VirtualDirectory/VirtualDirectoryListDto';

@Component({
  selector: 'create-virtual-directory',
  templateUrl: './create-virtual-directory.component.html',
  styleUrls: ['./create-virtual-directory.component.css']
})
export class CreateVirtualDirectoryComponent implements OnInit {

  @Input('vdId') currentVirtualDirectoryId: number;

  currentUser: UserDto;
  procedures: ProcedureListDto[];
  virtualDirectories: VirtualDirectoryListDto[];
  createVirtualDirectoryeModel = new VirtualDirectoryInputDto('', 0);

  constructor(
    private userService: UserService,
    private procedureService: ProcedureService,
    private virtualDirectoryService: VirtualDirectoryService) { }

  ngOnInit() {
    this.getAllVirtualDirectories();
    this.getAllProcedures();
    if (this.currentVirtualDirectoryId  != null && this.currentVirtualDirectoryId > 0)
      this.createVirtualDirectoryeModel.virtualDirectoryId = this.currentVirtualDirectoryId;
  }

  onCreateSubmit() {
    this.virtualDirectoryService.createVirtualDirectory(this.createVirtualDirectoryeModel).subscribe(result => {
      if (result)
        this.createVirtualDirectoryeModel = new VirtualDirectoryInputDto('', 0);
    });
  }

  private getAllVirtualDirectories() {
    this.userService.getCurrentUser().subscribe(result => {
      this.currentUser = result as UserDto;
      if (this.currentUser != null) {
        this.userService.getAllVirtualDirectories(this.currentUser.id).subscribe(result => {
          this.virtualDirectories = result as VirtualDirectoryListDto[];
          this.createVirtualDirectoryeModel.userId = this.currentUser.id;
        });
      }
    });
  }

  private getAllProcedures() {
    this.procedureService.getAllProcedures().subscribe(result => {
      this.procedures = result as ProcedureListDto[];
    });
  }

  private cancel() {
    this.createVirtualDirectoryeModel = new VirtualDirectoryInputDto('', 0);
  }

}
