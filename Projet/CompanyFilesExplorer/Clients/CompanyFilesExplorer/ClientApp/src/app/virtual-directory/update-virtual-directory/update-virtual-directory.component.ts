import { Component, OnInit, Input } from '@angular/core';

import { UserService } from '../../../services/user.service';
import { ProcedureService } from '../../../services/procedure.service';
import { VirtualDirectoryService } from '../../../services/virtual-directory.service';

import { UserDto } from '../../../entities/User/UserDto';
import { ProcedureListDto } from '../../../entities/Procedure/ProcedureListDto';
import { VirtualDirectoryDto } from '../../../entities/VirtualDirectory/VirtualDirectoryDto';
import { VirtualDirectoryInputDto } from '../../../entities/VirtualDirectory/VirtualDirectoryInputDto';
import { VirtualDirectoryListDto } from '../../../entities/VirtualDirectory/VirtualDirectoryListDto';

@Component({
    selector: 'update-virtual-directory',
    templateUrl: './update-virtual-directory.component.html',
    styleUrls: ['./update-virtual-directory.component.css']
})
export class UpdateVirtualDirectoryComponent implements OnInit {

  @Input('id') updateVirtualDirectoryId: number;
  currentVirtualDirectory: VirtualDirectoryDto;

  currentUser: UserDto;
  procedures: ProcedureListDto[];
  virtualDirectories: VirtualDirectoryListDto[];
  updateVirtualDirectoryModel = new VirtualDirectoryInputDto('', 0);

  constructor(
    private userService: UserService,
    private procedureService: ProcedureService,
    private virtualDirectoryService: VirtualDirectoryService) { }

  ngOnInit() {
    this.getAllVirtualDirectories();
    this.getAllProcedures();
    this.getUpdateVirtualDirectory(this.updateVirtualDirectoryId);
  }

  onUpdateSubmit() {
    this.virtualDirectoryService.updateVirtualDirectory(this.updateVirtualDirectoryId, this.updateVirtualDirectoryModel).subscribe(result => {
      if (result)
        this.updateVirtualDirectoryModel = new VirtualDirectoryInputDto('', 0);
      });
  }

  private getUpdateVirtualDirectory(currentId:number) {
    this.virtualDirectoryService.getVirtualDirectory(currentId).subscribe(result => {
      if (result != null) {
        this.updateVirtualDirectoryModel = result as VirtualDirectoryDto;
      }
    });
  }

  private getAllVirtualDirectories() {
    this.userService.getCurrentUser().subscribe(result => {
      this.currentUser = result as UserDto;
      if (this.currentUser != null) {
        this.userService.getAllVirtualDirectories(this.currentUser.id).subscribe(result => {
          this.virtualDirectories = result as VirtualDirectoryListDto[];
          this.updateVirtualDirectoryModel.userId = this.currentUser.id;
        });
      }
    });
  }

  private getAllProcedures() {
    this.procedureService.getAllProcedures().subscribe(result => {
      this.procedures = result as ProcedureListDto[];
    });
  }

  private cancel() {
    this.updateVirtualDirectoryModel = new VirtualDirectoryInputDto('', 0);
  }

}
