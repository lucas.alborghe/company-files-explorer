import { Component, OnInit } from '@angular/core';

import { MetadataService } from '../../services/metadata.service';
import { ProcedureService } from '../../services/procedure.service';

import { MetadataDto } from '../../entities/Metadata/MetadataDto';
import { MetadataInputDto } from '../../entities/Metadata/MetadataInputDto';
import { MetadataListDto } from '../../entities/Metadata/MetadataListDto';

import { ProcedureDto } from '../../entities/Procedure/ProcedureDto';
import { ProcedureListDto } from '../../entities/Procedure/ProcedureListDto';

@Component({
  selector: 'list-metadata',
  templateUrl: './list-metadata.component.html',
  styleUrls: ['./list-metadata.component.css']
})
export class ListMetadataComponent implements OnInit {

  metadataId: number = -1;
  metadata: MetadataDto;
  metadatas: MetadataListDto[];

  procedureId: number = -1;
  procedure: ProcedureDto;
  procedures: ProcedureListDto[];

  createModel = new MetadataInputDto(0, '', '');
  updateModel = new MetadataInputDto(0, '', '');

  constructor(
    private metadataService: MetadataService,
    private procedureService: ProcedureService) { }

  ngOnInit() {
    this.getAllProcedures();
    this.getAll();
  }

  setSelected(id: number) {
    this.procedureId = id;
    this.get(this.procedureId);
  }

  onCreateSubmit() {
    this.create(this.createModel);
  }

  onUpdateSubmit() {
    this.update(this.procedure.id, this.updateModel);
  }

  private getAllProcedures() {
    this.procedureService.getAllProcedures().subscribe(result => {
      this.procedures = result as ProcedureListDto[];
    });
  }

  private getAll() {
    this.metadataService.getAllMetadata().subscribe(result => {
      this.metadatas = result as MetadataListDto[];
    });
  }

  private get(id: number) {
    this.metadataService.getMetadata(id).subscribe(result => {
      this.metadata = result as MetadataDto;
      this.updateModel = this.metadata;
    });
  }

  private create(metadata: MetadataInputDto) {
    this.metadataService.createMetadata(metadata).subscribe(result => {
      if (result as MetadataDto != null) {
        this.createModel = new MetadataInputDto(0, '', '');
        this.getAll();
      }
    });
  }

  private update(id: number, metadata: MetadataInputDto) {
    this.metadataService.updateMetadata(id, metadata).subscribe(result => {
      if (result as MetadataDto != null) {
        this.getAll();
      }
    });
  }

  private delete(id: number) {
    if (confirm("Are you sure to delete this metadata?"))
    this.metadataService.deleteMetadata(id).subscribe(result => {
      if (result) {
        this.getAll();
      }
    });
  }

}
