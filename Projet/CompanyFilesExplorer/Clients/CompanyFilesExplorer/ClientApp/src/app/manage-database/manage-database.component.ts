import { Component, OnInit } from '@angular/core';

import { DatabaseManagerService } from '../../services/database-manager.service';
import { DirectoryService } from '../../services/directory.service';
import { RepositoryService } from '../../services/repository.service';

import { RepositoryDto } from '../../entities/Repository/RepositoryDto';
import { RepositoryListDto } from '../../entities/Repository/RepositoryListDto';
import { DirectoryInputDto } from '../../entities/Directory/DirectoryInputDto';
import { DirectoryListDto } from '../../entities/Directory/DirectoryListDto';
import { FileListDto } from '../../entities/File/FileListDto';

@Component({
  selector: 'manage-database',
  templateUrl: './manage-database.component.html',
  styleUrls: ['./manage-database.component.css']
})
export class ManageDatabaseComponent implements OnInit {

  disabled: boolean = true;
  option: number;
  pathToRefresh: string;

  files: FileListDto[];
  directory: DirectoryInputDto;
  directories: DirectoryListDto[];
  repository: RepositoryDto;
  repositories: RepositoryListDto[];

  constructor(
    private databaseManagerService: DatabaseManagerService,
    private directoryService: DirectoryService,
    private repositoryService: RepositoryService) { }

  ngOnInit() {
    this.getRepository();
  }

  setradio(o: number): void {
    this.disabled = false;
    this.option = o;
  }

  isSelected(i: number): boolean {
    return (this.option === i);
  }

  refresh() {
    if (!this.disabled) {
      if (this.option == 2) {
        if (this.pathToRefresh != undefined && this.pathToRefresh != null && this.pathToRefresh != "") {
          this.directory = new DirectoryInputDto(this.pathToRefresh, this.repository.id);
          if (confirm("Are you sure to refresh the database?")) {
            this.refreshDatabaseFromPath(this.directory);
          }
        }
      } else {
        if (confirm("Are you sure to refresh the database?")) {
          this.refreshDatabase();
        }
      }
    }
  }

  private refreshDatabase() {
    this.databaseManagerService.refreshDatabase().subscribe(result => {
      this.files = result as FileListDto[];
    });
  }

  private refreshDatabaseFromPath(directory: DirectoryInputDto) {
    this.databaseManagerService.refreshDatabaseFromPath(directory).subscribe(result => {
      this.files = result as FileListDto[];
    });
  }

  private getRepository() {
    this.repositoryService.getAllRepositories().subscribe(result => {
      this.repositories = result as RepositoryListDto[];
      if (this.repositories != null) {
        this.repository = this.repositories[0];
        this.getDirectories();
      }
    });
  }

  private getDirectories() {
    this.repositoryService.getAllDirectories(this.repository.id).subscribe(result => {
      this.directories = result as DirectoryListDto[];
    });
  }

}
