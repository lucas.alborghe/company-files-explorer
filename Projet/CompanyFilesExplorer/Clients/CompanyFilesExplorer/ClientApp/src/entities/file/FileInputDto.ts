export class FileInputDto {

  constructor(
    public repositoryId: number,
    public filename: string,
    public filepath: string,
    public extension: string) { }

}
