export class FileListDto {
  id: number;
  repositoryId: number;
  filename: string;
  filepath: string;
  extension: string;
  hashcode: string;
  createdDate: Date;
  modifiedDate: Date;
}
