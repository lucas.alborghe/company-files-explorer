export class FileMoveInputDto {

  constructor(
    public sourceRepositoryId: number,
    public sourceFilepath: string,
    public sourceFilename: string,
    public sourceExtension: string,
    public destinationRepositoryId: number,
    public destinationFilepath: string,
    public destinationFilename: string,
    public destinationExtension: string) { }

}
