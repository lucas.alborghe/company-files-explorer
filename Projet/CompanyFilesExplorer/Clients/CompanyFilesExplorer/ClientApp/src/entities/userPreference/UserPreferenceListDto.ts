export class UserPreferenceListDto {
  virtualDirectoryId: number;
  fileId: number;
  filename: string;
}
