export class UserPreferenceInputDto {

  constructor(
    public virtualDirectoryId: number,
    public fileId: number,
    public filename?: string) { }

}
