export class UserPreferenceDto {
  virtualDirectoryId: number;
  fileId: number;
  filename: string;
}
