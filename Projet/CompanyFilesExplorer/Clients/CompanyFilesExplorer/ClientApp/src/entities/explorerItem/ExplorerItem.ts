export class ExplorerItem {

  constructor(
    public type: string,
    public name: string,
    public path: string,
    public modifiedDate: Date,
    public id?: number,
    public extension?: string,
    public createdDate?: Date) { }

}
