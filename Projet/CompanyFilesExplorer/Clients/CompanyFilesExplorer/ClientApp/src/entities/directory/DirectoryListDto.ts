export class DirectoryListDto {
  path: string;
  repositoryId: number;
  lastAccessDate: Date;
}
