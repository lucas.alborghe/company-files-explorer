export class DirectoryDto {
  path: string;
  repositoryId: number;
  lastAccessDate: Date;
}
