export class DirectoryInputDto {

  constructor(
    public path: string,
    public repositoryId: number) { }

}
