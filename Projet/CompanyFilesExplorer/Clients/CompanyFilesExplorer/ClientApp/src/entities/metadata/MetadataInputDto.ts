export class MetadataInputDto {

  constructor(
    public procedureId: number,
    public name: string,
    public type: string,
    public fonction?: string,
    public fonctionParam?: string,
    public description?: string) { }

}
