export class MetadataDto {
  id: number;
  procedureId: number;
  name: string;
  type: string;
  fonction: string;
  fonctionParam: string;
  description: string;
}
