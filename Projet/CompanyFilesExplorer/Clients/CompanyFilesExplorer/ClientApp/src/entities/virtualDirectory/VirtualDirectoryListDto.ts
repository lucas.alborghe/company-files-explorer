export class VirtualDirectoryListDto {
  id: number;
  userId: number;
  virtualDirectoryId: number;
  procedureId: number;
  name: string;
  isActive: boolean;
}
