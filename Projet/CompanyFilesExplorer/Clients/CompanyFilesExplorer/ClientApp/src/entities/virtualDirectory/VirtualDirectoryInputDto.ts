export class VirtualDirectoryInputDto {

  constructor(
    public name: string,
    public userId: number,
    public virtualDirectoryId?: number,
    public procedureId?: number,
    public isActive?: boolean) { }

}
