export class VirtualDirectoryDto {
  id: number;
  userId: number;
  virtualDirectoryId: number;
  procedureId: number;
  name: string;
  isActive: boolean;
}
