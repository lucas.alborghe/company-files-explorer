export class RepositoryListDto {
  id: number;
  absolutePath: string;
  protocol: string;
  username: string;
  password: string;
}
