export class RepositoryInputDto {

  constructor(
    public absolutePath: string,
    public protocol?: string,
    public username?: string,
    public password?: string) { }

}
