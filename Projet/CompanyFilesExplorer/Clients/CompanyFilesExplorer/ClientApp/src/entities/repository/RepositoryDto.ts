export class RepositoryDto {
  id: number;
  absolutePath: string;
  protocol: string;
  username: string;
  password: string;
}
