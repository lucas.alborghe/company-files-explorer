export class ProcedureInputDto {

  constructor(
    public name: string,
    public rule: string,
    public description?: string) { }

}
