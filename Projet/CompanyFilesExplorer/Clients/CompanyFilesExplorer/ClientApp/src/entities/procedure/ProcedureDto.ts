export class ProcedureDto {
  id: number;
  name: string;
  rule: string;
  description: string;
}
