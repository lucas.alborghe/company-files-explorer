export class ProcedureListDto {
  id: number;
  name: string;
  rule: string;
  description: string;
}
