export class ExtensionListDto {
  id: number;
  extension: string;
}
