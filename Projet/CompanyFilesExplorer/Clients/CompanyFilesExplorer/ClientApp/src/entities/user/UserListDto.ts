export class UserListDto {
  id: number;
  login: string;
  isAdmin: boolean;
}
