export class UserInputDto {

  constructor(
    public login: string,
    public isAdmin?: boolean) { }

}
