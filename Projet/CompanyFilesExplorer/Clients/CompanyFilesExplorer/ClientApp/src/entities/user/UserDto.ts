export class UserDto {
  id: number;
  login: string;
  isAdmin: boolean;
}
