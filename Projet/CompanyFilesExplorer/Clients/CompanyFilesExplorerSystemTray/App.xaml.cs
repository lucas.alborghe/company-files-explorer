﻿using System.Windows;
using Shared.Utils;

namespace CompanyFilesExplorerSystemTray
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private SystemTray _systemTray;
        public static string AppName = ProtocolHandlerHelper.DisplayName;

        protected override void OnExit(ExitEventArgs e)
        {
            _systemTray.Dispose();
            base.OnExit(e);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            _systemTray = new SystemTray();
            base.OnStartup(e);
        }
    }
}