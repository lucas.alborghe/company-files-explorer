﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using Shared.Utils;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace CompanyFilesExplorerSystemTray
{
    public class SystemTray : IDisposable
    {
        private readonly NotifyIcon _systemTray;
        private readonly MenuItem _anAction;
        private readonly MenuItem _exitItem;
        private readonly MenuItem _showLog;
        private readonly ContextMenu _contextMenu;

        public SystemTray()
        {
            _contextMenu = new ContextMenu();
            _exitItem = new MenuItem("Exit", OnClose);
            _anAction = new MenuItem("Do something", DoSomething);
            _showLog = new MenuItem("Show log", ShowLog);

            _systemTray = new NotifyIcon
            {
                    Icon = new Icon(Path.Combine(Path.GetDirectoryName(GetType().Assembly.Location), "CompanyFilesExplorer.ico")),
                    Visible = true,
                    ContextMenu = _contextMenu
            };
            _contextMenu.Popup += (s, e) => CheckMenuItems();
        }

        private void OnClose(object sender, EventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you really want to close the system tray.", "Close system tray", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if(result == MessageBoxResult.No)
            {
                return;
            }

            Application.Current.Shutdown();
        }

        public void Dispose()
        {
            _systemTray.Visible = false;
        }

        public void Tooltip(string text)
        {
            // Max 63 characters for the NotifyIcon
            _systemTray.Text = text.Substring(0, Math.Min(text.Length, 63));
        }

        private void OpenConnect(object sender, EventArgs e)
        {
            string directory = Path.GetDirectoryName(typeof(SystemTray).Assembly.Location);
            if(directory == null)
            {
                return;
            }

            string connectExe = Path.Combine(directory, ProtocolHandlerHelper.FileName);
            if(File.Exists(connectExe))
            {
                Process.Start(connectExe);
            }
        }

        private void CheckMenuItems()
        {
            _contextMenu.MenuItems.Clear();
            _contextMenu.MenuItems.Add(_showLog);
            _contextMenu.MenuItems.Add(_anAction);
            _contextMenu.MenuItems.Add(_exitItem);
        }

        private void DoSomething(object sender, EventArgs e)
        {
            // Do something interesting
        }
        
        private void ShowLog(object sender, EventArgs e)
        {
            // Configure message box
            string caption = "Log file";
            string message = "[0] This is a log";
            MessageBoxImage icon = MessageBoxImage.Information;
            MessageBoxButton buttons = MessageBoxButton.OK;
            MessageBoxResult defaultResult = MessageBoxResult.OK;
            // Show message box
            MessageBoxResult result = MessageBox.Show(message, caption, buttons, icon, defaultResult);
        }
    }
}