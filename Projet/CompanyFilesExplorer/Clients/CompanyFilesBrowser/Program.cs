﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Core.Dtos.File;
using Newtonsoft.Json;

namespace CompanyFilesBrowser
{
    /// <summary>
    ///     This static class is able to read and interpret a FEX file.
    /// </summary>
    internal class Program
    {
        private static string API_URL = "http://localhost:63377/api/file/json/";

        private static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                ShowMissingFileError();
            }
            else
            {
                string content = args[0];
                try
                {
                    Uri uri = new Uri(content);
                    string scheme = uri.Scheme + Uri.SchemeDelimiter;
                    if (content.StartsWith(scheme))
                    {
                        int fileId;
                        string supposedFileId = content.Remove(0, scheme.Length);
                        if (supposedFileId.EndsWith('/'))
                            supposedFileId = supposedFileId.Remove(supposedFileId.Length - 1);

                        if (Int32.TryParse(supposedFileId, out fileId))
                            OpenThroughProtocol(fileId);
                        else
                            ShowBadParamError(supposedFileId);
                    }
                    else
                    {
                        OpenThroughExecutable(content);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception while parsing received content : '" + e + "'");
                }
            }
        }

        private static void OpenThroughProtocol(int fileId)
        {
            Console.WriteLine("=== FilesExplorer Protocol ===");
            try
            {
                // Creates an HttpWebRequest with the specified URL. 
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(Path.Combine(API_URL, fileId.ToString()));

                Console.WriteLine("Getting response from web request...");

                // Sends the HttpWebRequest and waits for the response.			
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                // Gets the stream associated with the response.
                Stream receiveStream = myHttpWebResponse.GetResponseStream();
                Encoding encode = Encoding.GetEncoding("utf-8");

                Console.WriteLine(" [+] Done");
                Console.WriteLine("Reading response...");

                // Pipes the stream to a higher level stream reader with the required encoding format. 
                StreamReader readStream = new StreamReader(receiveStream, encode);

                // Deserialize the data to JSON (FileDto)
                FileDto receivedDto = JsonConvert.DeserializeObject<FileDto>(readStream.ReadToEnd());

                Console.WriteLine(" [+] Done");
                Console.WriteLine("Releasing resources...");

                // Releases the resources of the response.
                myHttpWebResponse.Close();

                // Releases the resources of the Stream.
                readStream.Close();

                Console.WriteLine(" [+] Done");
                Console.WriteLine("Opening target file...");

                string filename = receivedDto.Filename + receivedDto.Extension;
                string absolutePath = Path.Combine(receivedDto.Filepath, filename);

                // Open the target file
                if (File.Exists(absolutePath))
                {
                    FileHelper.OpenFile(absolutePath);
                    Console.WriteLine(" [+] Done");
                }
                else
                {
                    ShowUnreachableFileError(absolutePath);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception while using filesexplorer protocol : '" + e + "'");
            }
        }

        private static void OpenThroughExecutable(string filePath)
        {
            Console.WriteLine("=== FilesExplorer Executable ===");
            try
            {
                if (File.Exists(filePath))
                {
                    string fileContent = File.ReadAllText(filePath);
                    FileDto description = JsonConvert.DeserializeObject<FileDto>(fileContent);

                    Console.WriteLine("Opening target file...");

                    string filename = description.Filename + description.Extension;
                    string absolutePath = Path.Combine(description.Filepath, filename);

                    // Open the target file
                    if (File.Exists(absolutePath))
                    {
                        FileHelper.OpenFile(absolutePath);
                        Console.WriteLine(" [+] Done");
                    }
                    else
                    {
                        ShowUnreachableFileError(absolutePath);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception while using FEX executable : '" + e + "'");
            }
        }

        private static void ShowMissingFileError()
        {
            Console.WriteLine(" [-] Need to specify a file first!");
            Console.WriteLine("");
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private static void ShowUnreachableFileError(string unreachableFile)
        {
            Console.WriteLine(" [-] Can't open selected file : Can't reach '" + unreachableFile + "'");
            Console.WriteLine("");
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private static void ShowBadParamError(string badParam)
        {
            Console.WriteLine(" [-] Bad parameter given : Can't use '" + badParam + "'");
            Console.WriteLine("");
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}