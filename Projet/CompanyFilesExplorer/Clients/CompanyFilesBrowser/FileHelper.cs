﻿using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CompanyFilesBrowser
{
    /// <summary>
    ///     This static class is able to manage the file opening.
    /// </summary>
    public static class FileHelper
    {
        public static void OpenFile(string url)
        {
            try
            {
                Process.Start(url);
            } catch
            {
                if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    url = url.Replace("&", "^&");
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = "cmd";
                    startInfo.Arguments = $"/c start \"Dummy Title\" " + "\"" + url + "\"";
                    startInfo.CreateNoWindow = true;
                    Process.Start(startInfo);
                } else if(RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    Process.Start("xdg-open", url);
                } else if(RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    Process.Start("open", url);
                } else
                {
                    throw;
                }
            }
        }
    }
}