﻿using System;

namespace WebsiteTests
{
    public abstract class AbstractTestBase : IDisposable
    {

        public void Dispose()
        {
        }

        /*
        private ApplicationDbContext _appContext;
        private UsersDbContext _usersContext;
        private static readonly IConfigurationRoot _configuration;

        /// <summary>
        ///     Gets the application context.
        /// </summary>
        /// <value>The application context.</value>
        protected ApplicationDbContext AppContext
        {
            get
            {
                if(_appContext == null)
                {
                    DbContextOptionsBuilder<ApplicationDbContext> appOptionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
                    string cs = _configuration.GetConnectionString("AdminUIConnection");
                    cs = cs.Replace(".db", $"{Guid.NewGuid()}.db");
                    appOptionsBuilder.UseSqlite(cs);
                    _appContext = new ApplicationDbContext(appOptionsBuilder.Options);
                    _appContext.Database.EnsureCreated();
                }

                return _appContext;
            }
        }

        /// <summary>
        ///     Gets the users context.
        /// </summary>
        /// <value>The users context.</value>
        protected UsersDbContext UsersContext
        {
            get
            {
                if(_usersContext == null)
                {
                    DbContextOptionsBuilder<UsersDbContext> usersOptionsBuilder = new DbContextOptionsBuilder<UsersDbContext>();
                    string cs = _configuration.GetConnectionString("UsersConnection");
                    cs = cs.Replace(".db", $"{Guid.NewGuid()}.db");
                    usersOptionsBuilder.UseSqlite(cs);
                    _usersContext = new UsersDbContext(usersOptionsBuilder.Options);
                    _usersContext.Database.EnsureCreated();
                }

                return _usersContext;
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AbstractTestBase" /> class.
        /// </summary>
        static AbstractTestBase()
        {
            _configuration = ConfigurationHelper.CreateDefaultConfigurationBuiler().Build();
            CustomConfigExtensions.LoadConfiguration(_configuration);
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if(_appContext != null)
            {
                AppContext.Database.EnsureDeleted();
                AppContext.Dispose();
            }

            if(_usersContext != null)
            {
                UsersContext.Database.EnsureDeleted();
                UsersContext.Dispose();
            }
        }
        */
    }
}