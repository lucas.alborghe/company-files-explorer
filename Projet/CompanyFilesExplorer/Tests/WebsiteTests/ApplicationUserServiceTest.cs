﻿namespace WebsiteTests
{
    public class ApplicationUserServiceTest : AbstractTestBase
    {
        /*
        private readonly ApplicationUserService _service;
        private readonly FakeUserManager _userManagerMock;

        public ApplicationUserServiceTest()
        {
            _userManagerMock = new FakeUserManager(UsersContext);
            Mock<ILogger<IApplicationUserService>> mock = new Mock<ILogger<IApplicationUserService>>();
            _service = new ApplicationUserService(UsersContext, mock.Object, _userManagerMock, new DateTimeService());
        }

        [Fact]
        public async Task GetAllAsyncShouldNeverReturnsSuperAdmin()
        {
            _userManagerMock.LoadRoles();

            IReadOnlyCollection<ApplicationUserListDto> values = await _service.GetAllAsync();
            values.Should().NotBeNull();
            values.Should().BeEmpty();
            ApplicationSuperAdminCreateDto sa = CreateSuperAdmin();
            await _service.CreateSuperAdminAsync(sa);
            values = await _service.GetAllAsync();
            values.Should().NotBeNull();
            values.Should().BeEmpty();

            ApplicationUserCreateDto admin = Create(ExplorerRole.Admin);
            ApplicationUserDto adminDto = await _service.CreateAsync(admin);

            values = await _service.GetAllAsync();
            values.Should().NotBeNull();
            values.Should().HaveCount(1);
            values.Single().ShouldBeEquivalentTo(adminDto, options => options.ExcludingMissingMembers());

            ApplicationUserCreateDto restrictedAdmin = Create(ExplorerRole.RestrictedAdmin);
            ApplicationUserDto restrictedAdminDto = await _service.CreateAsync(restrictedAdmin);

            values = await _service.GetAllAsync();
            values.Should().NotBeNull();
            values.ShouldBeEquivalentTo(new[]
            {
                    adminDto,
                    restrictedAdminDto
            }, options => options.ExcludingMissingMembers());
        }

        [Fact]
        public async Task CreateAsyncTest()
        {
            _userManagerMock.LoadRoles();

            ApplicationUserCreateDto user = Create(ExplorerRole.Admin);

            ApplicationUserDto newUserDto = await _service.CreateAsync(user);

            ApplicationUserDto createdUser = await _service.GetAsync(newUserDto.Id);
            createdUser.Should().NotBeNull();
            createdUser.Id.Should().NotBeNull();
            createdUser.UserName.Should().Be(user.UserName);
            createdUser.Roles.ShouldBeEquivalentTo(user.Roles);
            createdUser.PasswordValidityPeriodInDays.Should().Be(user.PasswordValidityPeriodInDays);
        }

        [Fact]
        public async Task CreateSuperAdminAsyncTest()
        {
            _userManagerMock.LoadRoles();

            ApplicationSuperAdminCreateDto sa = CreateSuperAdmin();

            ApplicationUserDto newUserDto = await _service.CreateSuperAdminAsync(sa);
            newUserDto.Should().NotBeNull();
            ApplicationUserDto createdUser = await _service.GetAsync(newUserDto.Id);
            createdUser.ShouldBeEquivalentTo(newUserDto);

            createdUser.Should().NotBeNull();
            createdUser.Id.Should().NotBeNull();
            createdUser.UserName.Should().Be(sa.UserName);
            createdUser.Roles.Count.Should().Be(1);
            createdUser.Roles.Single().Should().Be(ExplorerRole.SuperAdmin.ToString());
            createdUser.PasswordValidityPeriodInDays.Should().BeNull();
        }

        [Fact]
        public async Task GetAsyncTest()
        {
            _userManagerMock.LoadRoles();

            ApplicationSuperAdminCreateDto sa = CreateSuperAdmin();
            ApplicationUserDto createResult = await _service.CreateSuperAdminAsync(sa);
            ApplicationUserDto getResult = await _service.GetAsync(createResult.Id);
            getResult.ShouldBeEquivalentTo(createResult);

            ApplicationUserCreateDto admin = Create(ExplorerRole.Admin);
            createResult = await _service.CreateAsync(admin);
            getResult = await _service.GetAsync(createResult.Id);
            getResult.ShouldBeEquivalentTo(createResult);

            ApplicationUserCreateDto restrictedAdmin = Create(ExplorerRole.RestrictedAdmin);
            createResult = await _service.CreateAsync(restrictedAdmin);
            getResult = await _service.GetAsync(createResult.Id);
            getResult.ShouldBeEquivalentTo(createResult);
        }

        [Fact]
        public async Task UpdateAsyncTest()
        {
            _userManagerMock.LoadRoles();

            ApplicationUserCreateDto admin = Create(ExplorerRole.Admin);
            ApplicationUserCreateDto restrictedAdmin = Create(ExplorerRole.RestrictedAdmin);
            restrictedAdmin.PasswordValidityPeriodInDays++;
            ApplicationUserDto createResult = await _service.CreateAsync(admin);

            ApplicationUserDto udpateResult = await _service.UpdateAsync(createResult.Id, restrictedAdmin);
            udpateResult.Id.Should().Be(createResult.Id);
            udpateResult.UserName.Should().Be(admin.UserName);
            udpateResult.UserName.Should().NotBe(restrictedAdmin.UserName);
            udpateResult.PasswordValidityPeriodInDays.Should().Be(restrictedAdmin.PasswordValidityPeriodInDays);
            udpateResult.Roles.ShouldBeEquivalentTo(restrictedAdmin.Roles);

            udpateResult = await _service.UpdateAsync(createResult.Id, admin);
            udpateResult.Id.Should().Be(createResult.Id);
            udpateResult.UserName.Should().Be(admin.UserName);
            udpateResult.UserName.Should().NotBe(restrictedAdmin.UserName);
            udpateResult.PasswordValidityPeriodInDays.Should().Be(admin.PasswordValidityPeriodInDays);
            udpateResult.Roles.ShouldBeEquivalentTo(admin.Roles);
        }

        [Fact]
        public async Task UpdateAsyncPasswordTooShortTest()
        {
            _userManagerMock.LoadRoles();

            ApplicationUserCreateDto admin = Create(ExplorerRole.Admin);
            ApplicationUserDto createResult = await _service.CreateAsync(admin);
            admin.Password = "12abCD-";
            admin.PasswordConfirmation = "12abCD-";
            await Assert.ThrowsAsync<IdentityException>(() => _service.UpdateAsync(createResult.Id, admin));
        }

        [Fact]
        public async Task UpdateAsyncPasswordMissingSpecialCharsTest()
        {
            await TestPassword("123abcCDE");
        }

        [Fact]
        public async Task UpdateAsyncPasswordMissingUpperCaseTest()
        {
            await TestPassword("1234abcd-");
        }

        [Fact]
        public async Task UpdateAsyncPasswordMissingLowerCaseTest()
        {
            await TestPassword("1234ABCD-");
        }

        [Fact]
        public async Task DeleteAsyncTest()
        {
            _userManagerMock.LoadRoles();

            ApplicationUserDto saResult = await _service.CreateSuperAdminAsync(CreateSuperAdmin());
            ApplicationUserDto adminResult = await _service.CreateAsync(Create(ExplorerRole.Admin));
            ApplicationUserDto restrictedAdminResult = await _service.CreateAsync(Create(ExplorerRole.RestrictedAdmin));

            await _service.DeleteAsync(restrictedAdminResult.Id);
            await _service.DeleteAsync(adminResult.Id);
            await Assert.ThrowsAsync<ForbiddenActionException>(() => _service.DeleteAsync(saResult.Id));
            await Assert.ThrowsAsync<EntityNotFoundException>(() => _service.DeleteAsync(adminResult.Id));
            await Assert.ThrowsAsync<EntityNotFoundException>(() => _service.DeleteAsync(restrictedAdminResult.Id));
            await Assert.ThrowsAsync<EntityNotFoundException>(() => _service.DeleteAsync("wrong id"));

            // no exception?
        }

        private async Task TestPassword(string invalidPassword)
        {
            _userManagerMock.LoadRoles();

            ApplicationUserCreateDto admin = Create(ExplorerRole.Admin);
            string validPassword = admin.Password;
            admin.Password = invalidPassword;
            admin.PasswordConfirmation = invalidPassword;
            await Assert.ThrowsAsync<IdentityException>(() => _service.CreateAsync(admin));
            admin.Password = validPassword;
            admin.PasswordConfirmation = validPassword;
            ApplicationUserDto createResult = await _service.CreateAsync(admin);
            admin.Password = invalidPassword;
            admin.PasswordConfirmation = invalidPassword;
            await Assert.ThrowsAsync<IdentityException>(() => _service.UpdateAsync(createResult.Id, admin));
            admin.Password = validPassword;
            admin.PasswordConfirmation = validPassword;
            await _service.UpdateAsync(createResult.Id, admin);
        }

        private static ApplicationSuperAdminCreateDto CreateSuperAdmin()
        {
            return new ApplicationSuperAdminCreateDto
            {
                    UserName = "testSa",
                    Password = "PassworD$1234",
                    PasswordConfirmation = "PassworD$1234"
            };
        }

        private static ApplicationUserCreateDto Create(ExplorerRole role)
        {
            return new ApplicationUserCreateDto
            {
                    UserName = "test" + role,
                    Password = role + "$1234",
                    PasswordConfirmation = role + "$1234",
                    Roles = new[]
                    {
                            role.ToString()
                    },
                    PasswordValidityPeriodInDays = 99
            };
        }
    }

    public class FakeUserManager : UserManager<ApplicationUser>
    {
        private readonly UsersDbContext _usersContext;

        public FakeUserManager(UsersDbContext usersContext)
                : base(new Mock<IUserStore<ApplicationUser>>().Object, new Mock<IOptions<IdentityOptions>>().Object, new Mock<IPasswordHasher<ApplicationUser>>().Object,
                        new IUserValidator<ApplicationUser>[0], new IPasswordValidator<ApplicationUser>[0], new Mock<ILookupNormalizer>().Object, new Mock<IdentityErrorDescriber>().Object,
                        new Mock<IServiceProvider>().Object, new Mock<ILogger<UserManager<ApplicationUser>>>().Object)
        {
            _usersContext = usersContext;
        }

        public void LoadRoles()
        {
            foreach(ExplorerRole saiaRole in EnumExtensions.GetValues<ExplorerRole>())
            {
                IdentityRole role = new IdentityRole(saiaRole.ToString());
                _usersContext.Roles.Add(role);
            }
        }

        public override Task<string> GeneratePasswordResetTokenAsync(ApplicationUser user)
        {
            return Task.FromResult(string.Empty);
        }

        public override Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string token, string newPassword)
        {
            if(!Regex.IsMatch(newPassword, ApplicationUser.PasswordRegex))
            {
                throw new IdentityException("WRONG", new IdentityError[0]);
            }

            user.PasswordHash = newPassword;
            _usersContext.Update(user);
            _usersContext.SaveChanges();
            return Task.FromResult(IdentityResult.Success);
        }

        public override Task<IdentityResult> CreateAsync(ApplicationUser user, string password)
        {
            if(!Regex.IsMatch(password, ApplicationUser.PasswordRegex))
            {
                throw new IdentityException("WRONG", new IdentityError[0]);
            }

            _usersContext.Add(user);
            _usersContext.SaveChanges();
            return Task.FromResult(IdentityResult.Success);
        }

        public override Task<IdentityResult> DeleteAsync(ApplicationUser user)
        {
            _usersContext.Remove(user);
            _usersContext.SaveChanges();
            return Task.FromResult(IdentityResult.Success);
        }

        public override Task<IdentityResult> AddToRoleAsync(ApplicationUser user, string role)
        {
            string roleId = GetRoleId(role);
            if(!GetRolesAsync(user).Result.Contains(role))
            {
                IdentityUserRole<string> userRole = new IdentityUserRole<string>();
                userRole.RoleId = roleId;
                userRole.UserId = user.Id;
                _usersContext.UserRoles.Add(userRole);
                _usersContext.SaveChanges();
            }

            return Task.FromResult(IdentityResult.Success);
        }

        public override Task<IdentityResult> RemoveFromRoleAsync(ApplicationUser user, string role)
        {
            string roleId = GetRoleId(role);
            IdentityUserRole<string> userRole = _usersContext.UserRoles.FirstOrDefault(ur => ur.UserId == user.Id && ur.RoleId == roleId);
            if(userRole != null)
            {
                _usersContext.UserRoles.Remove(userRole);
                _usersContext.SaveChanges();
            }

            return Task.FromResult(IdentityResult.Success);
        }

        public override Task<IList<string>> GetRolesAsync(ApplicationUser user)
        {
            string[] rolesIds = _usersContext.UserRoles.Where(ur => ur.UserId == user.Id).Select(ur => ur.RoleId).ToArray();
            return Task.FromResult((IList<string>)_usersContext.Roles.Where(r => rolesIds.Contains(r.Id)).Select(r => r.Name).ToArray());
        }

        public override Task<bool> IsInRoleAsync(ApplicationUser user, string role)
        {
            return Task.FromResult(GetRolesAsync(user).Result.Contains(role));
        }

        public override IQueryable<ApplicationUser> Users => _usersContext.Users;

        private string GetRoleId(string role)
        {
            return _usersContext.Roles.Where(r => r.Name == role).Select(r => r.Id).First();
        }
        */
    }
}