using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.DTOs.Log;
using Core.Extensions;
using Core.Services;
using Core.Validation;
using Data.Entities;
using Data.Extensions;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Configuration;
using Serilog.Events;
using Serilog.Parsing;
using Xunit;

namespace CoreTests
{
    public class LogServiceTest : AbstractTestBase
    {
        // ------------------------------------------
        // Table doesn't exist, unit tests won't pass
        // ------------------------------------------

        /*
        private readonly LogService _service;
        private readonly LogAccessLayer _accessLayer;
        private readonly DateTimeService _nowService;
        private readonly string _adminUIDataSource;

        public LogServiceTest()
        {
            string adminUIDataSourceFormat = Configuration.GetConnectionString("AdminUILogsConnection");
            string dbName = Guid.NewGuid().ToString();
            _adminUIDataSource = string.Format(adminUIDataSourceFormat, dbName);
            _accessLayer = new LogAccessLayer();
            _nowService = new DateTimeService();
            _service = new LogService(_adminUIDataSource, _accessLayer);
        }

        public override void Dispose()
        {
            base.Dispose();
            _accessLayer.RemoveDatabase(_adminUIDataSource);
        }

        [Fact]
        public async Task CreateDatabaseShouldRemoveOldDatabaseStructure()
        {
            _accessLayer.ClearDatabase(_adminUIDataSource);
            _accessLayer.CreateDatabase(_adminUIDataSource);
            _accessLayer.AddAll(_adminUIDataSource, CreateEvent());
            _accessLayer.CreateDatabase(_adminUIDataSource);
            IReadOnlyCollection<LogListDto> logs = await _service.GetAllAsync(GetFilterDto());
            logs.Should().HaveCount(1);
        }

        [Fact]
        public async Task GetAllAsyncShouldMerge2Databases()
        {
            _accessLayer.ClearDatabase(_adminUIDataSource);
            _accessLayer.CreateDatabase(_adminUIDataSource);

            IReadOnlyCollection<LogListDto> logs = await _service.GetAllAsync(GetFilterDto());
            logs.Should().HaveCount(0);

            _accessLayer.AddAll(_adminUIDataSource, CreateEvent());
            logs = await _service.GetAllAsync(GetFilterDto());
            logs.Should().HaveCount(1);
            _accessLayer.AddAll(_adminUIDataSource, CreateEvent());
            logs = await _service.GetAllAsync(GetFilterDto());
            logs.Should().HaveCount(2);
        }

        [Fact]
        public async Task GetAllAsyncShouldManagePagingFor2Databases()
        {
            _accessLayer.ClearDatabase(_adminUIDataSource);
            _accessLayer.CreateDatabase(_adminUIDataSource);
            await TestPaging(0);

            _accessLayer.AddAll(_adminUIDataSource, CreateEvent());
            await TestPaging(1);

            _accessLayer.AddAll(_adminUIDataSource, CreateEvent());
            await TestPaging(2);

            int sourceTargetCount = 10;
            int totalCount = sourceTargetCount + 1;
            _accessLayer.AddAll(_adminUIDataSource, (sourceTargetCount - 1).ForEach(j => CreateEvent()).ToArray());
            await TestPaging(totalCount);

            totalCount += sourceTargetCount;
            _accessLayer.AddAll(_adminUIDataSource, sourceTargetCount.ForEach(j => CreateEvent()).ToArray());
            await TestPaging(totalCount);

            totalCount = 3 * sourceTargetCount;
            _accessLayer.AddAll(_adminUIDataSource, (sourceTargetCount - 1).ForEach(j => CreateEvent()).ToArray());
            await TestPaging(totalCount);

            totalCount += sourceTargetCount;
            _accessLayer.AddAll(_adminUIDataSource, sourceTargetCount.ForEach(j => CreateEvent()).ToArray());
            await TestPaging(totalCount);
        }

        [Fact]
        public async Task ClearDatabaseShouldNotThrowException()
        {
            _accessLayer.ClearDatabase(_adminUIDataSource);
            _accessLayer.CreateDatabase(_adminUIDataSource);
            LogEvent[] logs = 180.ForEach(e => CreateEvent()).ToArray();
            _accessLayer.AddAll(_adminUIDataSource, logs);
            StdLog[] allLogs = (await _accessLayer.GetLogsAsync(_adminUIDataSource, GetFilterDto())).ToArray();
            allLogs.Should().HaveCount(logs.Length);
            _accessLayer.ClearDatabase(_adminUIDataSource);
            allLogs = (await _accessLayer.GetLogsAsync(_adminUIDataSource, GetFilterDto())).ToArray();
            allLogs.Should().HaveCount(0);

            IReadOnlyCollection<LogListDto> logsDto = await _service.GetAllAsync(GetFilterDto());
            logsDto.Should().HaveCount(0);
            _accessLayer.AddAll(_adminUIDataSource, logs);
            _accessLayer.AddAll(_adminUIDataSource, logs);
            logsDto = await _service.GetAllAsync(GetFilterDto());
            logsDto.Should().HaveCount(logs.Length * 2);
            _service.ClearDatabase();
            logsDto = await _service.GetAllAsync(GetFilterDto());
            logsDto.Should().HaveCount(0);
        }

        [Fact]
        public async Task ClearLogsByTypeShouldNotThrowException()
        {
            _accessLayer.ClearDatabase(_adminUIDataSource);
            LogEventLevel[] levelsToRemove = EnumExtensions.GetValues<LogEventLevel>();
            LogEvent[] actualLogs = levelsToRemove.Select(l => CreateEvent(level: l)).ToArray();

            _accessLayer.CreateDatabase(_adminUIDataSource);
            _accessLayer.AddAll(_adminUIDataSource, actualLogs);
            _accessLayer.AddAll(_adminUIDataSource, actualLogs);

            for(int i = 0; i < levelsToRemove.Length; i++)
            {
                IReadOnlyCollection<LogListDto> logs = await _service.GetAllAsync(GetFilterDto());
                logs.Should().HaveCount(actualLogs.Length * 2);
                _service.ClearLogsByType(levelsToRemove[i]);
                LogEvent[] logsLeft = actualLogs.Where(l => l.Level >= levelsToRemove[i]).ToArray();
                foreach(LogEvent logLeft in logsLeft)
                {
                    (logLeft.Level < levelsToRemove[i]).Should().BeFalse();
                }

                LogEvent[] removedLogs = actualLogs.Except(logsLeft).ToArray();
                foreach(LogEvent removedLog in removedLogs)
                {
                    (removedLog.Level < levelsToRemove[i]).Should().BeTrue();
                }

                logs = await _service.GetAllAsync(GetFilterDto());
                logs.Should().HaveCount(logsLeft.Length * 2);
                int crtLevel = (int)LogLevelExtensions.Parse(levelsToRemove[i].ToString());
                foreach(LogListDto log in logs)
                {
                    ((int)log.Level).Should().BeGreaterOrEqualTo(crtLevel);
                }

                _accessLayer.AddAll(_adminUIDataSource, removedLogs);
                _accessLayer.AddAll(_adminUIDataSource, removedLogs);
            }
        }

        [Fact]
        public async Task DeleteLogsAsyncShouldNotThrowException()
        {
            _accessLayer.ClearDatabase(_adminUIDataSource);
            DtoValidationResultLevel[] levelsToRemove = EnumExtensions.GetValues<DtoValidationResultLevel>().Except(new[]
            {
                    DtoValidationResultLevel.None
            }).ToArray();
            LogEvent[] actualLogs = EnumExtensions.GetValues<LogEventLevel>().Select(l => CreateEvent(l.ToString(), level: l)).ToArray();

            _accessLayer.CreateDatabase(_adminUIDataSource);
            _accessLayer.AddAll(_adminUIDataSource, actualLogs);
            _accessLayer.AddAll(_adminUIDataSource, actualLogs);

            LogFilterDto filtering = GetFilterDto();
            for(int i = 0; i < levelsToRemove.Length; i++)
            {
                IReadOnlyCollection<LogListDto> logs = await _service.GetAllAsync(GetFilterDto());
                logs.Should().HaveCount(actualLogs.Length * 2);
                filtering.Level = levelsToRemove[i];
                DateTime to = filtering.To;
                filtering.To = filtering.From;
                await _service.DeleteAsync(filtering);
                filtering.To = to;
                (await _service.GetAllAsync(GetFilterDto())).Should().HaveCount(logs.Count);

                await _service.DeleteAsync(filtering);
                string[] removedLevels = levelsToRemove[i].ToSerilogLevels();
                LogEvent[] logsLeft = actualLogs.Where(l => !removedLevels.Contains(l.Level.ToString())).ToArray();
                foreach(LogEvent logLeft in logsLeft)
                {
                    removedLevels.Contains(logLeft.Level.ToString()).Should().BeFalse();
                }

                LogEvent[] removedLogs = actualLogs.Except(logsLeft).ToArray();
                foreach(LogEvent removedLog in removedLogs)
                {
                    removedLevels.Contains(removedLog.Level.ToString()).Should().BeTrue();
                }

                logs = await _service.GetAllAsync(GetFilterDto());
                logs.Should().HaveCount(logsLeft.Length * 2);
                int crtLevel = (int)levelsToRemove[i];
                foreach(LogListDto log in logs)
                {
                    ((int)log.Level).Should().NotBe(crtLevel);
                }

                _accessLayer.AddAll(_adminUIDataSource, removedLogs);
                _accessLayer.AddAll(_adminUIDataSource, removedLogs);
            }
        }

        private LogEvent CreateEvent(string message = "A message", DateTime? date = null, LogEventLevel level = LogEventLevel.Error, Exception exception = null)
        {
            return new LogEvent(new DateTimeOffset(date ?? _nowService.UtcNow()), level, exception, new MessageTemplate(message, new List<MessageTemplateToken>()), new List<LogEventProperty>());
        }

        private async Task TestPaging(int totalCount)
        {
            // All pages are full
            await TestPaging(10, totalCount);

            // The last page has only 1 element
            await TestPaging(Math.Max(0, totalCount - 1), totalCount);

            // There is exactly one page
            await TestPaging(totalCount, totalCount);

            // There is one page with too much space available
            await TestPaging(totalCount * 3, totalCount);
        }

        private async Task TestPaging(int pageSize, int totalCount)
        {
            int pagesCount = StdLogPage.ComputePagesCount(pageSize, totalCount);
            for(int pageIndex = 0; pageIndex < pagesCount + 1; pageIndex++)
            {
                PagedLogListDto logs = await _service.GetAllAsync(GetPagedFilterDto(pageIndex, pageSize));
                logs.Logs.Should().HaveCount(Math.Min(pageSize, Math.Max(0, totalCount - pageIndex * pageSize)));
                logs.PagesCount.Should().Be(pagesCount);
                logs.TotalCount.Should().Be(totalCount);
                logs.PageSize.Should().Be(pageSize);
                logs.PageIndex.Should().Be(Math.Min(pageIndex, pagesCount + 1));
            }
        }

        private LogFilterDto GetFilterDto()
        {
            DateTime now = _nowService.UtcNow();
            return new LogFilterDto
            {
                    Level = DtoValidationResultLevel.None,
                    From = now.AddYears(-1),
                    To = now.AddYears(1)
            };
        }

        private PagedLogFilterDto GetPagedFilterDto(int pageIndex, int pageSize)
        {
            DateTime now = _nowService.UtcNow();
            return new PagedLogFilterDto
            {
                    Level = DtoValidationResultLevel.None,
                    From = now.AddYears(-1),
                    To = now,
                    PageIndex = pageIndex,
                    PageSize = pageSize
            };
        }
    */
    }
}