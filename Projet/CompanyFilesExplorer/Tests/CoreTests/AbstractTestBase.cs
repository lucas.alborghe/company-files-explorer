﻿using System;
using System.Linq;
using Core.DataAccess;
using Core.Extensions;
using Core.Services;
using Core.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Shared.Configuration;

namespace CoreTests
{
    public abstract class AbstractTestBase : IDisposable
    {
        protected static readonly IConfigurationRoot Configuration;

        private ApplicationDbContext _context;

        /// <summary>
        ///     Gets the context.
        /// </summary>
        /// <value>The context.</value>
        protected ApplicationDbContext Context
        {
            get
            {
                if (_context == null)
                {
                    DbContextOptionsBuilder<ApplicationDbContext> optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
                    optionsBuilder.UseSqlite($"Data Source={Guid.NewGuid()}.db;");
                    _context = new ApplicationDbContext(optionsBuilder.Options);
                    _context.Database.EnsureCreated();
                }
                return _context;
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AbstractTestBase" /> class.
        /// </summary>
        static AbstractTestBase()
        {
            Configuration = ConfigurationHelper.CreateDefaultConfigurationBuiler().Build();
            CustomConfigExtensions.LoadConfiguration(Configuration);
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public virtual void Dispose()
        {
            if (_context == null)
            {
                return;
            }
            Context.Database.EnsureDeleted();
            Context.Dispose();
        }

        /// <summary>
        ///     Initializes the state of the context.
        /// </summary>
        protected void InitContextState()
        {
            foreach (EntityEntry entityEntry in Context.ChangeTracker.Entries().ToArray())
            {
                if (entityEntry.Entity != null)
                {
                    entityEntry.State = EntityState.Detached;
                }
            }
        }

        internal AllowedExtensionService CreateAllowedExtensionService()
        {
            Mock<ILogger<IAllowedExtensionService>> ccMock = new Mock<ILogger<IAllowedExtensionService>>();
            return new AllowedExtensionService(Context, ccMock.Object);
        }

        internal CustomConfigService CreateCustomConfigService()
        {
            DateTimeService dateTimeService = new DateTimeService();
            Mock<ILogger<ICustomConfigService>> ccMock = new Mock<ILogger<ICustomConfigService>>();
            return new CustomConfigService(Context, ccMock.Object, dateTimeService);
        }

        internal DirectoryService CreateDirectoryService()
        {
            DateTimeService dateTimeService = new DateTimeService();
            Mock<ILogger<IDirectoryService>> ccMock = new Mock<ILogger<IDirectoryService>>();
            return new DirectoryService(Context, ccMock.Object, dateTimeService, CreateRepositoryService());
        }

        internal FileService CreateFileService()
        {
            DateTimeService dateTimeService = new DateTimeService();
            Mock<ILogger<IFileService>> ccMock = new Mock<ILogger<IFileService>>();
            return new FileService(Context, ccMock.Object, dateTimeService, CreateRepositoryService());
        }

        internal MetadataService CreateMetadataService()
        {
            Mock<ILogger<IMetadataService>> ccMock = new Mock<ILogger<IMetadataService>>();
            return new MetadataService(Context, ccMock.Object, CreateProcedureService());
        }

        internal ProcedureService CreateProcedureService()
        {
            Mock<ILogger<IProcedureService>> ccMock = new Mock<ILogger<IProcedureService>>();
            return new ProcedureService(Context, ccMock.Object, CreateFileService());
        }

        internal RepositoryService CreateRepositoryService()
        {
            Mock<ILogger<IRepositoryService>> ccMock = new Mock<ILogger<IRepositoryService>>();
            return new RepositoryService(Context, ccMock.Object);
        }

        internal UserService CreateUserService()
        {
            Mock<ILogger<IUserService>> ccMock = new Mock<ILogger<IUserService>>();
            return new UserService(Context, ccMock.Object);
        }

        internal VirtualDirectoryService CreateVirtualDirectoryService()
        {
            Mock<ILogger<IVirtualDirectoryService>> ccMock = new Mock<ILogger<IVirtualDirectoryService>>();
            return new VirtualDirectoryService(Context, ccMock.Object, CreateProcedureService(), CreateUserService());
        }

        internal DatabaseManagerService CreateDatabaseManagerService()
        {
            Mock<ILogger<IDatabaseManagerService>> ccMock = new Mock<ILogger<IDatabaseManagerService>>();
            return new DatabaseManagerService(Context, ccMock.Object, CreateDirectoryService(), CreateFileService(), CreateRepositoryService(), CreateAllowedExtensionService());
        }

    }
}