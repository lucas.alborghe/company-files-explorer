using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.File;
using Core.Dtos.Repository;
using Core.Services.Interfaces;
using FluentAssertions;
using Newtonsoft.Json;
using Xunit;

namespace CoreTests
{
    public class FileServiceTest : AbstractTestBase
    {
        private readonly IFileService _fileService;
        private readonly IRepositoryService _repositoryService;

        public FileServiceTest()
        {
            _fileService = CreateFileService();
            _repositoryService = CreateRepositoryService();
        }

        [Fact]
        public async Task GetAllAsyncTest()
        {
            Func<Task<IReadOnlyCollection<FileListDto>>> getAll = async () => await _fileService.GetAllAsync();
            getAll.ShouldNotThrow();

            IReadOnlyCollection<FileListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            FileInputDto input = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(input);

            // shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            FileDto dto = await _fileService.GetAsync(1);
            dto.Should().NotBeNull();
            dto.Filepath.ShouldBeEquivalentTo(input.Filepath);
            dto.Filename.ShouldBeEquivalentTo(input.Filename);
            dto.Extension.ShouldBeEquivalentTo(input.Extension);

            // add a second item

            FileInputDto input2 = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "specs",
                Extension = ".pdf"
            };
            await _fileService.SaveAsync(input2);

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            dto = await _fileService.GetAsync(2);
            dto.Should().NotBeNull();
            dto.Filepath.ShouldBeEquivalentTo(input2.Filepath);
            dto.Filename.ShouldBeEquivalentTo(input2.Filename);
            dto.Extension.ShouldBeEquivalentTo(input2.Extension);

            // remove an item

            bool removed = await _fileService.RemoveAsync(1);
            removed.Should().BeTrue();

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);
        }

        [Fact]
        public async Task GetAllByFilenameAsyncTest()
        {
            Func<Task<IReadOnlyCollection<FileListDto>>> getAllByFilename = async () => await _fileService.GetAllByFilenameAsync("file");
            getAllByFilename.ShouldNotThrow();

            IReadOnlyCollection<FileListDto> allByFilename = await getAllByFilename();
            allByFilename.Should().NotBeNull();
            allByFilename.Should().BeEmpty();

            // add a first item (ok with filename)

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            FileInputDto input = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(input);

            // shouldnt be empty anymore

            allByFilename = await getAllByFilename();
            allByFilename.Should().NotBeNull();
            allByFilename.Should().NotBeEmpty();
            allByFilename.Should().HaveCount(1);

            // add a second item (nok with filename)

            FileInputDto input2 = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "specs",
                Extension = ".pdf"
            };
            await _fileService.SaveAsync(input2);

            allByFilename = await getAllByFilename();
            allByFilename.Should().NotBeNull();
            allByFilename.Should().NotBeEmpty();
            allByFilename.Should().HaveCount(1);

            // add a third item (ok with filename)

            FileInputDto input3 = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "alphabet_File",
                Extension = ".docx"
            };
            await _fileService.SaveAsync(input3);

            allByFilename = await getAllByFilename();
            allByFilename.Should().NotBeNull();
            allByFilename.Should().NotBeEmpty();
            allByFilename.Should().HaveCount(2);
        }

        [Fact]
        public async Task GetAllByPathAsyncTest()
        {
            Func<Task<IReadOnlyCollection<FileListDto>>> getAllByPath = async () => await _fileService.GetAllByPathAsync(@"C:\Users\Lucas\Desktop\UnitTests\Subdir");
            getAllByPath.ShouldNotThrow();

            IReadOnlyCollection<FileListDto> allByPath = await getAllByPath();
            allByPath.Should().NotBeNull();
            allByPath.Should().BeEmpty();

            // add a first item (ok with path)

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            FileInputDto input = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests\Subdir",
                Filename = "template",
                Extension = ".docx"
            };
            await _fileService.SaveAsync(input);

            // shouldnt be empty anymore

            allByPath = await getAllByPath();
            allByPath.Should().NotBeNull();
            allByPath.Should().NotBeEmpty();
            allByPath.Should().HaveCount(1);

            // add a second item (nok with path)

            FileInputDto input2 = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "specs",
                Extension = ".pdf"
            };
            await _fileService.SaveAsync(input2);

            allByPath = await getAllByPath();
            allByPath.Should().NotBeNull();
            allByPath.Should().NotBeEmpty();
            allByPath.Should().HaveCount(1);

            // add a third item (ok with path)

            FileInputDto input3 = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests\Subdir",
                Filename = "todo",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(input3);

            allByPath = await getAllByPath();
            allByPath.Should().NotBeNull();
            allByPath.Should().NotBeEmpty();
            allByPath.Should().HaveCount(2);

            // add a fourth item (ok with path)

            FileInputDto input4 = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests\Subdir\Subsubdir",
                Filename = "todo",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(input4);

            allByPath = await getAllByPath();
            allByPath.Should().NotBeNull();
            allByPath.Should().NotBeEmpty();
            allByPath.Should().HaveCount(3);
        }

        [Fact]
        public async Task GetRepositoryAsyncTest()
        {
            Func<Task<RepositoryDto>> getRepoItem1 = async () => await _fileService.GetRepositoryAsync(1);
            getRepoItem1.ShouldNotThrow();

            // item doesnt exist yet

            RepositoryDto repoItem1 = await getRepoItem1();
            repoItem1.Should().BeNull();

            // add the item

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            FileInputDto input = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(input);

            // item should exist

            repoItem1 = await getRepoItem1();
            repoItem1.Should().NotBeNull();
            repoItem1.AbsolutePath.ShouldAllBeEquivalentTo("//corp.sftcm.ch");
            repoItem1.Protocol.ShouldBeEquivalentTo("smb");
        }

        [Fact]
        public async Task GetJsonAsyncTest()
        {
            // prepare a new element

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            FileInputDto input = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            // add file
            await _fileService.SaveAsync(input);

            // get json
            string result = await _fileService.GetJsonAsync(1);
            result.Should().NotBeNull();

            FileDto dto = JsonConvert.DeserializeObject<FileDto>(result);
            dto.Id.ShouldBeEquivalentTo(1);
            dto.Filepath.ShouldBeEquivalentTo(@"C:\Users\Lucas\Desktop\UnitTests");
            dto.Filename.ShouldBeEquivalentTo("myFile");
            dto.Extension.ShouldBeEquivalentTo(".txt");
        }

        [Fact]
        public async Task GetAsyncTest()
        {
            Func<Task<FileDto>> getItem1 = async () => await _fileService.GetAsync(1);
            getItem1.ShouldNotThrow();

            // item doesnt exist yet

            FileDto item1 = await getItem1();
            item1.Should().BeNull();

            // add the item

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            FileInputDto input = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(input);

            // item should exist

            item1 = await getItem1();
            item1.Should().NotBeNull();
            item1.RepositoryId.ShouldBeEquivalentTo(1);
            item1.Filepath.ShouldBeEquivalentTo(@"C:\Users\Lucas\Desktop\UnitTests");

            // remove the item

            bool removed = await _fileService.RemoveAsync(1);
            removed.Should().BeTrue();

            // item shouldnt exist anymore

            item1 = await getItem1();
            item1.Should().BeNull();
        }

        [Fact]
        public async Task SaveAsCreateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<FileListDto>>> getAll = async () => await _fileService.GetAllAsync();
            getAll.ShouldNotThrow();

            IReadOnlyCollection<FileListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            FileInputDto input = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            FileDto created = await _fileService.SaveAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.Filepath.ShouldBeEquivalentTo(input.Filepath);
            created.Filename.ShouldBeEquivalentTo(input.Filename);

            // add the same item

            FileInputDto input2 = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(input2);

            // list should contain only 1 item

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // add a second item

            FileInputDto input3 = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests\Subdir",
                Filename = "todo",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(input3);

            // list should contain 2 items

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            // add the same item from an other repository

            RepositoryInputDto repoInput2 = new RepositoryInputDto()
            {
                AbsolutePath = @"C:\Users\Lucas\Desktop\UnitTests\Subdir",
                Protocol = "",
                Username = "",
                Password = ""
            };
            await _repositoryService.CreateAsync(repoInput2);

            FileInputDto input4 = new FileInputDto
            {
                RepositoryId = 2,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests\Subdir",
                Filename = "todo",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(input4);

            // list should contain 2 items

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);
        }
        
        [Fact]
        public async Task RemoveAsyncTest()
        {
            Func<Task<IReadOnlyCollection<FileListDto>>> getAll = async () => await _fileService.GetAllAsync();
            getAll.ShouldNotThrow();

            IReadOnlyCollection<FileListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            FileInputDto input = new FileInputDto
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(input);

            // list shouldnt be empty anymore (contains 1 item)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // remove an item

            bool removed = await _fileService.RemoveAsync(1);
            removed.Should().BeTrue();

            // list should be empty

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // try to remove the same item (shouldnt work)

            removed = await _fileService.RemoveAsync(1);
            removed.Should().BeFalse();
        }

    }
}