using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTOs.CustomConfig;
using Core.Services;
using FluentAssertions;
using Xunit;

namespace CoreTests
{
    public class CustomConfigServiceTest : AbstractTestBase
    {
        /*
        private readonly CustomConfigService _service;

        public CustomConfigServiceTest()
        {
            _service = CreateCustomConfigService();
        }

        [Fact]
        public async Task GetValuesAsyncShouldNotThrowException()
        {
            Func<Task<IReadOnlyCollection<CustomConfigListDto>>> getValues = async () => await _service.GetValuesAsync();
            getValues.ShouldNotThrow();

            IReadOnlyCollection<CustomConfigListDto> values = await getValues();
            values.Should().NotBeNull();
            values.Should().BeEmpty();

            CustomConfigInputDto input = new CustomConfigInputDto
            {
                    CustomLoginMessage = "Login",
                    CustomHomeMessage = "Home",
                    AutoApiKeyDeactivationInactivityPeriodInDays = 24,
                    MaxApiRequestsCountPerMinute = 234,
                    MaxPLCRequestsCountPerMinute = 234,
                    PasswordValidityPeriodInDays = 89,
                    AutoLogoutInactivityPeriodInMinutes = 10
            };
            await _service.SaveAsync(input);
            CustomConfigDto dto = await _service.GetAsync();
            dto.Should().NotBeNull();
            dto.ShouldBeEquivalentTo(input);
        }
        */
    }
}