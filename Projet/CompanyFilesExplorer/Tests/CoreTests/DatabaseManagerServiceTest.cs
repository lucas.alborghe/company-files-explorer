using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.Extension;
using Core.Dtos.File;
using Core.Dtos.Repository;
using Core.Services.Interfaces;
using FluentAssertions;
using Xunit;

namespace CoreTests
{
    public class DatabaseManagerServiceTest : AbstractTestBase
    {
        private readonly IDatabaseManagerService _databaseManagerService;
        private readonly IFileService _fileService;
        private readonly IRepositoryService _repositoryService;
        private readonly IAllowedExtensionService _extensionService;

        public DatabaseManagerServiceTest()
        {
            _databaseManagerService = CreateDatabaseManagerService();
            _fileService = CreateFileService();
            _repositoryService = CreateRepositoryService();
            _extensionService = CreateAllowedExtensionService();
        }

        [Fact]
        public async Task RefreshDatabaseAsyncTest()
        {
            Func<Task<IReadOnlyCollection<FileListDto>>> getRefreshedFiles = async () => await _databaseManagerService.RefreshDatabaseAsync();
            getRefreshedFiles.ShouldNotThrow();

            Func<Task<IReadOnlyCollection<FileListDto>>> getAllFiles = async () => await _fileService.GetAllAsync();
            getAllFiles.ShouldNotThrow();

            // should be empty because no repository is specified yet

            IReadOnlyCollection<FileListDto> refreshedFiles = await getRefreshedFiles();
            refreshedFiles.Should().NotBeNull();
            refreshedFiles.Should().BeEmpty();

            IReadOnlyCollection<FileListDto> getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().BeEmpty();

            // add a repository

            RepositoryInputDto input = new RepositoryInputDto()
            {
                AbsolutePath = @"C:\Users\Lucas\Desktop\UnitTests",
                Protocol = "",
                Username = "",
                Password = ""
            };
            await _repositoryService.CreateAsync(input);

            // should be empty because no allowed extension is specified yet

            refreshedFiles = await getRefreshedFiles();
            refreshedFiles.Should().NotBeNull();
            refreshedFiles.Should().BeEmpty();

            getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().BeEmpty();

            // add some allowed extensions (pdf and docx (not txt!))

            ExtensionInputDto allowedExtension1 = new ExtensionInputDto()
            {
                Extension = ".pdf"
            };
            await _extensionService.CreateAsync(allowedExtension1);

            ExtensionInputDto allowedExtension2 = new ExtensionInputDto()
            {
                Extension = ".docx"
            };
            await _extensionService.CreateAsync(allowedExtension2);

            // should not be empty anymore (3 items)

            refreshedFiles = await getRefreshedFiles();
            refreshedFiles.Should().NotBeNull();
            refreshedFiles.Should().NotBeEmpty();
            refreshedFiles.Should().HaveCount(3);

            getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().NotBeEmpty();
            getAll.Should().HaveCount(3);

            // now, add new extension for a real refresh (extension txt)

            ExtensionInputDto allowedExtension3 = new ExtensionInputDto()
            {
                Extension = ".txt"
            };
            await _extensionService.CreateAsync(allowedExtension3);

            // should contain more items (6)

            refreshedFiles = await getRefreshedFiles();
            refreshedFiles.Should().NotBeNull();
            refreshedFiles.Should().NotBeEmpty();
            refreshedFiles.Should().HaveCount(6);

            getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().NotBeEmpty();
            getAll.Should().HaveCount(6);

            // add a second repository

            RepositoryInputDto input2 = new RepositoryInputDto()
            {
                AbsolutePath = @"C:\Users\Lucas\Desktop\UnitTests\Subdir",
                Protocol = "",
                Username = "",
                Password = ""
            };
            await _repositoryService.CreateAsync(input2);

            // the files should be refreshed but not added

            refreshedFiles = await getRefreshedFiles();
            refreshedFiles.Should().NotBeNull();
            refreshedFiles.Should().NotBeEmpty();
            refreshedFiles.Should().HaveCount(9);

            getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().NotBeEmpty();
            getAll.Should().HaveCount(6);
        }

        [Fact]
        public async Task RefreshDatabaseFromPathAsyncTest()
        {
            Func<Task<IReadOnlyCollection<FileListDto>>> getRefreshedFilesFromPath = async () =>
                await _databaseManagerService.RefreshDatabaseFromPathAsync(@"C:\Users\Lucas\Desktop\UnitTests\Subdir");
            getRefreshedFilesFromPath.ShouldNotThrow();

            Func<Task<IReadOnlyCollection<FileListDto>>> getAllFiles = async () => await _fileService.GetAllAsync();
            getAllFiles.ShouldNotThrow();

            // should be empty because no repository is specified yet

            IReadOnlyCollection<FileListDto> refreshedFilesFromPath = await getRefreshedFilesFromPath();
            refreshedFilesFromPath.Should().NotBeNull();
            refreshedFilesFromPath.Should().BeEmpty();

            IReadOnlyCollection<FileListDto> getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().BeEmpty();

            // add a repository

            RepositoryInputDto input = new RepositoryInputDto()
            {
                AbsolutePath = @"C:\Users\Lucas\Desktop\UnitTests",
                Protocol = "",
                Username = "",
                Password = ""
            };
            await _repositoryService.CreateAsync(input);

            // should be empty because no allowed extension is specified yet

            refreshedFilesFromPath = await getRefreshedFilesFromPath();
            refreshedFilesFromPath.Should().NotBeNull();
            refreshedFilesFromPath.Should().BeEmpty();

            getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().BeEmpty();

            // add some allowed extensions (pdf and docx (not txt!))

            ExtensionInputDto allowedExtension1 = new ExtensionInputDto()
            {
                Extension = ".pdf"
            };
            await _extensionService.CreateAsync(allowedExtension1);

            ExtensionInputDto allowedExtension2 = new ExtensionInputDto()
            {
                Extension = ".docx"
            };
            await _extensionService.CreateAsync(allowedExtension2);

            // should not be empty anymore (1 item) (only 1 .docx in these folders)

            refreshedFilesFromPath = await getRefreshedFilesFromPath();
            refreshedFilesFromPath.Should().NotBeNull();
            refreshedFilesFromPath.Should().NotBeEmpty();
            refreshedFilesFromPath.Should().HaveCount(1);

            getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().NotBeEmpty();
            getAll.Should().HaveCount(1);

            // now, add new extension for a real refresh (extension txt)

            ExtensionInputDto allowedExtension3 = new ExtensionInputDto()
            {
                Extension = ".txt"
            };
            await _extensionService.CreateAsync(allowedExtension3);

            // should contain more items (3)

            refreshedFilesFromPath = await getRefreshedFilesFromPath();
            refreshedFilesFromPath.Should().NotBeNull();
            refreshedFilesFromPath.Should().NotBeEmpty();
            refreshedFilesFromPath.Should().HaveCount(3);

            getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().NotBeEmpty();
            getAll.Should().HaveCount(3);

            // add a second repository

            RepositoryInputDto input2 = new RepositoryInputDto()
            {
                AbsolutePath = @"C:\Users\Lucas\Desktop\UnitTests\Subdir",
                Protocol = "",
                Username = "",
                Password = ""
            };
            await _repositoryService.CreateAsync(input2);

            // the files should be refreshed but not added

            IReadOnlyCollection<FileListDto> refreshedFilesFromPath2 = await _databaseManagerService.RefreshDatabaseFromPathAsync(@"C:\Users\Lucas\Desktop\UnitTests\Subdir\Subsubdir");
            refreshedFilesFromPath2.Should().NotBeNull();
            refreshedFilesFromPath2.Should().NotBeEmpty();
            refreshedFilesFromPath2.Should().HaveCount(1);

            getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().NotBeEmpty();
            getAll.Should().HaveCount(3);
        }

        [Fact]
        public async Task RefreshDatabaseFromPathAsyncAfterRefreshDatabaseTest()
        {
            Func<Task<IReadOnlyCollection<FileListDto>>> getRefreshedFiles = async () => await _databaseManagerService.RefreshDatabaseAsync();
            getRefreshedFiles.ShouldNotThrow();

            Func<Task<IReadOnlyCollection<FileListDto>>> getAllFiles = async () => await _fileService.GetAllAsync();
            getAllFiles.ShouldNotThrow();

            // should be empty because no repository is specified yet

            IReadOnlyCollection<FileListDto> refreshedFiles = await getRefreshedFiles();
            refreshedFiles.Should().NotBeNull();
            refreshedFiles.Should().BeEmpty();

            IReadOnlyCollection<FileListDto> getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().BeEmpty();

            // add a repository

            RepositoryInputDto input = new RepositoryInputDto()
            {
                AbsolutePath = @"C:\Users\Lucas\Desktop\UnitTests",
                Protocol = "",
                Username = "",
                Password = ""
            };
            await _repositoryService.CreateAsync(input);

            ExtensionInputDto allowedExtension1 = new ExtensionInputDto()
            {
                Extension = ".pdf"
            };
            await _extensionService.CreateAsync(allowedExtension1);

            ExtensionInputDto allowedExtension2 = new ExtensionInputDto()
            {
                Extension = ".docx"
            };
            await _extensionService.CreateAsync(allowedExtension2);

            // should not be empty anymore (3 items)

            refreshedFiles = await getRefreshedFiles();
            refreshedFiles.Should().NotBeNull();
            refreshedFiles.Should().NotBeEmpty();
            refreshedFiles.Should().HaveCount(3);

            getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().NotBeEmpty();
            getAll.Should().HaveCount(3);

            // now, add new extension for a real refresh (extension txt)

            ExtensionInputDto allowedExtension3 = new ExtensionInputDto()
            {
                Extension = ".txt"
            };
            await _extensionService.CreateAsync(allowedExtension3);

            // should contain more items (6)

            refreshedFiles = await getRefreshedFiles();
            refreshedFiles.Should().NotBeNull();
            refreshedFiles.Should().NotBeEmpty();
            refreshedFiles.Should().HaveCount(6);

            getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().NotBeEmpty();
            getAll.Should().HaveCount(6);
            
            // try now to refresh subdir

            Func<Task<IReadOnlyCollection<FileListDto>>> getRefreshedFilesFromPath = async () =>
                await _databaseManagerService.RefreshDatabaseFromPathAsync(@"C:\Users\Lucas\Desktop\UnitTests\Subdir");
            getRefreshedFilesFromPath.ShouldNotThrow();
            
            // should contain the same number of items but 3 should have been refreshed

            refreshedFiles = await getRefreshedFilesFromPath();
            refreshedFiles.Should().NotBeNull();
            refreshedFiles.Should().NotBeEmpty();
            refreshedFiles.Should().HaveCount(3);

            getAll = await getAllFiles();
            getAll.Should().NotBeNull();
            getAll.Should().NotBeEmpty();
            getAll.Should().HaveCount(6);
        }
        
        /*
         * Organisation des fichiers pour les tests unitaires
         *
         *  C:\Users\Lucas\Desktop\UnitTests
         *  |
         *  |__ Subdir
         *          |__ Subsubdir
         *                      |__ todo.txt
         *          |__ template.docx
         *          |__ todo.txt
         *  |__ alphabet_File.docx
         *  |__ myFile.txt
         *  |__ specs.pdf
         */
    }
}