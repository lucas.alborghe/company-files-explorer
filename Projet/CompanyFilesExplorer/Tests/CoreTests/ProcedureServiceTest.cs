using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Dtos.File;
using Core.Dtos.Metadata;
using Core.Dtos.Procedure;
using Core.Dtos.Repository;
using Core.Dtos.User;
using Core.Dtos.VirtualDirectory;
using Core.Services.Interfaces;
using FluentAssertions;
using Xunit;

namespace CoreTests
{
    public class ProcedureServiceTest : AbstractTestBase
    {
        private readonly IProcedureService _procedureService;
        private readonly IMetadataService _metadataService;
        private readonly IUserService _userService;
        private readonly IVirtualDirectoryService _virtualDirectoryService;
        private readonly IFileService _fileService;
        private readonly IRepositoryService _repositoryService;

        public ProcedureServiceTest()
        {
            _procedureService = CreateProcedureService();
            _metadataService = CreateMetadataService();
            _userService = CreateUserService();
            _virtualDirectoryService = CreateVirtualDirectoryService();
            _fileService = CreateFileService();
            _repositoryService = CreateRepositoryService();
        }

        [Fact]
        public async Task GetAllAsyncTest()
        {
            Func<Task<IReadOnlyCollection<ProcedureListDto>>> getAll = async () => await _procedureService.GetAllAsync();
            getAll.ShouldNotThrow();

            IReadOnlyCollection<ProcedureListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            ProcedureInputDto input = new ProcedureInputDto
            {
                Name = "My procedure",
                Rule = "{0}_{1}.pdf",
                Description = "This is useful"
            };
            await _procedureService.CreateAsync(input);

            // shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            ProcedureDto dto = await _procedureService.GetAsync(1);
            dto.Should().NotBeNull();
            dto.Name.ShouldBeEquivalentTo(input.Name);
            dto.Rule.ShouldBeEquivalentTo(input.Rule);

            // add a second item

            ProcedureInputDto input2 = new ProcedureInputDto
            {
                Name = "My procedure 2",
                Rule = "{0}_{1}_{2}.pdf",
                Description = "This is useful again"
            };
            await _procedureService.CreateAsync(input2);

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            dto = await _procedureService.GetAsync(2);
            dto.Should().NotBeNull();
            dto.Name.ShouldBeEquivalentTo(input2.Name);
            dto.Rule.ShouldBeEquivalentTo(input2.Rule);

            // remove an item

            bool removed = await _procedureService.RemoveAsync(1);
            removed.Should().BeTrue();

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);
        }

        [Fact]
        public async Task GetAsyncTest()
        {
            Func<Task<ProcedureDto>> getItem1 = async () => await _procedureService.GetAsync(1);
            getItem1.ShouldNotThrow();

            // item doesnt exist yet

            ProcedureDto item1 = await getItem1();
            item1.Should().BeNull();

            // add the item

            ProcedureInputDto input = new ProcedureInputDto
            {
                Name = "My procedure",
                Rule = "{0}_{1}.pdf",
                Description = "This is useful"
            };
            await _procedureService.CreateAsync(input);

            // item should exist

            item1 = await getItem1();
            item1.Should().NotBeNull();
            item1.Name.ShouldBeEquivalentTo("My procedure");

            // remove the item

            bool removed = await _procedureService.RemoveAsync(1);
            removed.Should().BeTrue();

            // item shouldnt exist anymore

            item1 = await getItem1();
            item1.Should().BeNull();
        }

        [Fact]
        public async Task GetFilesFromRuleAsyncTest()
        {
            Func<Task<IReadOnlyCollection<FileListDto>>> getFilesFromRule = async () => await _procedureService.GetFilesFromRuleAsync(@"UnitTests\Subdir");
            getFilesFromRule.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<FileListDto> all = await getFilesFromRule();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            RepositoryInputDto input = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(input);

            FileInputDto fileInput1 = new FileInputDto()
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests\Subdir",
                Filename = "todo",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(fileInput1);

            // list shouldnt be empty anymore (contains 1 item)

            all = await getFilesFromRule();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // add a second item

            FileInputDto fileInput2 = new FileInputDto()
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "specs",
                Extension = ".pdf"
            };
            await _fileService.SaveAsync(fileInput2);

            // list shouldnt be empty (still contains 1 item)

            all = await getFilesFromRule();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // remove item2

            bool removed = await _fileService.RemoveAsync(2);
            removed.Should().BeTrue();

            // list shouldnt be empty (contains 1 item)

            all = await getFilesFromRule();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // remove item1

            bool removed2 = await _fileService.RemoveAsync(1);
            removed2.Should().BeTrue();

            // list should be empty

            all = await getFilesFromRule();
            all.Should().NotBeNull();
            all.Should().BeEmpty();
        }

        [Fact]
        public async Task GetMetadataAsyncTest()
        {
            Func<Task<IReadOnlyCollection<MetadataListDto>>> getMetadataItem1 = async () => await _procedureService.GetMetadataAsync(1);
            getMetadataItem1.ShouldNotThrow();

            // item doesnt exist yet

            IReadOnlyCollection<MetadataListDto> metadataItem1 = await getMetadataItem1();
            metadataItem1.Should().BeEmpty();

            // add the item

            ProcedureInputDto input = new ProcedureInputDto
            {
                Name = "My procedure",
                Rule = "{0}_{1}.pdf",
                Description = "This is useful"
            };
            await _procedureService.CreateAsync(input);

            // item shouldnt exist

            metadataItem1 = await getMetadataItem1();
            metadataItem1.Should().BeEmpty();

            // add metadata

            MetadataInputDto metadataInput1 = new MetadataInputDto
            {
                ProcedureId = 1,
                Name = "Client name",
                Type = "text"
            };
            await _metadataService.CreateAsync(metadataInput1);

            MetadataInputDto metadataInput2 = new MetadataInputDto
            {
                ProcedureId = 1,
                Name = "Start day",
                Type = "Date",
                Fonction = "dd.mm.yy"
            };
            await _metadataService.CreateAsync(metadataInput2);

            // children should exist

            metadataItem1 = await getMetadataItem1();
            metadataItem1.Should().NotBeNull();
            metadataItem1.Should().HaveCount(2);
        }

        [Fact]
        public async Task GetVirtualDirectoriesAsyncTest()
        {
            Func<Task<IReadOnlyCollection<VirtualDirectoryListDto>>> getVirtualDirectoriesItem1 = async () => await _procedureService.GetVirtualDirectoriesAsync(1);
            getVirtualDirectoriesItem1.ShouldNotThrow();

            // item doesnt exist yet

            IReadOnlyCollection<VirtualDirectoryListDto> virtualDirectoriesItem1 = await getVirtualDirectoriesItem1();
            virtualDirectoriesItem1.Should().BeEmpty();

            // add the item

            ProcedureInputDto input = new ProcedureInputDto
            {
                Name = "My procedure",
                Rule = "{0}_{1}.pdf",
                Description = "This is useful"
            };
            await _procedureService.CreateAsync(input);

            // item shouldnt exist

            virtualDirectoriesItem1 = await getVirtualDirectoriesItem1();
            virtualDirectoriesItem1.Should().BeEmpty();

            // add virtual directories

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            VirtualDirectoryInputDto vdInput1 = new VirtualDirectoryInputDto
            {
                UserId = 1,
                ProcedureId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(vdInput1);

            VirtualDirectoryInputDto vdInput2 = new VirtualDirectoryInputDto
            {
                UserId = 1,
                ProcedureId = 1,
                Name = "My others"
            };
            await _virtualDirectoryService.CreateAsync(vdInput2);

            // children should exist

            virtualDirectoriesItem1 = await getVirtualDirectoriesItem1();
            virtualDirectoriesItem1.Should().NotBeNull();
            virtualDirectoriesItem1.Should().HaveCount(2);
        }

        [Fact]
        public async Task CreateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<ProcedureListDto>>> getAll = async () => await _procedureService.GetAllAsync();
            getAll.ShouldNotThrow();

            IReadOnlyCollection<ProcedureListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            ProcedureInputDto input = new ProcedureInputDto
            {
                Name = "My procedure",
                Rule = "{0}_{1}.pdf",
                Description = "This is useful"
            };
            ProcedureDto created = await _procedureService.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.Name.ShouldBeEquivalentTo(input.Name);
        }

        [Fact]
        public async Task UpdateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<ProcedureListDto>>> getAll = async () => await _procedureService.GetAllAsync();
            getAll.ShouldNotThrow();

            IReadOnlyCollection<ProcedureListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            ProcedureInputDto input = new ProcedureInputDto
            {
                Name = "My procedure",
                Rule = "{0}_{1}.pdf",
                Description = "This is useful"
            };
            ProcedureDto created = await _procedureService.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.Name.ShouldBeEquivalentTo(input.Name);

            // update the item

            ProcedureInputDto inputUpdate = new ProcedureInputDto
            {
                Name = "My updated procedure",
                Rule = "{0}_{1}.pdf",
                Description = "This is useful"
            };
            ProcedureDto updated = await _procedureService.UpdateAsync(1, inputUpdate);

            // list should contain 1 item

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one updated

            updated.Should().NotBeNull();
            all.First().Rule.ShouldBeEquivalentTo(updated.Rule);
            all.First().Description.ShouldBeEquivalentTo(updated.Description);
            all.First().Name.Should().NotBeNull(); // HERE !
        }

        [Fact]
        public async Task RemoveAsyncTest()
        {
            Func<Task<IReadOnlyCollection<ProcedureListDto>>> getAll = async () => await _procedureService.GetAllAsync();
            getAll.ShouldNotThrow();

            IReadOnlyCollection<ProcedureListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            ProcedureInputDto input = new ProcedureInputDto
            {
                Name = "My procedure",
                Rule = "{0}_{1}.pdf",
                Description = "This is useful"
            };
            await _procedureService.CreateAsync(input);

            // list shouldnt be empty anymore (contains 1 item)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // remove an item

            bool removed = await _procedureService.RemoveAsync(1);
            removed.Should().BeTrue();

            // list should be empty

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // try to remove the same item (shouldnt work)

            removed = await _procedureService.RemoveAsync(1);
            removed.Should().BeFalse();
        }

    }
}