using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Dtos.Directory;
using Core.Dtos.File;
using Core.Dtos.Repository;
using Core.Services.Interfaces;
using FluentAssertions;
using Xunit;

namespace CoreTests
{
    public class RepositoryServiceTest : AbstractTestBase
    {
        private readonly IRepositoryService _repositoryService;
        private readonly IDirectoryService _directoryService;
        private readonly IFileService _fileService;

        public RepositoryServiceTest()
        {
            _repositoryService = CreateRepositoryService();
            _directoryService = CreateDirectoryService();
            _fileService = CreateFileService();
        }

        [Fact]
        public async Task GetAllAsyncTest()
        {
            Func<Task<IReadOnlyCollection<RepositoryListDto>>> getAll = async () => await _repositoryService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<RepositoryListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            RepositoryInputDto input = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(input);

            // list shouldnt be empty anymore (contains 1 item)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // add a second item

            RepositoryInputDto input2 = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.mrphn.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "54321"
            };
            await _repositoryService.CreateAsync(input2);

            // list shouldnt be empty (contains 2 items)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            // remove an item

            bool removed = await _repositoryService.RemoveAsync(1);
            removed.Should().BeTrue();

            // list shouldnt be empty (contains 1 item)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);
        }

        [Fact]
        public async Task GetDirectoriesAsyncTest()
        {
            Func<Task<IReadOnlyCollection<DirectoryListDto>>> getAllDirectories = async () => await _repositoryService.GetDirectoriesAsync(1);
            getAllDirectories.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<DirectoryListDto> all = await getAllDirectories();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            RepositoryInputDto input = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(input);

            DirectoryInputDto directoryInput1 = new DirectoryInputDto()
            {
                Path = "C:\\Users\\John",
                RepositoryId = 1
            };
            await _directoryService.SaveAsync(directoryInput1);

            // list shouldnt be empty anymore (contains 1 item)

            all = await getAllDirectories();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // add a second item
            
            DirectoryInputDto directoryInput2 = new DirectoryInputDto()
            {
                Path = "C:\\Users\\John\\Doe",
                RepositoryId = 1
            };
            await _directoryService.SaveAsync(directoryInput2);

            // list shouldnt be empty (contains 2 items)

            all = await getAllDirectories();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            // remove an item

            bool removed = await _directoryService.RemoveAsync("C:\\Users\\John\\Doe");
            removed.Should().BeTrue();

            // list shouldnt be empty (contains 1 item)

            all = await getAllDirectories();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);
        }

        [Fact]
        public async Task GetFilesAsyncTest()
        {
            Func<Task<IReadOnlyCollection<FileListDto>>> getAllFiles = async () => await _repositoryService.GetFilesAsync(1);
            getAllFiles.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<FileListDto> all = await getAllFiles();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            RepositoryInputDto input = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(input);

            FileInputDto fileInput1 = new FileInputDto()
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(fileInput1);

            // list shouldnt be empty anymore (contains 1 item)

            all = await getAllFiles();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // add a second item

            FileInputDto fileInput2 = new FileInputDto()
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "specs",
                Extension = ".pdf"
            };
            await _fileService.SaveAsync(fileInput2);

            // list shouldnt be empty (contains 2 items)

            all = await getAllFiles();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            // remove an item

            bool removed = await _fileService.RemoveAsync(1);
            removed.Should().BeTrue();

            // list shouldnt be empty (contains 1 item)

            all = await getAllFiles();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);
        }

        [Fact]
        public async Task GetAsyncTest()
        {
            Func<Task<RepositoryDto>> getItem1 = async () => await _repositoryService.GetAsync(1);
            getItem1.ShouldNotThrow();

            // item doesnt exist yet

            RepositoryDto item1 = await getItem1();
            item1.Should().BeNull();

            // add the item

            RepositoryInputDto input = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(input);

            // item should exist

            item1 = await getItem1();
            item1.Should().NotBeNull();
            item1.AbsolutePath.ShouldAllBeEquivalentTo("//corp.sftcm.ch");
            item1.Protocol.ShouldBeEquivalentTo("smb");

            // remove the item

            bool removed = await _repositoryService.RemoveAsync(1);
            removed.Should().BeTrue();

            // item shouldnt exist anymore

            item1 = await getItem1();
            item1.Should().BeNull();
        }

        [Fact]
        public async Task CreateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<RepositoryListDto>>> getAll = async () => await _repositoryService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<RepositoryListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add the item

            RepositoryInputDto input = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            RepositoryDto created = await _repositoryService.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.AbsolutePath.ShouldBeEquivalentTo(input.AbsolutePath);
            created.Protocol.ShouldBeEquivalentTo(input.Protocol);
        }

        [Fact]
        public async Task UpdateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<RepositoryListDto>>> getAll = async () => await _repositoryService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<RepositoryListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add the item

            RepositoryInputDto input = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            RepositoryDto created = await _repositoryService.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.AbsolutePath.ShouldBeEquivalentTo(input.AbsolutePath);
            created.Protocol.ShouldBeEquivalentTo(input.Protocol);

            // update the item

            RepositoryInputDto inputUpdate = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.mrphn.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "54321"
            };
            RepositoryDto updated = await _repositoryService.UpdateAsync(1, inputUpdate);

            // list should contain 1 item

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one updated

            updated.Should().NotBeNull();
            all.First().AbsolutePath.Should().NotBe(input.AbsolutePath);
            all.First().Password.Should().NotBe(input.Password);
            all.First().Protocol.ShouldBeEquivalentTo(input.Protocol);
            all.First().Username.ShouldBeEquivalentTo(input.Username);
        }

        [Fact]
        public async Task RemoveAsyncTest()
        {
            Func<Task<IReadOnlyCollection<RepositoryListDto>>> getAll = async () => await _repositoryService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<RepositoryListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add the item

            RepositoryInputDto input = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // remove an item

            bool removed = await _repositoryService.RemoveAsync(1);
            removed.Should().BeTrue();

            // list should be empty

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // try to remove the same item (shouldnt work)

            removed = await _repositoryService.RemoveAsync(1);
            removed.Should().BeFalse();
        }

    }
}