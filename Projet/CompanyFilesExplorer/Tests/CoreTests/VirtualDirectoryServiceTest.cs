using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Dtos.File;
using Core.Dtos.Procedure;
using Core.Dtos.Repository;
using Core.Dtos.User;
using Core.Dtos.VirtualDirectory;
using Core.Services.Interfaces;
using FluentAssertions;
using Xunit;

namespace CoreTests
{
    public class VirtualDirectoryServiceTest : AbstractTestBase
    {
        private readonly IVirtualDirectoryService _virtualDirectoryService;
        private readonly IUserService _userService;
        private readonly IFileService _fileService;
        private readonly IProcedureService _procedureService;
        private readonly IRepositoryService _repositoryService;

        public VirtualDirectoryServiceTest()
        {
            _virtualDirectoryService = CreateVirtualDirectoryService();
            _userService = CreateUserService();
            _fileService = CreateFileService();
            _procedureService = CreateProcedureService();
            _repositoryService = CreateRepositoryService();
        }

        [Fact]
        public async Task GetAllAsyncTest()
        {
            Func<Task<IReadOnlyCollection<VirtualDirectoryListDto>>> getAll = async () => await _virtualDirectoryService.GetAllAsync();
            getAll.ShouldNotThrow();

            IReadOnlyCollection<VirtualDirectoryListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            VirtualDirectoryInputDto input = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(input);

            // shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            VirtualDirectoryDto dto = await _virtualDirectoryService.GetAsync(1);
            dto.Should().NotBeNull();
            dto.UserId.ShouldBeEquivalentTo(input.UserId);
            dto.Name.ShouldBeEquivalentTo(input.Name);

            // add a second item

            VirtualDirectoryInputDto input2 = new VirtualDirectoryInputDto
            {
                UserId = 1,
                VirtualDirectoryId = 1,
                Name = "My templates"
            };
            await _virtualDirectoryService.CreateAsync(input2);

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            dto = await _virtualDirectoryService.GetAsync(2);
            dto.Should().NotBeNull();
            dto.UserId.ShouldBeEquivalentTo(input2.UserId);
            dto.Name.ShouldBeEquivalentTo(input2.Name);

            // remove an item

            bool removed = await _virtualDirectoryService.RemoveAsync(1);
            removed.Should().BeTrue();

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);
        }

        [Fact]
        public async Task GetAsyncTest()
        {
            Func<Task<VirtualDirectoryDto>> getItem1 = async () => await _virtualDirectoryService.GetAsync(1);
            getItem1.ShouldNotThrow();

            // item doesnt exist yet

            VirtualDirectoryDto item1 = await getItem1();
            item1.Should().BeNull();

            // add the item

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            VirtualDirectoryInputDto input = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(input);

            // item should exist

            item1 = await getItem1();
            item1.Should().NotBeNull();
            item1.UserId.ShouldBeEquivalentTo(1);
            item1.Name.ShouldBeEquivalentTo("My favorites");

            // remove the item

            bool removed = await _virtualDirectoryService.RemoveAsync(1);
            removed.Should().BeTrue();

            // item shouldnt exist anymore

            item1 = await getItem1();
            item1.Should().BeNull();
        }

        [Fact]
        public async Task GetProcedureAsyncTest()
        {
            Func<Task<ProcedureDto>> getProcedureItem1 = async () => await _virtualDirectoryService.GetProcedureAsync(1);
            getProcedureItem1.ShouldNotThrow();

            // item doesnt exist yet

            ProcedureDto procedureItem1 = await getProcedureItem1();
            procedureItem1.Should().BeNull();

            // add the item

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            ProcedureInputDto procedureInput = new ProcedureInputDto()
            {
                Name = "Add template",
                Rule = "{}_{}_{}",
                Description = "This is useful."
            };
            await _procedureService.CreateAsync(procedureInput);

            VirtualDirectoryInputDto input = new VirtualDirectoryInputDto
            {
                UserId = 1,
                ProcedureId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(input);

            // item should exist

            procedureItem1 = await getProcedureItem1();
            procedureItem1.Should().NotBeNull();
            procedureItem1.Name.ShouldAllBeEquivalentTo("Add template");
        }

        [Fact]
        public async Task GetUserAsyncTest()
        {
            Func<Task<UserDto>> getUserItem1 = async () => await _virtualDirectoryService.GetUserAsync(1);
            getUserItem1.ShouldNotThrow();

            // add the item

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            VirtualDirectoryInputDto input = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(input);

            // item should exist

            UserDto userItem1 = await getUserItem1();
            userItem1.Should().NotBeNull();
            userItem1.Login.ShouldAllBeEquivalentTo("john.doe");
        }

        [Fact]
        public async Task GetParentAsyncTest()
        {
            Func<Task<VirtualDirectoryDto>> getParentItem1 = async () => await _virtualDirectoryService.GetParentAsync(2);
            getParentItem1.ShouldNotThrow();

            // item doesnt exist yet

            VirtualDirectoryDto parentItem2 = await getParentItem1();
            parentItem2.Should().BeNull();

            // add the item

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            VirtualDirectoryInputDto parentInput = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My programs"
            };
            await _virtualDirectoryService.CreateAsync(parentInput);

            VirtualDirectoryInputDto input = new VirtualDirectoryInputDto
            {
                UserId = 1,
                VirtualDirectoryId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(input);

            // item should exist

            parentItem2 = await getParentItem1();
            parentItem2.Should().NotBeNull();
            parentItem2.Name.ShouldAllBeEquivalentTo("My programs");
        }

        [Fact]
        public async Task GetChildrenAsyncTest()
        {
            Func<Task<IReadOnlyCollection<VirtualDirectoryListDto>>> getChildrenItem1 =
                async () => await _virtualDirectoryService.GetChildrenAsync(1);
            getChildrenItem1.ShouldNotThrow();

            // item doesnt exist yet

            IReadOnlyCollection<VirtualDirectoryListDto> childrenItem1 = await getChildrenItem1();
            childrenItem1.Should().BeEmpty();

            // add the item

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            VirtualDirectoryInputDto input = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(input);

            // add children

            VirtualDirectoryInputDto childInput1 = new VirtualDirectoryInputDto
            {
                UserId = 1,
                VirtualDirectoryId = 1,
                Name = "My sub1"
            };
            await _virtualDirectoryService.CreateAsync(childInput1);

            VirtualDirectoryInputDto childInput2 = new VirtualDirectoryInputDto
            {
                UserId = 1,
                VirtualDirectoryId = 1,
                Name = "My sub2"
            };
            await _virtualDirectoryService.CreateAsync(childInput2);

            // children should exist

            childrenItem1 = await getChildrenItem1();
            childrenItem1.Should().NotBeNull();
            childrenItem1.Should().HaveCount(2);
        }

        [Fact]
        public async Task GetPreferencesAsyncTest()
        {
            Func<Task<IReadOnlyCollection<VirtualDirectoryPreferenceListDto>>> getPreferencesItem1 =
                async () => await _virtualDirectoryService.GetPreferencesAsync(1);
            getPreferencesItem1.ShouldNotThrow();

            // item doesnt exist yet

            IReadOnlyCollection<VirtualDirectoryPreferenceListDto> preferencesItem1 = await getPreferencesItem1();
            preferencesItem1.Should().BeEmpty();

            // add the item

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            VirtualDirectoryInputDto input = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(input);

            // item shouldnt exist

            preferencesItem1 = await getPreferencesItem1();
            preferencesItem1.Should().BeEmpty();

            // add preferences

            RepositoryInputDto repoInput1 = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput1);

            FileInputDto fileInput1 = new FileInputDto()
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(fileInput1);

            FileInputDto fileInput2 = new FileInputDto()
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "specs",
                Extension = ".pdf"
            };
            await _fileService.SaveAsync(fileInput2);

            await _virtualDirectoryService.SaveFileVirtualDirectoryAsync(1, 1, "My FEX File");
            await _virtualDirectoryService.SaveFileVirtualDirectoryAsync(1, 2, "My PDF File");

            // preferences should exist

            preferencesItem1 = await getPreferencesItem1();
            preferencesItem1.Should().NotBeNull();
            preferencesItem1.Should().HaveCount(2);
            preferencesItem1.ElementAt(0).Filename.ShouldBeEquivalentTo("My FEX File");
            preferencesItem1.ElementAt(1).Filename.ShouldBeEquivalentTo("My PDF File");
        }

        [Fact]
        public async Task GetFilesVirtualDirectoryAsyncTest()
        {
            Func<Task<IReadOnlyCollection<FileListDto>>> getFilesVirtualDirectory = async () => await _virtualDirectoryService.GetFilesVirtualDirectoryAsync(1);
            getFilesVirtualDirectory.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<FileListDto> all = await getFilesVirtualDirectory();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            RepositoryInputDto input = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(input);

            FileInputDto fileInput1 = new FileInputDto()
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(fileInput1);

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            VirtualDirectoryInputDto vdInput = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(vdInput);

            // add file pref 1

            await _virtualDirectoryService.SaveFileVirtualDirectoryAsync(1, 1, "My TXT File");

            // list shouldnt be empty anymore (contains 1 item)

            all = await getFilesVirtualDirectory();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // add a second item

            FileInputDto fileInput2 = new FileInputDto()
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "specs",
                Extension = ".pdf"
            };
            await _fileService.SaveAsync(fileInput2);

            VirtualDirectoryInputDto vdInput2 = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My others"
            };
            await _virtualDirectoryService.CreateAsync(vdInput2);

            // add file pref 2

            await _virtualDirectoryService.SaveFileVirtualDirectoryAsync(1, 2, "My PDF File");

            // list shouldnt be empty (contains 2 items)

            all = await getFilesVirtualDirectory();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            // remove an item

            bool removed = await _virtualDirectoryService.RemoveFileVirtualDirectoryAsync(1, 1);
            removed.Should().BeTrue();

            // list shouldnt be empty (contains 1 item)

            all = await getFilesVirtualDirectory();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);
        }

        [Fact]
        public async Task CreateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<VirtualDirectoryListDto>>> getAll = async () => await _virtualDirectoryService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<VirtualDirectoryListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // try to add an item (not working)

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);
            VirtualDirectoryInputDto input = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My favorites"
            };
            VirtualDirectoryDto created = await _virtualDirectoryService.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.Name.ShouldBeEquivalentTo(input.Name);
        }

        [Fact]
        public async Task UpdateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<VirtualDirectoryListDto>>> getAll = async () => await _virtualDirectoryService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<VirtualDirectoryListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add the item

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            VirtualDirectoryInputDto input = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My favorites"
            };
            VirtualDirectoryDto created = await _virtualDirectoryService.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.Name.ShouldBeEquivalentTo(input.Name);

            // update the item

            VirtualDirectoryInputDto inputUpdate = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My new favorites"
            };
            VirtualDirectoryDto updated = await _virtualDirectoryService.UpdateAsync(1, inputUpdate);

            // list should contain 1 item

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one updated

            updated.Should().NotBeNull();
            all.First().VirtualDirectoryId.ShouldBeEquivalentTo(updated.VirtualDirectoryId);
            all.First().UserId.ShouldBeEquivalentTo(updated.UserId);
            all.First().Name.Should().NotBeNull(); // HERE !
        }

        [Fact]
        public async Task RemoveAsyncTest()
        {
            Func<Task<IReadOnlyCollection<VirtualDirectoryListDto>>> getAll = async () => await _virtualDirectoryService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<VirtualDirectoryListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add the item

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            VirtualDirectoryInputDto input = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(input);

            // list shouldnt be empty anymore (contains 1 item)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // remove an item

            bool removed = await _virtualDirectoryService.RemoveAsync(1);
            removed.Should().BeTrue();

            // list should be empty

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // try to remove the same item (shouldnt work)

            removed = await _virtualDirectoryService.RemoveAsync(1);
            removed.Should().BeFalse();
        }

        [Fact]
        public async Task SaveFileVirtualDirectoryAsyncTestm()
        {
            // create some files

            RepositoryInputDto repoInput1 = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput1);

            FileInputDto fileInput1 = new FileInputDto()
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(fileInput1);

            FileInputDto fileInput2 = new FileInputDto()
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "specs",
                Extension = ".pdf"
            };
            await _fileService.SaveAsync(fileInput2);

            // add item

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            VirtualDirectoryInputDto input = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(input);

            // items should be true

            VirtualDirectoryPreferenceDto saveFile1 = await _virtualDirectoryService.SaveFileVirtualDirectoryAsync(1, 1, "My FEX File");
            saveFile1.Should().NotBeNull();
            saveFile1.VirtualDirectoryId.Should().Be(1);
            saveFile1.FileId.Should().Be(1);
            saveFile1.Filename.Should().Be("My FEX File");

            VirtualDirectoryPreferenceDto saveFile2 = await _virtualDirectoryService.SaveFileVirtualDirectoryAsync(1, 2, "My PDF File");
            saveFile2.Should().NotBeNull();
            saveFile2.VirtualDirectoryId.Should().Be(1);
            saveFile2.FileId.Should().Be(2);
            saveFile2.Filename.Should().Be("My PDF File");
        }

        [Fact]
        public async Task UpdateFileVirtualDirectoryAsyncTest()
        {
            // create some items

            RepositoryInputDto repoInput1 = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput1);

            FileInputDto fileInput1 = new FileInputDto()
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(fileInput1);

            FileInputDto fileInput2 = new FileInputDto()
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "specs",
                Extension = ".pdf"
            };
            await _fileService.SaveAsync(fileInput2);

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            VirtualDirectoryInputDto input = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(input);

            // item should exist

            VirtualDirectoryPreferenceDto saveFile1 = await _virtualDirectoryService.SaveFileVirtualDirectoryAsync(1, 1, "My FEX File");
            saveFile1.Should().NotBeNull();

            IReadOnlyCollection<VirtualDirectoryPreferenceListDto> preferencesItem1 = await _virtualDirectoryService.GetPreferencesAsync(1);
            preferencesItem1.Should().NotBeNull();
            preferencesItem1.Should().NotBeEmpty();
            preferencesItem1.Should().HaveCount(1);

            // update item

            VirtualDirectoryPreferenceDto updateFile1 = await _virtualDirectoryService.SaveFileVirtualDirectoryAsync(1, 1, "My PDF File");
            updateFile1.Should().NotBeNull();

            preferencesItem1 = await _virtualDirectoryService.GetPreferencesAsync(1);
            preferencesItem1.Should().NotBeNull();
            preferencesItem1.Should().NotBeEmpty();
            preferencesItem1.Should().HaveCount(1);
        }

        [Fact]
        public async Task RemoveFileVirtualDirectoryAsyncTest()
        {
            // create some items

            RepositoryInputDto repoInput1 = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput1);

            FileInputDto fileInput1 = new FileInputDto()
            {
                RepositoryId = 1,
                Filepath = @"C:\Users\Lucas\Desktop\UnitTests",
                Filename = "myFile",
                Extension = ".txt"
            };
            await _fileService.SaveAsync(fileInput1);

            UserInputDto userInput = new UserInputDto()
            {
                Login = "john.doe"
            };
            await _userService.CreateAsync(userInput);

            VirtualDirectoryInputDto input = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(input);

            // item should exist

            VirtualDirectoryPreferenceDto saveFile1 = await _virtualDirectoryService.SaveFileVirtualDirectoryAsync(1, 1, "My FEX File");
            saveFile1.Should().NotBeNull();

            IReadOnlyCollection<VirtualDirectoryPreferenceListDto> preferencesItem1 = await _virtualDirectoryService.GetPreferencesAsync(1);
            preferencesItem1.Should().NotBeNull();
            preferencesItem1.Should().NotBeEmpty();
            preferencesItem1.Should().HaveCount(1);

            // remove item

            bool removeFile1 = await _virtualDirectoryService.RemoveFileVirtualDirectoryAsync(1, 1);
            removeFile1.Should().BeTrue();

            preferencesItem1 = await _virtualDirectoryService.GetPreferencesAsync(1);
            preferencesItem1.Should().NotBeNull();
            preferencesItem1.Should().BeEmpty();
        }

    }
}