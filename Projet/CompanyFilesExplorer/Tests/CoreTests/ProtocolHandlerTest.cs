using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using FluentAssertions;
using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Microsoft.Win32;
using Shared.Utils;
using Xunit;

namespace CoreTests
{
    public class ProtocolHandlerTest : AbstractTestBase
    {
        /*
        private readonly string _exeLocation;

        public ProtocolHandlerTest()
        {
            _exeLocation = GetExeLocation();
            if (_exeLocation == null)
            {
                throw new ArgumentNullException("You must build CompanyFilesExplorer.Installer.CustomActions before launching this unit test in order to generate the .exe");
            }
        }

        [Fact]
        public void ProtocolHandlerRegistrationTest()
        {
            ProtocolHandlerHelper.Unregister();
            ProtocolHandlerHelper.IsRegistered().Should().BeFalse();
            ProtocolHandlerHelper.Register(_exeLocation);
            ProtocolHandlerHelper.IsRegistered().Should().BeTrue();
            string openKey = $"{ProtocolHandlerHelper.ProtocolKey}\\{ProtocolHandlerHelper.OpenKey}";
            RegistryKey key = Registry.ClassesRoot.OpenSubKey(openKey, true);
            Assert.NotNull(key);
            key.GetDefaultValue().StartsWith(GetSolutionDirectory()).Should().BeTrue();
            key.SetDefaultValue("test");
            ProtocolHandlerHelper.IsRegistered().Should().BeFalse();
            ProtocolHandlerHelper.Register(_exeLocation);
            ProtocolHandlerHelper.IsRegistered().Should().BeTrue();
            ProtocolHandlerHelper.Register(_exeLocation);
            ProtocolHandlerHelper.IsRegistered().Should().BeTrue();
            ProtocolHandlerHelper.Unregister();
            ProtocolHandlerHelper.IsRegistered().Should().BeFalse();
        }

        [Fact]
        public void ProtocolHandlerLinkTest()
        {
            ProtocolHandlerHelper.Register(_exeLocation);

            string urlToCall = $"{ProtocolHandlerHelper.ProtocolKey}://a_little_test";
            Process process = Process.Start(urlToCall);
            Assert.NotNull(process);
            string[] paths = GetProcess(process.Id);
            process.Kill();
            paths.Should().NotBeEmpty();
            paths.Should().HaveCount(1);
            string path = paths[0];
            Assert.NotNull(path);
            Path.GetFileName(path).Should().Be(ProtocolHandlerHelper.FileName);

            ProtocolHandlerHelper.Unregister();
        }

        [Fact]
        public void ProtocolHandlerConvertionTest()
        {
            string host = "https://morphean.com/fr/";
            ProtocolHandlerHelper.TryConvert(host, out string convertedHost).Should().BeFalse();
            host.Should().Be(convertedHost);

            host = "https:\\\\morphean.com\\fr\\";
            ProtocolHandlerHelper.TryConvert(host, out convertedHost).Should().BeFalse("because the conversion should not manage the URI consistency");
            host.Should().Be(convertedHost);

            host = $"{ProtocolHandlerHelper.ProtocolKey}://test";
            ProtocolHandlerHelper.TryConvert(host, out convertedHost).Should().BeTrue();
            host.Should().NotBe(convertedHost);
            host.Replace(ProtocolHandlerHelper.ProtocolKey, "https").Should().Be(convertedHost);

            host = $"{ProtocolHandlerHelper.ProtocolKey}://{ProtocolHandlerHelper.ProtocolKey}";
            ProtocolHandlerHelper.TryConvert(host, out convertedHost).Should().BeTrue();
            host.Should().NotBe(convertedHost);
            host.Replace(ProtocolHandlerHelper.ProtocolKey, "https").Should().NotBe(convertedHost, "because the conversion should the protocol, not the URL");
            $"https://{ProtocolHandlerHelper.ProtocolKey}".Should().Be(convertedHost, "because the conversion should the protocol, not the URL");
        }

        private static string GetSolutionDirectory()
        {
            string directory = Path.GetDirectoryName(typeof(ProtocolHandlerHelper).Assembly.Location);
            string solutionDirectory = directory.Split("Tests")[0];
            return solutionDirectory;
        }

        private static string GetExeLocation()
        {
            string solutionDirectory = GetSolutionDirectory();
            return Directory.GetFiles(solutionDirectory, ProtocolHandlerHelper.FileName, SearchOption.AllDirectories).FirstOrDefault();
        }

        private static string[] GetProcess(int processId)
        {
            string wmiQueryString = $"SELECT ProcessId, ExecutablePath, CommandLine FROM Win32_Process WHERE ProcessId = {processId}";
            using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(wmiQueryString))
            {
                using (ManagementObjectCollection results = searcher.Get())
                {
                    var query = from p in Process.GetProcesses()
                                join mo in results.Cast<ManagementObject>() on p.Id equals (int)(uint)mo["ProcessId"]
                                select new
                                {
                                    Process = p,
                                    Path = (string)mo["ExecutablePath"],
                                    CommandLine = (string)mo["CommandLine"]
                                };
                    return query.Select(q => q.Path).ToArray();
                }
            }
        }
        */
    }
}