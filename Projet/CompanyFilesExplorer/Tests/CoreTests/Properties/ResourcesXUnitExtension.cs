﻿using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;

namespace CoreTests.Properties
{
    public static class ResourcesXUnitExtension
    {
        public static string GetTextFile(this ResourceManager resourceManager, string fileName, CultureInfo culture)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string extension = fileName.StartsWith("StationMapping") ? "csv" : "json";
            string crtNamespace = typeof(Resources).Namespace.Replace("Properties", nameof(Resources));
            string resourceName = $"{crtNamespace}.{fileName}.{extension}";
            using(Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using(StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}