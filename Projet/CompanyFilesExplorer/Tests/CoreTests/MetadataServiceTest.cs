using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Dtos.Metadata;
using Core.Dtos.Procedure;
using Core.Services.Interfaces;
using FluentAssertions;
using Xunit;

namespace CoreTests
{
    public class MetadataServiceTest : AbstractTestBase
    {
        private readonly IMetadataService _metadataService;
        private readonly IProcedureService _procedureService;

        public MetadataServiceTest()
        {
            _metadataService = CreateMetadataService();
            _procedureService = CreateProcedureService();
        }

        [Fact]
        public async Task GetAllAsyncTest()
        {
            Func<Task<IReadOnlyCollection<MetadataListDto>>> getAll = async () => await _metadataService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<MetadataListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            ProcedureInputDto procedureInput = new ProcedureInputDto()
            {
                Name = "My procedure",
                Rule = "My rule",
                Description = "My description"
            };
            await _procedureService.CreateAsync(procedureInput);
            MetadataInputDto input = new MetadataInputDto()
            {
                ProcedureId = 1,
                Name = "Client name",
                Type = "text"
            };
            await _metadataService.CreateAsync(input);

            // list shouldnt be empty anymore (contains 1 item)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // add a second item

            MetadataInputDto input2 = new MetadataInputDto()
            {
                ProcedureId = 1,
                Name = "Date day",
                Type = "Date"
            };
            await _metadataService.CreateAsync(input2);

            // list shouldnt be empty (contains 2 items)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            // remove an item

            bool removed = await _metadataService.RemoveAsync(1);
            removed.Should().BeTrue();

            // list shouldnt be empty (contains 1 item)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);
        }

        [Fact]
        public async Task GetProcedureAsyncTest()
        {
            Func<Task<ProcedureDto>> getProcedureItem1 = async () => await _metadataService.GetProcedureAsync(1);
            getProcedureItem1.ShouldNotThrow();

            // item doesnt exist yet

            ProcedureDto procedureItem1 = await getProcedureItem1();
            procedureItem1.Should().BeNull();

            // add the item

            ProcedureInputDto procedureInput = new ProcedureInputDto()
            {
                Name = "My procedure",
                Rule = "My rule",
                Description = "My description"
            };
            await _procedureService.CreateAsync(procedureInput);
            MetadataInputDto input = new MetadataInputDto()
            {
                ProcedureId = 1,
                Name = "Client name",
                Type = "text"
            };
            await _metadataService.CreateAsync(input);

            // item should exist

            procedureItem1 = await getProcedureItem1();
            procedureItem1.Should().NotBeNull();
            procedureItem1.Name.ShouldAllBeEquivalentTo("My procedure");
        }

        [Fact]
        public async Task GetAsyncTest()
        {
            Func<Task<MetadataDto>> getItem1 = async () => await _metadataService.GetAsync(1);
            getItem1.ShouldNotThrow();

            // item doesnt exist yet

            MetadataDto item1 = await getItem1();
            item1.Should().BeNull();

            // add the item

            ProcedureInputDto procedureInput = new ProcedureInputDto()
            {
                Name = "My procedure",
                Rule = "My rule",
                Description = "My description"
            };
            await _procedureService.CreateAsync(procedureInput);
            MetadataInputDto input = new MetadataInputDto()
            {
                ProcedureId = 1,
                Name = "Client name",
                Type = "text"
            };
            await _metadataService.CreateAsync(input);

            // item should exist

            item1 = await getItem1();
            item1.Should().NotBeNull();
            item1.Name.ShouldAllBeEquivalentTo("Client name");
            item1.Type.ShouldBeEquivalentTo("text");

            // remove the item

            bool removed = await _metadataService.RemoveAsync(1);
            removed.Should().BeTrue();

            // item shouldnt exist anymore

            item1 = await getItem1();
            item1.Should().BeNull();
        }

        [Fact]
        public async Task CreateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<MetadataListDto>>> getAll = async () => await _metadataService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<MetadataListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add the item

            ProcedureInputDto procedureInput = new ProcedureInputDto()
            {
                Name = "My procedure",
                Rule = "My rule",
                Description = "My description"
            };
            await _procedureService.CreateAsync(procedureInput);
            MetadataInputDto input = new MetadataInputDto()
            {
                ProcedureId = 1,
                Name = "Client name",
                Type = "text"
            };
            MetadataDto created = await _metadataService.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.Name.ShouldBeEquivalentTo(input.Name);
            created.Type.ShouldBeEquivalentTo(input.Type);
        }

        [Fact]
        public async Task UpdateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<MetadataListDto>>> getAll = async () => await _metadataService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<MetadataListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add the item

            ProcedureInputDto procedureInput = new ProcedureInputDto()
            {
                Name = "My procedure",
                Rule = "My rule",
                Description = "My description"
            };
            await _procedureService.CreateAsync(procedureInput);
            MetadataInputDto input = new MetadataInputDto()
            {
                ProcedureId = 1,
                Name = "Client name",
                Type = "text"
            };
            MetadataDto created = await _metadataService.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.Name.ShouldBeEquivalentTo(input.Name);
            created.Type.ShouldBeEquivalentTo(input.Type);

            // update the item

            MetadataInputDto inputUpdate = new MetadataInputDto()
            {
                ProcedureId = 1,
                Name = "Name client",
                Type = "texte"
            };
            MetadataDto updated = await _metadataService.UpdateAsync(1, inputUpdate);

            // list should contain 1 item

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one updated

            updated.Should().NotBeNull();
            all.First().Name.Should().NotBe(input.Name);
            all.First().Type.Should().NotBe(input.Type);
        }

        [Fact]
        public async Task RemoveAsyncTest()
        {
            Func<Task<IReadOnlyCollection<MetadataListDto>>> getAll = async () => await _metadataService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<MetadataListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add the item

            ProcedureInputDto procedureInput = new ProcedureInputDto()
            {
                Name = "My procedure",
                Rule = "My rule",
                Description = "My description"
            };
            await _procedureService.CreateAsync(procedureInput);
            MetadataInputDto input = new MetadataInputDto()
            {
                ProcedureId = 1,
                Name = "Client name",
                Type = "text"
            };
            await _metadataService.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // remove an item

            bool removed = await _metadataService.RemoveAsync(1);
            removed.Should().BeTrue();

            // list should be empty

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // try to remove the same item (shouldnt work)

            removed = await _metadataService.RemoveAsync(1);
            removed.Should().BeFalse();
        }

    }
}