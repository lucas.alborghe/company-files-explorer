using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Dtos.Extension;
using Core.Services;
using FluentAssertions;
using Xunit;

namespace CoreTests
{
    public class AllowedExtensionServiceTest : AbstractTestBase
    {
        private readonly AllowedExtensionService _service;

        public AllowedExtensionServiceTest()
        {
            _service = CreateAllowedExtensionService();
        }

        [Fact]
        public async Task GetAllAsyncTest()
        {
            Func<Task<IReadOnlyCollection<ExtensionListDto>>> getAll = async () => await _service.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<ExtensionListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            ExtensionInputDto input = new ExtensionInputDto()
            {
                Extension = ".pdf"
            };
            await _service.CreateAsync(input);

            // list shouldnt be empty anymore (contains 1 item)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // add a second item

            ExtensionInputDto input2 = new ExtensionInputDto()
            {
                Extension = ".docx"
            };
            await _service.CreateAsync(input2);

            // list shouldnt be empty (contains 2 items)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            // remove an item

            bool removed = await _service.RemoveAsync(1);
            removed.Should().BeTrue();

            // list shouldnt be empty (contains 1 item)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);
        }

        [Fact]
        public async Task GetAsyncTest()
        {
            Func<Task<ExtensionDto>> getItem1 = async () => await _service.GetAsync(1);
            getItem1.ShouldNotThrow();

            // item doesnt exist yet

            ExtensionDto item1 = await getItem1();
            item1.Should().BeNull();

            // add the item

            ExtensionInputDto input = new ExtensionInputDto()
            {
                Extension = ".pdf"
            };
            await _service.CreateAsync(input);

            // item should exist

            item1 = await getItem1();
            item1.Should().NotBeNull();
            item1.Extension.ShouldAllBeEquivalentTo(".pdf");

            // remove the item

            bool removed = await _service.RemoveAsync(1);
            removed.Should().BeTrue();

            // item shouldnt exist anymore

            item1 = await getItem1();
            item1.Should().BeNull();
        }

        [Fact]
        public async Task CreateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<ExtensionListDto>>> getAll = async () => await _service.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<ExtensionListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            ExtensionInputDto input = new ExtensionInputDto()
            {
                Extension = ".pdf"
            };
            ExtensionDto created = await _service.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.Extension.ShouldBeEquivalentTo(input.Extension);
        }

        [Fact]
        public async Task UpdateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<ExtensionListDto>>> getAll = async () => await _service.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<ExtensionListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            ExtensionInputDto input = new ExtensionInputDto()
            {
                Extension = ".pdf"
            };
            ExtensionDto created = await _service.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.Extension.ShouldBeEquivalentTo(input.Extension);

            // update item

            ExtensionInputDto inputUpdated = new ExtensionInputDto()
            {
                Extension = ".docx"
            };
            ExtensionDto updated = await _service.UpdateAsync(1, inputUpdated);

            // list should contain 1 item

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one updated

            created.Should().NotBeNull();
            updated.Extension.ShouldBeEquivalentTo(inputUpdated.Extension);
        }

        [Fact]
        public async Task RemoveAsyncTest()
        {
            Func<Task<IReadOnlyCollection<ExtensionListDto>>> getAll = async () => await _service.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<ExtensionListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            ExtensionInputDto input = new ExtensionInputDto()
            {
                Extension = ".pdf"
            };
            await _service.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // remove an item

            bool removed = await _service.RemoveAsync(1);
            removed.Should().BeTrue();

            // list should be empty

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // try to remove the same item (shouldnt work)

            removed = await _service.RemoveAsync(1);
            removed.Should().BeFalse();
        }

    }
}