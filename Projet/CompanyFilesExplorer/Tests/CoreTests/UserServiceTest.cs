using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Dtos.User;
using Core.Dtos.VirtualDirectory;
using Core.Services.Interfaces;
using FluentAssertions;
using Xunit;

namespace CoreTests
{
    public class UserServiceTest : AbstractTestBase
    {
        private readonly IUserService _service;
        private readonly IVirtualDirectoryService _virtualDirectoryService;

        public UserServiceTest()
        {
            _service = CreateUserService();
            _virtualDirectoryService = CreateVirtualDirectoryService();
        }

        [Fact]
        public async Task GetAllAsyncTest()
        {
            Func<Task<IReadOnlyCollection<UserListDto>>> getAll = async () => await _service.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<UserListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            UserInputDto input = new UserInputDto
            {
                Login = "john.doe",
                IsAdmin = 0
            };
            await _service.CreateAsync(input);

            // list shouldnt be empty anymore (contains 1 item)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // add a second item

            UserInputDto input2 = new UserInputDto
            {
                Login = "bill.doe",
                IsAdmin = 0
            };
            await _service.CreateAsync(input2);

            // list shouldnt be empty (contains 2 items)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            // remove an item

            bool removed = await _service.RemoveAsync(1);
            removed.Should().BeTrue();

            // list shouldnt be empty (contains 1 item)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);
        }

        [Fact]
        public async Task GetAsyncTest()
        {
            Func<Task<UserDto>> getItem1 = async () => await _service.GetAsync(1);
            getItem1.ShouldNotThrow();

            // item doesnt exist yet

            UserDto item1 = await getItem1();
            item1.Should().BeNull();

            // add the item

            UserInputDto input = new UserInputDto
            {
                Login = "john.doe",
                IsAdmin = 0
            };
            await _service.CreateAsync(input);

            // item should exist

            item1 = await getItem1();
            item1.Should().NotBeNull();
            item1.Login.ShouldAllBeEquivalentTo("john.doe");
            item1.IsAdmin.ShouldBeEquivalentTo(0);

            // remove the item

            bool removed = await _service.RemoveAsync(1);
            removed.Should().BeTrue();

            // item shouldnt exist anymore

            item1 = await getItem1();
            item1.Should().BeNull();
        }

        [Fact]
        public async Task GetVirtualDirectoriesAsynTest()
        {
            Func<Task<IReadOnlyCollection<VirtualDirectoryListDto>>> getVirtualDirectories = async () => await _service.GetVirtualDirectoriesAsync(1);
            getVirtualDirectories.ShouldNotThrow();

            // item doesnt exist yet

            IReadOnlyCollection<VirtualDirectoryListDto> virtualDirectoriesItem1 = await getVirtualDirectories();
            virtualDirectoriesItem1.Should().BeEmpty();

            // add the item

            UserInputDto input = new UserInputDto
            {
                Login = "john.doe",
                IsAdmin = 0
            };
            await _service.CreateAsync(input);

            // item shouldnt exist

            virtualDirectoriesItem1 = await getVirtualDirectories();
            virtualDirectoriesItem1.Should().BeEmpty();

            // add virtual directories

            VirtualDirectoryInputDto vdInput1 = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My favorites"
            };
            await _virtualDirectoryService.CreateAsync(vdInput1);

            VirtualDirectoryInputDto vdInput2 = new VirtualDirectoryInputDto
            {
                UserId = 1,
                Name = "My others"
            };
            await _virtualDirectoryService.CreateAsync(vdInput2);

            // children should exist

            virtualDirectoriesItem1 = await getVirtualDirectories();
            virtualDirectoriesItem1.Should().NotBeNull();
            virtualDirectoriesItem1.Should().HaveCount(2);
        }

        [Fact]
        public async Task CreateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<UserListDto>>> getAll = async () => await _service.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<UserListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add the item

            UserInputDto input = new UserInputDto
            {
                Login = "john.doe",
                IsAdmin = 0
            };
            UserDto created = await _service.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.Login.ShouldBeEquivalentTo(input.Login);
            created.IsAdmin.ShouldBeEquivalentTo(input.IsAdmin);
        }

        [Fact]
        public async Task UpdateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<UserListDto>>> getAll = async () => await _service.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<UserListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add the item

            UserInputDto input = new UserInputDto
            {
                Login = "john.doe",
                IsAdmin = 0
            };
            UserDto created = await _service.CreateAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.Login.ShouldBeEquivalentTo(input.Login);
            created.IsAdmin.ShouldBeEquivalentTo(input.IsAdmin);

            // update the item

            UserInputDto inputUpdate = new UserInputDto
            {
                Login = "bill.doe",
                IsAdmin = 1
            };
            UserDto updated = await _service.UpdateAsync(1, inputUpdate);

            // list should contain 1 item

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one updated

            updated.Should().NotBeNull();
            all.First().Id.ShouldBeEquivalentTo(updated.Id);
            all.First().Login.ShouldBeEquivalentTo(updated.Login); // HERE !
            all.First().IsAdmin.ShouldBeEquivalentTo(updated.IsAdmin); // HERE !
        }

        [Fact]
        public async Task RemoveAsyncTest()
        {
            Func<Task<IReadOnlyCollection<UserListDto>>> getAll = async () => await _service.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<UserListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            UserInputDto input = new UserInputDto
            {
                Login = "john.doe",
                IsAdmin = 0
            };
            await _service.CreateAsync(input);

            // list shouldnt be empty anymore (contains 1 item)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // remove an item

            bool removed = await _service.RemoveAsync(1);
            removed.Should().BeTrue();

            // list should be empty

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // try to remove the same item (shouldnt work)

            removed = await _service.RemoveAsync(1);
            removed.Should().BeFalse();
        }

    }
}