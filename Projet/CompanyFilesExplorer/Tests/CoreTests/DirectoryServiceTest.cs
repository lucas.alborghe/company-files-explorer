using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Dtos.Directory;
using Core.Dtos.Repository;
using Core.Services.Interfaces;
using FluentAssertions;
using Xunit;

namespace CoreTests
{
    public class DirectoryServiceTest : AbstractTestBase
    {
        private readonly IDirectoryService _directoryService;
        private readonly IRepositoryService _repositoryService;

        public DirectoryServiceTest()
        {
            _directoryService = CreateDirectoryService();
            _repositoryService = CreateRepositoryService();
        }

        [Fact]
        public async Task GetAllAsyncTest()
        {
            Func<Task<IReadOnlyCollection<DirectoryListDto>>> getAll = async () => await _directoryService.GetAllAsync();
            getAll.ShouldNotThrow();

            IReadOnlyCollection<DirectoryListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            DirectoryInputDto input = new DirectoryInputDto
            {
                Path = @"C:\Users\John\Documents",
                RepositoryId = 1
            };
            await _directoryService.SaveAsync(input);

            // shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            DirectoryDto dto = await _directoryService.GetAsync(@"C:\Users\John\Documents");
            dto.Should().NotBeNull();
            dto.Path.ShouldBeEquivalentTo(input.Path);
            dto.RepositoryId.ShouldBeEquivalentTo(input.RepositoryId);

            // add a second item

            DirectoryInputDto input2 = new DirectoryInputDto
            {
                Path = @"C:\Users\Doe\Documents",
                RepositoryId = 1
            };
            await _directoryService.SaveAsync(input2);

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            dto = await _directoryService.GetAsync(@"C:\Users\Doe\Documents");
            dto.Should().NotBeNull();
            dto.Path.ShouldBeEquivalentTo(input2.Path);
            dto.RepositoryId.ShouldBeEquivalentTo(input2.RepositoryId);

            // remove an item

            bool removed = await _directoryService.RemoveAsync(@"C:\Users\John\Documents");
            removed.Should().BeTrue();

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);
        }

        [Fact]
        public async Task GetAllByPathAsyncTest()
        {
            Func<Task<IReadOnlyCollection<DirectoryListDto>>> getAllByPath = async () => await _directoryService.GetAllByPathAsync(@"C:\Users");
            getAllByPath.ShouldNotThrow();

            IReadOnlyCollection<DirectoryListDto> all = await getAllByPath();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            DirectoryInputDto input = new DirectoryInputDto
            {
                Path = @"C:\Users\John\Documents",
                RepositoryId = 1
            };
            await _directoryService.SaveAsync(input);

            // shouldnt be empty anymore

            all = await getAllByPath();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // add a second item

            DirectoryInputDto input2 = new DirectoryInputDto
            {
                Path = @"C:\Users\Doe\Documents",
                RepositoryId = 1
            };
            await _directoryService.SaveAsync(input2);

            all = await getAllByPath();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            // add a third item (but with different path)

            DirectoryInputDto input3 = new DirectoryInputDto
            {
                Path = @"C:\Program Files\Softcom SA",
                RepositoryId = 1
            };
            await _directoryService.SaveAsync(input3);

            all = await getAllByPath();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);
        }

        [Fact]
        public async Task GetAllByFilterAsyncTest()
        {
            Func<Task<IReadOnlyCollection<DirectoryListDto>>> getAllByFilter = async () => await _directoryService.GetAllByFilterAsync("Users");
            getAllByFilter.ShouldNotThrow();

            IReadOnlyCollection<DirectoryListDto> all = await getAllByFilter();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            DirectoryInputDto input = new DirectoryInputDto
            {
                Path = @"C:\Users\John\Documents",
                RepositoryId = 1
            };
            await _directoryService.SaveAsync(input);

            // shouldnt be empty anymore

            all = await getAllByFilter();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // add a second item

            DirectoryInputDto input2 = new DirectoryInputDto
            {
                Path = @"C:\Users\Doe\Documents",
                RepositoryId = 1
            };
            await _directoryService.SaveAsync(input2);

            all = await getAllByFilter();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);

            // add a third item (but with different path)

            DirectoryInputDto input3 = new DirectoryInputDto
            {
                Path = @"C:\Program Files\Softcom SA",
                RepositoryId = 1
            };
            await _directoryService.SaveAsync(input3);

            all = await getAllByFilter();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(2);
        }

        [Fact]
        public async Task GetRepositoryAsyncTest()
        {
            Func<Task<RepositoryDto>> getRepoItem1 = async () => await _directoryService.GetRepositoryAsync(@"C:\Users\John\Documents");
            getRepoItem1.ShouldNotThrow();

            // item doesnt exist yet

            RepositoryDto repoItem1 = await getRepoItem1();
            repoItem1.Should().BeNull();

            // add the item

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            DirectoryInputDto input = new DirectoryInputDto
            {
                Path = @"C:\Users\John\Documents",
                RepositoryId = 1
            };
            await _directoryService.SaveAsync(input);

            // item should exist

            repoItem1 = await getRepoItem1();
            repoItem1.Should().NotBeNull();
            repoItem1.AbsolutePath.ShouldAllBeEquivalentTo("//corp.sftcm.ch");
            repoItem1.Protocol.ShouldBeEquivalentTo("smb");
        }

        [Fact]
        public async Task GetAsyncTest()
        {
            Func<Task<DirectoryDto>> getItem1 = async () => await _directoryService.GetAsync(@"C:\Users\John\Documents");
            getItem1.ShouldNotThrow();

            // item doesnt exist yet

            DirectoryDto item1 = await getItem1();
            item1.Should().BeNull();

            // add the item

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            DirectoryInputDto input = new DirectoryInputDto
            {
                Path = @"C:\Users\John\Documents",
                RepositoryId = 1
            };
            await _directoryService.SaveAsync(input);

            // item should exist

            item1 = await getItem1();
            item1.Should().NotBeNull();
            item1.Path.ShouldAllBeEquivalentTo(@"C:\Users\John\Documents");
            item1.RepositoryId.ShouldBeEquivalentTo(1);

            // remove the item

            bool removed = await _directoryService.RemoveAsync(@"C:\Users\John\Documents");
            removed.Should().BeTrue();

            // item shouldnt exist anymore

            item1 = await getItem1();
            item1.Should().BeNull();
        }

        [Fact]
        public async Task SaveAsCreateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<DirectoryListDto>>> getAll = async () => await _directoryService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<DirectoryListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // try to add an item

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            DirectoryInputDto input = new DirectoryInputDto
            {
                Path = @"C:\Users\John\Documents",
                RepositoryId = 1
            };
            DirectoryDto created = await _directoryService.SaveAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.Path.ShouldBeEquivalentTo(input.Path);
            created.RepositoryId.ShouldBeEquivalentTo(input.RepositoryId);
        }

        [Fact]
        public async Task SaveAsUpdateAsyncTest()
        {
            Func<Task<IReadOnlyCollection<DirectoryListDto>>> getAll = async () => await _directoryService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<DirectoryListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add the item

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            DirectoryInputDto input = new DirectoryInputDto
            {
                Path = @"C:\Users\John\Documents",
                RepositoryId = 1
            };
            DirectoryDto created = await _directoryService.SaveAsync(input);

            // list shouldnt be empty anymore

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one added

            created.Should().NotBeNull();
            created.Path.ShouldBeEquivalentTo(input.Path);
            created.RepositoryId.ShouldBeEquivalentTo(input.RepositoryId);

            // update the item

            DirectoryInputDto inputUpdate = new DirectoryInputDto
            {
                Path = @"C:\Users\John\Documents",
                RepositoryId = 1
            };
            DirectoryDto updated = await _directoryService.SaveAsync(inputUpdate);

            // list should contain 1 item

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // the item should be the one updated

            updated.Should().NotBeNull();
            all.First().Path.ShouldBeEquivalentTo(updated.Path);
            all.First().RepositoryId.ShouldBeEquivalentTo(updated.RepositoryId);
            all.First().LastAccessDate.Should().NotBeNull(); // HERE !
        }

        [Fact]
        public async Task RemoveAsyncTest()
        {
            Func<Task<IReadOnlyCollection<DirectoryListDto>>> getAll = async () => await _directoryService.GetAllAsync();
            getAll.ShouldNotThrow();

            // list should be empty

            IReadOnlyCollection<DirectoryListDto> all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // add a first item

            RepositoryInputDto repoInput = new RepositoryInputDto()
            {
                AbsolutePath = "//corp.sftcm.ch",
                Protocol = "smb",
                Username = "john.doe",
                Password = "12345"
            };
            await _repositoryService.CreateAsync(repoInput);

            DirectoryInputDto input = new DirectoryInputDto
            {
                Path = @"C:\Users\John\Documents",
                RepositoryId = 1
            };
            await _directoryService.SaveAsync(input);

            // list shouldnt be empty anymore (contains 1 item)

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().NotBeEmpty();
            all.Should().HaveCount(1);

            // remove an item

            bool removed = await _directoryService.RemoveAsync(@"C:\Users\John\Documents");
            removed.Should().BeTrue();

            // list should be empty

            all = await getAll();
            all.Should().NotBeNull();
            all.Should().BeEmpty();

            // try to remove the same item (shouldnt work)

            removed = await _directoryService.RemoveAsync(@"C:\Users\John\Documents");
            removed.Should().BeFalse();
        }

    }
}