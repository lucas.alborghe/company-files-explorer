# Project informations

## Title
Company Files Explorer

## Abbreviation
FileExplo-I

## Description
L'idée de ce projet est de créer un " Files explorer " personnalisé gérant les fichiers éparpillés dans les disques partagés de l'entreprise SOFTCOM.
Le but de ce projet est de créer une application web permettant aux employés de gérer les fichiers (search, push, favoris, lister mes fichiers, etc...) et fournir aux administrateurs la possibilité de CRUD (create, read, update, delete) les workflows (procédures) des documents de l'entreprise, du département, de l'unité opérationnel, etc...

## Keywords
Systèmes distribués, Stokage