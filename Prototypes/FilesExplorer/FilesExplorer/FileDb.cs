﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FilesExplorer
{
    /// <summary>
    ///     This class represents the description of a file.
    /// </summary>
    class FileDb
    {
        public int Id { get; set; }
        public string DirectoryPath { get; set; }
        public string Filename { get; set; }
        public string Extension { get; set; }
        public string Hashcode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
