﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Net;

namespace FilesExplorer
{
    /// <summary>
    ///     This static class is able to read and interpret a FEX file.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                ShowMissingFileError();
            }
            else
            {
                string content = args[0];
                Uri uri = new Uri(content);
                String scheme = uri.Scheme + Uri.SchemeDelimiter;
                if (content.StartsWith(scheme))
                    OpenThroughProtocol(content.Remove(0, scheme.Length));
                else
                    OpenThroughExecutable(content);
            }
        }

        private static void OpenThroughProtocol(String file)
        {
            Console.WriteLine("=== FilesExplorer Protocol ===");

            // Creates an HttpWebRequest with the specified URL. 
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(Path.Combine("http://", file));

            Console.WriteLine("Getting response from web request...");

            // Sends the HttpWebRequest and waits for the response.			
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

            // Gets the stream associated with the response.
            Stream receiveStream = myHttpWebResponse.GetResponseStream();
            Encoding encode = Encoding.GetEncoding("utf-8");

            Console.WriteLine(" [+] Done");
            Console.WriteLine("Reading response...");

            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readStream = new StreamReader(receiveStream, encode);

            // Deserialize the data to JSON (FileDescription)
            FileDb description = JsonConvert.DeserializeObject<FileDb>(readStream.ReadToEnd());

            Console.WriteLine(" [+] Done");
            Console.WriteLine("Releasing resources...");

            // Releases the resources of the response.
            myHttpWebResponse.Close();

            // Releases the resources of the Stream.
            readStream.Close();

            Console.WriteLine(" [+] Done");
            Console.WriteLine("Opening target file...");
            
            string filename = description.Filename + description.Extension;
            string absolutePath = Path.Combine(description.DirectoryPath, filename);

            // Open the target file
            if (File.Exists(absolutePath))
            {
                FileHelper.OpenFile(absolutePath);
                Console.WriteLine(" [+] Done");
            }
            else
            {
                ShowUnreachableFileError();
            }
        }

        private static void OpenThroughExecutable(String filePath)
        {
            Console.WriteLine("=== FilesExplorer Executable ===");

            if (File.Exists(filePath))
            {
                string fileContent = File.ReadAllText(filePath);
                FileDb description = JsonConvert.DeserializeObject<FileDb>(fileContent);

                Console.WriteLine("Opening target file...");

                string filename = description.Filename + description.Extension;
                string absolutePath = Path.Combine(description.DirectoryPath, filename);

                // Open the target file
                if (File.Exists(absolutePath))
                {
                    FileHelper.OpenFile(absolutePath);
                    Console.WriteLine(" [+] Done");
                }
                else
                {
                    ShowUnreachableFileError();
                }
            }
        }

        private static void ShowMissingFileError()
        {
            Console.WriteLine(" [-] Need to specify a file first!");
            Console.WriteLine("");
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private static void ShowUnreachableFileError()
        {
            Console.WriteLine(" [-] Can't open selected file!");
            Console.WriteLine("");
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
