﻿using FexAssociation.FileExtensionHandler;
using P2_FilesExplorer.ProtocolHandler;
using System;
using System.IO;

namespace FexAssociation
{
    /// <summary>
    ///     This class is able to associate a file extension and a protocol to a specific application.
    /// </summary>
    class Program
    {
        private const string ApplicationPath = "C:\\Users\\Lucas\\Documents\\Semestre 6\\Projet de Bachelor\\Tests technologiques\\Prototype 1\\FilesExplorer\\FilesExplorer\\bin\\Release\\netcoreapp2.0\\win10-x64\\FilesExplorer.exe";
        private const string IconPath = "C:\\Users\\Lucas\\Documents\\Semestre 6\\Projet de Bachelor\\Tests technologiques\\Prototype 1\\FexAssociation\\FexAssociation\\Resources\\appIcon.ico";

        static void Main(string[] args)
        {
            // Association of the file extension
            Console.WriteLine($"Associating '.fex' type to '{Path.GetFileNameWithoutExtension(ApplicationPath)}'...");
            FileExtensionHandlerHelper.SetAssociation(ApplicationPath, IconPath);
            Console.WriteLine("Extension associated.");

            // Association of the protocol
            Console.WriteLine($"Associating protocol '{ProtocolHandlerHelper.ProtocolKey}' to '{Path.GetFileNameWithoutExtension(ApplicationPath)}'...");
            ProtocolHandlerHelper.Register(ApplicationPath);
            Console.WriteLine("Protocol associated.");

            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

    }
}
