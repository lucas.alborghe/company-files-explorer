﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Win32;

namespace P2_FilesExplorer.ProtocolHandler
{
    /// <summary>
    ///     This static class is able to manage the protocol handler registration/unregistration.
    /// </summary>
    public static class ProtocolHandlerHelper
    {
        public const string ProtocolKey = "filesexplorer";
        public const string FileName = "FilesExplorer.exe";

        internal const string OpenKey = @"shell\open\command";

        private const string DisplayName = "Files Explorer";
        private const string ProgramsKey = @"Software\Microsoft\Windows\CurrentVersion\Uninstall";
        private const string ProgramsStyleKey = @"SOFTWARE\Classes\Installer\Products";
        private const string ProductOwner = "Softcom Technologies SA";
        private const string ProtocolValue = "URL:Files Explorer Protocol";
        private const string UrlProtocolValueKey = "URL Protocol";
        private const string IconKey = "DefaultIcon";

        private static readonly string[] DefaultInstallationPaths = new[]
        {
                Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86),
                Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        }.SelectMany(e => new[]
        {
                Path.Combine(e, ProductOwner, DisplayName),
                e
        }).OrderByDescending(e => e.Length).ToArray();

        public static bool IsRegistered()
        {
            return Registry.ClassesRoot.TryRead(ProtocolKey, out RegistryKey protocolKey)
                    && protocolKey.TryRead(OpenKey, out RegistryKey open)
                    && File.Exists(open.GetDefaultValue());
        }

        public static void Register()
        {
            Register(FindExeLocation());
        }

        public static void Register(string exeLocation)
        {
            Registry.ClassesRoot.TryReadOrCreate(ProtocolKey, out RegistryKey protocolKey);
            protocolKey.SetDefaultValue(ProtocolValue);
            protocolKey.SetStringValue(UrlProtocolValueKey, string.Empty);

            protocolKey.TryReadOrCreate(IconKey, out RegistryKey iconKey);
            iconKey.SetDefaultValue($"{GetIconPath()},0");

            protocolKey.TryReadOrCreate(OpenKey, out RegistryKey openCmd);
            openCmd.SetDefaultValue($"{exeLocation} %1");
        }

        public static void Unregister()
        {
            Registry.ClassesRoot.DeleteSubKeyTree(ProtocolKey, false);
        }

        private static string GetIconPath()
        {
            if (!Registry.LocalMachine.TryRead(ProgramsStyleKey, out RegistryKey registryKey)
                    || !registryKey.TryFind("ProductName", DisplayName, out RegistryKey vpKey)
                    || !vpKey.TryGetValue("ProductIcon", out string iconPath)
                    || !File.Exists(iconPath))
            {
                iconPath = null;
            }

            return iconPath ?? @"%SystemRoot%\system32\url.dll";
        }

        private static string FindExeLocation()
        {
            string crtLocation = typeof(ProtocolHandlerHelper).Assembly.Location;
            if (Path.GetFileName(crtLocation).Equals(FileName, StringComparison.OrdinalIgnoreCase))
            {
                // The current executable is the VideoProtectorConnect.exe executable
                return crtLocation;
            }
            if (!Registry.LocalMachine.TryRead(ProgramsKey, out RegistryKey registryKey)
                    || !registryKey.TryFind("DisplayName", DisplayName, out RegistryKey vpKey)
                    || !vpKey.TryGetValue("InstallLocation", out string installPath))
            {
                installPath = null;
            }
            else
            {
                // The VideoProtectorConnect setup has been installed
                installPath = Path.Combine(installPath, FileName);
                if (!File.Exists(installPath))
                {
                    // The VideoProtectorConnect setup has been installed in one of the default paths
                    installPath = DefaultInstallationPaths.SelectMany(dir => Directory.GetFiles(dir, FileName, SearchOption.AllDirectories)).FirstOrDefault();
                }
            }

            return installPath ?? crtLocation;
        }

        public static bool TryConvert(string host, out string convertedHost)
        {
            string vpProtocol = $"{ProtocolKey}:";
            bool useProtocol = host.StartsWith(vpProtocol, StringComparison.OrdinalIgnoreCase);
            convertedHost = useProtocol ? $"https:{host.Substring(vpProtocol.Length)}" : host;
            return useProtocol;
        }
    }
}