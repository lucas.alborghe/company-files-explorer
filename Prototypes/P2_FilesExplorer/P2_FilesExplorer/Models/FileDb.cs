﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace P2_FilesExplorer.Models
{
    public partial class FileDb
    {
        public FileDb()
        {
            UserPreference = new HashSet<UserPreference>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string DirectoryPath { get; set; }
        public string Filename { get; set; }
        public string Extension { get; set; }
        public string Hashcode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public Directory DirectoryPathNavigation { get; set; }
        public ICollection<UserPreference> UserPreference { get; set; }
    }
}
