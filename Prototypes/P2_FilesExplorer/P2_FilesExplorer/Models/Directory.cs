﻿using System;
using System.Collections.Generic;

namespace P2_FilesExplorer.Models
{
    public partial class Directory
    {
        public Directory()
        {
            FileDb = new HashSet<FileDb>();
        }

        public string DirectoryPath { get; set; }

        public ICollection<FileDb> FileDb { get; set; }
    }
}
