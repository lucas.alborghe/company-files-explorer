﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace P2_FilesExplorer.Models
{
    public partial class VirtualDirectory
    {
        public VirtualDirectory()
        {
            InverseParent = new HashSet<VirtualDirectory>();
            UserPreference = new HashSet<UserPreference>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ParentId { get; set; }
        public int UserId { get; set; }
        public DateTime Name { get; set; }
        public byte IsActive { get; set; }

        public VirtualDirectory Parent { get; set; }
        public UserDb User { get; set; }
        public ICollection<VirtualDirectory> InverseParent { get; set; }
        public ICollection<UserPreference> UserPreference { get; set; }
    }
}
