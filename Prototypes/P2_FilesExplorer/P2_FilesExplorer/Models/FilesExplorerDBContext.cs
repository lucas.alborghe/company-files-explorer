﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace P2_FilesExplorer.Models
{
    public partial class FilesExplorerDBContext : DbContext
    {
        public FilesExplorerDBContext()
        {
        }

        public FilesExplorerDBContext(DbContextOptions<FilesExplorerDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AllowedExtensions> AllowedExtensions { get; set; }
        public virtual DbSet<Directory> Directory { get; set; }
        public virtual DbSet<FileDb> FileDb { get; set; }
        public virtual DbSet<RepositoryDetail> RepositoryDetail { get; set; }
        public virtual DbSet<UserDb> UserDb { get; set; }
        public virtual DbSet<UserPreference> UserPreference { get; set; }
        public virtual DbSet<VirtualDirectory> VirtualDirectory { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AllowedExtensions>(entity =>
            {
                entity.ToTable("allowedExtensions");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Extension)
                    .IsRequired()
                    .HasColumnName("extension")
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<Directory>(entity =>
            {
                entity.HasKey(e => e.DirectoryPath);

                entity.ToTable("directory");

                entity.Property(e => e.DirectoryPath)
                    .HasColumnName("directoryPath")
                    .HasMaxLength(256)
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<FileDb>(entity =>
            {
                entity.ToTable("fileDB");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("createdDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.DirectoryPath)
                    .IsRequired()
                    .HasColumnName("directory_path")
                    .HasMaxLength(256);

                entity.Property(e => e.Extension)
                    .IsRequired()
                    .HasColumnName("extension")
                    .HasMaxLength(32);

                entity.Property(e => e.Filename)
                    .IsRequired()
                    .HasColumnName("filename")
                    .HasMaxLength(64);

                entity.Property(e => e.Hashcode)
                    .IsRequired()
                    .HasColumnName("hashcode")
                    .HasMaxLength(256);

                entity.Property(e => e.ModifiedDate)
                    .HasColumnName("modifiedDate")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.DirectoryPathNavigation)
                    .WithMany(p => p.FileDb)
                    .HasForeignKey(d => d.DirectoryPath)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__fileDB__director__74AE54BC");
            });

            modelBuilder.Entity<RepositoryDetail>(entity =>
            {
                entity.ToTable("repositoryDetail");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AbsolutePath)
                    .IsRequired()
                    .HasColumnName("absolutePath")
                    .HasMaxLength(256);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(64);

                entity.Property(e => e.Protocol)
                    .HasColumnName("protocol")
                    .HasMaxLength(64);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(64);
            });

            modelBuilder.Entity<UserDb>(entity =>
            {
                entity.ToTable("userDB");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IsAdmin).HasColumnName("isAdmin");

                entity.Property(e => e.Login)
                    .IsRequired()
                    .HasColumnName("login")
                    .HasMaxLength(64);
            });

            modelBuilder.Entity<UserPreference>(entity =>
            {
                entity.HasKey(e => new { e.VirtualDirectoryId, e.FileId });

                entity.ToTable("userPreference");

                entity.Property(e => e.VirtualDirectoryId).HasColumnName("virtualDirectory_id");

                entity.Property(e => e.FileId).HasColumnName("file_id");

                entity.Property(e => e.Filename)
                    .HasColumnName("filename")
                    .HasMaxLength(64);

                entity.HasOne(d => d.File)
                    .WithMany(p => p.UserPreference)
                    .HasForeignKey(d => d.FileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__userPrefe__file___7C4F7684");

                entity.HasOne(d => d.VirtualDirectory)
                    .WithMany(p => p.UserPreference)
                    .HasForeignKey(d => d.VirtualDirectoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__userPrefe__virtu__7B5B524B");
            });

            modelBuilder.Entity<VirtualDirectory>(entity =>
            {
                entity.ToTable("virtualDirectory");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("date");

                entity.Property(e => e.ParentId).HasColumnName("parent_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__virtualDi__paren__778AC167");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.VirtualDirectory)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__virtualDi__user___787EE5A0");
            });
        }
    }
}
