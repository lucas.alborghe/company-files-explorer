﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace P2_FilesExplorer.Models
{
    public partial class UserDb
    {
        public UserDb()
        {
            VirtualDirectory = new HashSet<VirtualDirectory>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Login { get; set; }
        public byte IsAdmin { get; set; }

        public ICollection<VirtualDirectory> VirtualDirectory { get; set; }
    }
}
