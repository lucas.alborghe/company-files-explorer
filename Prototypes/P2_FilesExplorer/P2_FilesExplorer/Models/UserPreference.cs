﻿using System;
using System.Collections.Generic;

namespace P2_FilesExplorer.Models
{
    public partial class UserPreference
    {
        public int VirtualDirectoryId { get; set; }
        public int FileId { get; set; }
        public string Filename { get; set; }

        public FileDb File { get; set; }
        public VirtualDirectory VirtualDirectory { get; set; }
    }
}
