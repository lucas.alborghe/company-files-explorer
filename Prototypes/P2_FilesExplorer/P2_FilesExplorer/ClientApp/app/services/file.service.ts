﻿import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { FileDb } from '../beans/FileDb';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FileService {

    private headers = new Headers({ 'Content-Type': 'application/json' });
    private fileApiUrl = 'api/file';

    constructor(private http: Http) { }

    getMessage(): Promise<String> {
        return this.http.get(this.fileApiUrl + "/GetMessage")
            .toPromise()
            .then(response => response.text())
            .catch(this.handleError);
    }

    getFiles(): Promise<FileDb[]> {
        return this.http.get(this.fileApiUrl + "/GetFiles")
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getFile(id: Number): Promise<FileDb> {
        return this.http.get(this.fileApiUrl + "/GetFile/" + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

}