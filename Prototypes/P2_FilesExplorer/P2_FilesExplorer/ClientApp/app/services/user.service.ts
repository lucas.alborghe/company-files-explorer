﻿import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { UserDb } from '../beans/UserDb';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

    private headers = new Headers({ 'Content-Type': 'application/json' });
    private userApiUrl = 'api/user';

    constructor(private http: Http) { }

    getMessage(): Promise<String> {
        return this.http.get(this.userApiUrl + "/GetMessage")
            .toPromise()
            .then(response => response.text())
            .catch(this.handleError);
    }

    getUsers(): Promise<UserDb[]> {
        return this.http.get(this.userApiUrl + "/GetUsers")
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

}