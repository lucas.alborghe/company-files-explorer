﻿export class FileDb {
    id: Number = 0;
    directoryPath: String = "";
    filename: String = "";
    extension: String = "";
    hashcode: String = "";
    createdDate: Date = new Date("");
    modifiedDate: Date = new Date("");
}