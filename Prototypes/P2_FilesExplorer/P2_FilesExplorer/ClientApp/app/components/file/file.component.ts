﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserDb } from '../../beans/UserDb';
import { FileDb } from '../../beans/FileDb';

import { UserService } from '../../services/user.service';
import { FileService } from '../../services/file.service';

import { Headers, Http } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'file',
    templateUrl: './file.component.html',
    providers: [
        UserService,
        FileService
    ]
})
export class FileComponent implements OnInit {

    message: String = "";

    selectedUserIndex: Number = 0;
    selectedFileIndex: Number = 0;

    users: UserDb[] = [];
    filesDb: FileDb[] = [];

    currentUser: UserDb = {
        id: -1,
        login: '-'
    };

    currentFile: FileDb = {
        id: -1,
        directoryPath:  '-',
        filename: '-',
        extension: '-',
        hashcode: '-',
        createdDate: new Date(),
        modifiedDate: new Date()
    };

    // ======================================================================

    constructor(
        private sanitizer: DomSanitizer,
        private userService: UserService,
        private fileService: FileService,
        private router: Router) {
    }

    ngOnInit(): void {
        this.getUsers();
        this.getFiles();
    }

    updateSelectedUser(): void {
        console.log("selectedUserIndex : " + this.selectedUserIndex);
        this.getFiles();
    }

    setSelectedFile(id: Number): void {
        this.selectedFileIndex = id;
        this.getSelectedFile(this.selectedFileIndex);
    }

    getUsers(): void {
        this.userService
            .getUsers()
            .then(data => {
                this.users = data as UserDb[];
                console.log("getUsers : " + data.toString());
            });
    }

    getFiles(): void {
        this.fileService
            .getFiles()
            .then(data => {
                this.filesDb = data as FileDb[];
                console.log("getFiles : " + data.toString());
            });
    }

    getSelectedFile(id: Number): void {
        this.fileService
            .getFile(id)
            .then(data => {
                this.currentFile = data as FileDb;
                console.log("getFile : " + data.toString());
            });
    }

    updateFilenames() {
        //for (let i = 0; i < this.filesCustom.length; i++) {
        //    for (let j = 0; j < this.fexFiles.length; j++) {
        //        if (this.fexFiles[j].id == this.filesCustom[i].fileId) {
        //            console.log("fexFiles : " + this.fexFiles[j].filename + " filesCustom : " + this.filesCustom[i].filename);
        //            this.fexFiles[j].filename = this.filesCustom[i].filename;
        //            break;
        //        }
        //    }
        //}
    }

    openThroughProtocolHandler(id: Number) {
        return this.sanitizer.bypassSecurityTrustUrl("filesexplorer://localhost:54794/api/file/OpenFile/" + id);
    }

}