﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using P2_FilesExplorer.Models;

namespace P2_FilesExplorer.Controllers
{
    [Route("api/[controller]")]
    public class FileController : Controller
    {
        private static FilesExplorerDBContext _context;

        public FileController(FilesExplorerDBContext context)
        {
            _context = context;
        }

        [HttpGet("[action]", Name = "GetFiles")]
        public IEnumerable<FileDb> GetFiles()
        {
            return _context.FileDb;
        }

        [HttpGet("[action]/{id}", Name = "GetFile")]
        public FileDb GetFile(int id)
        {
            return _context.FileDb.Find(id);
        }

        [HttpGet("[action]/{id}", Name = "CreateAndDownloadFexFile")]
        public FileResult CreateAndDownloadFexFile(int id)
        {
            FileDb f = GetFile(id);

            String jsonFile = JsonConvert.SerializeObject(f, Formatting.Indented);

            String fileName = "tmpFile.fex";
            String filePath = $"C:\\Users\\Lucas\\Downloads\\{fileName}";

            System.IO.File.WriteAllText(filePath, jsonFile);
            FileStream fs = System.IO.File.OpenRead(filePath);

            Response.Headers.Add("Content-Disposition", $"inline; filename=\"{fileName}\"");
            return File(fs, "application/pdf", fileName);
        }

        [HttpGet("[action]/{id}", Name = "OpenFile")]
        public String OpenFile(int id)
        {
            FileDb f = GetFile(id);
            String json = JsonConvert.SerializeObject(f, Formatting.Indented);
            return json;
        }

        [HttpGet("[action]", Name = "FillDatabaseFromRoots")]
        public Boolean FillDatabaseFromRoots()
        {
            // List of extensions allowed from the database
            IEnumerable<AllowedExtensions> extensionsFromDb = _context.AllowedExtensions;
            string[] extensions = new string[extensionsFromDb.Count()];
            for (int i = 0; i < extensions.Length; i++)
                extensions[i] = "*" + extensionsFromDb.ElementAt(i).Extension;

            // List of repositories to get the files from
            IEnumerable<RepositoryDetail> repositories = _context.RepositoryDetail;

            try
            {
                // For each repository, get the files matching the allowed extensions
                foreach (RepositoryDetail repository in repositories)
                {
                    IEnumerable<string> filesPath = GetFilesFromPath(repository.AbsolutePath, extensions);
                    foreach (string file in filesPath)
                    {
                        // Get the directory (absolute path) of the current file
                        Models.Directory d = new Models.Directory()
                        {
                            DirectoryPath = Path.GetDirectoryName(file)
                        };

                        // Get the informations of the current file
                        FileDb f = new FileDb()
                        {
                            DirectoryPath = d.DirectoryPath,
                            Filename = Path.GetFileNameWithoutExtension(file),
                            Extension = Path.GetExtension(file),
                            Hashcode = CalculateMD5(file),
                            CreatedDate = DateTime.Today,
                            ModifiedDate = DateTime.Today
                        };

                        // If the directory doesn't exist yet, add it
                        if (_context.Directory.Find(d.DirectoryPath) == null)
                            _context.Directory.Add(d);

                        // If the file doesn't exist yet, add it (NOT WORKING RIGHT, BUT BE WRITTEN AGAIN)
                        if (_context.FileDb.Any(o => o.Hashcode == f.Hashcode))
                            _context.FileDb.Add(f);
                    }
                }
                // Save changes
                _context.SaveChanges();

                // Return true if everything went fine
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("[FillDatabaseFromRoots] An exception occured : {0}", e);
                // Return false if an exception occured
                return true;
            }
        }

        // If you need to represent the hash as a string, you could convert it to hex using BitConverter
        // Source : https://stackoverflow.com/questions/10520048/calculate-md5-checksum-for-a-file
        private string CalculateMD5(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = System.IO.File.OpenRead(filename))
                {
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        // Works in .Net 4.0 - takes same patterns as old method, and executes in parallel
        // Source : https://www.techmikael.com/2010/02/directory-search-with-multiple-filters.html
        private IEnumerable<string> GetFilesFromPath(string path, string[] searchPatterns, SearchOption searchOption = SearchOption.AllDirectories)
        {
            return searchPatterns.AsParallel().SelectMany(searchPattern => System.IO.Directory.EnumerateFiles(path, searchPattern, searchOption));
        }

        [HttpPost()]
        public IActionResult CreateFile([FromBody] FileDb file)
        {
            if (file == null)
                return BadRequest();

            _context.FileDb.Add(file);
            _context.SaveChanges();

            return CreatedAtRoute("GetFile", new { id = file.Id }, file);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateFile(int id, [FromBody] FileDb file)
        {
            if (file == null || file.Id != id)
                return BadRequest();

            var currentFile = _context.FileDb.Find(id);
            if (currentFile == null)
                return NotFound();

            currentFile.Filename = file.Filename;
            currentFile.Extension = file.Extension;
            currentFile.Hashcode = file.Hashcode;

            _context.FileDb.Update(currentFile);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteFile(int id)
        {
            var todo = _context.FileDb.Find(id);
            if (todo == null)
                return NotFound();

            _context.FileDb.Remove(todo);
            _context.SaveChanges();
            return NoContent();
        }
    }
}
