﻿using Microsoft.AspNetCore.Mvc;
using P2_FilesExplorer.Models;
using System.Collections.Generic;

namespace P2_FilesExplorer.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private static FilesExplorerDBContext _context;

        public UserController(FilesExplorerDBContext context)
        {
            _context = context;
        }

        [HttpGet("[action]")]
        public string GetMessage()
        {
            return "Hello World!";
        }

        [HttpGet("[action]")]
        public IEnumerable<UserDb> GetUsers()
        {
            return _context.UserDb;
        }

        [HttpGet("[action]/{id}")]
        public UserDb GetUser(int id)
        {
            return _context.UserDb.Find(id);
        }
    }
}
